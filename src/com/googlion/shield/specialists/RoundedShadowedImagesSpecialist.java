package com.googlion.shield.specialists;

import static android.graphics.Shader.TileMode.*;
import static android.graphics.Color.*;

import android.graphics.*;

import java.util.*;

public final class RoundedShadowedImagesSpecialist extends RoundedImagesSpecialist {
    public RoundedShadowedImagesSpecialist(final Map<Object, Object> SETTINGS) {
        super(SETTINGS);
    }

    protected final Bitmap getBitmapForSpecialImage() throws Exception {
        final Bitmap SOURCE = getScaledBitmapFromOriginalOne();
        final Bitmap OUTPUT = getOutputBitmapFromOriginalOne();
        final Canvas C = new Canvas(OUTPUT);
        final Float CX = (float) SOURCE.getWidth() / 2;
        final Float CY = (float) SOURCE.getHeight() / 2 - 8f;
        final Float RADIUS = (float) SOURCE.getWidth() / 2 - 12f;
        final Shader S = new BitmapShader(SOURCE, CLAMP, CLAMP);
        final Paint P = getPaintForCanvas();
        final Paint SP = getPaintForDrawingShadow();
        P.setShader(S);
        C.drawCircle(CX, CY, RADIUS, SP);
        C.drawCircle(CX, CY, RADIUS, P);
        return OUTPUT;
    }

    private Bitmap getScaledBitmapFromOriginalOne() throws Exception {
        final Bitmap SOURCE = getOriginalBitmapFromImageUrl();
        final Bitmap OUTPUT = getOutputBitmapFromOriginalOne();
        final Canvas C = new Canvas(OUTPUT);
        final Paint P = getPaintForCanvas();
        final Rect SRC = getRectangleForSourceBitmap();
        final Rect DST = getRectangleForScaledBitmap();
        C.drawBitmap(SOURCE, SRC, DST, P);
        return OUTPUT;
    }

    private Rect getRectangleForSourceBitmap() throws Exception {
        final Bitmap SOURCE = getOriginalBitmapFromImageUrl();
        final int LEFT = 0;
        final int TOP = 0;
        final int RIGHT = SOURCE.getWidth();
        final int BOTTOM = SOURCE.getHeight();
        return new Rect(LEFT, TOP, RIGHT, BOTTOM);
    }

    private Rect getRectangleForScaledBitmap() throws Exception {
        final Bitmap SOURCE = getOriginalBitmapFromImageUrl();
        final int LEFT = 12;
        final int TOP = 4;
        final int RIGHT = SOURCE.getWidth() - 12;
        final int BOTTOM = SOURCE.getHeight() - 20;
        return new Rect(LEFT, TOP, RIGHT, BOTTOM);
    }

    private Paint getPaintForDrawingShadow() {
        final Paint P = getPaintForCanvas();
        final Float SHADOW_RADIUS = 13f;
        final Integer SHADOW_COLOR = BLACK;
        final Float DX = 0f;
        final Float DY = 4f;
        P.setShadowLayer(SHADOW_RADIUS, DX, DY, SHADOW_COLOR);
        return P;
    }
}