package com.googlion.shield.specialists;

import android.app.*;
import android.view.*;
import java.util.*;

public class FontSpecialistForListView extends FontSpecialist {
    private View listViewItemToBeProcessed;

    public FontSpecialistForListView(final Map<Object, Map> SETTINGS) {
        super(SETTINGS);
        final Activity REQUESTER = getRequester();
        listViewItemToBeProcessed = new View(REQUESTER);
    }

    @Override
    protected final View getCurrentView() {
        final Integer ID = getCurrentViewIdFromSettings();
        return listViewItemToBeProcessed.findViewById(ID);
    }

    public final void setListViewItemToBeProcessed(final View V) {
        listViewItemToBeProcessed = V;
    }
}