package com.googlion.shield.specialists;

import android.app.*;
import android.view.*;
import com.example.starfishapp.*;
import com.jeremyfeinstein.slidingmenu.lib.*;

import java.util.*;

public final class MainMenuFontSpecialist extends FontSpecialist {
    private View mainMenu;

    public MainMenuFontSpecialist(final Map<Object, Map> SETTINGS) {
        super(SETTINGS);
        final Activity A = getRequester();
        final MainActivity MA = (MainActivity) A;
        final SlidingMenu SM = MA.getSlidingMenu();
        mainMenu = SM.getMenu();
    }

    protected final View getCurrentView() {
        final Integer ID = getCurrentViewIdFromSettings();
        return mainMenu.findViewById(ID);
    }
}