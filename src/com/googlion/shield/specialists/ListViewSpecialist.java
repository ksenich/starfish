package com.googlion.shield.specialists;

import android.app.*;
import android.content.*;
import android.view.*;
import android.widget.*;
import com.googlion.shield.adapters.*;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;

import java.util.*;

public class ListViewSpecialist {
    private Map<String, List<Map<String, Object>>> settings;

    public ListViewSpecialist(final Map<String, List<Map<String, Object>>> SETTINGS) {
        settings = SETTINGS;
    }

    public void setUpListView() {
        final ListView LV = getListViewFromSettings();
        final ListAdapter LA = getListAdapterForListView();
        LV.setAdapter(LA);
    }

    private ListView getListViewFromSettings() {
        final Activity A = getRequester();
        final int ID = getListViewIdFromSettings();
        final View V = A.findViewById(ID);
        return (ListView) V;
    }

    protected Activity getRequester() {
        final Map<String, Object> M = getOtherSettings();
        final Object O = M.get("Requester");
        return (Activity) O;
    }

    private Map<String, Object> getOtherSettings() {
        final String KEY = "Other settings";
        final List<Map<String, Object>> L = settings.get(KEY);
        final int LOCATION = 0;
        return L.get(LOCATION);
    }

    private int getListViewIdFromSettings() {
        final Map<String, Object> M = getOtherSettings();
        final Object O = M.get("List view id");
        return (Integer) O;
    }

    protected ListAdapter getListAdapterForListView() {
        final Context C = getRequester();
        final List<Map<String, Object>> DATA = getListData();
        final int LAYOUT_RESOURCE_ID = getLayoutResourceId();
        final String[] FROM = getItemHeadersArray();
        final int[] TO = getItemIdsArray();
        final PageScaler PS = getPageScaler();
        final ListViewItemsTranslator LVIT = getListViewItemsTranslator();
        final FontSpecialistForListView FSFLV = getFontSpecialistForListView();
        return new ShieldSimpleAdapter(C, DATA, LAYOUT_RESOURCE_ID, FROM, TO, PS, LVIT, FSFLV);
    }

    protected List<Map<String, Object>> getListData() {
        final String KEY = "List view items data";
        return settings.get(KEY);
    }

    protected int getLayoutResourceId() {
        final Map<String, Object> M = getOtherSettings();
        final Object O = M.get("Layout resource id");
        return (Integer) O;
    }

    protected String[] getItemHeadersArray() {
        final List<String> L = getItemHeadersList();
        final int SIZE = L.size();
        final String[] S = new String[SIZE];
        return L.toArray(S);
    }

    private List<String> getItemHeadersList() {
        final Map<String, Object> M = getListViewItemFieldsToBeFilledFromSettings();
        final Set<String> S = M.keySet();
        return new LinkedList<String>(S);
    }

    private Map<String, Object> getListViewItemFieldsToBeFilledFromSettings() {
        final String KEY = "List view item fields to be filled";
        final List<Map<String, Object>> L = settings.get(KEY);
        final int LOCATION = 0;
        return L.get(LOCATION);
    }

    protected int[] getItemIdsArray() {
        final List<Object> L = getItemIdsList();
        final int SIZE = L.size();
        final int[] I = new int[SIZE];
        for (int i = 0; i < SIZE; ++i) {
            I[i] = (Integer) L.get(i);
        }
        return I;
    }

    private List<Object> getItemIdsList() {
        final Map<String, Object> M = getListViewItemFieldsToBeFilledFromSettings();
        final List<String> HEADERS = getItemHeadersList();
        final List<Object> IDS = new LinkedList<Object>();
        for (final String KEY : HEADERS) {
            IDS.add(M.get(KEY));
        }
        return IDS;
    }

    protected PageScaler getPageScaler() {
        final Map<String, Object> M = getOtherSettings();
        final Object O = M.get("Page scaler");
        return (PageScaler) O;
    }

    protected ListViewItemsTranslator getListViewItemsTranslator() {
        final Map<String, Object> M = getOtherSettings();
        final Object O = M.get("List view items translator");
        return (ListViewItemsTranslator) O;
    }

    protected FontSpecialistForListView getFontSpecialistForListView() {
        final Map<String, Object> M = getOtherSettings();
        final Object O = M.get("Font specialist for list view");
        return (FontSpecialistForListView) O;
    }
}