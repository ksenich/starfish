package com.googlion.shield.specialists;

import android.app.*;
import android.content.res.*;
import android.graphics.*;
import android.view.*;
import android.widget.*;

import java.util.*;

import static android.graphics.Typeface.*;

public class FontSpecialist {
    private Map<Object, Map> settings;
    private int currentViewPosition;

    public FontSpecialist(final Map<Object, Map> SETTINGS) {
        settings = SETTINGS;
        currentViewPosition = 0;
    }

    public void setFonts() {
        resetCurrentViewPosition();
        setFontsForAllChosenViews();
    }

    private void resetCurrentViewPosition() {
        currentViewPosition = 0;
    }

    private void setFontsForAllChosenViews() {
        while (thereAreViewsNeedToBeAnalyzed()) {
            setFontForCurrentView();
        }
    }

    private boolean thereAreViewsNeedToBeAnalyzed() {
        final List L = getViewsIdsFromSettings();
        final int SIZE = L.size();
        return currentViewPosition < SIZE;
    }

    private List getViewsIdsFromSettings() {
        final Map M = settings.get("Main settings");
        final Set S = M.keySet();
        return new ArrayList<Object>(S);
    }

    private void setFontForCurrentView() {
        setFontForView();
        goToTheNextView();
    }

    protected void setFontForView() {
        final List L = getViewsIdsFromSettings();
        final Map M = settings.get("Main settings");
        final Activity A = getRequester();
        final Object X = L.get(currentViewPosition);
        final View V = getCurrentView();
        final TextView TV = (TextView) V;
        final AssetManager MGR = A.getAssets();
        final Object Y = M.get(X);
        final String PATH = (String) Y;
        final Typeface TF = createFromAsset(MGR, PATH);
        TV.setTypeface(TF);
    }

    protected Activity getRequester() {
        final Map M = settings.get("Other settings");
        final Object O = M.get("Requester");
        return (Activity) O;
    }

    protected View getCurrentView() {
        final Activity A = getRequester();
        final Integer ID = getCurrentViewIdFromSettings();
        return A.findViewById(ID);
    }

    protected Integer getCurrentViewIdFromSettings() {
        final List L = getViewsIdsFromSettings();
        final Object X = L.get(currentViewPosition);
        return (Integer) X;
    }

    private void goToTheNextView() {
        ++currentViewPosition;
    }
}