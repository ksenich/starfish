package com.googlion.shield.specialists;

import static android.graphics.Bitmap.*;
import static android.graphics.Bitmap.Config.*;
import static android.graphics.BitmapFactory.*;
import static android.graphics.Shader.TileMode.*;

import android.app.*;
import android.content.res.*;
import android.graphics.*;
import android.graphics.Bitmap.*;
import android.graphics.drawable.*;
import android.os.*;
import android.widget.*;

import java.io.*;
import java.net.*;
import java.util.*;

public class RoundedImagesSpecialist extends AsyncTask<Void, Void, Drawable> {
    private Activity requester;
    private String imageUrl;
    private ImageView imageView;

    public RoundedImagesSpecialist(final Map<Object, Object> SETTINGS) {
        imageUrl = (String) SETTINGS.get("Image url");
        requester = (Activity) SETTINGS.get("Requester");
        imageView = (ImageView) SETTINGS.get("Image view");
    }

    public final void makeImageLookSpecial() {
        execute();
    }

    protected final Drawable doInBackground(final Void... V) {
        try {
            return getSpecialImageDrawable();
        } catch (final Exception E) {
            return getDrawableByDefault();
        }
    }

    protected final Drawable getSpecialImageDrawable() throws Exception {
        final Bitmap B = getBitmapForSpecialImage();
        final Resources RES = requester.getResources();
        return new BitmapDrawable(RES, B);
    }

    protected Bitmap getBitmapForSpecialImage() throws Exception {
        final Bitmap SOURCE = getOriginalBitmapFromImageUrl();
        final Bitmap OUTPUT = getOutputBitmapFromOriginalOne();
        final Canvas C = new Canvas(OUTPUT);
        final Float CX = (float) SOURCE.getWidth() / 2;
        final Float CY = (float) SOURCE.getHeight() / 2;
        final Float RADIUS = (float) SOURCE.getWidth() / 2;
        final Shader S = new BitmapShader(SOURCE, CLAMP, CLAMP);
        final Paint P = getPaintForCanvas();
        P.setShader(S);
        C.drawCircle(CX, CY, RADIUS, P);
        return OUTPUT;
    }

    protected final Bitmap getOriginalBitmapFromImageUrl() throws Exception {
        final URL URL = new URL(imageUrl);
        final URLConnection C = URL.openConnection();
        final InputStream IS = C.getInputStream();
        return decodeStream(IS);
    }

    protected final Bitmap getOutputBitmapFromOriginalOne() throws Exception {
        final Bitmap SOURCE = getOriginalBitmapFromImageUrl();
        final Integer WIDTH = SOURCE.getWidth();
        final Integer HEIGHT = SOURCE.getHeight();
        final Config CONFIG = ARGB_8888;
        return createBitmap(WIDTH, HEIGHT, CONFIG);
    }

    protected final Paint getPaintForCanvas() {
        final Paint P = new Paint();
        P.setAntiAlias(true);
        return P;
    }

    private Drawable getDrawableByDefault() {
        final Bitmap BITMAP = getBitmapByDefault();
        final Resources RES = requester.getResources();
        return new BitmapDrawable(RES, BITMAP);
    }

    private Bitmap getBitmapByDefault() {
        final Integer WIDTH = 1;
        final Integer HEIGHT = 1;
        final Config CONFIG = ARGB_8888;
        return createBitmap(WIDTH, HEIGHT, CONFIG);
    }

    protected final void onPostExecute(final Drawable D) {
        imageView.setImageDrawable(D);
    }
}