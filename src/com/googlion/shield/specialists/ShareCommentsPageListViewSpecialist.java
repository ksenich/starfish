package com.googlion.shield.specialists;

import android.content.*;
import android.widget.*;
import com.googlion.shield.adapters.*;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;

import java.util.*;

public class ShareCommentsPageListViewSpecialist extends ListViewSpecialist {
    public ShareCommentsPageListViewSpecialist(final Map<String, List<Map<String, Object>>> SETTINGS) {
        super(SETTINGS);
    }

    protected ListAdapter getListAdapterForListView() {
        final Context C = getRequester();
        final List<Map<String, Object>> DATA = getListData();
        final int LAYOUT_RESOURCE_ID = getLayoutResourceId();
        final String[] FROM = getItemHeadersArray();
        final int[] TO = getItemIdsArray();
        final PageScaler PS = getPageScaler();
        final ListViewItemsTranslator LVIT = getListViewItemsTranslator();
        final FontSpecialistForListView FSFLV = getFontSpecialistForListView();
        return new ShareCommentsPageAdapter(C, DATA, LAYOUT_RESOURCE_ID, FROM, TO, PS, LVIT, FSFLV);
    }
}