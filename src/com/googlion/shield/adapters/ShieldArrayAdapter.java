package com.googlion.shield.adapters;

import android.app.*;
import android.content.*;
import android.view.*;
import android.widget.*;
import com.googlion.shield.utilities.*;

import java.util.*;

public class ShieldArrayAdapter<T> extends ArrayAdapter<T> {

    private Activity requester;
    private PageScaler pageScaler;
    private int layoutResourceId;
    private View convertView;
    private ViewGroup parent;
    private int currentItemPosition;


    public ShieldArrayAdapter(final Context C, final int RESOURCE, final T[] OBJECTS, final PageScaler PS) {
        super(C, RESOURCE, OBJECTS);
        requester = (Activity) C;
        pageScaler = PS;
        layoutResourceId = RESOURCE;
        convertView = null;
        parent = null;
        currentItemPosition = 0;
    }

    public ShieldArrayAdapter(final Context C, final int RESOURCE, final List<T> OBJECTS, final PageScaler PS) {
        super(C, RESOURCE, OBJECTS);
        requester = (Activity) C;
        pageScaler = PS;
        layoutResourceId = RESOURCE;
        convertView = null;
        parent = null;
        currentItemPosition = 0;
    }

    @Override
    public View getView(final int POSITION, View CONVERT_VIEW, ViewGroup PARENT) {
        retrieveConvertViewFrom(CONVERT_VIEW);
        retrieveParentFrom(PARENT);
        retrieveCurrentItemPositionFrom(POSITION);
        if (convertViewIsUndefined()) {
            defineConvertView();
        }
        fillConvertViewWithData();
        return convertView;
    }

    private void retrieveConvertViewFrom(final View V) {
        convertView = V;
    }

    private void retrieveParentFrom(final ViewGroup VG) {
        parent = VG;
    }

    private void retrieveCurrentItemPositionFrom(final int POSITION) {
        currentItemPosition = POSITION;
    }

    private boolean convertViewIsUndefined() {
        return convertView == null;
    }

    private void defineConvertView() {
        final LayoutInflater LI = requester.getLayoutInflater();
        final ViewGroup ROOT = parent;
        final boolean ATTACH_TO_ROOT = false;
        convertView = LI.inflate(layoutResourceId, ROOT, ATTACH_TO_ROOT);
        pageScaler.scale(convertView);
    }

    private void fillConvertViewWithData() {
        final TextView TV = (TextView) convertView;
        final Object O = getItem(currentItemPosition);
        final String TEXT = O.toString();
        TV.setText(TEXT);
    }

    @Override
    public View getDropDownView(final int POSITION, final View CONVERT_VIEW, ViewGroup PARENT) {
        return getView(POSITION, CONVERT_VIEW, PARENT);
    }
}