package com.googlion.shield.adapters;

import android.app.*;
import android.content.*;
import android.text.SpannableStringBuilder;
import android.view.*;
import android.widget.*;
import com.googlion.shield.specialists.FontSpecialistForListView;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;
import com.loopj.android.image.*;

import java.util.*;

import static android.text.Html.*;

public class ShieldSimpleAdapter extends SimpleAdapter {
    private Activity requester;
    private PageScaler pageScaler;
    private ListViewItemsTranslator listViewItemsTranslator;
    private FontSpecialistForListView fontSpecialistForListView;
    private int layoutResourceId;
    private View convertView;
    private View currentViewToBeFilledIn;
    private ViewGroup parent;
    private Map<String, ?> currentListItem;
    private List<? extends Map<String, ?>> list;
    private String[] headerKeys;
    private Object currentDataToBeFilledBy;
    private int[] headerIds;
    private int currentListItemDataHeaderPosition;

    public ShieldSimpleAdapter(final Context C, final List<? extends Map<String, ?>> DATA,
                               final int LAYOUT_RESOURCE_ID, String[] FROM, int[] TO,
                               final PageScaler PS) {
        super(C, DATA, LAYOUT_RESOURCE_ID, FROM, TO);
        requester = (Activity) C;
        pageScaler = PS;
        listViewItemsTranslator = null;
        fontSpecialistForListView = null;
        layoutResourceId = LAYOUT_RESOURCE_ID;
        convertView = new View(C);
        currentViewToBeFilledIn = new View(C);
        parent = new ListView(C);
        currentListItem = new HashMap<String, String>();
        list = DATA;
        headerKeys = FROM;
        currentDataToBeFilledBy = new Object();
        headerIds = TO;
        currentListItemDataHeaderPosition = 0;
    }

    public ShieldSimpleAdapter(final Context C, final List<? extends Map<String, ?>> DATA,
                               final int LAYOUT_RESOURCE_ID, String[] FROM, int[] TO,
                               final PageScaler PS, final ListViewItemsTranslator LVIT) {
        this(C, DATA, LAYOUT_RESOURCE_ID, FROM, TO, PS);
        listViewItemsTranslator = LVIT;
    }

    public ShieldSimpleAdapter(final Context C, final List<? extends Map<String, ?>> DATA,
                               final int LAYOUT_RESOURCE_ID, String[] FROM, int[] TO,
                               final PageScaler PS, final ListViewItemsTranslator LVIT,
                               final FontSpecialistForListView FSFLV) {
        this(C, DATA, LAYOUT_RESOURCE_ID, FROM, TO, PS, LVIT);
        fontSpecialistForListView = FSFLV;
    }

    @Override
    public View getView(final int POSITION, final View CONVERT_VIEW, final ViewGroup PARENT) {
        retrieveConvertViewFrom(CONVERT_VIEW);
        retrieveParentFrom(PARENT);
        retrieveCurrentListItemFrom(POSITION);
        if (convertViewIsUndefined()) {
            defineConvertView();
        }
        fillInConvertViewByCurrentListItemData();
        return convertView;
    }

    private void retrieveConvertViewFrom(final View V) {
        convertView = V;
    }

    private void retrieveParentFrom(final ViewGroup VG) {
        parent = VG;
    }

    private void retrieveCurrentListItemFrom(final int POSITION) {
        currentListItem = list.get(POSITION);
    }

    private boolean convertViewIsUndefined() {
        return convertView == null;
    }

    private void defineConvertView() {
        defineView();
        translateView();
        setFontsForAllTextualViewsInConvertView();
    }

    private void defineView() {
        final LayoutInflater LI = requester.getLayoutInflater();
        final ViewGroup ROOT = parent;
        final boolean ATTACH_TO_ROOT = false;
        convertView = LI.inflate(layoutResourceId, ROOT, ATTACH_TO_ROOT);
        pageScaler.scale(convertView);
    }

    private void translateView() {
        try {
            translate();
        } catch (final Exception E) {
            E.getMessage();
        }
    }

    private void translate() {
        listViewItemsTranslator.setListViewItemToBeTranslated(convertView);
        listViewItemsTranslator.translate();
    }

    private void setFontsForAllTextualViewsInConvertView() {
        try {
            setFontsForAllTextualViews();
        } catch (final Exception E) {
            E.getMessage();
        }
    }

    private void setFontsForAllTextualViews() {
        fontSpecialistForListView.setListViewItemToBeProcessed(convertView);
        fontSpecialistForListView.setFonts();
    }

    private void fillInConvertViewByCurrentListItemData() {
        resetCurrentListItemDataHeaderPosition();
        while (itIsNotLastHeaderOfCurrentListItemData()) {
            fillInConvertViewByCurrentItemOfCurrentListItemData();
        }
    }

    private void resetCurrentListItemDataHeaderPosition() {
        currentListItemDataHeaderPosition = 0;
    }

    private boolean itIsNotLastHeaderOfCurrentListItemData() {
        return currentListItemDataHeaderPosition < headerKeys.length;
    }

    private void fillInConvertViewByCurrentItemOfCurrentListItemData() {
        retrieveCurrentViewToBeFilledInFromCurrentListItemDataHeaderId();
        retrieveCurrentDataToBeFilledByFromCurrentListItemDataHeaderKey();
        if (currentViewToBeFilledInExtendsView()) {
            fillInMainParameterOfCurrentViewToBeFilledIn();
        }
        goToTheNextCurrentListItemDataHeader();
    }

    private void retrieveCurrentViewToBeFilledInFromCurrentListItemDataHeaderId() {
        final int ID = headerIds[currentListItemDataHeaderPosition];
        currentViewToBeFilledIn = convertView.findViewById(ID);
    }

    private void retrieveCurrentDataToBeFilledByFromCurrentListItemDataHeaderKey() {
        final String KEY = headerKeys[currentListItemDataHeaderPosition];
        currentDataToBeFilledBy = currentListItem.get(KEY);
    }

    private boolean currentViewToBeFilledInExtendsView() {
        boolean b = currentViewToBeFilledInIsATextView();
        b |= currentViewToBeFilledInIsASmartImageView();
        b |= currentViewToBeFilledInIsAnImageView();
        return b;
    }

    private boolean currentViewToBeFilledInIsATextView() {
        return currentViewToBeFilledIn instanceof TextView;
    }

    private boolean currentViewToBeFilledInIsASmartImageView() {
        return currentViewToBeFilledIn instanceof SmartImageView;
    }

    private boolean currentViewToBeFilledInIsAnImageView() {
        return currentViewToBeFilledIn instanceof ImageView;
    }

    private void fillInMainParameterOfCurrentViewToBeFilledIn() {
        if (currentViewToBeFilledInIsATextView()) {
            setTextForCurrentViewToBeFilledIn();
        }
        if (currentViewToBeFilledInIsASmartImageView()) {
            setImageOfCurrentViewToBeFilledInThroughURL();
        } else if (currentViewToBeFilledInIsAnImageView()) {
            setImageOfCurrentViewToBeFilledInThroughID();
        }
    }

    private void setTextForCurrentViewToBeFilledIn() {
        final TextView TV = (TextView) currentViewToBeFilledIn;
        final CharSequence CS = getCurrentDataToBeFilledByInAcceptableForm();
        TV.setText(CS);
    }

    private CharSequence getCurrentDataToBeFilledByInAcceptableForm() {
        try {
            return getCurrentDataToBeFilledByInSpannedForm();
        } catch(final Exception E) {
            return getCurrentDataToBeFilledByInSpannableStringBuilderForm();
        }
    }

    private CharSequence getCurrentDataToBeFilledByInSpannedForm() {
        final String SOURCE = (String) currentDataToBeFilledBy;
        return fromHtml(SOURCE);
    }

    private CharSequence getCurrentDataToBeFilledByInSpannableStringBuilderForm() {
        return (SpannableStringBuilder) currentDataToBeFilledBy;
    }

    private void setImageOfCurrentViewToBeFilledInThroughURL() {
        final SmartImageView SIV = (SmartImageView) currentViewToBeFilledIn;
        final String URL = (String) currentDataToBeFilledBy;
        SIV.setImageUrl(URL);
    }

    private void setImageOfCurrentViewToBeFilledInThroughID() {
        final ImageView IV = (ImageView) currentViewToBeFilledIn;
        final int RESOURCE_ID = (Integer) currentDataToBeFilledBy;
        IV.setImageResource(RESOURCE_ID);
    }

    private void goToTheNextCurrentListItemDataHeader() {
        ++currentListItemDataHeaderPosition;
    }
}