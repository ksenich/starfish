package com.googlion.shield.adapters;

import android.content.*;
import android.view.*;
import android.widget.*;
import com.example.starfishapp.R.*;
import com.googlion.shield.specialists.*;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;
import android.view.View.*;

import java.util.*;

import static android.content.Context.*;
import static android.content.ClipData.*;

public class UserCouponsPageAdapter extends ShieldSimpleAdapter implements OnClickListener {
    private Context requester;

    public UserCouponsPageAdapter(final Context C, final List<? extends Map<String, ?>> DATA,
                                  final int LAYOUT_RESOURCE_ID, String[] FROM, int[] TO,
                                  final PageScaler PS, final ListViewItemsTranslator LVIT,
                                  final FontSpecialistForListView FSFLV) {
        super(C, DATA, LAYOUT_RESOURCE_ID, FROM, TO, PS, LVIT, FSFLV);
        requester = C;
    }

    public View getView(final int POSITION, final View CONVERT_VIEW, final ViewGroup PARENT) {
        final View V = super.getView(POSITION, CONVERT_VIEW, PARENT);
        final View COPY_THE_COUPON_CODE = V.findViewById(id.tvCopyTheCouponCode);
        final OnClickListener L = UserCouponsPageAdapter.this;
        final View TAG = V.findViewById(id.tvCouponCode);
        COPY_THE_COUPON_CODE.setOnClickListener(L);
        COPY_THE_COUPON_CODE.setTag(TAG);
        return V;
    }

    public void onClick(final View V) {
        final Object TAG = V.getTag();
        final View COUPON_CODE = (View) TAG;
        final TextView TV = (TextView) COUPON_CODE;
        final String NAME = CLIPBOARD_SERVICE;
        final Object O = requester.getSystemService(NAME);
        final ClipboardManager CM = (ClipboardManager) O;
        final CharSequence LABEL = "";
        final CharSequence TEXT = TV.getText();
        final ClipData CD = newPlainText(LABEL, TEXT);
        CM.setPrimaryClip(CD);
    }
}