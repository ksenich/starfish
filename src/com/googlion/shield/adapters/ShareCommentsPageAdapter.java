package com.googlion.shield.adapters;

import com.example.starfishapp.R.*;
import android.content.*;
import android.view.*;
import com.googlion.shield.specialists.*;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;

import java.util.*;

public class ShareCommentsPageAdapter extends ShieldSimpleAdapter {
    private final List<? extends Map<String, ?>> COMMENTS_LIST;
    private final Context REQUESTER;
    private View currentView;
    private int currentListItemPosition;

    public ShareCommentsPageAdapter(final Context C, final List<? extends Map<String, ?>> DATA,
                                    final int LAYOUT_RESOURCE_ID, final String[] FROM,
                                    final int[] TO, final PageScaler PS,
                                    final ListViewItemsTranslator LVIT, final FontSpecialistForListView FSFLV) {
        super(C, DATA, LAYOUT_RESOURCE_ID, FROM, TO, PS, LVIT, FSFLV);
        COMMENTS_LIST = DATA;
        REQUESTER = C;
        currentView = new View(C);
        currentListItemPosition = 0;
    }

    public View getView(final int POSITION, final View CONVERT_VIEW, final ViewGroup PARENT) {
        final View V = super.getView(POSITION, CONVERT_VIEW, PARENT);
        retrieveCurrentListItemPositionFrom(POSITION);
        roundUserPhotoIn(V);
        return V;
    }

    private void retrieveCurrentListItemPositionFrom(final int POSITION) {
        currentListItemPosition = POSITION;
    }

    private void roundUserPhotoIn(final View V) {
        retrieveCurrentViewFrom(V);
        roundUserPhotoInCurrentView();
    }

    private void retrieveCurrentViewFrom(final View V) {
        currentView = V;
    }

    private void roundUserPhotoInCurrentView() {
        final Map<Object, Object> SETTINGS = getSettingsForRoundingImage();
        final RoundedImagesSpecialist S = new RoundedImagesSpecialist(SETTINGS);
        S.makeImageLookSpecial();
    }

    private Map<Object, Object> getSettingsForRoundingImage() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Image url", getImageUrlFromCommentsList());
        M.put("Requester", REQUESTER);
        M.put("Image view", getImageViewForImageRounding());
        return M;
    }

    private Object getImageUrlFromCommentsList() {
        final Map<String, ?> M = COMMENTS_LIST.get(currentListItemPosition);
        final String KEY = "Comment author photo";
        return M.get(KEY);
    }

    private Object getImageViewForImageRounding() {
        final Integer ID = id.sivCommentAuthorPhoto;
        return currentView.findViewById(ID);
    }
}