package com.googlion.shield.callbacks;

import static android.widget.Toast.*;

import android.content.*;
import android.widget.*;
import com.example.starfishapp.*;
import com.example.starfishapp.Starfish.*;
import com.example.starfishapp.pages.*;
import org.json.*;

public class AuthorizationPageLoginCallback implements LoginCallback {
    private AuthorizationPage requester;
    private String authorizationResult;

    public AuthorizationPageLoginCallback(final AuthorizationPage REQUESTER) {
        requester = REQUESTER;
        authorizationResult = "";
    }

    @Override
    public void onResult(final String RESULT) {
        retrieveAuthorizationResultFrom(RESULT);
        continueAuthorization();
    }

    private void retrieveAuthorizationResultFrom(final String RESULT) {
        authorizationResult = RESULT;
    }

    private void continueAuthorization() {
        if (authorizationIsSuccessful()) {
            goToTheMainPage();
        } else {
            showErrorMessage();
        }
    }

    private boolean authorizationIsSuccessful() {
        final CharSequence CS = "error";
        final boolean B = authorizationResult.contains(CS);
        return !B;
    }

    private void goToTheMainPage() {
        final Context CT = requester;
        final Class<?> C = MainActivity.class;
        final Intent I = new Intent(CT, C);
        requester.startActivity(I);
        requester.finish();
    }

    private void showErrorMessage() {
        final Context CT = requester;
        final CharSequence TEXT = getErrorMessageFromAuthorizationResult();
        final int DURATION = LENGTH_SHORT;
        final Toast T = makeText(CT, TEXT, DURATION);
        T.show();
    }

    private String getErrorMessageFromAuthorizationResult() {
        try {
            return getErrorMessageFromResult();
        } catch (final Exception E) {
            return getErrorMessageByDefault();
        }
    }

    private String getErrorMessageFromResult() throws Exception {
        final JSONObject O = new JSONObject(authorizationResult);
        final JSONObject ERROR = O.getJSONObject("error");
        return ERROR.getString("message");
    }

    private String getErrorMessageByDefault() {
        return "";
    }
}