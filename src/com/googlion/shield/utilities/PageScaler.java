package com.googlion.shield.utilities;

import static android.R.attr.state_checked;
import static android.R.id.*;
import static com.example.starfishapp.R.*;
import static android.graphics.Bitmap.*;
import static android.util.DisplayMetrics.*;
import static android.util.StateSet.*;
import static android.util.TypedValue.*;
import static android.view.ViewGroup.LayoutParams.*;
import static java.lang.Math.*;

import android.annotation.*;
import android.app.*;
import android.content.res.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.view.ViewGroup.*;
import android.widget.*;
import com.googlion.shield.views.*;

public class PageScaler implements Parcelable {
    private Activity requester;
    private View currentView;
    private Bundle originalDrawablesResourceIds;
    private float scale;
    private int originalDrawableResourceId;

    public PageScaler(final Activity REQUESTER) {
        requester = REQUESTER;
        currentView = getRootViewFromRequester();
        originalDrawablesResourceIds = new Bundle();
        scale = getScaleFromRootViewAndRequester();
        originalDrawableResourceId = 0;
    }

    private View getRootViewFromRequester() {
        final View V = requester.findViewById(content);
        final ViewGroup VG = (ViewGroup) V;
        final int ROOT_VIEW_INDEX = 0;
        return VG.getChildAt(ROOT_VIEW_INDEX);
    }

    private float getScaleFromRootViewAndRequester() {
        final DisplayMetrics DM = getDisplayMetricsFromRequester();
        final LayoutParams LP = currentView.getLayoutParams();
        final float SCREEN_WIDTH = (float) DM.widthPixels;
        final float SCREEN_HEIGHT = (float) DM.heightPixels;
        final int BASE_DENSITY = DENSITY_MEDIUM;
        final float CURRENT_DENSITY = (float) DM.densityDpi;
        final float DENSITY_SCALE = CURRENT_DENSITY / BASE_DENSITY;
        final float ROOT_WIDTH = LP.width;
        final float SCALED_ROOT_WIDTH = ROOT_WIDTH / DENSITY_SCALE;
        final float ROOT_HEIGHT = LP.height;
        final float SCALED_ROOT_HEIGHT = ROOT_HEIGHT / DENSITY_SCALE;
        final float HORIZONTAL_SCALE = SCREEN_WIDTH / SCALED_ROOT_WIDTH;
        final float VERTICAL_SCALE = SCREEN_HEIGHT / SCALED_ROOT_HEIGHT;
        return min(HORIZONTAL_SCALE, VERTICAL_SCALE) / DENSITY_SCALE;
    }

    private DisplayMetrics getDisplayMetricsFromRequester() {
        final DisplayMetrics DM = new DisplayMetrics();
        final WindowManager WM = requester.getWindowManager();
        final Display D = WM.getDefaultDisplay();
        D.getMetrics(DM);
        return DM;
    }

    public void scaleThePage() {
        if (pageIsDefinedToBeScalable()) {
            scale();
        }
    }

    private boolean pageIsDefinedToBeScalable() {
        boolean b = currentViewWidthIsNotDefinedBySpecialValue();
        b &= currentViewHeightIsNotDefinedBySpecialValue();
        return b;
    }

    private boolean currentViewWidthIsNotDefinedBySpecialValue() {
        final LayoutParams LP = currentView.getLayoutParams();
        boolean b = LP.width != MATCH_PARENT;
        b &= LP.width != WRAP_CONTENT;
        return b;
    }

    private boolean currentViewHeightIsNotDefinedBySpecialValue() {
        final LayoutParams LP = currentView.getLayoutParams();
        boolean b = LP.height != MATCH_PARENT;
        b &= LP.height != WRAP_CONTENT;
        return b;
    }

    private void scale() {
        stretchPageToTheDeviceScreenSize();
        scale(currentView);
    }

    private void stretchPageToTheDeviceScreenSize() {
        final LayoutParams LP = currentView.getLayoutParams();
        LP.width = MATCH_PARENT;
        LP.height = MATCH_PARENT;
    }

    @SuppressLint("NewApi")
    public void scale(final View V) {
        retrieveCurrentViewFrom(V);
        scaleCurrentViewPadding();
        scaleCurrentViewIfThisViewIsScalable();
        scaleCurrentViewMarginsIfTheseMarginsAreScalable();
        scaleCurrentViewOtherParametersIfCurrentViewExtendsView();
        scaleCurrentViewChildrenIfThisViewHasOnes();
    }

    private void retrieveCurrentViewFrom(final View V) {
        currentView = V;
    }

    private void scaleCurrentViewPadding() {
        final float L = currentView.getPaddingLeft() * scale;
        final float T = currentView.getPaddingTop() * scale;
        final float R = currentView.getPaddingRight() * scale;
        final float B = currentView.getPaddingBottom() * scale;
        final int LEFT = (int) L;
        final int TOP = (int) T;
        final int RIGHT = (int) R;
        final int BOTTOM = (int) B;
        currentView.setPadding(LEFT, TOP, RIGHT, BOTTOM);
    }

    private void scaleCurrentViewIfThisViewIsScalable() {
        if (currentViewIsScalable()) {
            scaleCurrentViewItself();
        }
    }

    private boolean currentViewIsScalable() {
        return !(currentView instanceof LineSeparator);
    }

    private void scaleCurrentViewItself() {
        scaleCurrentViewWidthIfThisWidthIsNotDefinedBySpecialValue();
        scaleCurrentViewHeightIfThisHeightIsNotDefinedBySpecialValue();
    }

    private void scaleCurrentViewWidthIfThisWidthIsNotDefinedBySpecialValue() {
        if (currentViewWidthIsNotDefinedBySpecialValue()) {
            scaleCurrentViewWidth();
        }
    }

    private void scaleCurrentViewWidth() {
        final LayoutParams LP = currentView.getLayoutParams();
        LP.width *= scale;
    }

    private void scaleCurrentViewHeightIfThisHeightIsNotDefinedBySpecialValue() {
        if (currentViewHeightIsNotDefinedBySpecialValue()) {
            scaleCurrentViewHeight();
        }
    }

    private void scaleCurrentViewHeight() {
        final LayoutParams LP = currentView.getLayoutParams();
        LP.height *= scale;
    }

    private void scaleCurrentViewMarginsIfTheseMarginsAreScalable() {
        if (currentViewHasScalableMargins()) {
            scaleCurrentViewMargins();
        }
    }

    private boolean currentViewHasScalableMargins() {
        final LayoutParams LP = currentView.getLayoutParams();
        boolean b = currentView instanceof SeekBarTextView;
        b |= currentView instanceof SegmentOfSeekBarScale;
        b = !b;
        b &= LP instanceof MarginLayoutParams;
        return b;
    }

    private void scaleCurrentViewMargins() {
        final LayoutParams LP = currentView.getLayoutParams();
        final MarginLayoutParams MLP = (MarginLayoutParams) LP;
        MLP.leftMargin *= scale;
        MLP.rightMargin *= scale;
        MLP.topMargin *= scale;
        MLP.bottomMargin *= scale;
    }

    private void scaleCurrentViewOtherParametersIfCurrentViewExtendsView() {
        if (currentViewExtendsView()) {
            scaleCurrentViewOtherParameters();
        }
    }

    private boolean currentViewExtendsView() {
        boolean b = currentViewIsATextView();
        b |= currentViewIsASeekBar();
        b |= currentViewIsACompoundButton();
        return b;
    }

    private boolean currentViewIsATextView() {
        return currentView instanceof TextView;
    }

    private boolean currentViewIsASeekBar() {
        return currentView instanceof SeekBar;
    }

    private boolean currentViewIsACompoundButton() {
        return currentView instanceof CompoundButton;
    }

    private void scaleCurrentViewOtherParameters() {
        scaleCurrentViewTextViewParametersIfThisViewIsATextView();
        scaleCurrentViewThumbIfThisViewIsASeekBar();
        scaleCurrentViewButtonDrawableIfThisViewIsACompoundButton();
    }

    private void scaleCurrentViewTextViewParametersIfThisViewIsATextView() {
        scaleCurrentViewTextSizeIfThisViewIsATextView();
        scaleCurrentViewCheckMarkDrawableIfThisViewIsACheckedTextView();
    }

    private void scaleCurrentViewTextSizeIfThisViewIsATextView() {
        if (currentViewIsATextView()) {
            scaleTextSize();
        }
    }

    private void scaleTextSize() {
        final TextView TV = (TextView) currentView;
        final float OLD_SIZE = TV.getTextSize();
        final float NEW_SIZE = OLD_SIZE * scale;
        final int DIMENSION_UNIT = COMPLEX_UNIT_PX;
        TV.setTextSize(DIMENSION_UNIT, NEW_SIZE);
    }

    private void scaleCurrentViewCheckMarkDrawableIfThisViewIsACheckedTextView() {
        if (currentViewIsACheckedTextView()) {
            scaleCheckedTextViewCheckMarkDrawable();
        }
    }

    private boolean currentViewIsACheckedTextView() {
        return currentView instanceof CheckedTextView;
    }

    private void scaleCheckedTextViewCheckMarkDrawable() {
        prepareCheckMarkDrawableForScaling();
        scaleCheckMarkDrawable();
    }

    private void prepareCheckMarkDrawableForScaling() {
        final Bundle B;
        if (currentViewIsAnAlertDialogRadioButton()) {
            B = getOriginalDrawablesResourceIdsForRadioButtonScaling();
        } else {
            B = getOriginalDrawablesResourceIdsForCheckBoxScaling();
        }
        originalDrawablesResourceIds = B;
    }

    private boolean currentViewIsAnAlertDialogRadioButton() {
        return currentView instanceof AlertDialogRadioButton;
    }

    private Bundle getOriginalDrawablesResourceIdsForRadioButtonScaling() {
        final Bundle B = new Bundle();
        final String CHECKED_STATE_KEY = "Drawable resource id for checked state";
        final String UNCHECKED_STATE_KEY = "Drawable resource id for unchecked state";
        final int CHECKED_STATE_VALUE = drawable.radio_button_checked_image;
        final int UNCHECKED_STATE_VALUE = drawable.radio_button_unchecked_image;
        B.putInt(CHECKED_STATE_KEY, CHECKED_STATE_VALUE);
        B.putInt(UNCHECKED_STATE_KEY, UNCHECKED_STATE_VALUE);
        return B;
    }

    private Bundle getOriginalDrawablesResourceIdsForCheckBoxScaling() {
        final Bundle B = new Bundle();
        final String CHECKED_STATE_KEY = "Drawable resource id for checked state";
        final String UNCHECKED_STATE_KEY = "Drawable resource id for unchecked state";
        final int CHECKED_STATE_VALUE = drawable.check_box_checked_image;
        final int UNCHECKED_STATE_VALUE = drawable.check_box_unchecked_image;
        B.putInt(CHECKED_STATE_KEY, CHECKED_STATE_VALUE);
        B.putInt(UNCHECKED_STATE_KEY, UNCHECKED_STATE_VALUE);
        return B;
    }

    private void scaleCheckMarkDrawable() {
        final Drawable D = getScaledStateListDrawableFromOriginalDrawablesResourceIds();
        final CheckedTextView CTV = (CheckedTextView) currentView;
        CTV.setCheckMarkDrawable(D);
    }

    private Drawable getScaledStateListDrawableFromOriginalDrawablesResourceIds() {
        final Drawable CHECKED_STATE_DRAWABLE = getDrawableForCheckedState();
        final Drawable UNCHECKED_STATE_DRAWABLE = getDrawableForUncheckedState();
        final StateListDrawable SLD = new StateListDrawable();
        final int[] CHECKED_STATE_SET = {state_checked};
        SLD.addState(CHECKED_STATE_SET, CHECKED_STATE_DRAWABLE);
        SLD.addState(WILD_CARD, UNCHECKED_STATE_DRAWABLE);
        return SLD;
    }

    private Drawable getDrawableForCheckedState() {
        retrieveOriginalDrawableResourceIdForCheckedState();
        return getScaledDrawableFromItsResourceId();
    }

    private void retrieveOriginalDrawableResourceIdForCheckedState() {
        final String KEY = "Drawable resource id for checked state";
        final Bundle B = originalDrawablesResourceIds;
        originalDrawableResourceId = B.getInt(KEY);
    }

    private Drawable getScaledDrawableFromItsResourceId() {
        final Resources R = requester.getResources();
        final Drawable D = R.getDrawable(originalDrawableResourceId);
        final BitmapDrawable BD = (BitmapDrawable) D;
        final Bitmap SOURCE = BD.getBitmap();
        final float SCALED_WIDTH = SOURCE.getWidth() * scale;
        final float SCALED_HEIGHT = SOURCE.getHeight() * scale;
        final int W = (int) SCALED_WIDTH;
        final int H = (int) SCALED_HEIGHT;
        final boolean FILTER = false;
        final Bitmap B = createScaledBitmap(SOURCE, W, H, FILTER);
        return new BitmapDrawable(R, B);
    }

    private Drawable getDrawableForUncheckedState() {
        retrieveOriginalDrawableResourceIdForUncheckedState();
        return getScaledDrawableFromItsResourceId();
    }

    private void retrieveOriginalDrawableResourceIdForUncheckedState() {
        final String KEY = "Drawable resource id for unchecked state";
        final Bundle B = originalDrawablesResourceIds;
        originalDrawableResourceId = B.getInt(KEY);
    }

    private void scaleCurrentViewThumbIfThisViewIsASeekBar() {
        if (currentViewIsASeekBar()) {
            scaleSeekBarThumb();
        }
    }

    private void scaleSeekBarThumb() {
        retrieveThumbResourceId();
        scaleThumb();
    }

    private void retrieveThumbResourceId() {
        originalDrawableResourceId = drawable.thumb;
    }

    private void scaleThumb() {
        final Drawable D = getScaledDrawableFromItsResourceId();
        final SeekBar SB = (SeekBar) currentView;
        SB.setThumb(D);
    }

    private void scaleCurrentViewButtonDrawableIfThisViewIsACompoundButton() {
        if (currentViewIsACompoundButton()) {
            scaleButtonDrawableOfCompoundButton();
        }
    }

    private void scaleButtonDrawableOfCompoundButton() {
        prepareButtonDrawableForScaling();
        scaleButtonDrawable();
    }

    private void prepareButtonDrawableForScaling() {
        final Bundle B;
        if (currentViewIsARadioButton()) {
            B = getOriginalDrawablesResourceIdsForRadioButtonScaling();
        } else {
            B = getOriginalDrawablesResourceIdsForCheckBoxScaling();
        }
        originalDrawablesResourceIds = B;
    }

    private boolean currentViewIsARadioButton() {
        return currentView instanceof RadioButton;
    }

    private void scaleButtonDrawable() {
        final Drawable D = getScaledStateListDrawableFromOriginalDrawablesResourceIds();
        final CompoundButton CB = (CompoundButton) currentView;
        CB.setButtonDrawable(D);
    }

    private void scaleCurrentViewChildrenIfThisViewHasOnes() {
        if (currentViewHasChildren()) {
            scaleChildren();
        }
    }

    private boolean currentViewHasChildren() {
        return currentView instanceof ViewGroup;
    }

    private void scaleChildren() {
        final ViewGroup VG = (ViewGroup) currentView;
        final int SIZE = VG.getChildCount();
        for (int i = 0; i < SIZE; ++i) {
            scale(VG.getChildAt(i));
        }
    }

    public void scaleChildrenOf(final ViewGroup VG) {
        final int SIZE = VG.getChildCount();
        for (int i = 0; i < SIZE; ++i) {
            scale(VG.getChildAt(i));
        }
    }

    public void scaleDividerHeightOf(final ListView LV) {
        final int DIVIDER_OLD_HEIGHT = LV.getDividerHeight();
        final float DIVIDER_SCALED_HEIGHT = DIVIDER_OLD_HEIGHT * scale;
        final int DIVIDER_NEW_HEIGHT = (int) DIVIDER_SCALED_HEIGHT;
        LV.setDividerHeight(DIVIDER_NEW_HEIGHT);
    }

    public final float getScale() {
        return scale;
    }

    public final float getScreenScale() {
        final DisplayMetrics DM = getDisplayMetricsFromRequester();
        final int BASE_DENSITY = DENSITY_MEDIUM;
        final float CURRENT_DENSITY = (float) DM.densityDpi;
        final float DENSITY_SCALE = CURRENT_DENSITY / BASE_DENSITY;
        return scale * DENSITY_SCALE;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel DESTINATION, final int FLAGS) {
    }
}