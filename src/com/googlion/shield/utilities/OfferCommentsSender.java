package com.googlion.shield.utilities;

import android.content.*;
import android.os.*;
import com.example.starfishapp.*;
import com.example.starfishapp.pages.*;

import java.net.*;

import static com.example.starfishapp.Starfish.*;

public class OfferCommentsSender extends AsyncTask<Void, Void, Void> {

    private Bundle offerCommentsSendingSettings;
    private CommentsActivity requester;
    private int offerId;
    private String comment;

    public OfferCommentsSender(final Bundle SETTINGS) {
        offerCommentsSendingSettings = SETTINGS;
        requester = getRequesterFromOfferCommentsSendingSettings();
        comment = getCommentFromOfferCommentsSendingSettings();
        offerId = getOfferIdFromOfferCommentsSendingSettings();
    }

    private CommentsActivity getRequesterFromOfferCommentsSendingSettings() {
        final String KEY = "Requester";
        return offerCommentsSendingSettings.getParcelable(KEY);
    }

    private String getCommentFromOfferCommentsSendingSettings() {
        final String KEY = "Comment";
        return offerCommentsSendingSettings.getString(KEY);
    }

    private int getOfferIdFromOfferCommentsSendingSettings() {
        final String KEY = "Offer id";
        return offerCommentsSendingSettings.getInt(KEY);
    }

    @Override
    protected Void doInBackground(final Void... V) {
        final Context C = requester;
        final Starfish S = getInstance(C);
        final String COM = URLEncoder.encode(comment);
        S.sendComment(COM, offerId);
        return null;
    }

    @Override
    protected void onPostExecute(final Void V) {
        requester.startLoadingComments();
    }
}

