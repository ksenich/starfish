package com.googlion.shield.utilities;

import com.example.starfishapp.helpers.*;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;

public class MapVisibleRegionBoundsCreator {

    private GoogleMap map;
    private LatLng northEastBound;
    private LatLng southWestBound;
    private LatLngBounds mapVisibleRegionLatitudeLongitudeBounds;
    private double northEastBoundLatitude;
    private double southWestBoundLatitude;
    private double northEastBoundLongitude;
    private double southWestBoundLongitude;
    private double lowLatitude;
    private double lowLongitude;
    private double highLatitude;
    private double highLongitude;

    public MapVisibleRegionBoundsCreator(final GoogleMap MAP) {
        map = MAP;
        mapVisibleRegionLatitudeLongitudeBounds = getMapVisibleRegionLatitudeLongitudeBoundsFromMap();
        northEastBound = getNorthEastBoundFromMapVisibleRegionLatitudeLongitudeBounds();
        southWestBound = getSouthWestBoundFromMapVisibleRegionLatitudeLongitudeBounds();
        northEastBoundLatitude = getNorthEastBoundLatitudeFromNorthEastBound();
        southWestBoundLatitude = getSouthWestBoundLatitudeFromSouthWestBound();
        northEastBoundLongitude = getNorthEastBoundLongitudeFromNorthEastBound();
        southWestBoundLongitude = getSouthWestBoundLongitudeFromSouthWestBound();
        lowLatitude = getLowLatitudeForMapVisibleRegion();
        lowLongitude = getLowLongitudeForMapVisibleRegion();
        highLatitude = getHighLatitudeForMapVisibleRegion();
        highLongitude = getHighLongitudeForMapVisibleRegion();
    }

    private LatLngBounds getMapVisibleRegionLatitudeLongitudeBoundsFromMap() {
        final Projection P = map.getProjection();
        final VisibleRegion VR = P.getVisibleRegion();
        return VR.latLngBounds;
    }

    private LatLng getNorthEastBoundFromMapVisibleRegionLatitudeLongitudeBounds() {
        return mapVisibleRegionLatitudeLongitudeBounds.northeast;
    }

    private LatLng getSouthWestBoundFromMapVisibleRegionLatitudeLongitudeBounds() {
        return mapVisibleRegionLatitudeLongitudeBounds.southwest;
    }

    private double getNorthEastBoundLatitudeFromNorthEastBound() {
        return northEastBound.latitude;
    }

    private double getSouthWestBoundLatitudeFromSouthWestBound() {
        return southWestBound.latitude;
    }

    private double getNorthEastBoundLongitudeFromNorthEastBound() {
        return northEastBound.longitude;
    }

    private double getSouthWestBoundLongitudeFromSouthWestBound() {
        return southWestBound.longitude;
    }

    private double getLowLatitudeForMapVisibleRegion() {
        final double NEBL = northEastBoundLatitude;
        final double SWBL = southWestBoundLatitude;
        return NEBL < SWBL ? NEBL : SWBL;
    }

    private double getLowLongitudeForMapVisibleRegion() {
        final double NEBL = northEastBoundLongitude;
        final double SWBL = southWestBoundLongitude;
        return NEBL < SWBL ? NEBL : SWBL;
    }

    private double getHighLatitudeForMapVisibleRegion() {
        final double NEBL = northEastBoundLatitude;
        final double SWBL = southWestBoundLatitude;
        return NEBL < SWBL ? SWBL : NEBL;
    }

    private double getHighLongitudeForMapVisibleRegion() {
        final double NEBL = northEastBoundLongitude;
        final double SWBL = southWestBoundLongitude;
        return NEBL < SWBL ? SWBL : NEBL;
    }

    public boolean pointIsVisible(final RouteBoxer.LatLng POINT) {
        boolean b = POINT.lat <= highLatitude;
        b &= POINT.lat >= lowLatitude;
        b &= POINT.lng <= highLongitude;
        b &= POINT.lng >= lowLongitude;
        return b;
    }
}
