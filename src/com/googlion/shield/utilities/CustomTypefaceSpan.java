package com.googlion.shield.utilities;

import android.graphics.*;
import android.text.*;
import android.text.style.*;
import static android.graphics.Typeface.*;

public class CustomTypefaceSpan extends TypefaceSpan {
	private Typeface newTypeFace;
	private Paint paint;

	public CustomTypefaceSpan(final String FAMILY, final Typeface TF) {
		super(FAMILY);
		newTypeFace = TF;
		paint = new Paint();
	}

	public void updateDrawState(final TextPaint TP) {
		applyCustomTypeFaceTo(TP);
	}

	private void applyCustomTypeFaceTo(final TextPaint TP) {
		retrievePaintFrom(TP);
		applyCustomTypeFaceToPaint();
	}

	private void retrievePaintFrom(final TextPaint TP) {
		paint = TP;
	}

	private void applyCustomTypeFaceToPaint() {
		setBoldTypeFaceIfItWasRequested();
		setItalicTypeFaceIfItWasRequested();
		setNewTypeFaceForPaint();
	}

	private void setBoldTypeFaceIfItWasRequested() {
		if (newTypeFaceShouldBeBold()) {
			setBoldTypeFaceForPaint();
		}
	}

	private boolean newTypeFaceShouldBeBold() {
		final int A = getOldStyleFromPaint();
		final int B = A & ~newTypeFace.getStyle();
		final int C = B & BOLD;
		return C != 0;
	}

	private int getOldStyleFromPaint() {
		try {
			return getOldStyleFromTypeFace();
		} catch (final Exception E) {
			return getOldStyleByDefault();
		}
	}

	private int getOldStyleFromTypeFace() {
		final Typeface TF = paint.getTypeface();
		return TF.getStyle();
	}

	private int getOldStyleByDefault() {
		return 0;
	}

	private void setBoldTypeFaceForPaint() {
		paint.setFakeBoldText(true);
	}

	private void setItalicTypeFaceIfItWasRequested() {
		if (newTypeFaceShouldBeItalic()) {
			setItalicTypeFaceForPaint();
		}
	}

	private boolean newTypeFaceShouldBeItalic() {
		final int A = getOldStyleFromPaint();
		final int B = A & ~newTypeFace.getStyle();
		final int C = B & ITALIC;
		return C != 0;
	}

	private void setItalicTypeFaceForPaint() {
		final float SKEW_X = -0.25f;
		paint.setTextSkewX(SKEW_X);
	}

	private void setNewTypeFaceForPaint() {
		paint.setTypeface(newTypeFace);
	}

	public void updateMeasureState(final TextPaint TP) {
		applyCustomTypeFaceTo(TP);
	}
}