package com.googlion.shield.pages;

import static com.example.starfishapp.MapManager.*;
import static java.lang.Integer.*;
import static android.graphics.Typeface.*;
import static android.text.Spanned.*;

import android.content.*;
import android.content.res.*;
import android.graphics.*;
import android.os.*;
import android.support.v7.app.*;
import android.text.*;
import android.view.*;
import android.widget.*;
import android.widget.AdapterView.*;
import com.example.starfishapp.R.*;
import com.googlion.shield.loaders.*;
import com.googlion.shield.specialists.*;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;

import java.util.*;

public class UserCouponsPage extends ActionBarActivity implements OnItemClickListener {
    private UserCouponsPageTranslator pageTranslator;
    private PageScaler pageScaler;
    private Map<String, List<Map<String, Object>>> userCoupons;
    private List<Map<String, Object>> userCouponsList;

    @Override
    protected void onCreate(final Bundle SAVED_INSTANCE_STATE) {
        performOnCreateMethodFromSuperclass(SAVED_INSTANCE_STATE);
        setContentViewForThisPage();
        initializeFields();
        translateThisPage();
        setFontsForAllTextualViewsOfThisPage();
        scale();
        setOnItemClickListenerForUserCouponsList();
        loadUserCoupons();
    }

    private void performOnCreateMethodFromSuperclass(final Bundle SAVED_INSTANCE_STATE) {
        super.onCreate(SAVED_INSTANCE_STATE);
    }

    private void setContentViewForThisPage() {
        setContentView(layout.user_coupons_page);
    }

    private void initializeFields() {
        pageTranslator = new UserCouponsPageTranslator(getSettingsForPageTranslation());
        pageScaler = new PageScaler(UserCouponsPage.this);
        userCoupons = new HashMap<String, List<Map<String, Object>>>();
        userCouponsList = new LinkedList<Map<String, Object>>();
    }

    private Map<String, Map<String, Object>> getSettingsForPageTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getPageViewsIds());
        M.put("Page views default texts", getPageViewsDefaultTexts());
        M.put("Other settings", getOtherSettings());
        return M;
    }

    private Map<String, Object> getPageViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("bActiveCoupons", id.bActiveCoupons);
        M.put("bUsedCoupons", id.bUsedCoupons);
        M.put("tvActiveUserCouponsNumberHeader", id.tvUserCouponsNumber);
        return M;
    }

    private Map<String, Object> getPageViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("bActiveCoupons", "Дійсні");
        M.put("bUsedCoupons", "Використані");
        M.put("tvActiveUserCouponsNumberHeader", "Дійсних талонів у Вас:");
        return M;
    }

    private Map<String, Object> getOtherSettings() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", UserCouponsPage.this);
        M.put("Page title translation key", "User coupons page action bar title");
        M.put("Page title default text", "Мої талони");
        return M;
    }

    private void translateThisPage() {
        pageTranslator.translate();
    }

    private void setFontsForAllTextualViewsOfThisPage() {
        final Map<Object, Map> S = getSettingsForSettingFonts();
        final FontSpecialist FS = new FontSpecialist(S);
        FS.setFonts();
    }

    private Map<Object, Map> getSettingsForSettingFonts() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingFonts());
        M.put("Other settings", getOtherSettingsForSettingFonts());
        return M;
    }

    private Map getMainSettingsForSettingFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.bActiveCoupons, "robotobold.otf");
        M.put(id.bUsedCoupons, "robotobold.otf");
        M.put(id.tvUserCouponsNumber, "kelsonsanslightru.otf");
        return M;
    }

    private Map getOtherSettingsForSettingFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", UserCouponsPage.this);
        return M;
    }

    private void scale() {
        scaleThisPage();
        scaleDividerHeightOfUserCouponsList();
    }

    private void scaleThisPage() {
        pageScaler.scaleThePage();
    }

    private void scaleDividerHeightOfUserCouponsList() {
        final ListView LV = getListViewOfThisPage();
        pageScaler.scaleDividerHeightOf(LV);
    }

    private ListView getListViewOfThisPage() {
        final View V = findViewById(id.lvUserCoupons);
        return (ListView) V;
    }

    private void setOnItemClickListenerForUserCouponsList() {
        final OnItemClickListener L = UserCouponsPage.this;
        final ListView LV = getListViewOfThisPage();
        LV.setOnItemClickListener(L);
    }

    private void loadUserCoupons() {
        final UserCouponsPage REQUESTER = UserCouponsPage.this;
        final UserCouponsLoader UCL = new UserCouponsLoader(REQUESTER);
        UCL.loadUserCoupons();
    }

    public void apply(final Map<String, List<Map<String, Object>>> M) {
        retrieveUserCouponsFrom(M);
        setUpPageForActiveCouponsList();
    }

    private void retrieveUserCouponsFrom(final Map<String, List<Map<String, Object>>> M) {
        userCoupons = M;
    }

    private void setUpPageForActiveCouponsList() {
        final Context C = UserCouponsPage.this;
        final View V = new View(C);
        setUpPageForActiveCouponsList(V);
    }

    public void setUpPageForActiveCouponsList(final View V) {
        fillUpUserCouponsListWithActiveCoupons();
        setUpHeaderForTheActiveCouponsNumber();
        setUpPageForCouponsList();
    }

    private void fillUpUserCouponsListWithActiveCoupons() {
        final String KEY = "Active coupons";
        userCouponsList = userCoupons.get(KEY);
    }

    private void setUpHeaderForTheActiveCouponsNumber() {
        final View V = findViewById(id.tvUserCouponsNumber);
        final TextView TV = (TextView) V;
        final CharSequence TEXT = pageTranslator.getHeaderForTheActiveCouponsNumber();
        TV.setText(TEXT);
    }

    private void setUpPageForCouponsList() {
        displayUserCoupons();
        displayCouponsNumber();
    }

    private void displayUserCoupons() {
        final Map<String, List<Map<String, Object>>> SETTINGS = getSettingsForListDisplaying();
        final ListViewSpecialist LVS = new UserCouponsPageListViewSpecialist(SETTINGS);
        LVS.setUpListView();
    }

    private Map<String, List<Map<String, Object>>> getSettingsForListDisplaying() {
        final Map<String, List<Map<String, Object>>> M = new HashMap<String, List<Map<String, Object>>>();
        M.put("List view items data", getListViewItemsData());
        M.put("List view item fields to be filled", getListViewItemFieldsToBeFilled());
        M.put("Other settings", getOtherSettingsForListDisplaying());
        return M;
    }

    private List<Map<String, Object>> getListViewItemsData() {
        return userCouponsList;
    }

    private List<Map<String, Object>> getListViewItemFieldsToBeFilled() {
        final List<Map<String, Object>> L = new LinkedList<Map<String, Object>>();
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Coupon image", id.sivCouponImage);
        M.put("Coupon title", id.tvCouponTitle);
        M.put("Coupon description", id.tvCouponDescription);
        M.put("Coupon code", id.tvCouponCode);
        L.add(M);
        return L;
    }

    private List<Map<String, Object>> getOtherSettingsForListDisplaying() {
        final List<Map<String, Object>> L = new LinkedList<Map<String, Object>>();
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", UserCouponsPage.this);
        M.put("List view id", id.lvUserCoupons);
        M.put("Layout resource id", layout.user_coupons_page_list_item);
        M.put("Page scaler", pageScaler);
        M.put("List view items translator", getTranslatorForListViewItems());
        M.put("Font specialist for list view", getFontSpecialistForProcessingListView());
        L.add(M);
        return L;
    }

    private Object getTranslatorForListViewItems() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForListItemTranslation();
        return new ListViewItemsTranslator(SETTINGS);
    }

    private Map<String, Map<String, Object>> getSettingsForListItemTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getListItemViewsIds());
        M.put("Page views default texts", getListItemsViewsDefaultTexts());
        M.put("Other settings", getOtherSettings());
        return M;
    }

    private Map<String, Object> getListItemViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvCouponTitle", id.tvCouponTitle);
        M.put("tvCouponDescription", id.tvCouponDescription);
        M.put("tvCopyTheCouponCode", id.tvCopyTheCouponCode);
        return M;
    }

    private Map<String, Object> getListItemsViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvCouponTitle", "Заголовок відсутній.");
        M.put("tvCouponDescription", "Опис відсутній.");
        M.put("tvCopyTheCouponCode", "Скопіювати");
        return M;
    }

    private FontSpecialistForListView getFontSpecialistForProcessingListView() {
        final Map<Object, Map> S = getSettingsForSettingFontsInListView();
        return new FontSpecialistForListView(S);
    }

    private Map<Object, Map> getSettingsForSettingFontsInListView() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingFontsInListView());
        M.put("Other settings", getOtherSettingsForSettingFontsInListView());
        return M;
    }

    private Map getMainSettingsForSettingFontsInListView() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvCouponTitle, "kelsonsansregularru.otf");
        M.put(id.tvCouponDescription, "robotoregular0.otf");
        M.put(id.tvCouponCode, "robotobold.otf");
        M.put(id.tvCopyTheCouponCode, "robotobold.otf");
        return M;
    }

    private Map getOtherSettingsForSettingFontsInListView() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", UserCouponsPage.this);
        return M;
    }

    private void displayCouponsNumber() {
        final View V = findViewById(id.tvUserCouponsNumber);
        final TextView TV = (TextView) V;
        final CharSequence OLD_TEXT = TV.getText();
        final SpannableStringBuilder NEW_TEXT = new SpannableStringBuilder(OLD_TEXT);
        final CharSequence COUPONS_NUMBER = getUserCouponsNumberFromUserCouponsList();
        NEW_TEXT.append(' ');
        NEW_TEXT.append(COUPONS_NUMBER);
        TV.setText(NEW_TEXT);
    }

    private CharSequence getUserCouponsNumberFromUserCouponsList() {
        final CharSequence SIZE = userCouponsList.size() + "";
        final SpannableStringBuilder SSB = new SpannableStringBuilder(SIZE);
        final String FAMILY = "";
        final AssetManager MGR = getAssets();
        final String PATH = "kelsonsansboldru.otf";
        final Typeface TF = createFromAsset(MGR, PATH);
        final CustomTypefaceSpan WHAT = new CustomTypefaceSpan(FAMILY, TF);
        final Integer START = 0;
        final Integer END = SSB.length();
        SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_INCLUSIVE);
        return SSB;
    }

    public void setUpPageForUsedCouponsList(final View V) {
        fillUpUserCouponsListWithUsedCoupons();
        setUpHeaderForTheUsedCouponsNumber();
        setUpPageForCouponsList();
    }

    private void fillUpUserCouponsListWithUsedCoupons() {
        final String KEY = "Used coupons";
        userCouponsList = userCoupons.get(KEY);
    }

    private void setUpHeaderForTheUsedCouponsNumber() {
        final View V = findViewById(id.tvUserCouponsNumber);
        final TextView TV = (TextView) V;
        final CharSequence TEXT = pageTranslator.getHeaderForTheUsedCouponsNumber();
        TV.setText(TEXT);
    }

    @Override
    public void onItemClick(final AdapterView<?> PARENT, final View V, final int POSITION, final long ID) {
        final Map<String, Object> M = userCouponsList.get(POSITION);
        final String KEY = "Coupon id";
        final Context C = UserCouponsPage.this;
        final Object O = M.get(KEY);
        final String S = (String) O;
        final int COUPON_ID = parseInt(S);
        goToOfferPage(C, COUPON_ID);
    }
}