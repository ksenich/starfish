package com.googlion.shield.pages;

import android.os.*;
import android.support.v7.app.*;
import android.text.SpannableStringBuilder;
import android.text.style.RelativeSizeSpan;
import android.view.*;
import android.widget.*;
import com.example.starfishapp.R.*;
import com.example.starfishapp.model.*;
import com.googlion.shield.loaders.*;
import com.googlion.shield.specialists.*;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;

import java.util.*;

import static android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE;

public class UserBillPage extends ActionBarActivity {
    private PageScaler pageScaler;
    private UserBillPageTranslator pageTranslator;
    private List<Map<String, Object>> userBillHistoryList;

    @Override
    protected void onCreate(final Bundle SAVED_INSTANCE_STATE) {
        performOnCreateMethodFromSuperclass(SAVED_INSTANCE_STATE);
        setContentViewForThisPage();
        initializeFields();
        translateThisPage();
        scale();
        setFontsForAllTextualViewsOfThisPage();
        loadUserInformation();
        displayUserBillHistories();
    }

    private void performOnCreateMethodFromSuperclass(final Bundle SAVED_INSTANCE_STATE) {
        super.onCreate(SAVED_INSTANCE_STATE);
    }

    private void setContentViewForThisPage() {
        setContentView(layout.user_bill_page);
    }

    private void initializeFields() {
        pageScaler = new PageScaler(UserBillPage.this);
        pageTranslator = getInitializedPageTranslator();
        userBillHistoryList = new LinkedList<Map<String, Object>>();
    }

    private UserBillPageTranslator getInitializedPageTranslator() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForUserBillPageTranslation();
        return new UserBillPageTranslator(SETTINGS);
    }

    private Map<String, Map<String, Object>> getSettingsForUserBillPageTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getPageViewsIds());
        M.put("Page views default texts", getPageViewsDefaultTexts());
        M.put("Other settings", getOtherSettingsForTranslation());
        return M;
    }

    private Map<String, Object> getPageViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvUserBonusesAvailable", id.tvUserBonusesAvailable);
        M.put("bEnrichTheBill", id.bEnrichTheBill);
        M.put("tvUserBillHistoryHeader", id.tvUserBillHistoryHeader);
        return M;
    }

    private Map<String, Object> getPageViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvUserBonusesAvailable", "Доступно бонусів:");
        M.put("bEnrichTheBill", "Поповнити рахунок");
        M.put("tvUserBillHistoryHeader", "Історія");
        return M;
    }

    private Map<String, Object> getOtherSettingsForTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", UserBillPage.this);
        M.put("Page title translation key", "User bill page action bar title");
        M.put("Page title default text", "Мій рахунок");
        return M;
    }

    private void translateThisPage() {
        pageTranslator.translate();
    }

    private void scale() {
        scaleThisPage();
        scaleDividerHeightOfUserBillHistoriesList();
    }

    private void scaleThisPage() {
        pageScaler.scaleThePage();
    }

    private void scaleDividerHeightOfUserBillHistoriesList() {
        final View V = findViewById(id.lvUserBillHistory);
        final ListView LV = (ListView) V;
        pageScaler.scaleDividerHeightOf(LV);
    }

    private void setFontsForAllTextualViewsOfThisPage() {
        final Map<Object, Map> S = getSettingsForSettingFonts();
        final FontSpecialist FS = new FontSpecialist(S);
        FS.setFonts();
    }

    private Map<Object, Map> getSettingsForSettingFonts() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingFonts());
        M.put("Other settings", getOtherSettingsForSettingFonts());
        return M;
    }

    private Map getMainSettingsForSettingFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvUserBonusesAvailable, "robotoregular0.otf");
        M.put(id.tvUserBonusesQuantity, "kelsonsansboldru.otf");
        M.put(id.bEnrichTheBill, "robotobold.otf");
        M.put(id.tvUserBillHistoryHeader, "robotoregular0.otf");
        return M;
    }

    private Map getOtherSettingsForSettingFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", UserBillPage.this);
        return M;
    }

    private void loadUserInformation() {
        final ActionBarActivity REQUESTER = UserBillPage.this;
        final UserInformationLoader UIL = new UserBillPageUserInformationLoader(REQUESTER);
        UIL.loadUserInformation();
    }

    public void apply(final UserInfo UI) {
        final View V = findViewById(id.tvUserBonusesQuantity);
        final TextView TV = (TextView) V;
        final CharSequence TEXT = UI.cnt_bonuses;
        TV.setText(TEXT);
    }

    private void displayUserBillHistories() {
        final Map<String, List<Map<String, Object>>> SETTINGS = getSettingsForListDisplaying();
        final ListViewSpecialist LVS = new ListViewSpecialist(SETTINGS);
        LVS.setUpListView();
    }

    private Map<String, List<Map<String, Object>>> getSettingsForListDisplaying() {
        final Map<String, List<Map<String, Object>>> M = new HashMap<String, List<Map<String, Object>>>();
        M.put("List view items data", getListViewItemsData());
        M.put("List view item fields to be filled", getListViewItemFieldsToBeFilled());
        M.put("Other settings", getOtherSettingsForListDisplaying());
        return M;
    }

    private List<Map<String, Object>> getListViewItemsData() {
        addUserBillHistoriesToList();
        return userBillHistoryList;
    }

    private void addUserBillHistoriesToList() {
        final int SIZE = 5;
        for (int i = 0; i < SIZE; ++i) {
            addUserBillHistoryToList();
        }
    }

    private void addUserBillHistoryToList() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("User bill history price", getPriceOfUserBillHistory());
        userBillHistoryList.add(M);
    }

    private CharSequence getPriceOfUserBillHistory() {
        final CharSequence VALUE = getValueOfUserBillHistoryPrice();
        final SpannableStringBuilder TEXT = new SpannableStringBuilder(VALUE);
        final CharSequence HEADER = pageTranslator.getUserBillHistoryPriceHeader();
        TEXT.append('\n');
        TEXT.append(HEADER);
        return TEXT;
    }

    private CharSequence getValueOfUserBillHistoryPrice() {
        final RelativeSizeSpan WHAT = getRelativeSizeSpanForUserBillHistoryPriceValue();
        final Integer START = 0;
        final CharSequence PRICE = "52";
        final SpannableStringBuilder SSB = new SpannableStringBuilder(PRICE);
        final Integer END = SSB.length();
        SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
        return SSB;
    }

    private RelativeSizeSpan getRelativeSizeSpanForUserBillHistoryPriceValue() {
        final Float VALUE_SIZE_FOR_MDPI = 44.44f;
        final Float HEADER_SIZE_FOR_MDPI = 22.22f;
        final Float PROPORTION = VALUE_SIZE_FOR_MDPI / HEADER_SIZE_FOR_MDPI;
        return new RelativeSizeSpan(PROPORTION);
    }

    private List<Map<String, Object>> getListViewItemFieldsToBeFilled() {
        final List<Map<String, Object>> L = new LinkedList<Map<String, Object>>();
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("User bill history price", id.tvUserBillHistoryPrice);
        L.add(M);
        return L;
    }

    private List<Map<String, Object>> getOtherSettingsForListDisplaying() {
        final List<Map<String, Object>> L = new LinkedList<Map<String, Object>>();
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", UserBillPage.this);
        M.put("List view id", id.lvUserBillHistory);
        M.put("Layout resource id", layout.user_bill_page_list_item_1);
        M.put("Page scaler", pageScaler);
        M.put("List view items translator", getTranslatorForListViewItems());
        M.put("Font specialist for list view", getFontSpecialistForProcessingListView());
        L.add(M);
        return L;
    }

    private Object getTranslatorForListViewItems() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForListItemTranslation();
        return new ListViewItemsTranslator(SETTINGS);
    }

    private Map<String, Map<String, Object>> getSettingsForListItemTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getListItemViewsIds());
        M.put("Page views default texts", getListItemsViewsDefaultTexts());
        M.put("Other settings", getOtherSettingsForTranslation());
        return M;
    }

    private Map<String, Object> getListItemViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvUserBillHistoryDate", id.tvUserBillHistoryDate);
        M.put("tvUserBillHistorySecondaryInformation", id.tvUserBillHistoryInformation);
        return M;
    }

    private Map<String, Object> getListItemsViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvUserBillHistoryDate", "Не вказано");
        M.put("tvUserBillHistorySecondaryInformation", "Опис відсутній.");
        return M;
    }

    private FontSpecialistForListView getFontSpecialistForProcessingListView() {
        final Map<Object, Map> S = getSettingsForSettingFontsInListView();
        return new FontSpecialistForListView(S);
    }

    private Map<Object, Map> getSettingsForSettingFontsInListView() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingFontsInListView());
        M.put("Other settings", getOtherSettingsForSettingFontsInListView());
        return M;
    }

    private Map getMainSettingsForSettingFontsInListView() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvUserBillHistoryPrice, "kelsonsansboldru.otf");
        M.put(id.tvUserBillHistoryDate, "robotoregular0.otf");
        M.put(id.tvUserBillHistoryInformation, "robotoregular0.otf");
        return M;
    }

    private Map getOtherSettingsForSettingFontsInListView() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", UserBillPage.this);
        return M;
    }
}