package com.googlion.shield.translators;

public interface Translator {
    public void translate();
}