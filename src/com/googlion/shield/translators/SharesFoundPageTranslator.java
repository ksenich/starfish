package com.googlion.shield.translators;

import org.json.*;
import java.util.*;

public class SharesFoundPageTranslator extends PageTranslator {
    public SharesFoundPageTranslator(final Map<String, Map<String, Object>> SETTINGS) {
        super(SETTINGS);
    }

    public CharSequence getSharePriceHeader() {
        try {
            return getHeaderForSharePrice();
        } catch (final Exception E) {
            return getDefaultHeaderForSharePrice();
        }
    }

    private CharSequence getHeaderForSharePrice() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "tvOfferPriceHeader";
        return O.getString(NAME);
    }

    private CharSequence getDefaultHeaderForSharePrice() {
        return "грн";
    }

    public CharSequence getShareDiscountPriceHeader() {
        try {
            return getHeaderForShareDiscountPrice();
        } catch (final Exception E) {
            return getDefaultHeaderForShareDiscountPrice();
        }
    }

    private CharSequence getHeaderForShareDiscountPrice() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "tvOfferDiscountPriceHeader";
        return O.getString(NAME);
    }

    private CharSequence getDefaultHeaderForShareDiscountPrice() {
        return "грн";
    }
}