package com.googlion.shield.translators;

import org.json.*;
import java.util.*;

public class UserBillPageTranslator extends TranslatorOfThePageWithActionBar {
    public UserBillPageTranslator(final Map<String, Map<String, Object>> SETTINGS) {
        super(SETTINGS);
    }

    public CharSequence getUserBillHistoryPriceHeader() {
        try {
            return getHeaderForUserBillHistoryPrice();
        } catch (final Exception E) {
            return getDefaultHeaderForUserBillHistoryPrice();
        }
    }

    private CharSequence getHeaderForUserBillHistoryPrice() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "tvUserBillHistoryPriceHeader";
        return O.getString(NAME);
    }

    private CharSequence getDefaultHeaderForUserBillHistoryPrice() {
        return "грн";
    }
}