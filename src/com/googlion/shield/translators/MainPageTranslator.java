package com.googlion.shield.translators;

import org.json.*;

import java.util.*;

public final class MainPageTranslator extends PageTranslator {
    public MainPageTranslator(final Map<String, Map<String, Object>> SETTINGS) {
        super(SETTINGS);
    }

    public final CharSequence getHeaderForApproximateTime() {
        try {
            return getHeaderForTime();
        } catch (final Exception E) {
            return getDefaultTextForTime();
        }
    }

    private CharSequence getHeaderForTime() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "tvApproximateTime";
        return O.getString(NAME);
    }

    private CharSequence getDefaultTextForTime() {
        return "хв";
    }

    public final CharSequence getHeaderForApproximateDistance() {
        try {
            return getHeaderForDistance();
        } catch (final Exception E) {
            return getDefaultTextForDistance();
        }
    }

    private CharSequence getHeaderForDistance() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "tvApproximateDistance";
        return O.getString(NAME);
    }

    private CharSequence getDefaultTextForDistance() {
        return "м";
    }

    public final CharSequence getRouteRadiusHeaderOne() {
        try {
            return getHeaderOneForRouteRadius();
        } catch (final Exception E) {
            return getDefaultHeaderOneForRouteRadius();
        }
    }

    private CharSequence getHeaderOneForRouteRadius() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "tvRouteRadiusHeader";
        return O.getString(NAME);
    }

    private CharSequence getDefaultHeaderOneForRouteRadius() {
        return "Радіус маршруту: ";
    }

    public final CharSequence getRouteRadiusHeaderTwo() {
        try {
            return getHeaderTwoForRouteRadius();
        } catch (final Exception E) {
            return getDefaultHeaderTwoForRouteRadius();
        }
    }

    private CharSequence getHeaderTwoForRouteRadius() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "tvRouteRadius";
        return O.getString(NAME);
    }

    private CharSequence getDefaultHeaderTwoForRouteRadius() {
        return "м";
    }

    public final String getLanguageAbbreviation() {
        try {
            return getAbbreviationOfLanguage();
        } catch (final Exception E) {
            return getLanguageAbbreviationByDefault();
        }
    }

    private String getAbbreviationOfLanguage() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "Language abbreviation";
        return O.getString(NAME);
    }

    private String getLanguageAbbreviationByDefault() {
        return "uk";
    }

    public final CharSequence getSharePriceHeader() {
        try {
            return getHeaderForSharePrice();
        } catch (final Exception E) {
            return getDefaultHeaderForSharePrice();
        }
    }

    private CharSequence getHeaderForSharePrice() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "tvOfferPriceHeader";
        return O.getString(NAME);
    }

    private CharSequence getDefaultHeaderForSharePrice() {
        return "грн";
    }

    public final CharSequence getShareDiscountPriceHeader() {
        try {
            return getHeaderForShareDiscountPrice();
        } catch (final Exception E) {
            return getDefaultHeaderForShareDiscountPrice();
        }
    }

    private CharSequence getHeaderForShareDiscountPrice() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "tvOfferDiscountPriceHeader";
        return O.getString(NAME);
    }

    private CharSequence getDefaultHeaderForShareDiscountPrice() {
        return "грн";
    }
}