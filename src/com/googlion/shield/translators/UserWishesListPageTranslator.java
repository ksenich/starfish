package com.googlion.shield.translators;

import java.util.*;

public class UserWishesListPageTranslator extends SharePageTranslator {
    public UserWishesListPageTranslator(final Map<String, Map<String, Object>> SETTINGS) {
        super(SETTINGS);
    }

    public CharSequence getWishPriceHeader() {
        return getSharePriceHeader();
    }

    public CharSequence getWishTimeInDaysHeader() {
        return getShareTimeInDaysHeader();
    }

    public CharSequence getWishTimeInHoursHeader() {
        return getShareTimeInHoursHeader();
    }
}