package com.googlion.shield.translators;

import org.json.*;

import java.util.*;

public class SharePageTranslator extends PageTranslator {
    public SharePageTranslator(final Map<String, Map<String, Object>> SETTINGS) {
        super(SETTINGS);
    }

    public CharSequence getBuyCouponMenuActionBarTitle() {
        try {
            return getActionBarTitleForBuyCouponMenu();
        } catch (final Exception E) {
            return getDefaultActionBarTitleForBuyCouponMenu();
        }
    }

    private CharSequence getActionBarTitleForBuyCouponMenu() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "Buy coupon menu action bar title";
        return O.getString(NAME);
    }

    private CharSequence getDefaultActionBarTitleForBuyCouponMenu() {
        return "Покупка талону";
    }

    public CharSequence getSharePriceHeader() {
        try {
            return getHeaderForSharePrice();
        } catch (final Exception E) {
            return getDefaultHeaderForSharePrice();
        }
    }

    private CharSequence getHeaderForSharePrice() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "tvSharePriceHeader";
        return O.getString(NAME);
    }

    private CharSequence getDefaultHeaderForSharePrice() {
        return "грн";
    }

    public CharSequence getShareTimeInDaysHeader() {
        try {
            return getHeaderForShareTimeInDays();
        } catch (final Exception E) {
            return getDefaultHeaderForShareTimeInDays();
        }
    }

    private CharSequence getHeaderForShareTimeInDays() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "tvShareTimeInDaysHeader";
        return O.getString(NAME);
    }

    private CharSequence getDefaultHeaderForShareTimeInDays() {
        return "Днів:";
    }

    public CharSequence getShareTimeInHoursHeader() {
        try {
            return getHeaderForShareTimeInHours();
        } catch (final Exception E) {
            return getDefaultHeaderForShareTimeInHours();
        }
    }

    private CharSequence getHeaderForShareTimeInHours() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "tvShareTimeInHoursHeader";
        return O.getString(NAME);
    }

    private CharSequence getDefaultHeaderForShareTimeInHours() {
        return "Годин:";
    }
}