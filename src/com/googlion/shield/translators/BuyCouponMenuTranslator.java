package com.googlion.shield.translators;

import org.json.*;

import java.util.*;

public class BuyCouponMenuTranslator extends PageTranslator {
    public BuyCouponMenuTranslator(final Map<String, Map<String, Object>> SETTINGS) {
        super(SETTINGS);
    }

    public CharSequence getCouponsTotalHeaderTwo() {
        try {
            return getHeaderTwoForCouponsTotal();
        } catch (final Exception E) {
            return getDefaultHeaderTwoForCouponsTotal();
        }
    }

    private CharSequence getHeaderTwoForCouponsTotal() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "tvCouponTotalHeaderTwo";
        return O.getString(NAME);
    }

    private CharSequence getDefaultHeaderTwoForCouponsTotal() {
        return "грн";
    }
}