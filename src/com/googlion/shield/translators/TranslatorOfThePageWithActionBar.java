package com.googlion.shield.translators;

import android.app.Activity;
import android.support.v7.app.*;
import org.json.*;

import java.util.*;

public class TranslatorOfThePageWithActionBar extends PageTranslator {
    public TranslatorOfThePageWithActionBar(final Map<String, Map<String, Object>> SETTINGS) {
        super(SETTINGS);
    }

    @Override
    public void translate() {
        translatePageTitle();
        translateRestOfThePage();
    }

    private void translatePageTitle() {
        final Activity A = getRequester();
        final ActionBarActivity ABA = (ActionBarActivity) A;
        final ActionBar AB = ABA.getSupportActionBar();
        final CharSequence CS = getTextForActionBarTitle();
        AB.setTitle(CS);
    }

    private CharSequence getTextForActionBarTitle() {
        try {
            return getPageTitleTextFromSettings();
        } catch (final Exception E) {
            return getPageTitleDefaultTextFromSettings();
        }
    }

    private CharSequence getPageTitleTextFromSettings() throws Exception {
        final Map<String, Map<String, Object>> SETTINGS = getSettings();
        final Map<String, Object> M = SETTINGS.get("Other settings");
        final JSONObject O = getChosenLanguage();
        final String KEY = "Page title translation key";
        final Object TITLE_KEY = M.get(KEY);
        final String NAME = (String) TITLE_KEY;
        return O.getString(NAME);
    }

    private String getPageTitleDefaultTextFromSettings() {
        final Map<String, Map<String, Object>> SETTINGS = getSettings();
        final Map<String, Object> M = SETTINGS.get("Other settings");
        final String KEY = "Page title default text";
        final Object O = M.get(KEY);
        return (String) O;
    }

    private void translateRestOfThePage() {
        super.translate();
    }
}