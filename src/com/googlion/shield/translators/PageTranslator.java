package com.googlion.shield.translators;

import android.app.*;
import android.content.*;
import android.view.*;
import android.widget.*;
import org.json.*;

import java.util.*;

import static android.content.Context.*;

public class PageTranslator implements Translator {
    private Map<String, Map<String, Object>> settings;
    private JSONObject chosenLanguage;
    private View currentViewToBeTranslated;
    private int currentPageViewPosition;

    public PageTranslator(final Map<String, Map<String, Object>> SETTINGS) {
        settings = SETTINGS;
        chosenLanguage = getChosenLanguageFromRequester();
        currentViewToBeTranslated = getCurrentViewToBeTranslatedByDefault();
        currentPageViewPosition = 0;
    }

    private JSONObject getChosenLanguageFromRequester() {
        try {
            return getLanguageFromRequester();
        } catch (final Exception E) {
            return new JSONObject();
        }
    }

    private JSONObject getLanguageFromRequester() throws Exception {
        final String NAME = "Chosen language";
        final int MODE = MODE_PRIVATE;
        final Activity A = getRequester();
        final SharedPreferences SP = A.getSharedPreferences(NAME, MODE);
        final String KEY = "Language content";
        final String DEFAULT_VALUE = "";
        final String S = SP.getString(KEY, DEFAULT_VALUE);
        return new JSONObject(S);
    }

    protected Activity getRequester() {
        final Map<String, Object> M = settings.get("Other settings");
        final Object O = M.get("Requester");
        return (Activity) O;
    }

    private View getCurrentViewToBeTranslatedByDefault() {
        final Context C = getRequester();
        return new View(C);
    }

    @Override
    public void translate() {
        resetCurrentPageViewPosition();
        translatePageViews();
    }

    private void resetCurrentPageViewPosition() {
        currentPageViewPosition = 0;
    }

    private void translatePageViews() {
        try {
            translateViews();
        } catch (final Exception E) {
            E.getMessage();
        }
    }

    private void translateViews() {
        while (thereArePageViewsNeedToBeTranslated()) {
            translateCurrentPageView();
        }
    }

    private boolean thereArePageViewsNeedToBeTranslated() {
        final List<String> L = getPageViewsTranslationKeys();
        final int SIZE = L.size();
        return currentPageViewPosition < SIZE;
    }

    private List<String> getPageViewsTranslationKeys() {
        final Map<String, Object> M = settings.get("Page views ids");
        final Set<String> S = M.keySet();
        return new LinkedList<String>(S);
    }

    private void translateCurrentPageView() {
        retrieveCurrentViewToBeTranslated();
        translateCurrentView();
        goToTheNextViewToBeTranslated();
    }

    protected void retrieveCurrentViewToBeTranslated() {
        final Activity A = getRequester();
        final int ID = getCurrentViewId();
        currentViewToBeTranslated = A.findViewById(ID);
    }

    protected int getCurrentViewId() {
        final List<String> L = getPageViewsTranslationKeys();
        final String TRANSLATION_KEY = L.get(currentPageViewPosition);
        final String KEY = "Page views ids";
        final Map<String, Object> M = settings.get(KEY);
        final Object O = M.get(TRANSLATION_KEY);
        return (Integer) O;
    }

    private void translateCurrentView() {
        try {
            translateView();
        } catch (final Exception E) {
            E.getMessage();
        }
    }

    private void translateView() {
        if (currentViewIsAnEditText()) {
            translateCurrentViewHint();
        } else {
            translateCurrentViewText();
        }
    }

    private boolean currentViewIsAnEditText() {
        return currentViewToBeTranslated instanceof EditText;
    }

    private void translateCurrentViewHint() {
        final EditText ET = (EditText) currentViewToBeTranslated;
        final CharSequence HINT = getTextForCurrentViewToBeTranslated();
        ET.setHint(HINT);
    }

    private CharSequence getTextForCurrentViewToBeTranslated() {
        try {
            return getTextForViewToBeTranslated();
        } catch (final Exception E) {
            return getTextForCurrentViewByDefault();
        }
    }

    private CharSequence getTextForViewToBeTranslated() throws Exception {
        final List<String> L = getPageViewsTranslationKeys();
        final String NAME = L.get(currentPageViewPosition);
        return chosenLanguage.getString(NAME);
    }

    private CharSequence getTextForCurrentViewByDefault() {
        final Map<String, Object> M = settings.get("Page views default texts");
        final List<String> L = getPageViewsTranslationKeys();
        final String KEY = L.get(currentPageViewPosition);
        final Object O = M.get(KEY);
        return (CharSequence) O;
    }

    private void translateCurrentViewText() {
        final TextView TV = (TextView) currentViewToBeTranslated;
        final CharSequence TEXT = getTextForCurrentViewToBeTranslated();
        TV.setText(TEXT);
    }

    private void goToTheNextViewToBeTranslated() {
        ++currentPageViewPosition;
    }

    protected Map<String, Map<String, Object>> getSettings() {
        return settings;
    }

    protected void setCurrentViewToBeTranslated(final View V) {
        currentViewToBeTranslated = V;
    }

    protected void setPageViewsIds(final Map<String, Object> M) {
        final String KEY = "Page views ids";
        settings.put(KEY, M);
    }

    protected void setPageViewsDefaultTexts(final Map<String, Object> M) {
        final String KEY = "Page views default texts";
        settings.put(KEY, M);
    }

    public JSONObject getChosenLanguage() {
        return chosenLanguage;
    }

    public void setChosenLanguage(final JSONObject O) {
        try {
            setLanguage(O);
        } catch (final Exception E) {
            E.getMessage();
        }
    }

    private void setLanguage(final JSONObject O) throws Exception {
        final String NAME = "Language content";
        chosenLanguage = O.getJSONObject(NAME);
    }
}