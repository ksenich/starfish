package com.googlion.shield.translators;

import java.util.*;

public class UserRecommendationsPageTranslator extends SharePageTranslator {
    public UserRecommendationsPageTranslator(final Map<String, Map<String, Object>> SETTINGS) {
        super(SETTINGS);
    }

    public CharSequence getRecommendationPriceHeader() {
        return getSharePriceHeader();
    }

    public CharSequence getRecommendationTimeInDaysHeader() {
        return getShareTimeInDaysHeader();
    }

    public CharSequence getRecommendationTimeInHoursHeader() {
        return getShareTimeInHoursHeader();
    }
}