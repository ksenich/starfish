package com.googlion.shield.translators;

import android.app.*;
import android.view.*;

import java.util.*;

public final class ListViewItemsTranslator extends PageTranslator {
    private View listViewItemToBeTranslated;

    public ListViewItemsTranslator(final Map<String, Map<String, Object>> SETTINGS) {
        super(SETTINGS);
        final Activity REQUESTER = getRequester();
        listViewItemToBeTranslated = new View(REQUESTER);
    }

    protected final void retrieveCurrentViewToBeTranslated() {
        final int ID = getCurrentViewId();
        final View V = listViewItemToBeTranslated.findViewById(ID);
        setCurrentViewToBeTranslated(V);
    }

    public final void setListViewItemToBeTranslated(final View V) {
        listViewItemToBeTranslated = V;
    }
}