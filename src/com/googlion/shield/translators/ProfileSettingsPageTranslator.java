package com.googlion.shield.translators;

import com.example.starfishapp.R.*;

import java.util.*;

public class ProfileSettingsPageTranslator extends TranslatorOfThePageWithActionBar {
    private Map<String, Object> pageHeadersViewsIds;
    private Map<String, Object> pageHeadersViewsDefaultTexts;
    private boolean applicationLanguageHasNotBeenChanged;

    public ProfileSettingsPageTranslator(final Map<String, Map<String, Object>> SETTINGS) {
        super(SETTINGS);
        pageHeadersViewsIds = SETTINGS.get("Page views ids");
        pageHeadersViewsDefaultTexts = SETTINGS.get("Page views default texts");
        applicationLanguageHasNotBeenChanged = true;
    }

    @Override
    public void translate() {
        translatePage();
        translatePageValuesViewIfUserHasNotAlreadyChangedApplicationLanguage();
    }

    private void translatePage() {
        super.translate();
    }

    private void translatePageValuesViewIfUserHasNotAlreadyChangedApplicationLanguage() {
        if (userHasNotAlreadyChangedApplicationLanguage()) {
            translatePageValuesViews();
        }
    }

    private boolean userHasNotAlreadyChangedApplicationLanguage() {
        return applicationLanguageHasNotBeenChanged;
    }

    private void translatePageValuesViews() {
        markThatUserHasAlreadyChangedApplicationLanguage();
        preparePageValuesViewsForTranslation();
        translatePage();
        moveFocusFromPageValuesViewsToPageHeadersOnes();
    }

    private void markThatUserHasAlreadyChangedApplicationLanguage() {
        applicationLanguageHasNotBeenChanged = false;
    }

    private void preparePageValuesViewsForTranslation() {
        final Map<String, Object> IDS = getPageValuesViewsIds();
        final Map<String, Object> DEFAULT_TEXTS = getPageValuesViewsDefaultTexts();
        setPageViewsIds(IDS);
        setPageViewsDefaultTexts(DEFAULT_TEXTS);
    }

    private Map<String, Object> getPageValuesViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvUserName", id.tvUserName);
        M.put("tvChosenLanguage", id.tvUserChosenLanguage);
        M.put("tvChosenCountry", id.tvUserChosenCountry);
        M.put("tvChosenCity", id.tvUserChosenCity);
        M.put("tvChosenFirstName", id.etUserFirstName);
        M.put("tvChosenLastName", id.etUserLastName);
        return M;
    }

    private Map<String, Object> getPageValuesViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvUserName", "Невідомо");
        M.put("tvChosenLanguage", "Невідомо");
        M.put("tvChosenCountry", "Невідомо");
        M.put("tvChosenCity", "Невідомо");
        M.put("tvChosenFirstName", "Невідомо");
        M.put("tvChosenLastName", "Невідомо");
        return M;
    }

    private void moveFocusFromPageValuesViewsToPageHeadersOnes() {
        setPageViewsIds(pageHeadersViewsIds);
        setPageViewsDefaultTexts(pageHeadersViewsDefaultTexts);
    }
}