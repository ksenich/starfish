package com.googlion.shield.translators;

import org.json.*;

import java.util.*;

public class UserCouponsPageTranslator extends TranslatorOfThePageWithActionBar {
    public UserCouponsPageTranslator(final Map<String, Map<String, Object>> SETTINGS) {
        super(SETTINGS);
    }

    public CharSequence getHeaderForTheActiveCouponsNumber() {
        try {
            return getChosenLanguageTextForTheActiveCoupons();
        } catch (final Exception E) {
            return getDefaultTextForTheActiveCoupons();
        }
    }

    private CharSequence getChosenLanguageTextForTheActiveCoupons() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "tvActiveUserCouponsNumberHeader";
        return O.getString(NAME);
    }

    private CharSequence getDefaultTextForTheActiveCoupons() {
        return "Дійсних талонів у Вас:";
    }

    public CharSequence getHeaderForTheUsedCouponsNumber() {
        try {
            return getChosenLanguageTextForTheUsedCoupons();
        } catch (final Exception E) {
            return getDefaultTextForTheUsedCoupons();
        }
    }

    private CharSequence getChosenLanguageTextForTheUsedCoupons() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "tvUsedUserCouponsNumberHeader";
        return O.getString(NAME);
    }

    private CharSequence getDefaultTextForTheUsedCoupons() {
        return "Використаних талонів у Вас:";
    }
}