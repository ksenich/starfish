package com.googlion.shield.translators;

import android.app.*;
import android.view.*;
import com.example.starfishapp.*;
import com.jeremyfeinstein.slidingmenu.lib.*;
import org.json.*;
import java.util.*;

public final class MainMenuTranslator extends PageTranslator {
    private View mainMenu;

    public MainMenuTranslator(final Map<String, Map<String, Object>> SETTINGS) {
        super(SETTINGS);
        final Activity A = getRequester();
        final MainActivity MA = (MainActivity) A;
        final SlidingMenu SM = MA.getSlidingMenu();
        mainMenu = SM.getMenu();
    }

    protected final void retrieveCurrentViewToBeTranslated() {
        final int ID = getCurrentViewId();
        final View V = mainMenu.findViewById(ID);
        setCurrentViewToBeTranslated(V);
    }

    public final CharSequence getUserCountryHeader() {
        try {
            return getHeaderForUserCountry();
        } catch (final Exception E) {
            return getDefaultHeaderForUserCountry();
        }
    }

    private CharSequence getHeaderForUserCountry() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "tvMainMenuUserCountryHeader";
        return O.getString(NAME);
    }

    private CharSequence getDefaultHeaderForUserCountry() {
        return "Країна:";
    }

    public final CharSequence getUserCityHeader() {
        try {
            return getHeaderForUserCity();
        } catch (final Exception E) {
            return getDefaultHeaderForUserCity();
        }
    }

    private CharSequence getHeaderForUserCity() throws Exception {
        final JSONObject O = getChosenLanguage();
        final String NAME = "tvMainMenuUserTownHeader";
        return O.getString(NAME);
    }

    private CharSequence getDefaultHeaderForUserCity() {
        return "Місто:";
    }
}