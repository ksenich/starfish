package com.googlion.shield.listeners;

import android.text.*;
import android.text.style.*;
import android.view.*;
import android.widget.*;
import android.widget.SeekBar.*;
import com.example.starfishapp.*;
import com.example.starfishapp.R.*;
import com.googlion.shield.translators.*;

import static android.graphics.Color.*;
import static android.text.Spanned.*;

public class OnMainPageSeekBarChangeListener implements OnSeekBarChangeListener {
    private MainActivity requester;
    private double progress;

    public OnMainPageSeekBarChangeListener(final MainActivity REQUESTER) {
        requester = REQUESTER;
        progress = 0;
    }

    @Override
    public void onProgressChanged(final SeekBar SB, final int PROGRESS, final boolean FROM_USER) {
        showRouteRadiusRetrievedFrom(SB);
    }

    private void showRouteRadiusRetrievedFrom(final SeekBar SB) {
        retrieveProgressFrom(SB);
        scaleProgressToRouteRadius();
        showRouteRadius();
    }

    private void retrieveProgressFrom(final SeekBar SB) {
        progress = SB.getProgress();
    }

    private void scaleProgressToRouteRadius() {
        if (radiusIsLessThanOneKilometer()) {
            scaleProgressToRadiusThatIsLessThanOneKilometer();
        } else {
            scaleProgressToRadiusThatIsNotLessThanOneKilometer();
        }
    }

    private boolean radiusIsLessThanOneKilometer() {
        return progress < 2000;
    }

    private void scaleProgressToRadiusThatIsLessThanOneKilometer() {
        progress /= 2;
    }

    private void scaleProgressToRadiusThatIsNotLessThanOneKilometer() {
        progress -= 1000;
    }

    public void showRouteRadius() {
        final SpannableStringBuilder VALUE = getValueForRouteRadius();
        final ForegroundColorSpan WHAT = getForegroundColorSpanForRouteRadiusValue();
        final Integer START = 0;
        final Integer END = VALUE.length();
        final MainPageTranslator MPT = requester.getPageTranslator();
        final CharSequence HEADER = MPT.getRouteRadiusHeaderOne();
        final SpannableStringBuilder TEXT = new SpannableStringBuilder(HEADER);
        final View V = requester.findViewById(id.tvRouteRadius);
        final TextView TV = (TextView) V;
        VALUE.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
        TEXT.append(VALUE);
        TV.setText(TEXT);
    }

    private SpannableStringBuilder getValueForRouteRadius() {
        final SpannableStringBuilder TEXT = new SpannableStringBuilder();
        final MainPageTranslator MPT = requester.getPageTranslator();
        final CharSequence HEADER = MPT.getRouteRadiusHeaderTwo();
        final String RADIUS = progress + "";
        TEXT.append(RADIUS);
        TEXT.append(" ");
        TEXT.append(HEADER);
        return TEXT;
    }

    private ForegroundColorSpan getForegroundColorSpanForRouteRadiusValue() {
        final Integer RED = 250;
        final Integer GREEN = 63;
        final Integer BLUE = 63;
        final int COLOR = rgb(RED, GREEN, BLUE);
        return new ForegroundColorSpan(COLOR);
    }

    @Override
    public void onStartTrackingTouch(final SeekBar SB) {
    }

    @Override
    public void onStopTrackingTouch(final SeekBar SB) {
        showRouteRadiusRetrievedFrom(SB);
        setRadiusForTheRoute();
    }

    private void setRadiusForTheRoute() {
        final double RADIUS = getRadiusForTheRoute();
        requester.setRadius(RADIUS);
    }

    private double getRadiusForTheRoute() {
        return progress <= 0 ? progress + 0.1 : progress;
    }
}