package com.googlion.shield.listeners;

import android.support.v7.widget.SearchView.*;
import com.example.starfishapp.pages.*;

public class OnOffersSearchPageQueryTextListener implements OnQueryTextListener {

    private OffersSearchActivity requester;

    public OnOffersSearchPageQueryTextListener(final OffersSearchActivity REQUESTER) {
        requester = REQUESTER;
    }

    @Override
    public boolean onQueryTextSubmit(final String S) {
        requester.updateOffers(S);
        return false;
    }

    @Override
    public boolean onQueryTextChange(final String S) {
        return false;
    }
}
