package com.googlion.shield.listeners;

import static com.example.starfishapp.ShareActivity.*;

import android.app.*;
import android.content.*;
import android.view.*;
import android.widget.*;
import android.widget.AdapterView.*;
import com.example.starfishapp.*;
import com.example.starfishapp.model.*;

import java.util.*;

public abstract class OnGooglionSharesListItemClickListener implements OnItemClickListener {
    private Activity requester;
    private List<Offer> downloadedSharesList;

    public OnGooglionSharesListItemClickListener(final Activity REQUESTER) {
        requester = REQUESTER;
        downloadedSharesList = getDownloadedSharesListFromRequester();
    }

    protected abstract List<Offer> getDownloadedSharesListFromRequester();

    @Override
    public void onItemClick(final AdapterView<?> PARENT, final View V, final int POSITION, final long ID) {
        final Offer CLICKED_OFFER = downloadedSharesList.get(POSITION);
        final int CLICKED_OFFER_ID = CLICKED_OFFER.id;
        final Context CT = requester;
        final Class<?> C = ShareActivity.class;
        final Intent I = new Intent(CT, C);
        I.putExtra(OFFER_KEY, CLICKED_OFFER_ID);
        CT.startActivity(I);
    }

    protected Activity getRequester() {
        return requester;
    }
}