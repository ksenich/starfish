package com.googlion.shield.listeners;

import android.view.*;
import android.widget.SlidingDrawer.*;
import com.example.starfishapp.MainActivity;
import com.example.starfishapp.R.*;

import static android.view.View.*;

public class OnGooglionDrawerCloseListener implements OnDrawerCloseListener {

    private MainActivity requester;
    private View goToTheSeeShareListPage;
    private View goToTheSeeOnTheMapPage;

    public OnGooglionDrawerCloseListener(final MainActivity REQUESTER) {
        requester = REQUESTER;
        goToTheSeeShareListPage = REQUESTER.findViewById(id.llWatchSharesList);
        goToTheSeeOnTheMapPage = REQUESTER.findViewById(id.llGoToTheMap);
    }

    @Override
    public void onDrawerClosed() {
        goToTheSeeShareListPage.setVisibility(VISIBLE);
        goToTheSeeOnTheMapPage.setVisibility(GONE);
        requester.loadOffersOnTheMap();
    }
}
