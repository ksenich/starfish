package com.googlion.shield.listeners;

import static com.example.starfishapp.MapManager.*;

import android.content.*;
import android.view.*;
import android.widget.AdapterView.*;
import android.widget.*;
import com.example.starfishapp.*;

import java.util.*;

public class OnUserRecommendationsListItemClickListener implements OnItemClickListener {
    private RecommendationsActivity requester;

    public OnUserRecommendationsListItemClickListener(final RecommendationsActivity REQUESTER) {
        requester = REQUESTER;
    }

    @Override
    public void onItemClick(final AdapterView<?> PARENT, final View V, final int POSITION, final long ID) {
        final Context C = requester;
        final List<Map<String, Object>> L = requester.getListViewItemsData();
        final Map<String, Object> M = L.get(POSITION);
        final String KEY = "Recommendation id";
        final Object O = M.get(KEY);
        final String S = (String) O;
        final int OFFER_ID = Integer.parseInt(S);
        goToOfferPage(C, OFFER_ID);
    }
}