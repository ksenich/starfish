package com.googlion.shield.listeners;

import android.content.*;
import android.view.*;
import android.widget.*;
import com.example.starfishapp.model.*;
import android.widget.AdapterView.*;
import com.example.starfishapp.pages.*;

import java.util.*;

import static com.example.starfishapp.pages.PointActivity.*;

public class OnGooglionVenuesListItemClickListener implements OnItemClickListener {

    private List<Supplier> downloadedVenuesList;
    private VenuesListActivity requester;

    public OnGooglionVenuesListItemClickListener(final VenuesListActivity REQUESTER) {
        requester = REQUESTER;
        downloadedVenuesList = requester.getDownloadedVenuesList();
    }

    @Override
    public void onItemClick(final AdapterView<?> PARENT, final View V, final int POSITION, final long ID) {
        final Supplier CLICKED_VENUE = downloadedVenuesList.get(POSITION);
        final Context CT = requester;
        final Class<?> C = PointActivity.class;
        final Intent I = new Intent(CT, C);
        final String CLICKED_VENUE_NAME = CLICKED_VENUE.getName();
        final int CLICKED_VENUE_ID = CLICKED_VENUE.getId();
        I.putExtra("Action bar title", CLICKED_VENUE_NAME);
        I.putExtra(SUPPLIER_ID_KEY, CLICKED_VENUE_ID);
        CT.startActivity(I);
    }
}