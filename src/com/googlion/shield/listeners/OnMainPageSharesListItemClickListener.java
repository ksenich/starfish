package com.googlion.shield.listeners;

import android.app.*;
import com.example.starfishapp.*;
import com.example.starfishapp.model.*;

import java.util.*;

public class OnMainPageSharesListItemClickListener extends OnGooglionSharesListItemClickListener {

    public OnMainPageSharesListItemClickListener(final Activity REQUESTER) {
        super(REQUESTER);
    }

    @Override
    protected List<Offer> getDownloadedSharesListFromRequester() {
        final MainActivity MA = (MainActivity) getRequester();
        final OnGooglionDrawerOpenListener OGDOL = MA.getOpenListener();
        return OGDOL.getDownloadedSharesList();
    }
}