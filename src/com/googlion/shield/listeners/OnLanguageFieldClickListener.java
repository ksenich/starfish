package com.googlion.shield.listeners;

import static android.content.Context.*;

import android.app.*;
import android.app.AlertDialog.*;
import android.content.*;
import android.content.res.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import com.example.starfishapp.*;
import com.example.starfishapp.R.*;
import com.googlion.shield.adapters.*;
import com.googlion.shield.utilities.*;
import org.json.*;

import java.io.*;

public class OnLanguageFieldClickListener implements OnClickListener {
    private Activity requester;
    private int chosenLanguagePosition;

    public OnLanguageFieldClickListener(final Activity REQUESTER) {
        requester = REQUESTER;
        chosenLanguagePosition = getChosenLanguagePositionFromSharedPreferences();
    }

    private int getChosenLanguagePositionFromSharedPreferences() {
        final String NAME = "Chosen language";
        final int MODE = MODE_PRIVATE;
        final SharedPreferences SP = requester.getSharedPreferences(NAME, MODE);
        final String KEY = "Chosen language position";
        final int DEFAULT_VALUE = 0;
        return SP.getInt(KEY, DEFAULT_VALUE);
    }

    @Override
    public void onClick(final View V) {
        final AlertDialog AD = getAlertDialog();
        AD.show();
    }

    private AlertDialog getAlertDialog() {
        final Context C = requester;
        final Builder B = new Builder(C);
        final int CHECKED_ITEM = chosenLanguagePosition;
        final OnLanguageFieldClickListener REQUESTER = OnLanguageFieldClickListener.this;
        final DialogInterface.OnClickListener L = new OnLanguageFieldAlertDialogClickListener(REQUESTER);
        final ListAdapter ADAPTER = getListAdapterForAlertDialog();
        B.setSingleChoiceItems(ADAPTER, CHECKED_ITEM, L);
        return B.create();
    }

    private ListAdapter getListAdapterForAlertDialog() {
        final Context C = requester;
        final AccountSettingsActivity ASA = (AccountSettingsActivity) requester;
        final int LAYOUT_RESOURCE_ID = layout.googlion_select_dialog_item;
        final CharSequence[] ITEMS = getAvailableLanguagesNamesFromJSONArray();
        final PageScaler PS = ASA.getPageScaler();
        return new ShieldArrayAdapter<CharSequence>(C, LAYOUT_RESOURCE_ID, ITEMS, PS);
    }

    private CharSequence[] getAvailableLanguagesNamesFromJSONArray() {
        try {
            return getAvailableNames();
        } catch (final Exception E) {
            return new CharSequence[0];
        }
    }

    private CharSequence[] getAvailableNames() throws Exception {
        final JSONArray A = getAvailableLanguages();
        final int SIZE = A.length();
        final CharSequence[] CS = new CharSequence[SIZE];
        for (int i = 0; i < SIZE; ++i) {
            CS[i] = getAvailableLanguageNameFrom(i);
        }
        return CS;
    }

    public JSONArray getAvailableLanguages() {
        try {
            return getAvailableLanguagesFromResources();
        } catch (final Exception E) {
            return new JSONArray();
        }
    }

    private JSONArray getAvailableLanguagesFromResources() throws Exception {
        final String DATA = getJSONDataFromResources();
        final JSONObject O = new JSONObject(DATA);
        return O.getJSONArray("Languages");
    }

    private String getJSONDataFromResources() throws Exception {
        final Resources RESOURCES = requester.getResources();
        final InputStream IS = RESOURCES.openRawResource(raw.languages);
        final int SIZE = IS.available();
        final byte[] BUFFER = new byte[SIZE];
        final int I = IS.read(BUFFER);
        if (I == -1) {
            BUFFER[0] += 0;
        }
        IS.close();
        return new String(BUFFER);
    }

    private CharSequence getAvailableLanguageNameFrom(final int POSITION) throws Exception {
        final JSONArray A = getAvailableLanguages();
        final JSONObject O = A.getJSONObject(POSITION);
        final String NAME = "Language name";
        return O.getString(NAME);
    }

    public Activity getRequester() {
        return requester;
    }

    public void setChosenLanguagePosition(final int I) {
        chosenLanguagePosition = I;
    }
}