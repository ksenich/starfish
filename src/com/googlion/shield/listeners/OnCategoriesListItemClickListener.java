package com.googlion.shield.listeners;

import android.content.*;
import android.view.*;
import android.widget.*;
import android.widget.AdapterView.*;
import com.example.starfishapp.model.*;
import com.example.starfishapp.pages.*;

import java.util.*;

public class OnCategoriesListItemClickListener implements OnItemClickListener {
    private VenuesActivity requester;

    public OnCategoriesListItemClickListener(final VenuesActivity REQUESTER) {
        requester = REQUESTER;
    }

    @Override
    public void onItemClick(final AdapterView<?> PARENT, final View V, final int POSITION, final long ID) {
        final Context CT = requester;
        final List<Category> L = requester.getDownloadedCategoriesList();
        final Class<?> CLASS = VenuesListActivity.class;
        final Intent I = new Intent(CT, CLASS);
        final Category C = L.get(POSITION);
        I.putExtra("category_id", C.id);
        I.putExtra("Action bar title", C.name);
        requester.startActivity(I);
    }
}