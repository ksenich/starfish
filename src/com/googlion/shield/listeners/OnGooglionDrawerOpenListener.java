package com.googlion.shield.listeners;

import static android.text.Spanned.*;
import static android.view.View.*;

import android.os.*;
import android.text.*;
import android.text.style.*;
import android.view.*;
import android.widget.*;
import android.widget.SlidingDrawer.*;
import com.example.starfishapp.*;
import com.example.starfishapp.R.*;
import com.example.starfishapp.model.*;
import com.googlion.shield.loaders.*;
import com.googlion.shield.specialists.*;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;
import android.widget.AdapterView.*;
import android.view.View.*;

import java.util.*;

public class OnGooglionDrawerOpenListener implements OnDrawerOpenListener, OnClickListener {
    private Bundle settings;
    private boolean sharesListIsOpenedForTheFirstTime;
    private List<Map<String, Object>> sharesList;
    private List<Offer> downloadedSharesList;
    private int pagesQuantity;

    public OnGooglionDrawerOpenListener(final Bundle SETTINGS) {
        settings = SETTINGS;
        sharesListIsOpenedForTheFirstTime = true;
        sharesList = new LinkedList<Map<String, Object>>();
        downloadedSharesList = new LinkedList<Offer>();
        pagesQuantity = 1;
    }

    public void translateFooterView() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForTranslation();
        final Translator T = new PageTranslator(SETTINGS);
        T.translate();
    }

    private Map<String, Map<String, Object>> getSettingsForTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getPageViewsIdsForTranslation());
        M.put("Page views default texts", getPageViewsDefaultTextsForTranslation());
        M.put("Other settings", getOtherSettingsForTranslation());
        return M;
    }

    private Map<String, Object> getPageViewsIdsForTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("bGetMoreShares", id.bGetMoreShares);
        return M;
    }

    private Map<String, Object> getPageViewsDefaultTextsForTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("bGetMoreShares", "Ще");
        return M;
    }

    private Map<String, Object> getOtherSettingsForTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", getRequesterFromSettings());
        return M;
    }

    private MainActivity getRequesterFromSettings() {
        final String KEY = "Requester";
        return settings.getParcelable(KEY);
    }

    @Override
    public void onDrawerOpened() {
        hideGoToTheSeeSharesListPageButton();
        showGoToTheSeeOnTheMapPageButton();
        setUpSharesListUnderTheMapIfThisListIsOpenedForTheFirstTime();
        updateSharesListUnderTheMap();
    }

    private void hideGoToTheSeeSharesListPageButton() {
        final MainActivity MA = getRequesterFromSettings();
        final View V = MA.findViewById(id.llWatchSharesList);
        V.setVisibility(GONE);
    }

    private void showGoToTheSeeOnTheMapPageButton() {
        final MainActivity MA = getRequesterFromSettings();
        final View V = MA.findViewById(id.llGoToTheMap);
        V.setVisibility(VISIBLE);
    }

    private void setUpSharesListUnderTheMapIfThisListIsOpenedForTheFirstTime() {
        if (userHaveOpenedListUnderTheMapForTheFirstTime()) {
            setUpSharesListUnderTheMap();
        }
    }

    private boolean userHaveOpenedListUnderTheMapForTheFirstTime() {
        return sharesListIsOpenedForTheFirstTime;
    }

    private void setUpSharesListUnderTheMap() {
        scaleSharesListDividerHeight();
        markSharesListUnderTheMapAsVisited();
    }

    private void scaleSharesListDividerHeight() {
        final ListView LV = getListViewOfTheMainPage();
        final PageScaler PS = getPageScalerFromSettings();
        PS.scaleDividerHeightOf(LV);
    }

    private ListView getListViewOfTheMainPage() {
        final MainActivity MA = getRequesterFromSettings();
        final View V = MA.findViewById(id.lvShares);
        return (ListView) V;
    }

    private PageScaler getPageScalerFromSettings() {
        final String KEY = "Page scaler";
        return settings.getParcelable(KEY);
    }

    private void markSharesListUnderTheMapAsVisited() {
        sharesListIsOpenedForTheFirstTime = false;
    }

    public void updateSharesListUnderTheMap() {
        resetPagesQuantity();
        clearSharesList();
        clearDownloadedSharesList();
        loadShares();
    }

    private void resetPagesQuantity() {
        pagesQuantity = 1;
    }

    public void clearSharesList() {
        sharesList.clear();
    }

    private void clearDownloadedSharesList() {
        downloadedSharesList.clear();
    }

    private void loadShares() {
        final MainActivity MA = getRequesterFromSettings();
        final OffersLoader OL = new MainPageOffersLoader(MA);
        OL.loadOffers();
    }

    public void apply(final List<Offer> L) {
        retrieveDownloadedSharesListFrom(L);
        displayShares();
        setOnItemClickListenerForSharesList();
        leaveUserOnTheLastItemBeforeMoreButtonIsPressed();
    }

    private void retrieveDownloadedSharesListFrom(final List<Offer> L) {
        downloadedSharesList.addAll(L);
    }

    public void displayShares() {
        final Map<String, List<Map<String, Object>>> SETTINGS = getSettingsForListDisplaying();
        final ListViewSpecialist LVS = new ListViewSpecialist(SETTINGS);
        LVS.setUpListView();
    }

    private Map<String, List<Map<String, Object>>> getSettingsForListDisplaying() {
        final Map<String, List<Map<String, Object>>> M = new HashMap<String, List<Map<String, Object>>>();
        M.put("List view items data", getListViewItemsData());
        M.put("List view item fields to be filled", getListViewItemFieldsToBeFilled());
        M.put("Other settings", getOtherSettingsForListDisplaying());
        return M;
    }

    private List<Map<String, Object>> getListViewItemsData() {
        addSharesToList();
        return sharesList;
    }

    private void addSharesToList() {
        for (final Offer O : downloadedSharesList) {
            addShareToList(O);
        }
    }

    private void addShareToList(final Offer O) {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Share title", O.name);
        M.put("Share description", O.description);
        M.put("Share price", getSharePriceFrom(O));
        M.put("Share image url", O.img_url);
        M.put("Share discount", getShareDiscountFrom(O));
        M.put("Share discount price", getShareDiscountPriceFrom(O));
        sharesList.add(M);
    }

    private Object getSharePriceFrom(final Offer O) {
        final CharSequence VALUE = getValueOfSharePriceFrom(O);
        final SpannableStringBuilder TEXT = new SpannableStringBuilder(VALUE);
        final Map<String, Map<String, Object>> S = getSettingsForPageTranslation();
        final MainPageTranslator T = new MainPageTranslator(S);
        final CharSequence HEADER = T.getSharePriceHeader();
        TEXT.append(' ');
        TEXT.append(HEADER);
        return TEXT;
    }

    private CharSequence getValueOfSharePriceFrom(final Offer O) {
        final RelativeSizeSpan WHAT = getRelativeSizeSpanForValue();
        final Integer START = 0;
        final CharSequence PRICE = O.price + "";
        final SpannableStringBuilder SSB = new SpannableStringBuilder(PRICE);
        final Integer END = SSB.length();
        SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
        return SSB;
    }

    private RelativeSizeSpan getRelativeSizeSpanForValue() {
        final Float VALUE_SIZE_FOR_MDPI = 40f;
        final Float HEADER_SIZE_FOR_MDPI = 26.67f;
        final Float PROPORTION = VALUE_SIZE_FOR_MDPI / HEADER_SIZE_FOR_MDPI;
        return new RelativeSizeSpan(PROPORTION);
    }

    private Object getShareDiscountFrom(final Offer O) {
        final CharSequence VALUE = getValueOfShareDiscountFrom(O);
        final SpannableStringBuilder TEXT = new SpannableStringBuilder(VALUE);
        final CharSequence HEADER = "%";
        TEXT.append(' ');
        TEXT.append(HEADER);
        return TEXT;
    }

    private CharSequence getValueOfShareDiscountFrom(final Offer O) {
        final RelativeSizeSpan WHAT = getRelativeSizeSpanForValue();
        final Integer START = 0;
        final CharSequence PRICE = O.discount + "";
        final SpannableStringBuilder SSB = new SpannableStringBuilder(PRICE);
        final Integer END = SSB.length();
        SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
        return SSB;
    }

    private Object getShareDiscountPriceFrom(final Offer CURRENT_OFFER) {
        final CharSequence VALUE = getValueOfShareDiscountPriceFrom(CURRENT_OFFER);
        final SpannableStringBuilder TEXT = new SpannableStringBuilder(VALUE);
        final Map<String, Map<String, Object>> S = getSettingsForPageTranslation();
        final MainPageTranslator T = new MainPageTranslator(S);
        final CharSequence HEADER = T.getShareDiscountPriceHeader();
        TEXT.append(' ');
        TEXT.append(HEADER);
        return TEXT;
    }

    private CharSequence getValueOfShareDiscountPriceFrom(final Offer CURRENT_OFFER) {
        final RelativeSizeSpan WHAT = getRelativeSizeSpanForValue();
        final Integer START = 0;
        final CharSequence PRICE = CURRENT_OFFER.priceCoupon + "";
        final SpannableStringBuilder SSB = new SpannableStringBuilder(PRICE);
        final Integer END = SSB.length();
        SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
        return SSB;
    }

    private Map<String, Map<String, Object>> getSettingsForPageTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getIdsOfPageViews());
        M.put("Page views default texts", getDefaultTextsOfPageViews());
        M.put("Other settings", getOtherSettingsForTranslation());
        return M;
    }

    private Map<String, Object> getIdsOfPageViews() {
        return new HashMap<String, Object>();
    }

    private Map<String, Object> getDefaultTextsOfPageViews() {
        return new HashMap<String, Object>();
    }

    private List<Map<String, Object>> getListViewItemFieldsToBeFilled() {
        final List<Map<String, Object>> L = new LinkedList<Map<String, Object>>();
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Share title", id.tvOfferTitle);
        M.put("Share description", id.tvOfferDescription);
        M.put("Share price", id.tvOfferPrice);
        M.put("Share image url", id.sivOfferImage);
        M.put("Share discount", id.tvOfferDiscount);
        M.put("Share discount price", id.tvOfferDiscountPrice);
        L.add(M);
        return L;
    }

    private List<Map<String, Object>> getOtherSettingsForListDisplaying() {
        final List<Map<String, Object>> L = new LinkedList<Map<String, Object>>();
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", getRequesterFromSettings());
        M.put("List view id", id.lvShares);
        M.put("Layout resource id", layout.see_shares_list_page_list_item);
        M.put("Page scaler", getPageScalerFromSettings());
        M.put("List view items translator", getTranslatorForListViewItems());
        M.put("Font specialist for list view", getFontSpecialistForProcessingListView());
        L.add(M);
        return L;
    }

    private Object getTranslatorForListViewItems() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForListItemTranslation();
        return new ListViewItemsTranslator(SETTINGS);
    }

    private Map<String, Map<String, Object>> getSettingsForListItemTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getListItemViewsIds());
        M.put("Page views default texts", getListItemViewsDefaultTexts());
        M.put("Other settings", getOtherSettingsForTranslation());
        return M;
    }

    private Map<String, Object> getListItemViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvOfferTitle", id.tvOfferTitle);
        return M;
    }

    private Map<String, Object> getListItemViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvOfferTitle", "Заголовок відсутній.");
        return M;
    }

    private FontSpecialistForListView getFontSpecialistForProcessingListView() {
        final Map<Object, Map> S = getSettingsForSettingFontsInListView();
        return new FontSpecialistForListView(S);
    }

    private Map<Object, Map> getSettingsForSettingFontsInListView() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingFontsInListView());
        M.put("Other settings", getOtherSettingsForSettingFontsInListView());
        return M;
    }

    private Map getMainSettingsForSettingFontsInListView() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvOfferTitle, "kelsonsansregularru.otf");
        M.put(id.tvOfferPrice, "kelsonsansboldru.otf");
        M.put(id.tvOfferDiscount, "kelsonsansboldru.otf");
        M.put(id.tvOfferDiscountPrice, "kelsonsansboldru.otf");
        return M;
    }

    private Map getOtherSettingsForSettingFontsInListView() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", getRequesterFromSettings());
        return M;
    }

    private void setOnItemClickListenerForSharesList() {
        final MainActivity MA = getRequesterFromSettings();
        final OnItemClickListener LISTENER = new OnMainPageSharesListItemClickListener(MA);
        final ListView LV = getListViewOfTheMainPage();
        LV.setOnItemClickListener(LISTENER);
    }

    private void leaveUserOnTheLastItemBeforeMoreButtonIsPressed() {
        final int ITEMS_PER_PAGE = 10;
        final int CURRENT_PAGE = pagesQuantity - 1;
        final int LAST_ITEM_POSITION = ITEMS_PER_PAGE * CURRENT_PAGE - 1;
        final ListView LV = getListViewOfTheMainPage();
        LV.setSelection(LAST_ITEM_POSITION);
    }

    @Override
    public void onClick(final View V) {
        increasePagesQuantity();
        clearSharesList();
        loadShares();
    }

    private void increasePagesQuantity() {
        ++pagesQuantity;
    }

    public List<Offer> getDownloadedSharesList() {
        return downloadedSharesList;
    }

    public int getPagesQuantity() {
        return pagesQuantity;
    }
}