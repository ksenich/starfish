package com.googlion.shield.listeners;

import android.app.*;
import android.content.*;
import android.content.DialogInterface.*;
import android.content.res.*;
import android.view.*;
import android.widget.*;
import com.example.starfishapp.*;
import com.googlion.shield.translators.*;
import org.json.*;
import com.example.starfishapp.R.*;
import java.io.*;

public class OnLanguageFieldAlertDialogClickListener implements OnClickListener {
    private Activity primaryRequester;
    private OnLanguageFieldClickListener secondaryRequester;
    private JSONArray availableLanguages;
    private DialogInterface dialog;
    private int which;

    public OnLanguageFieldAlertDialogClickListener(final OnLanguageFieldClickListener OLFCL) {
        primaryRequester = OLFCL.getRequester();
        secondaryRequester = OLFCL;
        availableLanguages = secondaryRequester.getAvailableLanguages();
        dialog = new Dialog(primaryRequester);
        which = -1;
    }

    @Override
    public void onClick(final DialogInterface DIALOG, final int WHICH) {
        retrieveDialogFrom(DIALOG);
        retrieveWhichFrom(WHICH);
        performClickOnAlertDialogListItem();
    }

    private void retrieveDialogFrom(final DialogInterface DI) {
        dialog = DI;
    }

    private void retrieveWhichFrom(final int I) {
        which = I;
    }

    private void performClickOnAlertDialogListItem() {
        try {
            performClick();
        } catch (final Exception E) {
            E.getMessage();
        }
    }

    private void performClick() throws Exception {
        final AccountSettingsActivity ASA = (AccountSettingsActivity) primaryRequester;
        final View V = ASA.findViewById(id.tvUserChosenLanguage);
        final TextView TV = (TextView) V;
        final JSONObject O = availableLanguages.getJSONObject(which);
        final String NAME = "Language name";
        final String TEXT = O.getString(NAME);
        final String DATA = getChosenLanguageContentFromResourcesAnd(O);
        final JSONObject OBJECT = new JSONObject(DATA);
        final ProfileSettingsPageTranslator PSPT = ASA.getPageTranslator();
        PSPT.setChosenLanguage(OBJECT);
        PSPT.translate();
        TV.setText(TEXT);
        ASA.setChosenLanguagePosition(which);
        dialog.dismiss();
        secondaryRequester.setChosenLanguagePosition(which);
    }

    private String getChosenLanguageContentFromResourcesAnd(final JSONObject O) throws Exception {
        final Resources RESOURCES = primaryRequester.getResources();
        final String LANGUAGE_NAME_IN_ENGLISH_HEADER = "Language name in english";
        final String LANGUAGE_NAME_IN_ENGLISH = O.getString(LANGUAGE_NAME_IN_ENGLISH_HEADER);
        final String DEF_TYPE = "raw";
        final String DEF_PACKAGE = "com.example.starfishapp";
        final int ID = RESOURCES.getIdentifier(LANGUAGE_NAME_IN_ENGLISH, DEF_TYPE, DEF_PACKAGE);
        final InputStream IS = RESOURCES.openRawResource(ID);
        final int SIZE = IS.available();
        final byte[] BUFFER = new byte[SIZE];
        final int I = IS.read(BUFFER);
        final StringBuilder SB = new StringBuilder();
        final int START = 0;
        final int END = SB.length();
        final String STRING = new String(BUFFER);
        SB.append(I);
        SB.replace(START, END, STRING);
        IS.close();
        return SB.toString();
    }
}