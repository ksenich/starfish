package com.googlion.shield.listeners;

import static android.view.View.*;

import android.support.v4.app.*;
import android.support.v4.widget.*;
import android.view.*;
import com.example.starfishapp.*;
import com.jeremyfeinstein.slidingmenu.lib.*;
import com.example.starfishapp.R.*;

public final class GooglionDrawerListener extends ActionBarDrawerToggle {
    private SlidingMenu slidingMenu;

    public GooglionDrawerListener(final MainActivity REQUESTER, final DrawerLayout DL) {
        super(REQUESTER, DL, drawable.drawer, string.drawer_open, string.drawer_close);
        slidingMenu = REQUESTER.getSlidingMenu();
    }

    public final void onDrawerOpened(final View V) {
        slidingMenu.showMenu();
        V.setVisibility(GONE);
    }

    public final void onDrawerClosed(final View V) {
        slidingMenu.showMenu();
    }
}