package com.googlion.shield.listeners;

import android.app.*;
import com.example.starfishapp.model.*;
import com.example.starfishapp.pages.*;

import java.util.*;

public class OnOffersSearchPageOffersListItemClickListener extends OnGooglionSharesListItemClickListener {
    public OnOffersSearchPageOffersListItemClickListener(final Activity REQUESTER) {
        super(REQUESTER);
    }

    @Override
    protected List<Offer> getDownloadedSharesListFromRequester() {
        final OffersSearchActivity OSA = (OffersSearchActivity) getRequester();
        return OSA.getDownloadedOffersList();
    }
}