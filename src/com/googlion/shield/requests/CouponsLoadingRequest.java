package com.googlion.shield.requests;

import static com.example.starfishapp.Starfish.*;
import static com.example.starfishapp.Starfish.METHOD.*;

import org.json.*;

import java.util.*;

public class CouponsLoadingRequest implements Request<Map<String, Object>> {
    private String couponsCategoryName;

    public CouponsLoadingRequest(final String COUPONS_CATEGORY_NAME) {
        couponsCategoryName = COUPONS_CATEGORY_NAME;
    }

    @Override
    public METHOD getMethod() {
        return GET;
    }

    @Override
    public String getAction() {
        return "get_user_coupons";
    }

    @Override
    public RequestOptions getOptions() {
        return new RequestOptions();
    }

    @Override
    public String getName() {
        return couponsCategoryName;
    }

    @Override
    public Map<String, Object> parseOne(final JSONObject JO) throws JSONException {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Id", JO.getString("id"));
        M.put("Coupon id", JO.getString("offer_id"));
        M.put("Coupon image", JO.getString("url_img"));
        M.put("Coupon title", JO.getString("name"));
        M.put("Coupon description", JO.getString("description"));
        M.put("Coupon code", JO.getString("coupon"));
        return M;
    }

    @Override
    public void onIntExtra(final String NAME, final int VALUE) {
    }
}