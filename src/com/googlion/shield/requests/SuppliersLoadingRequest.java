package com.googlion.shield.requests;

import static com.example.starfishapp.Starfish.*;
import static com.example.starfishapp.Starfish.METHOD.*;

import org.json.*;
import com.example.starfishapp.model.*;


public class SuppliersLoadingRequest implements Request<Supplier> {
    private int categoryId;

    public SuppliersLoadingRequest(final int CATEGORY_ID) {
        categoryId = CATEGORY_ID;
    }

    @Override
    public METHOD getMethod() {
        return GET;
    }

    @Override
    public String getAction() {
        return "get_suppliers";
    }

    @Override
    public RequestOptions getOptions() {
        final String[] S = new String[1];
        S[0] = "category_id=" + categoryId;
        return new RequestOptions(S);
    }

    @Override
    public String getName() {
        return "suppliers";
    }

    @Override
    public Supplier parseOne(final JSONObject O) throws JSONException {
        return new Supplier(O);
    }

    @Override
    public void onIntExtra(final String NAME, final int VALUE) {
    }
}