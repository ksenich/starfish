package com.googlion.shield.loaders;

import android.app.*;
import com.example.starfishapp.model.*;
import com.googlion.shield.pages.*;

public class UserBillPageUserInformationLoader extends UserInformationLoader {
    public UserBillPageUserInformationLoader(Activity REQUESTER) {
        super(REQUESTER);
    }

    @Override
    protected void onPostExecute(final UserInfo UI) {
        final Activity A = getRequester();
        final UserBillPage UBP = (UserBillPage) A;
        UBP.apply(UI);
    }
}
