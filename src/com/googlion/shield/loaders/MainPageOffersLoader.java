package com.googlion.shield.loaders;

import static com.example.starfishapp.R.*;

import android.app.*;
import android.view.*;
import android.widget.*;
import com.example.starfishapp.*;
import com.example.starfishapp.model.*;
import com.googlion.shield.listeners.*;
import com.googlion.shield.utilities.*;

import java.util.*;

public class MainPageOffersLoader extends OffersLoader {
    public MainPageOffersLoader(final Activity REQUESTER) {
        super(REQUESTER);
    }

    @Override
    protected int getSelectedRegionId() {
        final CategorySelector CS = getCategorySelector();
        final Region REGION = CS.getSelectedRegion();
        return REGION.id;
    }

    private CategorySelector getCategorySelector() {
        final MainActivity MA = getRequester();
        return MA.getCategorySelector();
    }

    protected MainActivity getRequester() {
        final Activity A = super.getRequester();
        return (MainActivity) A;
    }

    @Override
    protected int[] getSelectedCategoriesIds() {
        final CategorySelector CS = getCategorySelector();
        return CS.getSelectedCategoriesIds();
    }

    @Override
    protected int getPagesQuantity() {
        final OnGooglionDrawerOpenListener OGDOL = getOpenListener();
        return OGDOL.getPagesQuantity();
    }

    private OnGooglionDrawerOpenListener getOpenListener() {
        final MainActivity MA = getRequester();
        return MA.getOpenListener();
    }

    @Override
    protected String getTextToSearch() {
        return "";
    }

    @Override
    protected ListView getListViewFromRequester() {
        final MainActivity MA = getRequester();
        final View V = MA.findViewById(R.id.lvShares);
        return (ListView) V;
    }

    @Override
    protected void addFooterViewToListView() {
        final MainActivity MA = getRequester();
        final PageScaler PS = MA.getPageScaler();
        final LayoutInflater LI = MA.getLayoutInflater();
        final int RESOURCE = layout.more_button;
        final ListView ROOT = getListViewFromRequester();
        final boolean ATTACH_TO_ROOT = false;
        final View V = LI.inflate(RESOURCE, ROOT, ATTACH_TO_ROOT);
        final OnGooglionDrawerOpenListener OGDOL = MA.getOpenListener();
        PS.scale(V);
        ROOT.addFooterView(V);
        OGDOL.translateFooterView();
        V.setOnClickListener(OGDOL);
    }

    @Override
    protected void applyDownloadedOffers() {
        final OnGooglionDrawerOpenListener OGDOL = getOpenListener();
        final List<Offer> L = getOffersList();
        OGDOL.apply(L);
    }
}