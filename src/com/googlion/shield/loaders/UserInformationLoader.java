package com.googlion.shield.loaders;

import android.app.*;
import android.content.*;
import android.os.*;
import com.example.starfishapp.*;
import com.example.starfishapp.model.*;

import static com.example.starfishapp.Starfish.*;

public abstract class UserInformationLoader extends AsyncTask<Void, Void, UserInfo> {
    private Activity requester;

    public UserInformationLoader(final Activity REQUESTER) {
        requester = REQUESTER;
    }

    public void loadUserInformation() {
        execute();
    }

    @Override
    protected UserInfo doInBackground(final Void... V) {
        final Context C = requester;
        final Starfish S = getInstance(C);
        return S.loadUserInfo();
    }

    protected Activity getRequester() {
        return requester;
    }
}