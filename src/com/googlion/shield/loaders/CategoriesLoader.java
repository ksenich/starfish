package com.googlion.shield.loaders;

import static com.example.starfishapp.Starfish.*;

import android.os.*;
import com.example.starfishapp.*;
import com.example.starfishapp.model.*;
import com.example.starfishapp.pages.*;

import java.util.*;

public class CategoriesLoader extends AsyncTask<Void, Void, List<Category>> {
    private VenuesActivity requester;

    public CategoriesLoader(final VenuesActivity REQUESTER) {
        requester = REQUESTER;
    }

    public void loadCategories() {
        execute();
    }

    @Override
    protected List<Category> doInBackground(final Void... V) {
        final Starfish S = getInstance(requester);
        return S.getCategories();
    }

    @Override
    protected void onPostExecute(final List<Category> L) {
        requester.apply(L);
    }
}