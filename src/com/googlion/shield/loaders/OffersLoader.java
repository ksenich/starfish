package com.googlion.shield.loaders;

import java.util.*;

import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import com.example.starfishapp.*;
import com.example.starfishapp.Starfish.*;
import com.example.starfishapp.model.*;
import com.example.starfishapp.R.*;

import static com.example.starfishapp.Starfish.*;


public abstract class OffersLoader extends AsyncTask<Void, Void, OffersPage> {
    private Activity requester;
    private OffersPage offers;

    public OffersLoader(final Activity REQUESTER) {
        requester = REQUESTER;
        offers = getOffersByDefault();
    }

    private OffersPage getOffersByDefault() {
        final List<Offer> L = new LinkedList<Offer>();
        return new OffersPage(L);
    }

    public void loadOffers() {
        execute();
    }

    @Override
    protected OffersPage doInBackground(final Void... V) {
        final int REGION_ID = getSelectedRegionId();
        final int[] CATEGORIES_IDS = getSelectedCategoriesIds();
        final int PAGES = getPagesQuantity();
        final int ITEMS_PER_PAGE = 10;
        final String TEXT_TO_SEARCH = getTextToSearch();
        final Starfish S = getInstance(requester);
        return S.loadOffers(REGION_ID, CATEGORIES_IDS, PAGES, ITEMS_PER_PAGE, TEXT_TO_SEARCH);
    }

    protected abstract int getSelectedRegionId();

    protected abstract int[] getSelectedCategoriesIds();

    protected abstract int getPagesQuantity();

    protected abstract String getTextToSearch();

    @Override
    protected void onPostExecute(final OffersPage OP) {
        retrieveOffersFrom(OP);
        checkIfThereAreMoreOffersToBeAddedToList();
        applyDownloadedOffers();
    }

    private void retrieveOffersFrom(final OffersPage OP) {
        offers = OP;
    }

    private void checkIfThereAreMoreOffersToBeAddedToList() {
        if (thereAreMoreOffers()) {
            addFooterViewToListViewIfThereAreNoFooterViewsThere();
        } else {
            removeFooterViewFromListView();
        }
    }

    private boolean thereAreMoreOffers() {
        return offers.hasMore;
    }

    private void addFooterViewToListViewIfThereAreNoFooterViewsThere() {
        if (listViewHasNotFooterViews()) {
            addFooterViewToListView();
        }
    }

    private boolean listViewHasNotFooterViews() {
        final ListView LV = getListViewFromRequester();
        final int SIZE = LV.getFooterViewsCount();
        return SIZE == 0;
    }

    protected abstract ListView getListViewFromRequester();

    protected abstract void addFooterViewToListView();

    private void removeFooterViewFromListView() {
        final View V = requester.findViewById(id.bGetMoreShares);
        final ListView LV = getListViewFromRequester();
        LV.removeFooterView(V);
    }

    protected abstract void applyDownloadedOffers();

    protected Activity getRequester() {
        return requester;
    }

    protected List<Offer> getOffersList() {
        return offers.list;
    }
}