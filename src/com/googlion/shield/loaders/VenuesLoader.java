package com.googlion.shield.loaders;

import static com.example.starfishapp.Starfish.*;

import android.content.*;
import android.os.*;
import com.example.starfishapp.*;
import com.example.starfishapp.model.*;
import com.example.starfishapp.pages.*;

import java.util.*;

public class VenuesLoader extends AsyncTask<Void, Void, List<Supplier>> {

    private VenuesListActivity requester;

    public VenuesLoader(final VenuesListActivity REQUESTER) {
        requester = REQUESTER;
    }

    public void loadVenues() {
        execute();
    }

    @Override
    protected List<Supplier> doInBackground(final Void... V) {
        final Intent I = requester.getIntent();
        final String NAME = "category_id";
        final Context C = requester;
        final Starfish S = getInstance(C);
        final int DEFAULT_VALUE = 1;
        final int CATEGORY_ID = I.getIntExtra(NAME, DEFAULT_VALUE);
        return S.getSuppliers(CATEGORY_ID);
    }

    @Override
    protected void onPostExecute(List<Supplier> VENUES) {
        requester.apply(VENUES);
    }
}