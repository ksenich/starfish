package com.googlion.shield.loaders;

import static com.example.starfishapp.Starfish.*;

import android.content.*;
import android.os.*;
import com.example.starfishapp.*;
import com.googlion.shield.pages.*;

import java.util.*;

public class UserCouponsLoader extends AsyncTask<Void, Void, Map<String, List<Map<String, Object>>>> {
    private UserCouponsPage requester;

    public UserCouponsLoader(final UserCouponsPage REQUESTER) {
        requester = REQUESTER;
    }

    public void loadUserCoupons() {
        execute();
    }

    @Override
    protected Map<String, List<Map<String, Object>>> doInBackground(final Void... V) {
        final Context C = requester;
        final Starfish S = getInstance(C);
        return S.loadUserCoupons();
    }

    @Override
    protected void onPostExecute(final Map<String, List<Map<String, Object>>> M) {
        requester.apply(M);
    }
}