package com.googlion.shield.loaders;

import android.app.*;
import com.example.starfishapp.model.*;
import com.example.starfishapp.pages.*;

public class BuyCouponMenuUserInformationLoader extends UserInformationLoader {
    public BuyCouponMenuUserInformationLoader(final Activity REQUESTER) {
        super(REQUESTER);
    }

    @Override
    protected void onPostExecute(final UserInfo UI) {
        final Activity A = getRequester();
        final BuyActivity BA = (BuyActivity) A;
        BA.retrieveUserInformationFrom(UI);
    }
}