package com.googlion.shield.loaders;

import android.content.*;
import android.os.*;
import com.example.starfishapp.*;
import com.example.starfishapp.model.*;
import com.example.starfishapp.pages.*;

import static com.example.starfishapp.Starfish.*;
import static com.example.starfishapp.pages.PointActivity.*;

public class PointLoader extends AsyncTask<Void, Void, Supplier> {
    private PointActivity requester;

    public PointLoader(final PointActivity REQUESTER) {
        requester = REQUESTER;
    }

    @Override
    protected Supplier doInBackground(final Void... V) {
        final Context C = requester;
        final Starfish S = getInstance(C);
        final Intent I = requester.getIntent();
        final String NAME = SUPPLIER_ID_KEY;
        final int DEFAULT_VALUE = 1;
        final int SUPPLIER_ID = I.getIntExtra(NAME, DEFAULT_VALUE);
        return S.findSupplier(SUPPLIER_ID);
    }

    @Override
    protected void onPostExecute(final Supplier S) {
        requester.setUpVenueDataFrom(S);
    }
}