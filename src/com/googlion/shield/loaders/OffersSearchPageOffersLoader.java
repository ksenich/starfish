package com.googlion.shield.loaders;

import static com.example.starfishapp.R.*;

import android.app.*;
import android.view.*;
import android.widget.*;
import com.example.starfishapp.model.*;
import com.example.starfishapp.pages.*;
import com.googlion.shield.utilities.*;

import java.util.*;

public class OffersSearchPageOffersLoader extends OffersLoader {
    public OffersSearchPageOffersLoader(final Activity REQUESTER) {
        super(REQUESTER);
    }

    @Override
    protected int getSelectedRegionId() {
        final OffersSearchActivity OSA = getRequester();
        return OSA.getSelectedRegionId();
    }

    protected OffersSearchActivity getRequester() {
        final Activity A = super.getRequester();
        return (OffersSearchActivity) A;
    }

    @Override
    protected int[] getSelectedCategoriesIds() {
        final OffersSearchActivity OSA = getRequester();
        return OSA.getSelectedCategoriesIds();
    }

    @Override
    protected int getPagesQuantity() {
        final OffersSearchActivity OSA = getRequester();
        return OSA.getPagesQuantity();
    }

    @Override
    protected String getTextToSearch() {
        final OffersSearchActivity OSA = getRequester();
        return OSA.getTextToSearch();
    }

    @Override
    protected ListView getListViewFromRequester() {
        final OffersSearchActivity OSA = getRequester();
        final View V = OSA.findViewById(id.lvOffers);
        return (ListView) V;
    }

    @Override
    protected void addFooterViewToListView() {
        final OffersSearchActivity OSA = getRequester();
        final PageScaler PS = OSA.getPageScaler();
        final LayoutInflater LI = OSA.getLayoutInflater();
        final int RESOURCE = layout.more_button;
        final ListView ROOT = getListViewFromRequester();
        final boolean ATTACH_TO_ROOT = false;
        final View V = LI.inflate(RESOURCE, ROOT, ATTACH_TO_ROOT);
        PS.scale(V);
        ROOT.addFooterView(V);
        OSA.translateFooterView();
        V.setOnClickListener(OSA);
    }

    @Override
    protected void applyDownloadedOffers() {
        final OffersSearchActivity OSA = getRequester();
        final List<Offer> L = getOffersList();
        OSA.apply(L);
    }
}