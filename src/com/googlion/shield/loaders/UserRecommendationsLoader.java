package com.googlion.shield.loaders;

import static com.example.starfishapp.Starfish.*;

import android.content.*;
import android.os.*;
import com.example.starfishapp.*;

import java.util.*;

public class UserRecommendationsLoader extends AsyncTask<Void, Void, List<Map<String, Object>>> {
    private RecommendationsActivity requester;

    public UserRecommendationsLoader(final RecommendationsActivity REQUESTER) {
        requester = REQUESTER;
    }

    public void loadRecommendations() {
        execute();
    }

    @Override
    protected List<Map<String, Object>> doInBackground(final Void... V) {
        final Context C = requester;
        final Starfish S = getInstance(C);
        return S.loadRecommendations();
    }

    @Override
    protected void onPostExecute(final List<Map<String, Object>> L) {
        requester.apply(L);
    }
}