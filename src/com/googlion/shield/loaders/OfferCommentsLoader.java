package com.googlion.shield.loaders;

import android.content.*;
import android.os.*;
import com.example.starfishapp.*;
import com.example.starfishapp.model.*;
import com.example.starfishapp.pages.*;

import java.util.*;

import static com.example.starfishapp.Starfish.*;

public class OfferCommentsLoader extends AsyncTask<Void, Void, List<Comment>> {
    private Bundle offerCommentsLoadingSettings;
    private CommentsActivity requester;
    private int offerId;

    public OfferCommentsLoader(final Bundle SETTINGS) {
        offerCommentsLoadingSettings = SETTINGS;
        requester = getRequesterFromOfferCommentsLoadingSettings();
        offerId = getOfferIdFromOfferCommentsLoadingSettings();
    }

    private CommentsActivity getRequesterFromOfferCommentsLoadingSettings() {
        final String KEY = "Requester";
        return offerCommentsLoadingSettings.getParcelable(KEY);
    }

    private int getOfferIdFromOfferCommentsLoadingSettings() {
        final String KEY = "Offer id";
        return offerCommentsLoadingSettings.getInt(KEY);
    }

    public void loadComments() {
        execute();
    }

    @Override
    protected List<Comment> doInBackground(final Void... V) {
        final Context C = requester;
        final Starfish S = getInstance(C);
        return S.loadComments(offerId);
    }

    @Override
    protected void onPostExecute(final List<Comment> L) {
        requester.apply(L);
    }
}
