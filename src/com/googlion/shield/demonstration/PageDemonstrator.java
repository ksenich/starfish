package com.googlion.shield.demonstration;

import android.app.*;
import android.os.*;
import com.googlion.shield.utilities.PageScaler;

public class PageDemonstrator extends Activity {

	@Override
	protected void onCreate(final Bundle SAVED_INSTANCE_STATE) {
		super.onCreate(SAVED_INSTANCE_STATE);
		setContentView(getLayoutResourceIdFromExtras());
	}

	private int getLayoutResourceIdFromExtras() {
		final Bundle B = getIntent().getExtras();
		final String KEY = "Layout resource id";
		return B.getInt(KEY);
	}

	@Override
	public void onWindowFocusChanged(final boolean HAS_FOCUS) {
		super.onWindowFocusChanged(HAS_FOCUS);
		final Activity REQUESTER = PageDemonstrator.this;
		final PageScaler PS = new PageScaler(REQUESTER);
		PS.scaleThePage();
	}

}