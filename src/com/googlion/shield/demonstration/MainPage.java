package com.googlion.shield.demonstration;

import static android.R.layout.*;
import java.util.*;

import android.app.*;
import android.content.*;
import android.content.res.*;
import android.os.*;
import android.view.*;
import android.widget.*;

public class MainPage extends ListActivity {

	private String layoutNames[];

	@Override
	protected void onCreate(final Bundle SAVED_INSTANCE_STATE) {
		super.onCreate(SAVED_INSTANCE_STATE);
		initializeLayoutNames();
		setListAdapterForThisPage();
	}

	private void initializeLayoutNames() {
		final List<String> L = getLayoutNamesList();
		final int SIZE = L.size();
		layoutNames = new String[SIZE];
		layoutNames = L.toArray(layoutNames);
	}

	// Закоментовані класи
	// Layout1Activity (1)
	// Layout4Activity(2)
	// Layout4Activity(3)
	// Layout4Activity (4-1-map)
	// EntertainmentActivity (6-2)
	// PointActivity (6-3)
	// ShareActivity (6-4-1-акция)
	// ShareActivity/CommentsDialogFragment (6-4-3-comments)
	// MyDiscountsActivity (7-рекомендации)
	// MyHistoryActivity (8-счёт)
	// MyFriendsActivity (9-1-друзья)
	// RecomendationsActivity (9-2)
	// AddFriendsActivity (9-4-2-add_friend_lvl2)
	// CouponsActivity (10-мои-купоны)
	// WishListActivity (11-список-желаний)
	// FavouriteCategoriesActivity (12-2-мои-категории)

	private List<String> getLayoutNamesList() {
		final List<String> L = new LinkedList<String>();
		L.addAll(getOneHalfOfLayouts());
		L.addAll(getAnotherHalfOfLayouts());
		return L;
	}

	private List<String> getOneHalfOfLayouts() {
		final List<String> L = new LinkedList<String>();
		L.add("0 (welcome_page)");
		L.add("1 (entry_page)");
		L.add("1-1-registration (registration_page)");
		L.add("1-2-enter (authorization_page)");
		L.add("2 (user_application_settings_menu)");
		L.add("3 (user_favorite_categories_menu)");
		L.add("4-1-map (see_on_the_map_page)");
		L.add("4-2-list (see_share_list_page)");
		L.add("4-3-android-settings (application_settings_menu)");
		L.add("4-6-search-filter (search_results_filter_menu)");
		L.add("4-7-akciya (add_share_page)");
		L.add("5_main_menu (main_menu)");
		L.add("6-1-katalog-akciy (venues_page)");
		L.add("6-2 (venues_list_page)");
		L.add("6-3 (point_page)");
		return L;
	}

	private List<String> getAnotherHalfOfLayouts() {
		final List<String> L = new LinkedList<String>();
		L.add("6-4-1-акция (share_page)");
		L.add("6-4-3-comments (share_comments_page)");
		L.add("6-5-купон (buy_coupon_menu)");
		L.add("7-рекомендации (user_recommendations_page)");
		L.add("8-счёт (user_bill_page)");
//		L.add("9-1-друзья (user_friends_page)");
//		L.add("9-2 (friend_information_page)");
//		L.add("9-4-1-add_friend (invite_friends_from_social_network_menu)");
//		L.add("9-4-2-add_friend_lvl2 (friends_from_social_network_page)");
		L.add("10-мои-купоны (user_coupons_page)");
		L.add("11-список-желаний (wish_list_page)");
		L.add("12-1-настройки-профиля (profile_settings_page)");
		L.add("12-1-профиль-соцсети (profile_binding_page)");
		L.add("12-2-мои-категории (user_favorite_categories_page)");
		return L;
	}

	private void setListAdapterForThisPage() {
		final ListAdapter LA = getListAdapterForThisPage();
		setListAdapter(LA);
	}

	private ListAdapter getListAdapterForThisPage() {
		final Context C = MainPage.this;
		final int RESOURCE = simple_list_item_1;
		return new ArrayAdapter<String>(C, RESOURCE, layoutNames);
	}

	@Override
	protected void onListItemClick(final ListView L, final View V,
			final int POSITION, final long ID) {
		final Context CT = MainPage.this;
		final Class<?> C = PageDemonstrator.class;
		final Intent I = new Intent(CT, C);
		final Bundle EXTRAS = getExtrasFrom(POSITION);
		I.putExtras(EXTRAS);
		startActivity(I);
	}

	private Bundle getExtrasFrom(final int POSITION) {
		final Bundle B = new Bundle();
		final Resources R = getResources();
		final String CHOSEN_LAYOUT_NAME = layoutNames[POSITION];
		final int START = CHOSEN_LAYOUT_NAME.indexOf("(") + 1;
		final int END = CHOSEN_LAYOUT_NAME.indexOf(")");
		final String KEY = "Layout resource id";
		final String NAME = CHOSEN_LAYOUT_NAME.substring(START, END);
		final String DEF_TYPE = "layout";
		final String DEF_PACKAGE = getPackageName();
		final int VALUE = R.getIdentifier(NAME, DEF_TYPE, DEF_PACKAGE);
		B.putInt(KEY, VALUE);
		return B;
	}
}