package com.example.starfishapp;

import android.app.*;
import android.content.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import com.example.starfishapp.pages.*;
import com.example.starfishapp.R.*;
import com.example.starfishapp.utilities.*;
import com.facebook.*;
import com.google.android.gms.auth.*;
import com.google.android.gms.common.*;
import com.google.android.gms.common.api.*;
import com.google.android.gms.plus.*;
import com.google.android.gms.plus.model.people.*;
import com.googlion.shield.specialists.FontSpecialist;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;
import com.vk.sdk.*;
import com.vk.sdk.api.*;

import java.io.*;
import java.util.*;

public class WelcomeActivity extends Activity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    public static final int GP_TOKEN_RESULT = 157;
    public static final String ACCESS_TOKEN_KEY = "access_token";
    private VKSdkListener VKlistener = new VKSdkListener() {
        @Override
        public void onCaptchaError(VKError captchaError) {

        }

        @Override
        public void onTokenExpired(VKAccessToken expiredToken) {

        }

        @Override
        public void onAccessDenied(VKError authorizationError) {

        }

        @Override
        public void onReceiveNewToken(VKAccessToken newToken) {
            Bundle bundle = new Bundle();
            bundle.putString(ACCESS_TOKEN_KEY, newToken.accessToken);
            bundle.putString("uid", newToken.userId);
//            bundle.putString("secret", newToken.secret);
            Starfish.getInstance(self).loginSocial("vk", bundle, new Starfish.LoginCallback() {
                @Override
                public void onResult(String result) {
                    goToMap();
                }
            });
        }
    };
    private static final int REQUEST_CODE_RESOLVE_ERR = 9000;
    private static final int RC_SIGN_IN = 0;
    private static final int STATE_DEFAULT = 0;
    // Used to store the PendingIntent most recently returned by Google Play
    // services until the user clicks 'sign in'.
    private static final int STATE_SIGN_IN = 1;
    private static final int STATE_IN_PROGRESS = 2;
    private final WelcomeActivity self = WelcomeActivity.this;
    Session.StatusCallback statusCallback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            Log.d("fbpath", "call state:" + state.toString() + " isopened:" + state.isOpened() + " -- session:" + session.toString() + " isopened:" + session.isOpened());
            onSessionStateChange(session, state, exception);
        }

        public void onSessionStateChange(Session session, SessionState state, Exception exception) {
            //Toast.makeText(self, "FB sessionState: " + state.toString(), Toast.LENGTH_LONG).show();
            Log.d("fbpath", "onSessionStateChange state:" + state.toString() + " isopened:" + state.isOpened() + " -- session:" + session.toString() + " isopened:" + session.isOpened());
            if (state.isOpened() && session.isOpened() && state.toString().equals("OPENED")) {
                Log.d("fbpath", "state.isopened");
                Session as = Session.getActiveSession();
                Log.d("fbpath", "before request permissions");
                as.requestNewReadPermissions(new Session.NewPermissionsRequest(self, Arrays.asList("email")));
                Log.d("fbpath", "after request");
                final String fb_token = as.getAccessToken();
                Log.d("fbpath", "after gettoken");
                Bundle params = new Bundle();
                params.putString(ACCESS_TOKEN_KEY, fb_token);
//                Request req = Request.newMeRequest(as, new Request.GraphUserCallback() {
//                    @Override
//                    public void onCompleted(GraphUser user, Response response) {
//                        String name = user.getFirstName();
//                        String last = user.getLastName();
//                        String uid = user.getId();
//                        final String mail = (String) user.getProperty("email");
                Log.d("facebook_access_token", fb_token);
                Starfish.getInstance(self).loginSocial("fb", params, new Starfish.LoginCallback() {
                    @Override
                    public void onResult(String result) {
                        Log.d("facebook_access_token_2", fb_token);
                        goToMap();
                    }
                });

//                    }
//                });
//                Bundle params = req.getParameters();
//                params.putString("fields", "name,email");
//                req.setParameters(params);
//                req.executeAsync();
            } else if (state.isClosed()) {
                Log.d("facebook", "Logged out...");
            }
        }
    };
    private int mSignInProgress;
    //    private LoginButton fb_login = new LoginButton(this);
    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog mConnectionProgressDialog;
    private ConnectionResult mConnectionResult;
    // Used to store the PendingIntent most recently returned by Google Play
    // services until the user clicks 'sign in'.
    private PendingIntent mSignInIntent;
    // Used to store the error code most recently returned by Google Play services
    // until the user clicks 'sign in'.
    private int mSignInError;
    private ImageButton gpButton;

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.entry_page);
        scaleThisPage();
        setFontsForAllTextualViewsOfThisPage();
        translateThisPage();

        String appId = "4232990";
        VKAccessToken token = null;
        VKSdk.initialize(VKlistener, appId, token);
        mGoogleApiClient = new GoogleApiClient.Builder(
                self, self, self)
                .addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();

        if (isLoggedIn()) {
            goToMap();
        }
        openFacebookSession(savedInstanceState);
        final SignInButton gp_login = new SignInButton(this);
        mConnectionProgressDialog = new ProgressDialog(this);
        mConnectionProgressDialog.setMessage("Signing in...");
        setOnClickListeners();
    }

    private void scaleThisPage() {
        final Activity REQUESTER = WelcomeActivity.this;
        final PageScaler PS = new PageScaler(REQUESTER);
        PS.scaleThePage();
    }

    private void setFontsForAllTextualViewsOfThisPage() {
        final Map<Object, Map> S = getSettingsForSettingFonts();
        final FontSpecialist FS = new FontSpecialist(S);
        FS.setFonts();
    }

    private Map<Object, Map> getSettingsForSettingFonts() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingsFonts());
        M.put("Other settings", getOtherSettingsForSettingsFonts());
        return M;
    }

    private Map getMainSettingsForSettingsFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvEntryPageDiscounts, "kelsonsanslightru.otf");
        M.put(id.tvEntryPageShares, "kelsonsanslightru.otf");
        M.put(id.tvEntryPageSales, "kelsonsansregularru.otf");
        M.put(id.tvEntryPageDiscountsMobileMap, "kelsonsansboldru.otf");
        M.put(id.bGoToTheAuthorizationPage, "robotobold.otf");
        M.put(id.bGoToTheRegistrationPage, "robotobold.otf");
        M.put(id.tvEnterTheApplicationThroughSocialNetworks, "robotoregular0.otf");
        return M;
    }

    private Map getOtherSettingsForSettingsFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", WelcomeActivity.this);
        return M;
    }

    private void translateThisPage() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForEntryPageTranslation();
        final Translator T = new PageTranslator(SETTINGS);
        T.translate();
    }

    private Map<String, Map<String, Object>> getSettingsForEntryPageTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getPageViewsIds());
        M.put("Page views default texts", getPageViewsDefaultTexts());
        M.put("Other settings", getOtherSettings());
        return M;
    }

    private Map<String, Object> getPageViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvEntryPageDiscounts", id.tvEntryPageDiscounts);
        M.put("tvEntryPageShares", id.tvEntryPageShares);
        M.put("tvEntryPageSales", id.tvEntryPageSales);
        M.put("tvEntryPageDiscountsMobileMap", id.tvEntryPageDiscountsMobileMap);
        M.put("bGoToTheAuthorizationPage", id.bGoToTheAuthorizationPage);
        M.put("bGoToTheRegistrationPage", id.bGoToTheRegistrationPage);
        M.put("tvEnterTheApplicationThroughSocialNetworks", id.tvEnterTheApplicationThroughSocialNetworks);
        return M;
    }

    private Map<String, Object> getPageViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvEntryPageDiscounts", "ЗНИЖКИ");
        M.put("tvEntryPageShares", "АКЦІЇ");
        M.put("tvEntryPageSales", "РОЗПРОДАЖІ");
        M.put("tvEntryPageDiscountsMobileMap", "Рухома карта знижок");
        M.put("bGoToTheAuthorizationPage", "Вхід");
        M.put("bGoToTheRegistrationPage", "Приписка");
        M.put("tvEnterTheApplicationThroughSocialNetworks", "Увійти через соціальні мережі:");
        return M;
    }

    private Map<String, Object> getOtherSettings() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", WelcomeActivity.this);
        return M;
    }

    private void setOnClickListeners() {
        findViewById(R.id.vEnterTheApplicationThroughGooglePlus).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                googleSignIn();
            }
        });

        findViewById(R.id.vEnterTheApplicationThroughVkontakte).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VKSdk.authorize(null, false, true);
            }
        });

        findViewById(R.id.vEnterTheApplicationThroughFacebook).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fbSignIn();
            }
        });

        findViewById(R.id.bGoToTheAuthorizationPage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(self, AuthorizationPage.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.bGoToTheRegistrationPage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(self, RegistrationPage.class);
                startActivity(intent);
            }
        });
    }

    private void openFacebookSession(Bundle savedInstanceState) {
        Log.d("fbpath", "openfbsesion");
        Session session = Session.getActiveSession();
        if (session == null) {
            if (savedInstanceState != null) {
                Log.d("fbpath", "savedinstancestate not nul. restoring sesion");
                session = Session.restoreSession(this, null, statusCallback, savedInstanceState);
            }
            if (session == null) {
                Log.d("fbpath", "session is null");
                session = new Session(this);
            }
            Log.d("fbpath", "before setactive");
            Session.setActiveSession(session);
            Log.d("fbpath", "after setactive");
            if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
                Log.d("fbpath", "state eq created tk loaded");
                session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback));
            }
        }
    }

    private void googleSignIn() {
        //                final Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://starfishapp.pp.ua/app.php?action=get_token_soc&type=gp"));
//                startActivity(intent);
//                if (!mGoogleApiClient.isConnecting()) {
//                    resolveSignInError();
//                }
//        AlertDialog.Builder alert = new AlertDialog.Builder(this);
//        alert.setTitle("Title here");
//
//        WebView wv = new WebView(this);
//        final PopupWindow popupWindow = new PopupWindow(
//                wv,
//                ViewGroup.LayoutParams.WRAP_CONTENT,
//                ViewGroup.LayoutParams.WRAP_CONTENT);
//popupWindow.showAtLocation(findViewById(R.id.huh),0,0,0);
//        wv.requestFocus(View.FOCUS_DOWN);
//        wv.setOnTouchListener(new View.OnTouchListener()
//        {
//            @Override
//            public boolean onTouch(View v, MotionEvent event)
//            {
//                switch (event.getAction())
//                {
//                    case MotionEvent.ACTION_DOWN:
//                    case MotionEvent.ACTION_UP:
//                        if (!v.hasFocus())
//                        {
//                            v.requestFocus();
//                        }
//                        break;
//                }
//                return false;
//            }
//        });
//        wv.loadUrl("http://starfishapp.pp.ua/app.php?action=get_token_soc&type=gp");
//        wv.setWebViewClient(new WebViewClient() {
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                view.loadUrl(url);
//
//                return true;
//            }
//        });

//        alert.setView(wv);
//        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
//        alert.show();
        Intent intent = new Intent(this, WebActivity.class);
        startActivityForResult(intent, GP_TOKEN_RESULT);
    }

    private void fbSignIn() {
        Log.d("FBLogin", "subj");
        Session session = Session.getActiveSession();
        if (!session.isOpened() && !session.isClosed()) {
            session.openForRead(new Session.OpenRequest(self).setCallback(statusCallback));
        } else {
            Session.openActiveSession(self, true, statusCallback);
        }
    }

    private void goToMap() {
        final Context CT = this;
        final Class<?> C = MainActivity.class;
        final Intent I = new Intent(CT, C);
        startActivity(I);
        finish();
    }

    private boolean isVKLoggedIn() {
        final SharedPreferences sp = getSharedPreferences("tokens", MODE_PRIVATE);
        String vkToken = sp.getString("vk_token", null);
        String vkUserId = sp.getString("vk_user_id", null);
        View vkButton = findViewById(R.id.vEnterTheApplicationThroughVkontakte);

        if (vkToken != null && vkUserId != null) {

//            vkButton.setText("logout");
            vkButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sp.edit().clear().commit();
                }
            });
            return true;
        } else {
            vkButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return false;
        }
    }

    private boolean isLoggedIn() {
        boolean r = false;

        return /*isFBLoggedIn() || isVKLoggedIn() || */isAPILoggedIn();
    }

    private boolean isAPILoggedIn() {
        return Starfish.getInstance(self).isLoggedIn();
    }

    private void FbLogout() {
        Session session = Session.getActiveSession();
        if (!session.isClosed()) {
            session.closeAndClearTokenInformation();
        }
    }

    private boolean isFBLoggedIn() {
        boolean r = false;
        Session fbSession = Session.getActiveSession();
        SessionState state = fbSession.getState();
        if (state.isOpened()) {
            r = true;
        }
        return r;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //VK
        VKUIHelper.onActivityResult(requestCode, resultCode, data);
        //FB
        Log.d("fbpath", "in onacres");
        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
        //GP
        if (requestCode == REQUEST_CODE_RESOLVE_ERR && resultCode == RESULT_OK) {
            mConnectionResult = null;
            mGoogleApiClient.connect();
        }
        switch (requestCode) {
            case RC_SIGN_IN: {
                if (resultCode == RESULT_OK) {
                    // If the error resolution was successful we should continue
                    // processing errors.
                    mSignInProgress = STATE_SIGN_IN;
                } else {
                    // If the error resolution was not successful or the user canceled,
                    // we should stop processing errors.
                    mSignInProgress = STATE_DEFAULT;
                }

                if (!mGoogleApiClient.isConnecting()) {
                    // If Google Play services resolved the issue with a dialog then
                    // onStart is not called so we need to re-attempt connection here.
                    mGoogleApiClient.connect();
                }
                break;
            }
            //API
            case GP_TOKEN_RESULT: {
                if (data != null && data.hasExtra(ACCESS_TOKEN_KEY)) {
                    Starfish.getInstance(self).setToken(data.getStringExtra(ACCESS_TOKEN_KEY));
                    Bundle b = new Bundle();
                    b.putString(ACCESS_TOKEN_KEY, data.getStringExtra(ACCESS_TOKEN_KEY));
                    goToMap();
                }
                break;
            }
        }
        if (isLoggedIn()) {
            //goToMap();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
//        Toast.makeText(this, "G+ connection failed: "+ result.getErrorCode(), Toast.LENGTH_LONG).show();
//        if (mConnectionProgressDialog.isShowing()) {
//            // Пользователь уже нажал кнопку входа. Запустите, чтобы устранить
//            // ошибки подключения. Дождитесь появления onConnected(), чтобы скрыть
//            // диалоговое окно подключения.
//            if (result.hasResolution()) {
//                try {
//                    result.startResolutionForResult(this, REQUEST_CODE_RESOLVE_ERR);
//                } catch (IntentSender.SendIntentException e) {
//                    mGoogleApiClient.connect();
//                }
//            }
//        }
        Log.i("gptoken", "onConnectionFailed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());

        if (mSignInProgress != STATE_IN_PROGRESS) {
            // We do not have an intent in progress so we should store the latest
            // error resolution intent for use when the sign in button is clicked.
            mSignInIntent = result.getResolution();
            mSignInError = result.getErrorCode();

            if (mSignInProgress == STATE_SIGN_IN) {
                // STATE_SIGN_IN indicates the user already clicked the sign in button
                // so we should continue processing errors until the user is signed in
                // or they click cancel.
                resolveSignInError();
            }
        }
        mConnectionResult = result;
    }

    @Override
    public void onConnected(Bundle bundle) {
//        String name = mGoogleApiClient.getAccountName();
        final Person currentUser = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
        mSignInProgress = STATE_DEFAULT;

        Toast.makeText(this, "User is connected!", Toast.LENGTH_LONG).show();
        Log.d("gptoken", "hello");
        if (currentUser != null) {

            new Thread(new Runnable() {
                @Override
                public void run() {
                    String mail = Plus.AccountApi.getAccountName(mGoogleApiClient);
                    try {

                        String token = GoogleAuthUtil.getToken(self, mail + "", "oauth2:" + Scopes.PLUS_LOGIN + " " + Scopes.PLUS_ME);

//                        checkToken(token);
                        Bundle bundle = new Bundle();
                        bundle.putString(ACCESS_TOKEN_KEY, token);
                        Starfish.getInstance(self).loginSocial("gp", bundle, new Starfish.LoginCallback() {
                            @Override
                            public void onResult(String result) {
                                goToMap();
                            }
                        });
                    } catch (IOException e) {

                        e.printStackTrace();
                    } catch (GoogleAuthException e) {

                        e.printStackTrace();
                    }

                }
            }).start();
        }
//        Plus.AccountApi
//                .revokeAccessAndDisconnect(mGoogleApiClient)
//                .setResultCallback(new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(Status status) {
//                        Log.d("gp", "Disconnected");
//                    }
//                });

        mConnectionProgressDialog.dismiss();
    }

    private void checkToken(String token) {
        String id = "https://www.googleapis.com/oauth2/v1/tokeninfo?id_token=" + token;
        String access = "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=" + token;
        new DownloadWebPageTask().executeWOToken(id, access);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this, "G+ suspended: ", Toast.LENGTH_LONG).show();
        mGoogleApiClient.connect();
    }

    private void resolveSignInError() {
        if (mSignInIntent != null) {
            // We have an intent which will allow our user to sign in or
            // resolve an error.  For example if the user needs to
            // select an account to sign in with, or if they need to consent
            // to the permissions your app is requesting.

            try {
                // Send the pending intent that we stored on the most recent
                // OnConnectionFailed callback.  This will allow the user to
                // resolve the error currently preventing our connection to
                // Google Play services.
                mSignInProgress = STATE_IN_PROGRESS;
                startIntentSenderForResult(mSignInIntent.getIntentSender(),
                        RC_SIGN_IN, null, 0, 0, 0);
            } catch (IntentSender.SendIntentException e) {
                Log.i("gptoken", "Sign in intent could not be sent: "
                        + e.getLocalizedMessage());
                // The intent was canceled before it was sent.  Attempt to connect to
                // get an updated ConnectionResult.
                mSignInProgress = STATE_SIGN_IN;
                mGoogleApiClient.connect();
            }
        } else {
            // Google Play services wasn't able to provide an intent for some
            // error types, so we show the default Google Play services error
            // dialog which may still start an intent on our behalf if the
            // user can resolve the issue.
            Toast.makeText(this, "play services error", Toast.LENGTH_LONG).show();
//            showDialog(DIALOG_PLAY_SERVICES_ERROR);
        }
    }
}
