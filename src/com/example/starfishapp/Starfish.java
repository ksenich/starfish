package com.example.starfishapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;
import com.example.starfishapp.model.*;
import com.example.starfishapp.utilities.DownloadWebPageTask;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.plus.Plus;
import com.googlion.shield.requests.CouponsLoadingRequest;
import com.googlion.shield.requests.SuppliersLoadingRequest;
import com.googlion.shield.translators.*;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static android.text.Spanned.*;
import static android.graphics.Color.*;

public class Starfish {
    public static final String baseUrl = "http://starfishapp.pp.ua/app.php";
    public static final String ID_LANGUAGE_KEY = "id_language";
    public static final String ID_COUNTRY_KEY = "id_country";
    public static final String ID_REGION_KEY = "id_region";
    public static final String NAME_LANGUAGE_KEY = "name_language";
    public static final String NAME_COUNTRY_KEY = "name_country";
    public static final String NAME_REGION_KEY = "name_region";
    public static final String USER_PREFS_KEY = "user";
    public static final String TOKENS_KEY = "tokens";
    public static final String API_TOKEN_KEY = "api_token";
    public static final String PHOTO_URL_KEY = "photo_url";
    public static final String SETTINGS_SET_KEY = "settings_set";
    public static final String USER_NAME_KEY = "name";
    private static Starfish instance = new Starfish();
    Context context;
    SparseArray<Country> countryById = new SparseArray<Country>();
    GoogleApiClient mPlusClient;
    SparseArray<Bitmap> catIcons = new SparseArray<Bitmap>();
    private SparseArray<Category> catById = new SparseArray<Category>();
    private String token = "";

    public static Starfish getInstance(Context ctx) {
        instance.setContext(ctx);
        return instance;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private <T> void load(List<T> list, Request<T> request) {
        try {
            String current_url = request.getAction();
            Log.d("loading request", current_url);
            String server_response = "";
            switch (request.getMethod()) {
                case GET:
                    server_response = readWebpage(current_url, request.getOptions().getOptions);
                    break;
                case POST:
                    server_response = readWebpage(current_url, request.getOptions().getOptions, request.getOptions().postOptions);
                    break;

            }
            Log.d("loading response get", server_response);

            Object o = new JSONTokener(server_response).nextValue();
            if (!(o instanceof JSONObject)) {
                Log.e("not a json", o.toString());
                return;
            }
            JSONObject parsedServerResponse = (JSONObject) o;
            JSONArray array = parsedServerResponse.getJSONArray(request.getName());
            for (int i = 0, end = array.length(); i < end; ++i) {
                try {
                    JSONObject object = (JSONObject) array.get(i);
                    list.add(request.parseOne(object));
                } catch (Exception e) {
                    Log.e("BadJSON", e.getMessage());
                }

            }
            Log.d("Loaded ", list.size() + "items");
            Iterator ki = parsedServerResponse.keys();
            Log.d("loadkeys", ki.toString());
            while (ki.hasNext()) {
                String key = (String) ki.next();
                Log.d("loadkeys", key);
                Object v = parsedServerResponse.get(key);
                if (v instanceof Integer) {
                    request.onIntExtra(key, (Integer) v);
                }
            }
        } catch (JSONException e) {
            Log.e("BadJSON", e.getMessage());
        }
    }

    private String readWebpage(String current_url, String[] getOptions, Map<String, String> postOptions) {
        return readWebpage(current_url, getOptions, postOptions, null, null);
    }

    private String readWebpage(String current_url, String[] getOptions, Map<String, String> postOptions, String name, File file) {
        DownloadWebPageTask task = new DownloadWebPageTask();
        String constructUrl = constructUrl(current_url, getOptions);
        Log.d("Loading", constructUrl);
        String res = task.executePostWithToken(context, constructUrl, postOptions, name, file);
        Log.d("Loaded", res);
        return res;
    }

    private List<Category> loadCategories() {
        List<Category> categories = new ArrayList<Category>();
        load(categories, new Request<Category>() {
            @Override
            public METHOD getMethod() {
                return METHOD.GET;
            }

            @Override
            public String getAction() {
                return "get_categories";
            }

            @Override
            public RequestOptions getOptions() {
                return new RequestOptions();
            }

            @Override
            public String getName() {
                return "categories";
            }

            @Override
            public Category parseOne(JSONObject jo) throws JSONException {
                String name = jo.getString(USER_NAME_KEY);
                int id = jo.getInt("id");
//                String url = jo.getString("url");
                Category cat = new Category(id, name);
                cat.img_url = jo.getString("img_category").replaceAll("\\\\", "");
                catById.put(id, cat);
                return cat;
            }

            @Override
            public void onIntExtra(String name, int value) {

            }
        });
        return categories;
    }

    private List<Country> loadCountries() {
        List<Country> countries = new ArrayList<Country>();
        load(countries, new Request<Country>() {
            @Override
            public METHOD getMethod() {
                return METHOD.GET;
            }

            @Override
            public String getAction() {
                return "get_countries";
            }

            @Override
            public RequestOptions getOptions() {
                return new RequestOptions();
            }

            @Override
            public String getName() {
                return "countries";
            }

            @Override
            public Country parseOne(JSONObject jo) throws JSONException {
                String name = jo.getString("title");
                int id = jo.getInt("id");
                Country country = new Country(name, id);
                countryById.put(id, country);
                return country;
            }

            @Override
            public void onIntExtra(String name, int value) {

            }
        });
        return countries;
    }

    private List<Region> loadRegions(final int id) {
        List<Region> regions = new ArrayList<Region>();
        load(regions, new Request<Region>() {
            @Override
            public METHOD getMethod() {
                return METHOD.GET;
            }

            @Override
            public String getAction() {
                return "get_regions";
            }

            @Override
            public RequestOptions getOptions() {
                return id == -1 ? new RequestOptions() : new RequestOptions(new String[]{"country_id=" + id});
            }

            @Override
            public String getName() {
                return "regions";
            }

            @Override
            public Region parseOne(JSONObject jo) throws JSONException {
                String name = jo.getString(USER_NAME_KEY);
                int id = jo.getInt("id");
                int countryId = jo.getInt("country_id");
                Region region = new Region(name, id, countryId);
                Country c = countryById.get(countryId);
                if (c != null) {
                    c.regions.add(region);
                }
                Log.d("Region created", region.toString());
                return region;
            }

            @Override
            public void onIntExtra(String name, int value) {

            }
        });

        return regions;
    }

    private String readWebpage(String url, String[] options) {
        DownloadWebPageTask task = new DownloadWebPageTask();
        String constructUrl = constructUrl(url, options);
        Log.d("Loading", constructUrl);
        String res = task.executeWithToken(context, constructUrl);
        Log.d("Loaded", res.substring(0, Math.min(80, res.length())));
        return res;
    }

    String constructUrl(String action, String[] options) {
        StringBuilder sb = new StringBuilder(baseUrl).append("?action=").append(action);
        for (String o : options) {
            sb.append("&").append(o);
        }
        return sb.toString();
    }

    public OffersPage loadOffers(final int region_id, final int[] category_id, int page, int itemsPerPage, String searchText) {
        final List<Offer> offers = new ArrayList<Offer>();
        OffersPage pg = new OffersPage(offers);
        load(offers, new OfferRequestBySearchText(pg, region_id, category_id, page, itemsPerPage, searchText));
        return pg;
    }

    private List<Offer> loadOffers(final int region_id, final int[] category_id, final LatLng topLeft, final LatLng botRight) {
        final List<Offer> offers = new ArrayList<Offer>();
        load(offers, new OfferRequestByBounds(region_id, category_id, topLeft, botRight));
        return offers;
    }

    public List<Category> getCategories() {
        List<Category> res;
        res = loadCategories();
        return res;
    }

    public List<Country> getCountries() {
        return loadCountries();
    }

    public List<Offer> getOffers(int r, int[] c, LatLng topLeft, LatLng botRight) {
        Log.d("Starfish", String.format("getOffers: reg:%d, loc:%s;%s", r, topLeft, botRight));
        return loadOffers(r, c, topLeft, botRight);
    }

    public List<Region> getRegions() {
        return loadRegions();
    }

    private List<Region> loadRegions() {
        return loadRegions(-1);
    }

    public void register(final String email, final String password, String first_name, String last_name) {
        final HttpClient register_httpclient = new DefaultHttpClient();
        final HttpPost register_post = new HttpPost("http://starfishapp.pp.ua/app.php?action=account");
        final List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        try {
            nameValuePairs.add(new BasicNameValuePair("username", URLEncoder.encode(email, "UTF-8")));
            nameValuePairs.add(new BasicNameValuePair("password", URLEncoder.encode(password, "UTF-8")));
            nameValuePairs.add(new BasicNameValuePair("first_name", URLEncoder.encode(first_name, "UTF-8")));
            nameValuePairs.add(new BasicNameValuePair("last_name", URLEncoder.encode(last_name, "UTF-8")));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        AsyncTask<Void, Void, String> register_task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void[] objects) {
                try {

                    Log.d("register response1", "started");
                    register_post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse register_response = register_httpclient.execute(register_post);
                    String body = EntityUtils.toString(register_response.getEntity(), "utf-8");
                    return body;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return "ERROR:couln't get register http responce";
            }


            @Override
            protected void onPostExecute(String result) {
                Log.d("register response", result);
                final Map<String, Object> SETTINGS = new HashMap<String, Object>();
                final LoginCallback LC = new LoginCallback() {


                    @Override
                    public void onResult(String result) {
                        if (!result.contains("error")) {
                            Intent intent = new Intent(getContext(), MainActivity.class);
                            getContext().startActivity(intent);
                        } else {
                            Toast.makeText(getContext(), result, Toast.LENGTH_SHORT).show();
                        }
                    }
                };
                SETTINGS.put("Login", email);
                SETTINGS.put("Password", password);
                SETTINGS.put("Callback", LC);

                login(SETTINGS);
            }
        };
        register_task.execute();

    }

    public void login(final Map<String, Object> SETTINGS) {
        AsyncTask<Void, Void, String> login_task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(final Void[] V) {
                try {
                    return getResponseOfAuthorization();
                } catch (final Exception E) {
                    return E.getMessage();
                }
            }

            private String getResponseOfAuthorization() throws Exception {
                final HttpResponse HR = getHttpResponseFromHttpClient();
                final HttpEntity ENTITY = HR.getEntity();
                final String DEFAULT_CHARSET = "utf-8";
                return EntityUtils.toString(ENTITY, DEFAULT_CHARSET);
            }

            private HttpResponse getHttpResponseFromHttpClient() throws Exception {
                final HttpClient HC = new DefaultHttpClient();
                final String URI = "http://starfishapp.pp.ua/app.php?action=get_token";
                final HttpPost HP = new HttpPost(URI);
                final List<NameValuePair> PARAMETERS = getParametersFromSettings();
                final HttpEntity HE = new UrlEncodedFormEntity(PARAMETERS);
                HP.setEntity(HE);
                return HC.execute(HP);
            }

            private List<NameValuePair> getParametersFromSettings() {
                final List<NameValuePair> L = new LinkedList<NameValuePair>();
                L.add(getNameValuePairForUserName());
                L.add(getNameValuePairForPassword());
                return L;
            }

            private NameValuePair getNameValuePairForUserName() {
                final String NAME = "username";
                final String VALUE = getUserNameFromSettings();
                return new BasicNameValuePair(NAME, VALUE);
            }

            private String getUserNameFromSettings() {
                final String KEY = "Login";
                final Object O = SETTINGS.get(KEY);
                return (String) O;
            }

            private NameValuePair getNameValuePairForPassword() {
                final String NAME = "password";
                final String VALUE = getPasswordFromSettings();
                return new BasicNameValuePair(NAME, VALUE);
            }

            private String getPasswordFromSettings() {
                final String KEY = "Password";
                final Object O = SETTINGS.get(KEY);
                return (String) O;
            }

            @Override
            protected void onPostExecute(final String RESULT) {
                tryToSaveToken(RESULT);
                provideCallbackOnResult(RESULT);
            }

            private void tryToSaveToken(final String RESULT) {
                try {
                    saveToken(RESULT);
                } catch (final Exception E) {
                    printStackTraceOf(E);
                }
            }

            private void saveToken(final String RESULT) throws Exception {
                final JSONTokener T = new JSONTokener(RESULT);
                final Object O = T.nextValue();
                final JSONObject JO = (JSONObject) O;
                final String TOKEN = JO.getString("token");
                final Context C = getContext();
                final String NAME = TOKENS_KEY;
                final int MODE = Context.MODE_PRIVATE;
                final SharedPreferences SP = C.getSharedPreferences(NAME, MODE);
                SharedPreferences.Editor E = SP.edit();
                E.putString(API_TOKEN_KEY, TOKEN);
                E.commit();
            }

            private void printStackTraceOf(final Exception E) {
                E.printStackTrace();
            }

            private void provideCallbackOnResult(final String RESULT) {
                final String KEY = "Callback";
                final Object O = SETTINGS.get(KEY);
                final LoginCallback LC = (LoginCallback) O;
                LC.onResult(RESULT);
            }
        };
        login_task.execute();
    }

    public String getToken() {
        final SharedPreferences sp = getContext().getSharedPreferences(TOKENS_KEY, Context.MODE_PRIVATE);
        String token = sp.getString(API_TOKEN_KEY, null);
        return token;
    }

    public void setToken(String t) {
        getContext()
                .getSharedPreferences(TOKENS_KEY, Context.MODE_PRIVATE)
                .edit()
                .putString(API_TOKEN_KEY, t)
                .commit();
        Log.d("login", "token saved:" + t);
    }

    public void logout() {
        GoogleApiClient.ConnectionCallbacks connectedListener = new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(Bundle bundle) {
                Plus.AccountApi.clearDefaultAccount(mPlusClient);
                mPlusClient.disconnect();
            }

            @Override
            public void onConnectionSuspended(int i) {

            }
        };
        mPlusClient = new GoogleApiClient.Builder(getContext(),
                connectedListener,
                new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {

                    }
                })
                .addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();
        mPlusClient.connect();

        SharedPreferences.Editor e = getContext().getSharedPreferences(TOKENS_KEY, Context.MODE_PRIVATE).edit();
        e.clear();
        e.commit();
    }

    public boolean isLoggedIn() {
        final SharedPreferences sp = getContext().getSharedPreferences(TOKENS_KEY, Context.MODE_PRIVATE);
        String token = sp.getString(API_TOKEN_KEY, null);
        return token != null;
    }

    public void loginSocial(String type, Bundle bundle, final LoginCallback cb) {
        Log.d("facebook_access_token_3", "fb_token");
        final HttpPost post = new HttpPost("http://starfishapp.pp.ua/app.php?action=get_token_soc&type=" + type);
        final List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair("type", type));
        JSONObject jo = new JSONObject();
        //{"access_token":"access_token","uid":"uid","email":"email","first_name":"first_name","last_name":"last_name"}
        try {
            for (String s : bundle.keySet()) {
                Log.d("bundle", s + " = " + bundle.get(s));
                jo.put(s, bundle.get(s).toString());
            }
            Log.d("login jo constructed", jo.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        nameValuePairs.add(new BasicNameValuePair("params", jo.toString()));
        AsyncTask<Void, Void, String> login_task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void[] objects) {
                try {

                    Log.d("login response1", "started:" + nameValuePairs.toString());
                    post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse login_response = new DefaultHttpClient().execute(post);
                    final String body = EntityUtils.toString(login_response.getEntity(), "utf-8");
                    Log.d("logining in as ", post.getURI().toString() + ":" + body);
                    return body;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return "ERROR:couln't get login http responce";
            }

            @Override
            protected void onPostExecute(final String response) {
                try {
                    Log.d("login", response);
                    Object ro = (new JSONTokener(response)).nextValue();
                    if (!(ro instanceof JSONObject))
                        return;
                    JSONObject jo = (JSONObject) ro;
                    String token = jo.getString("token");
                    SharedPreferences.Editor e = getContext().getSharedPreferences(TOKENS_KEY, Context.MODE_PRIVATE).edit();
                    e.putString(API_TOKEN_KEY, token);
                    e.commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("login response", response);
                rememberUserInfo(cb, response);
            }
        };

        login_task.execute();

    }

    private void rememberUserInfo(final LoginCallback cb, final String response) {
        Log.d("facebook_access_token_4", "remember user info");
        new AsyncTask<Void, Void, UserInfo>() {

            @Override
            protected UserInfo doInBackground(Void... voids) {
                return loadUserInfo();
            }

            @Override
            protected void onPostExecute(UserInfo userInfo) {
                Log.d("facebook_access_token_5", "remember user info post execute");
                SharedPreferences.Editor e = context.getSharedPreferences(USER_PREFS_KEY, Context.MODE_PRIVATE).edit();
                int settingsSet = userInfo.settingsSet;
                if (settingsSet == 0) {
                    e.putBoolean("settings_set", false);
                }
                Log.d("saving user info", userInfo.toString());
                e.putString(PHOTO_URL_KEY, userInfo.img_url.replaceAll("\\\\", ""));
                e.putInt(ID_COUNTRY_KEY, userInfo.country);
                e.putInt(ID_REGION_KEY, userInfo.region);
                e.putInt(ID_LANGUAGE_KEY, userInfo.lang);
                e.putString(NAME_REGION_KEY, userInfo.region_name);
                e.putString(NAME_COUNTRY_KEY, userInfo.country_name);
                e.putString(NAME_LANGUAGE_KEY, userInfo.lang_name);
                e.putString(USER_NAME_KEY, userInfo.name);
                e.commit();
                Log.d("saved user info", userInfo.toString());
                cb.onResult(response);
            }
        }.execute();
    }

    public Category getCategoryById(int id) {
        Log.d("category by id", "" + id);
        return catById.get(id);
    }

    public Bitmap getCategoryIcon(int id) {
        return catIcons.get(id);
    }

    public String getPhotoUrl() {
        return context.getSharedPreferences(USER_PREFS_KEY, Context.MODE_PRIVATE).getString(PHOTO_URL_KEY, "");
    }

    String getName() {
        SharedPreferences sp = context.getSharedPreferences(USER_PREFS_KEY, Context.MODE_PRIVATE);
        return sp.getString(USER_NAME_KEY, "John Bull");
    }

    public Map<String, List<Map<String, Object>>> loadUserCoupons() {
        final Map<String, List<Map<String, Object>>> M = new HashMap<String, List<Map<String, Object>>>();
        M.put("Active coupons", getActiveCoupons());
        M.put("Used coupons", getUsedCoupons());
        return M;
    }

    private List<Map<String, Object>> getActiveCoupons() {
        final List<Map<String, Object>> L = new LinkedList<Map<String, Object>>();
        final String NAME = "active";
        final CouponsLoadingRequest CLR = new CouponsLoadingRequest(NAME);
        load(L, CLR);
        return L;
    }

    private List<Map<String, Object>> getUsedCoupons() {
        final List<Map<String, Object>> L = new LinkedList<Map<String, Object>>();
        final String NAME = "used";
        final CouponsLoadingRequest CLR = new CouponsLoadingRequest(NAME);
        load(L, CLR);
        return L;
    }

    public UserInfo loadUserInfo() {
        List<UserInfo> ui = new ArrayList<UserInfo>();
        load(ui, new Request<UserInfo>() {
            @Override
            public METHOD getMethod() {
                return METHOD.GET;
            }

            @Override
            public String getAction() {
                return "get_user_info";
            }

            @Override
            public RequestOptions getOptions() {
                return new RequestOptions();
            }

            @Override
            public String getName() {
                return "user";
            }

            @Override
            public UserInfo parseOne(JSONObject jo) throws JSONException {
                // {"user":[{"username":"virtaras@gmail.com","first_name":"Taras","last_name":"Virniy","id_language":"1","id_country":"1","id_region":"1","settings_set":"0","language_name":"\u0420\u0443\u0441\u0441\u043a\u0438\u0439","language_code":"ru","country_title":"\u0423\u043a\u0440\u0430\u0438\u043d\u0430","region_name":"\u041a\u0438\u0435\u0432","img_url":"http:\/\/starfishapp.pp.ua\/images\/users\/19.jpg"}]}
                UserInfo ui = new UserInfo();
                ui.first_name = jo.getString("first_name");
                ui.last_name = jo.getString("last_name");
                ui.name = ui.first_name + " " + ui.last_name;
                ui.img_url = jo.getString("img_url");
                ui.settingsSet = jo.getInt("settings_set");
                ui.lang = jo.getInt(ID_LANGUAGE_KEY);
                ui.country = jo.getInt(ID_COUNTRY_KEY);
                ui.region = jo.getInt(ID_REGION_KEY);
                ui.country_name = jo.getString("country_title");
                ui.region_name = jo.getString("region_name");
                ui.lang_name = jo.getString("language_name");
                ui.telephone = jo.getString("phone");
                ui.email = jo.getString("email");
                ui.cnt_likes = jo.getString("cnt_likes");
                ui.cnt_bonuses = jo.getString("cnt_bonuses");
                Log.d("userobj ", ui.toString());
                return ui;
            }

            @Override
            public void onIntExtra(String name, int value) {

            }
        });

        return ui.get(0);
    }

    List<Language> loadLanguages() {
        List<Language> languages = new ArrayList<Language>();
        load(languages, new Request<Language>() {
            @Override
            public METHOD getMethod() {
                return METHOD.GET;
            }

            @Override
            public String getAction() {
                return "get_languages";
            }

            @Override
            public RequestOptions getOptions() {
                return new RequestOptions();
            }

            @Override
            public String getName() {
                return "languages";
            }

            @Override
            public Language parseOne(JSONObject jo) throws JSONException {
                Language language = new Language();
                language.code = jo.getString("code");
                language.id = jo.getInt("id");
                language.name = jo.getString(USER_NAME_KEY);
                return language;
            }

            @Override
            public void onIntExtra(String name, int value) {

            }
        });
        return languages;
    }

    public List<Language> getLanguages() {
        return loadLanguages();
    }

    public List<Region> getRegions(int selectedCountry) {
        return loadRegions(selectedCountry);
    }

    public UserInfo getUserInfo() {
        UserInfo user = new UserInfo();
        SharedPreferences sp = context.getSharedPreferences(USER_PREFS_KEY, Context.MODE_PRIVATE);
        user.name = sp.getString(USER_NAME_KEY, "null");
        user.img_url = sp.getString(PHOTO_URL_KEY, "");
        user.country_name = sp.getString(NAME_COUNTRY_KEY, "null");
        user.lang_name = sp.getString(NAME_LANGUAGE_KEY, "null");
        user.region_name = sp.getString(NAME_REGION_KEY, "null");
        return user;
    }

    public List<Supplier> getSuppliers(final int CATEGORY_ID) {
        final List<Supplier> L = new LinkedList<Supplier>();
        final SuppliersLoadingRequest SLR = new SuppliersLoadingRequest(CATEGORY_ID);
        load(L, SLR);
        return L;
    }

    public Supplier findSupplier(final int supplier_id) {
        String something = readWebpage("get_supplier", new String[]{
                "supplier_id=" + supplier_id
        });
        try {
            JSONObject resp = (JSONObject) new JSONTokener(something).nextValue();
            JSONObject jo = resp.getJSONObject("supplier");
            Supplier sup = new Supplier(jo);
            return sup;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void saveUserInfo(UserInfo user) {
        Map<String, String> po = new HashMap<String, String>();
        po.put("id_language", String.valueOf(user.lang));
        po.put("id_country", String.valueOf(user.country));
        po.put("id_region", String.valueOf(user.region));
        po.put("phone", user.telephone);
        po.put("email", user.email);
        String response = readWebpage("set_user_info", new String[]{}, po, null, null);
        Log.d("load save response", response);
    }

    public void saveUserInfo(UserInfo user, File file) {
        Map<String, String> po = new HashMap<String, String>();
        po.put("id_language", String.valueOf(user.lang));
        po.put("id_country", String.valueOf(user.country));
        po.put("id_region", String.valueOf(user.region));
        po.put("phone", user.telephone);
        po.put("email", user.email);
        po.put("first_name", user.first_name);
        po.put("last_name", user.last_name);
        String response = readWebpage("set_user_info", new String[]{}, po, "user_img", file);
        Log.d("load save response", response);
    }

    public List<Discount> loadDiscounts() {
        List<Discount> res = new ArrayList<Discount>();
        load(res, new Request<Discount>() {
            @Override
            public METHOD getMethod() {
                return METHOD.GET;
            }

            @Override
            public String getAction() {
                return "get_user_offers";
            }

            @Override
            public RequestOptions getOptions() {
                return new RequestOptions();
            }

            @Override
            public String getName() {
                return "offers";
            }

            @Override
            public Discount parseOne(JSONObject jo) throws JSONException {
                // id,img_url,name,cnt_likes,cnt_bonuses
                Discount d = new Discount();
                d.id = jo.getInt("id");
                d.count_bonuses = 0;//jo.getInt("cnt_bonuses");
                d.count_likes = 0;//jo.getInt("cnt_likes");
                d.img_url = jo.getString("url_img");
                d.name = jo.getString("name");
                return d;
            }

            @Override
            public void onIntExtra(String name, int value) {

            }
        });
        return res;
    }

    public void addOffer(Offer offer, File image) {
        String action = "add_user_offer";
        String[] options = {};
        Map<String, String> po = new HashMap<String, String>();
        // name, description, discount_percent, price_after_discount, location_lat, location_lng
        po.put("name", offer.name);
        po.put("description", offer.description);
        po.put("discount_percent", String.valueOf(offer.discount));
        po.put("price_after_discount", String.valueOf(offer.priceCoupon));
        readWebpage(action, options, po, "offer_img", image);
    }

    public void editOffer(Offer offer, File image) {
        String action = "edit_user_offer";
        String[] options = {"offer_id=" + offer.id};
        Map<String, String> po = new HashMap<String, String>();
        // name, description, discount_percent, price_after_discount, location_lat, location_lng
        po.put("name", offer.name);
        po.put("description", offer.description);
        po.put("discount_percent", String.valueOf(offer.discount));
        po.put("price_after_discount", String.valueOf(offer.priceCoupon));
        readWebpage(action, options, po, "offer_img", image);
    }

    public Offer findOffer(Integer discount_id) {
        String action = "get_user_offer";
        String[] options = {"offer_id=" + discount_id};
        String response = readWebpage(action, options);
        JSONTokener jt = new JSONTokener(response);
        Object o = null;
        try {
            o = jt.nextValue();
            if (!(o instanceof JSONObject)) {
                return null;
            }
            JSONObject jo = ((JSONObject) o).getJSONObject("offer");
            Offer r = new Offer();

            r.name = jo.getString("name");
            r.description = jo.getString("description");
            r.price = jo.getInt("price");
            r.discount = jo.getDouble("discount");
            r.lat = jo.getDouble("location_lat");
            r.lng = jo.getDouble("location_lng");
            r.img_url = jo.getString("url_img");
            return r;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Comment> loadComments(final int offer_id) {
        List<Comment> res = new ArrayList<Comment>();
        load(res, new Request<Comment>() {
            @Override
            public METHOD getMethod() {
                return METHOD.GET;
            }

            @Override
            public String getAction() {
                return "get_offer_comments";
            }

            @Override
            public RequestOptions getOptions() {
                String[] options = {"offer_id=" + offer_id};
                return new RequestOptions(options);
            }

            @Override
            public String getName() {
                return "comments";
            }

            @Override
            public Comment parseOne(JSONObject jo) throws JSONException {
                Comment com = new Comment();
                com.text = jo.getString("comment_info");
                com.user_name = jo.getString("comment_name");
                com.img_url = jo.getString("comment_img");
                com.time = jo.getString("create_date");
                com.id = jo.getInt("id");
                return com;
            }

            @Override
            public void onIntExtra(String name, int value) {

            }
        });
        return res;
    }

    public void sendComment(String com, int offer_id) {
        Map<String, String> po = new HashMap<String, String>();
        po.put("comment_info", com);
        String[] go = {"offer_id=" + offer_id};
        String res = readWebpage("add_offer_comment", go, po);
        try {
            Object o = new JSONTokener(res).nextValue();
            if (!(o instanceof JSONObject)) {
                Log.d("load send comment", "received not json: " + o.toString());
                return;
            }
            JSONObject jo = (JSONObject) o;
            if (jo.has("success")) {
                Log.d("load send comment success", jo.getJSONObject("success").getString("message"));
            }
            if (jo.has("error")) {
                Log.d("load send comment failure", jo.getJSONObject("error").getString("message"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public OfferUserAttitude loadOfferUserAttitude(final int offerId) {
        ArrayList<OfferUserAttitude> rs = new ArrayList<OfferUserAttitude>();
        load(rs, new Request<OfferUserAttitude>() {
            @Override
            public METHOD getMethod() {
                return METHOD.GET;
            }

            @Override
            public String getAction() {
                return "get_offer_user";
            }

            @Override
            public RequestOptions getOptions() {
                String[] go = {"offer_id=" + offerId};
                return new RequestOptions(go);
            }

            @Override
            public String getName() {
                return "offer";
            }

            @Override
            public OfferUserAttitude parseOne(final JSONObject JO) throws JSONException {
                return new OfferUserAttitude(JO);
            }

            @Override
            public void onIntExtra(String name, int value) {
            }
        });

        return rs.get(0);
    }

    public List<Map<String, Object>> loadWishes() {
        ArrayList<Map<String, Object>> res = new ArrayList<Map<String, Object>>();
        load(res, new Request<Map<String, Object>>() {
            @Override
            public METHOD getMethod() {
                return METHOD.GET;
            }

            @Override
            public String getAction() {
                return "get_user_offers_favourites";
            }

            @Override
            public RequestOptions getOptions() {
                return new RequestOptions();
            }

            @Override
            public String getName() {
                return "offers";
            }

            @Override
            public Map<String, Object> parseOne(JSONObject jo) throws Exception {
                Map<String, Object> w = new HashMap<String, Object>();
                w.put("Wish title", jo.getString("name"));
                w.put("Wish description", jo.getString("description"));
                w.put("Wish image url", jo.getString("url_img"));
                w.put("Wish id", jo.getString("id"));
                w.put("Wish price", getWishPriceFrom(jo));
                w.put("Wish time", getWishTimeFrom(jo));
                return w;
            }

            private Object getWishPriceFrom(final JSONObject JO) throws Exception {
                final CharSequence VALUE = getWishPriceValueFrom(JO);
                final SpannableStringBuilder TEXT = new SpannableStringBuilder(VALUE);
                final Map<String, Map<String, Object>> S = getSettingsForPageTranslation();
                final UserWishesListPageTranslator T = new UserWishesListPageTranslator(S);
                final CharSequence HEADER = T.getWishPriceHeader();
                TEXT.append(' ');
                TEXT.append(HEADER);
                return TEXT;
            }

            private CharSequence getWishPriceValueFrom(final JSONObject JO) throws Exception {
                final RelativeSizeSpan WHAT = getRelativeSizeSpanForWishPriceValue();
                final Integer START = 0;
                final Double D = JO.getDouble("pricecoupon");
                final CharSequence PRICE = D + "";
                final SpannableStringBuilder SSB = new SpannableStringBuilder(PRICE);
                final Integer END = SSB.length();
                SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
                return SSB;
            }

            private RelativeSizeSpan getRelativeSizeSpanForWishPriceValue() {
                final Float VALUE_SIZE_FOR_MDPI = 40f;
                final Float HEADER_SIZE_FOR_MDPI = 26.67f;
                final Float PROPORTION = VALUE_SIZE_FOR_MDPI / HEADER_SIZE_FOR_MDPI;
                return new RelativeSizeSpan(PROPORTION);
            }

            private Map<String, Map<String, Object>> getSettingsForPageTranslation() {
                final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
                M.put("Page views ids", getIdsOfPageViews());
                M.put("Page views default texts", getPageViewsDefaultTexts());
                M.put("Other settings", getOtherSettingsForTranslation());
                return M;
            }

            private Map<String, Object> getIdsOfPageViews() {
                return new HashMap<String, Object>();
            }

            private Map<String, Object> getPageViewsDefaultTexts() {
                return new HashMap<String, Object>();
            }

            private Map<String, Object> getOtherSettingsForTranslation() {
                final Map<String, Object> M = new HashMap<String, Object>();
                M.put("Requester", context);
                return M;
            }

            private Object getWishTimeFrom(final JSONObject JO) throws Exception {
                final Map<String, Map<String, Object>> S = getSettingsForPageTranslation();
                final UserWishesListPageTranslator T = new UserWishesListPageTranslator(S);
                final CharSequence DAYS_HEADER = T.getWishTimeInDaysHeader();
                final SpannableStringBuilder TEXT = new SpannableStringBuilder(DAYS_HEADER);
                final CharSequence DAYS_VALUE = getWishTimeInDaysFrom(JO);
                final CharSequence HOURS_HEADER = T.getWishTimeInHoursHeader();
                final CharSequence HOURS_VALUE = getWishTimeInHoursFrom(JO);
                TEXT.append(' ');
                TEXT.append(DAYS_VALUE);
                TEXT.append(' ');
                TEXT.append(HOURS_HEADER);
                TEXT.append(' ');
                TEXT.append(HOURS_VALUE);
                return TEXT;
            }

            private CharSequence getWishTimeInDaysFrom(final JSONObject JO) throws Exception {
                final RelativeSizeSpan WHAT_SIZE = getRelativeSizeSpanForWishTimeValue();
                final ForegroundColorSpan WHAT_COLOR = getForegroundColorSpanForWishTimeValue();
                final Integer START = 0;
                final String DATE = JO.getString("end_time");
                final Date END_DATE = parseDate(DATE);
                final Long DAYS_LEFT = daysTo(END_DATE);
                final CharSequence DAYS = DAYS_LEFT + "";
                final SpannableStringBuilder SSB = new SpannableStringBuilder(DAYS);
                final Integer END = SSB.length();
                SSB.setSpan(WHAT_SIZE, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
                SSB.setSpan(WHAT_COLOR, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
                return SSB;
            }

            private RelativeSizeSpan getRelativeSizeSpanForWishTimeValue() {
                final Float VALUE_SIZE_FOR_MDPI = 35.56f;
                final Float HEADER_SIZE_FOR_MDPI = 22.22f;
                final Float PROPORTION = VALUE_SIZE_FOR_MDPI / HEADER_SIZE_FOR_MDPI;
                return new RelativeSizeSpan(PROPORTION);
            }

            private ForegroundColorSpan getForegroundColorSpanForWishTimeValue() {
                final Integer RED = 250;
                final Integer GREEN = 63;
                final Integer BLUE = 63;
                final int COLOR = rgb(RED, GREEN, BLUE);
                return new ForegroundColorSpan(COLOR);
            }

            private CharSequence getWishTimeInHoursFrom(final JSONObject JO) throws Exception {
                final RelativeSizeSpan WHAT_SIZE = getRelativeSizeSpanForWishTimeValue();
                final ForegroundColorSpan WHAT_COLOR = getForegroundColorSpanForWishTimeValue();
                final Integer START = 0;
                final String DATE = JO.getString("end_time");
                final Date END_DATE = parseDate(DATE);
                final Long HOURS_LEFT = hoursTo(END_DATE);
                final CharSequence HOURS = HOURS_LEFT + "";
                final SpannableStringBuilder SSB = new SpannableStringBuilder(HOURS);
                final Integer END = SSB.length();
                SSB.setSpan(WHAT_SIZE, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
                SSB.setSpan(WHAT_COLOR, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
                return SSB;
            }

            @Override
            public void onIntExtra(String name, int value) {

            }
        });
        return res;
    }

    Date parseDate(String date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.ENGLISH);
        try {
            return df.parse(date);
        } catch (ParseException e) {
            Log.e("shareActivity date parse error", "" + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    long daysTo(Date date) {
        Date now = new Date();
        long diff = date.getTime() - now.getTime();
        final long millisInDay = 1000 * 60 * 60 * 24;
        return diff / millisInDay;
    }

    private long hoursTo(Date date) {
        Date now = new Date();
        long diff = date.getTime() - now.getTime();
        final long millisInHour = 1000 * 60 * 60;
        long hoursInDay = 24;
        return (diff / millisInHour) % hoursInDay;
    }

    public void likeOffer(int offerId) {
        String[] go = {};
        Map<String, String> po = new HashMap<String, String>();
        po.put("offer_id", String.valueOf(offerId));
        readWebpage("add_offer_like", go, po);
    }

    public void dislikeOffer(int offerId) {
        String[] go = {};
        Map<String, String> po = new HashMap<String, String>();
        po.put("offer_id", String.valueOf(offerId));
        readWebpage("add_offer_dislike", go, po);
    }

    public void favOffer(int offerId) {
        String[] go = {};
        Map<String, String> po = new HashMap<String, String>();
        po.put("offer_id", String.valueOf(offerId));
        readWebpage("add_offer_favourite", go, po);
    }

    public void unFavOffer(int offerId) {
        String[] go = {};
        Map<String, String> po = new HashMap<String, String>();
        po.put("offer_id", String.valueOf(offerId));
        readWebpage("remove_offer_favourite", go, po);
    }

    public void unLikeOffer(int offerId) {
        String[] go = {};
        Map<String, String> po = new HashMap<String, String>();
        po.put("offer_id", String.valueOf(offerId));
        readWebpage("remove_offer_like", go, po);
    }

    public void unDislikeOffer(int offerId) {
        String[] go = {};
        Map<String, String> po = new HashMap<String, String>();
        po.put("offer_id", String.valueOf(offerId));
        readWebpage("remove_offer_dislike", go, po);
    }

    public List<Map<String, Object>> loadRecommendations() {
        ArrayList<Map<String, Object>> res = new ArrayList<Map<String, Object>>();
        load(res, new Request<Map<String, Object>>() {
            @Override
            public METHOD getMethod() {
                return METHOD.GET;
            }

            @Override
            public String getAction() {
                return "get_user_offers_likes";
            }

            @Override
            public RequestOptions getOptions() {
                return new RequestOptions();
            }

            @Override
            public String getName() {
                return "offers";
            }

            @Override
            public Map<String, Object> parseOne(JSONObject jo) throws Exception {
                Map<String, Object> w = new HashMap<String, Object>();
                w.put("Recommendation title", jo.getString("name"));
                w.put("Recommendation description", jo.getString("description"));
                w.put("Recommendation image url", jo.getString("url_img"));
                w.put("Recommendation id", jo.getString("id"));
                w.put("Recommendation price", getRecommendationPriceFrom(jo));
                w.put("Recommendation time", getRecommendationTimeFrom(jo));
                return w;
            }

            private Object getRecommendationPriceFrom(final JSONObject JO) throws Exception {
                final CharSequence VALUE = getRecommendationPriceValueFrom(JO);
                final SpannableStringBuilder TEXT = new SpannableStringBuilder(VALUE);
                final Map<String, Map<String, Object>> S = getSettingsForPageTranslation();
                final UserRecommendationsPageTranslator T = new UserRecommendationsPageTranslator(S);
                final CharSequence HEADER = T.getRecommendationPriceHeader();
                TEXT.append(' ');
                TEXT.append(HEADER);
                return TEXT;
            }

            private CharSequence getRecommendationPriceValueFrom(final JSONObject JO) throws Exception {
                final RelativeSizeSpan WHAT = getRelativeSizeSpanForRecommendationPriceValue();
                final Integer START = 0;
                final Double D = JO.getDouble("pricecoupon");
                final CharSequence PRICE = D + "";
                final SpannableStringBuilder SSB = new SpannableStringBuilder(PRICE);
                final Integer END = SSB.length();
                SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
                return SSB;
            }

            private RelativeSizeSpan getRelativeSizeSpanForRecommendationPriceValue() {
                final Float VALUE_SIZE_FOR_MDPI = 40f;
                final Float HEADER_SIZE_FOR_MDPI = 26.67f;
                final Float PROPORTION = VALUE_SIZE_FOR_MDPI / HEADER_SIZE_FOR_MDPI;
                return new RelativeSizeSpan(PROPORTION);
            }

            private Map<String, Map<String, Object>> getSettingsForPageTranslation() {
                final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
                M.put("Page views ids", getIdsOfPageViews());
                M.put("Page views default texts", getPageViewsDefaultTexts());
                M.put("Other settings", getOtherSettingsForTranslation());
                return M;
            }

            private Map<String, Object> getIdsOfPageViews() {
                return new HashMap<String, Object>();
            }

            private Map<String, Object> getPageViewsDefaultTexts() {
                return new HashMap<String, Object>();
            }

            private Map<String, Object> getOtherSettingsForTranslation() {
                final Map<String, Object> M = new HashMap<String, Object>();
                M.put("Requester", context);
                return M;
            }

            private Object getRecommendationTimeFrom(final JSONObject JO) throws Exception {
                final Map<String, Map<String, Object>> S = getSettingsForPageTranslation();
                final UserRecommendationsPageTranslator T = new UserRecommendationsPageTranslator(S);
                final CharSequence DAYS_HEADER = T.getRecommendationTimeInDaysHeader();
                final SpannableStringBuilder TEXT = new SpannableStringBuilder(DAYS_HEADER);
                final CharSequence DAYS_VALUE = getRecommendationTimeInDaysFrom(JO);
                final CharSequence HOURS_HEADER = T.getRecommendationTimeInHoursHeader();
                final CharSequence HOURS_VALUE = getRecommendationTimeInHoursFrom(JO);
                TEXT.append(' ');
                TEXT.append(DAYS_VALUE);
                TEXT.append(' ');
                TEXT.append(HOURS_HEADER);
                TEXT.append(' ');
                TEXT.append(HOURS_VALUE);
                return TEXT;
            }

            private CharSequence getRecommendationTimeInDaysFrom(final JSONObject JO) throws Exception {
                final RelativeSizeSpan WHAT_SIZE = getRelativeSizeSpanForRecommendationTimeValue();
                final ForegroundColorSpan WHAT_COLOR = getForegroundColorSpanForRecommendationTimeValue();
                final Integer START = 0;
                final String DATE = JO.getString("end_time");
                final Date END_DATE = parseDate(DATE);
                final Long DAYS_LEFT = daysTo(END_DATE);
                final CharSequence DAYS = DAYS_LEFT + "";
                final SpannableStringBuilder SSB = new SpannableStringBuilder(DAYS);
                final Integer END = SSB.length();
                SSB.setSpan(WHAT_SIZE, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
                SSB.setSpan(WHAT_COLOR, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
                return SSB;
            }

            private RelativeSizeSpan getRelativeSizeSpanForRecommendationTimeValue() {
                final Float VALUE_SIZE_FOR_MDPI = 35.56f;
                final Float HEADER_SIZE_FOR_MDPI = 22.22f;
                final Float PROPORTION = VALUE_SIZE_FOR_MDPI / HEADER_SIZE_FOR_MDPI;
                return new RelativeSizeSpan(PROPORTION);
            }

            private ForegroundColorSpan getForegroundColorSpanForRecommendationTimeValue() {
                final Integer RED = 250;
                final Integer GREEN = 63;
                final Integer BLUE = 63;
                final int COLOR = rgb(RED, GREEN, BLUE);
                return new ForegroundColorSpan(COLOR);
            }

            private CharSequence getRecommendationTimeInHoursFrom(final JSONObject JO) throws Exception {
                final RelativeSizeSpan WHAT_SIZE = getRelativeSizeSpanForRecommendationTimeValue();
                final ForegroundColorSpan WHAT_COLOR = getForegroundColorSpanForRecommendationTimeValue();
                final Integer START = 0;
                final String DATE = JO.getString("end_time");
                final Date END_DATE = parseDate(DATE);
                final Long HOURS_LEFT = hoursTo(END_DATE);
                final CharSequence HOURS = HOURS_LEFT + "";
                final SpannableStringBuilder SSB = new SpannableStringBuilder(HOURS);
                final Integer END = SSB.length();
                SSB.setSpan(WHAT_SIZE, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
                SSB.setSpan(WHAT_COLOR, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
                return SSB;
            }


            @Override
            public void onIntExtra(String name, int value) {

            }
        });
        return res;
    }

    public enum METHOD {
        GET,
        POST
    }

    public interface Request<T> {
        public METHOD getMethod();

        public String getAction();

        public RequestOptions getOptions();

        public String getName();

        public T parseOne(JSONObject jo) throws JSONException, Exception;

        public void onIntExtra(String name, int value);
    }

    public interface LoginCallback {
        void onResult(String result);
    }

    public static class OffersPage {
        public List<Offer> list;
        public boolean hasMore;

        public OffersPage(List<Offer> list) {
            this.list = list;
        }
    }

    public static class RequestOptions {
        String[] getOptions;
        Map<String, String> postOptions;

        public RequestOptions(String[] strings) {
            getOptions = strings;
        }

        public RequestOptions() {
            getOptions = new String[0];
        }
    }

    abstract class OfferRequest implements Request<Offer> {
        int region_id;
        int[] category_id;

        protected OfferRequest(int region_id, int[] category_id) {
            this.region_id = region_id;
            this.category_id = category_id;
        }


        protected ArrayList<String> getGetOptions() {
            ArrayList<String> result = new ArrayList<String>();
            result.add("region_id=" + region_id);
            if (category_id.length > 0) {
                String s = join();
                result.add("category_id=" + s);
            }
            return result;
        }

        private String join() {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < category_id.length - 1; ++i) {
                sb.append(category_id[i]).append(",");
            }
            sb.append(category_id[category_id.length - 1]);
            return sb.toString();
        }

        @Override
        public String getName() {
            return "offers";
        }

        @Override
        public Offer parseOne(JSONObject jo) throws JSONException {
            Offer offer = new Offer(jo.getInt("id"));
            offer.name = jo.getString(USER_NAME_KEY);
            offer.price = jo.getInt("price");
            offer.priceCoupon = jo.getDouble("pricecoupon");
            offer.discount = jo.getDouble("discount");
            offer.exchange = jo.getString("exchange");
            offer.img_url = jo.getString("url_img");
            offer.url = jo.getString("url");
            offer.category_id = jo.getInt("category_id");
            if (jo.has("location_lat")) {
                offer.lat = jo.getDouble("location_lat");
            }
            if (jo.has("location_lng")) {
                offer.lng = jo.getDouble("location_lng");
            }
            return offer;
        }

    }

    class OfferRequestByBounds extends OfferRequest {
        LatLng topLeft;
        LatLng botRight;

        protected OfferRequestByBounds(int region_id, int[] category_id, LatLng topLeft, LatLng botRight) {
            super(region_id, category_id);
            this.topLeft = topLeft;
            this.botRight = botRight;
        }

        @Override
        public String getAction() {
            return "get_offers_map";
        }

        @Override
        public METHOD getMethod() {
            return METHOD.GET;
        }

        @Override
        public RequestOptions getOptions() {
            ArrayList<String> result = getGetOptions();
            result.add("map_top_left_x=" + topLeft.latitude);
            result.add("map_top_left_y=" + topLeft.longitude);
            result.add("map_bottom_right_x=" + botRight.latitude);
            result.add("map_bottom_right_y=" + botRight.longitude);
            RequestOptions ro = new RequestOptions(result.toArray(new String[result.size()]));
            return ro;
        }

        @Override
        public void onIntExtra(String name, int value) {

        }
    }

    class OfferRequestBySearchText extends OfferRequest {
        String searchText;
        int page;
        int pagesize;
        OffersPage result;

        public OfferRequestBySearchText(OffersPage r, int region_id, int[] category_id, int page, int itemsPerPage, String text) {
            super(region_id, category_id);
            searchText = text;
            this.page = page;
            this.pagesize = itemsPerPage;
            result = r;
        }

        @Override
        public String getAction() {
            return "get_offers";
        }

        @Override
        public METHOD getMethod() {
            return METHOD.GET;
        }

        @Override
        public RequestOptions getOptions() {
            ArrayList<String> result = getGetOptions();
            result.add("page=" + page);
            result.add("pagecount=" + pagesize);
            if (searchText != null)
                result.add("q=" + URLEncoder.encode(searchText));
            RequestOptions ro = new RequestOptions(result.toArray(new String[result.size()]));
            return ro;

        }

        @Override
        public void onIntExtra(String name, int value) {
            if (name.equals("has_more")) {
                result.hasMore = (value == 1);
            }
        }
    }

}
