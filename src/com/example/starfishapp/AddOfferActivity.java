package com.example.starfishapp;

import android.app.Activity;
import android.content.*;
import android.os.*;
import android.support.v7.app.*;
import android.view.*;
import android.widget.*;
import com.example.starfishapp.model.*;
import com.example.starfishapp.R.*;
import com.example.starfishapp.utilities.*;
import com.google.android.gms.maps.model.*;
import com.googlion.shield.specialists.*;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;
import com.loopj.android.image.*;
import java.io.*;
import java.net.*;
import java.util.*;

public class AddOfferActivity extends ActionBarActivity {
    public static final String EDIT_KEY = "edit";
    private Activity self = this;
    private EditText nameView;
    private EditText descriptionView;
    private EditText discountView;
    private EditText priceView;
    private SmartImageView imageView;
    private TextView locationView;
    private boolean newOffer;
    private ImagePicker picker;

    // name, description, discount_percent, price_after_discount, location_lat, location_lng
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.add_share_page);
        setUpActionBar();
        setFontsForAllTextualViewsOfThisPage();
        scaleThisPage();
        translateThisPage();
        setupViews();
        Intent intent = getIntent();
        int toEdit = intent.getIntExtra(EDIT_KEY, -1);
        newOffer = (toEdit == -1);
        new AsyncTask<Integer, Void, Offer>() {

            @Override
            protected Offer doInBackground(Integer... discount_ids) {
                return Starfish.getInstance(self).findOffer(discount_ids[0]);
            }

            @Override
            protected void onPostExecute(Offer offer) {
                if (offer != null) {
                    fillInViews(offer);
                }
            }
        }.execute(toEdit);
    }

    private void setUpActionBar() {
        final ActionBar AB = getSupportActionBar();
        final boolean B = true;
        AB.setDisplayHomeAsUpEnabled(B);
    }

    private void setFontsForAllTextualViewsOfThisPage() {
        final Map<Object, Map> S = getSettingsForSettingFonts();
        final FontSpecialist FS = new FontSpecialist(S);
        FS.setFonts();
    }

    private Map<Object, Map> getSettingsForSettingFonts() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingsFonts());
        M.put("Other settings", getOtherSettingsForSettingsFonts());
        return M;
    }

    private Map getMainSettingsForSettingsFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvSelectedImage, "kelsonsansboldru.otf");
        M.put(id.bLoadImage, "robotobold.otf");
        M.put(id.tvNewShareName, "kelsonsansboldru.otf");
        M.put(id.etNewShareName, "kelsonsanslightru.otf");
        M.put(id.tvNewShareDescription, "kelsonsansboldru.otf");
        M.put(id.etNewShareDescription, "kelsonsanslightru.otf");
        M.put(id.tvDiscountSize, "kelsonsansboldru.otf");
        M.put(id.etDiscountSize, "kelsonsanslightru.otf");
        M.put(id.tvDiscountPrice, "kelsonsansboldru.otf");
        M.put(id.etDiscountPrice, "kelsonsanslightru.otf");
        M.put(id.tvNewShareLocation, "kelsonsansboldru.otf");
        M.put(id.etNewShareLocation, "kelsonsanslightru.otf");
        M.put(id.bAddNewShare, "robotoregular0.otf");
        return M;
    }

    private Map getOtherSettingsForSettingsFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", AddOfferActivity.this);
        return M;
    }

    private void scaleThisPage() {
        final Activity REQUESTER = AddOfferActivity.this;
        PageScaler PS = new PageScaler(REQUESTER);
        PS.scaleThePage();
    }

    private void translateThisPage() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForUserBillPageTranslation();
        final Translator T = new TranslatorOfThePageWithActionBar(SETTINGS);
        T.translate();
    }

    private Map<String, Map<String, Object>> getSettingsForUserBillPageTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getPageViewsIds());
        M.put("Page views default texts", getPageViewsDefaultTexts());
        M.put("Other settings", getOtherSettings());
        return M;
    }

    private Map<String, Object> getPageViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvSelectedImage", id.tvSelectedImage);
        M.put("bLoadImage", id.bLoadImage);
        M.put("tvNewShareName", id.tvNewShareName);
        M.put("tvNewShareDescription", id.tvNewShareDescription);
        M.put("tvDiscountSize", id.tvDiscountSize);
        M.put("tvDiscountPrice", id.tvDiscountPrice);
        M.put("etDiscountPrice", id.etDiscountPrice);
        M.put("tvNewShareLocation", id.tvNewShareLocation);
        M.put("bAddNewShare", id.bAddNewShare);
        return M;
    }

    private Map<String, Object> getPageViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvSelectedImage", "Світлина");
        M.put("bLoadImage", "Завантажити світлину");
        M.put("tvNewShareName", "Назва акції");
        M.put("tvNewShareDescription", "Опис акції");
        M.put("tvDiscountSize", "Розмір знижки");
        M.put("tvDiscountPrice", "Ціна зі знижкою");
        M.put("etDiscountPrice", "грн");
        M.put("tvNewShareLocation", "Місцезнаходження");
        M.put("bAddNewShare", "Додати акцію");
        return M;
    }

    private Map<String, Object> getOtherSettings() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", AddOfferActivity.this);
        M.put("Page title translation key", "Add share page action bar title");
        M.put("Page title default text", "Додати акцію");
        return M;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        picker.onActivityResult(requestCode, resultCode, data);
    }

    private void fillInViews(Offer toEdit) {
        nameView.setText(toEdit.name);
        descriptionView.setText(toEdit.description);
        discountView.setText(String.valueOf(toEdit.discount));
        priceView.setText(String.valueOf(toEdit.priceCoupon));
        imageView.setImageUrl(toEdit.img_url);
    }

    private void setupViews() {
        nameView = (EditText) findViewById(id.etNewShareName);
        descriptionView = (EditText) findViewById(id.etNewShareDescription);
        discountView = (EditText) findViewById(id.etDiscountSize);
        priceView = (EditText) findViewById(id.etDiscountPrice);
        imageView = (SmartImageView) findViewById(id.sivSelectedImage);
        locationView = (TextView) findViewById(id.tvNewShareLocation);
        picker = new ImagePicker(this, imageView);
        ImageButton mapButton = (ImageButton) findViewById(id.ibShowMap);
        Button addButton = (Button) findViewById(id.bAddNewShare);
        Button loadImage = (Button) findViewById(id.bLoadImage);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitUserOffer();
            }
        });

        loadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.pick();
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.pick();
            }
        });

        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: what?
            }
        });
    }

    public boolean isNewOffer() {
        return newOffer;
    }

    private void submitUserOffer() {
        final Offer offer = new Offer();
        try {
            offer.name = URLEncoder.encode(nameView.getText().toString(), "UTF-8");
            offer.description = URLEncoder.encode(descriptionView.getText().toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        offer.priceCoupon = Double.parseDouble(priceView.getText().toString());
        offer.discount = Double.parseDouble(discountView.getText().toString());
        String address = locationView.getText().toString();
        new AsyncTask<String, Void, LatLng>() {

            @Override
            protected LatLng doInBackground(String... params) {
                GeocoderWrapper gc = new GeocoderWrapper(self);
                return gc.getLatLng(params[0]);
                //TODO: what if several addresses are applicable?
            }

            @Override
            protected void onPostExecute(LatLng latLng) {
                if (latLng != null) {
                    offer.lat = latLng.latitude;
                    offer.lng = latLng.longitude;
                }
                sendOffer(offer);
            }
        }.execute(address);

    }

    private void sendOffer(final Offer offer) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                if (isNewOffer()) {
                    Starfish.getInstance(self).addOffer(offer, picker.getImage());
                } else {
                    Starfish.getInstance(self).editOffer(offer, picker.getImage());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                finish();
            }
        }.execute();
    }

}
