package com.example.starfishapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.AdapterView.*;
import android.view.View;
import android.content.DialogInterface.*;
import android.widget.*;
import com.example.starfishapp.model.Category;
import com.example.starfishapp.model.Country;
import com.example.starfishapp.model.Region;
import com.googlion.shield.adapters.*;
import com.googlion.shield.utilities.PageScaler;
import android.app.AlertDialog.*;
import com.example.starfishapp.R.*;

import static android.widget.ListView.CHOICE_MODE_MULTIPLE;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CategorySelector {
    int selected_region = 0;
    int selected_country = 0;
    boolean[] selected_categories;
    private List<Category> categories;
    private List<Country> countries;
    private List<Region> regions;
    private ShieldArrayAdapter<Region> regions_adapter;
    private ShieldArrayAdapter<Country> countries_adapter;
    private ArrayAdapter<Country> categories_adapter;
    private CharSequence[] categories_list;
    private onCategoryChoiceListener categories_listener;
    private Context mContext;
    private PageScaler pageScaler;
    private onRegionSelectedListener regionsListener = new onRegionSelectedListener() {
        @Override
        public void onRegionSelected(Region r) {

        }
    };
    private ArrayList<ShieldArrayAdapter<Region>> adapters = new ArrayList<ShieldArrayAdapter<Region>>();

    public CategorySelector(Context mContext, final PageScaler PS) {
        this.mContext = mContext;
        pageScaler = PS;
        setupSpinners();
    }

    public void onEverythingLoaded() {

    }

    public final Country getSelectedCountry() {
        if (countries.isEmpty()) {
            //TODO: what is better?
//            throw new Exception("Countries not loaded or empty");
            return null;
        }
        return countries.get(selected_country);
    }

    public final Region getSelectedRegion() {
        Country selectedCountry = null;
        selectedCountry = getSelectedCountry();
        if (selectedCountry == null || selectedCountry.regions.isEmpty()) {
            return null;
        }
        return selectedCountry.regions.get(selected_region);
    }

    public void setCategorySelected(int s, boolean b) {
        selected_categories[s] = b;
    }

    private void setupSpinners() {
        AsyncTask<Object, Object, Object> task = new AsyncTask<Object, Object, Object>() {
            @Override
            protected Object doInBackground(Object[] params) {
                Log.d("loading icons", "before getCat");
                categories = Starfish.getInstance(getContext()).getCategories();
                countries = Starfish.getInstance(getContext()).getCountries();
                regions = Starfish.getInstance(getContext()).getRegions();
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                categories_list = new String[categories.size()];
                selected_categories = new boolean[categories.size()];
                for (int i = 0, categoriesSize = categories.size(); i < categoriesSize; i++) {
                    Category cat = categories.get(i);
                    categories_list[i] = cat.name;
                }
                countries_adapter = new ShieldArrayAdapter<Country>(getContext(), R.layout.googlion_select_dialog_item, countries, pageScaler);
                for (Country country : countries) {
                    List<Region> cities = country.regions;
                    final ShieldArrayAdapter<Region> cities_adapter_temp = new ShieldArrayAdapter<Region>(getContext(), R.layout.googlion_select_dialog_item, cities, pageScaler);
                    adapters.add(cities_adapter_temp);
                }
                if (!adapters.isEmpty()) {
                    regions_adapter = adapters.get(0);
                }
                onEverythingLoaded();
            }
        };
        task.execute();
    }

    void injectOnCategoriesListener(View categoriesV) {
        categoriesV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUpCategoriesAlertDialog();
                //NOTE Ігорів code (який не дозволяє масштабування) знаходиться у method'і setUpAlertDialog()
                //setUpAlertDialog();
            }

            private void setUpCategoriesAlertDialog() {
                final Builder B = getBuilderWithAdapterSetUp();
                final AlertDialog AD = B.create();
                final ListView LV = AD.getListView();
                final OnItemClickListener ITEM_CLICK_LISTENER = new OnItemClickListener() {
                    @Override
                    public void onItemClick(final AdapterView<?> PARENT, final View V, final int POSITION, final long ID) {
                        final CheckedTextView CTV = (CheckedTextView) V;
                        final boolean SELECTED_VIEW_IS_CHECKED = CTV.isChecked();
                        final Category SELECTED_CATEGORY = categories.get(POSITION);
                        setCategorySelected(POSITION, SELECTED_VIEW_IS_CHECKED);
                        categories_listener.onCategoryChoice(SELECTED_CATEGORY, SELECTED_VIEW_IS_CHECKED);
                    }
                };
                final OnShowListener SHOW_LISTENER = new OnShowListener() {

                    @Override
                    public void onShow(final DialogInterface DIALOG) {
                        final int SIZE = LV.getCount();
                        for (int i = 0; i < SIZE; ++i) {
                            LV.setItemChecked(i, selected_categories[i]);
                        }
                    }
                };
                LV.setChoiceMode(CHOICE_MODE_MULTIPLE);
                LV.setOnItemClickListener(ITEM_CLICK_LISTENER);
                AD.setOnShowListener(SHOW_LISTENER);
                AD.show();
            }

            private Builder getBuilderWithAdapterSetUp() {
                final Context C = mContext;
                final Builder B = new Builder(C);
                final int RESOURCE = layout.googlion_select_dialog_multichoice;
                final CharSequence[] OBJECTS = categories_list;
                final PageScaler PS = pageScaler;
                final ListAdapter LA = new ShieldArrayAdapter<CharSequence>(C, RESOURCE, OBJECTS, PS);
                final OnClickListener NO_LISTENER = null;
                B.setAdapter(LA, NO_LISTENER);
                return B;
            }

            private void setUpAlertDialog() {
                final AlertDialog.Builder B = new AlertDialog.Builder(getContext());
                B.setMultiChoiceItems(categories_list, selected_categories, new OnMultiChoiceClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                        setCategorySelected(i, b);
                        categories_listener.onCategoryChoice(categories.get(i), b);
                    }
                });
                final AlertDialog AD = B.create();
                AD.show();
            }
        });
    }

    void injectOnCountriesListener(View countries) {
        countries.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder b = new AlertDialog.Builder(getContext());
                b.setSingleChoiceItems(countries_adapter, selected_country, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        selected_country = i;
                        regions_adapter = adapters.get(i);
                        dialogInterface.dismiss();
                    }
                });
                b.create().show();
            }
        });
    }

    void injectOnRegionsListener(View regions) {
        regions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder b = new AlertDialog.Builder(getContext());
                b.setSingleChoiceItems(regions_adapter, selected_region, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        selected_region = i;
                        Log.d("Region selected", i + ":" + CategorySelector.this.regions.get(i));
                        Country selected = getSelectedCountry();
                        if (selected != null && !selected.regions.isEmpty()) {
                            regionsListener.onRegionSelected(selected.regions.get(i));
                        }
                        dialogInterface.dismiss();
                    }
                });
                b.create().show();
            }
        });//Adapter(regions_adapter);
    }

    private Context getContext() {
        return mContext;
    }

    public int[] getSelectedCategoriesIds() {
        List<Integer> cats = getSelectedIndices();
        int[] ids = new int[cats.size()];
        for (int i = 0; i < ids.length; ++i) {
            ids[i] = categories.get(cats.get(i)).id;
        }
        return ids;
    }

    private List<Integer> getSelectedIndices() {
        List<Integer> selection = new LinkedList<Integer>();
        for (int i = 0; i < categories.size(); ++i) {
            if (selected_categories[i]) {
                selection.add(i);
            }
        }
        return selection;
    }

    public void setCategoriesListener(final onCategoryChoiceListener listener) {
        categories_listener = listener;
    }

    public void setRegionSelectedListener(final onRegionSelectedListener listener) {
        regionsListener = listener;
    }

    public boolean isRegionSelected() {
        return countries.size() > selected_country && getSelectedCountry().regions.size() > selected_region;
    }


    public interface onCategoryChoiceListener {
        public void onCategoryChoice(Category category, boolean on);
    }

    public interface onRegionSelectedListener {
        public void onRegionSelected(Region r);
    }
}
