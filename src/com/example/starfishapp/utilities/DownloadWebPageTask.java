package com.example.starfishapp.utilities;

import android.content.Context;
import android.util.Log;
import com.example.starfishapp.Starfish;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DownloadWebPageTask {

    public String executeWithToken(Context ctx, String... urls) {
        String response = "";
        for (String url : urls) {
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Authorization", "Token " + Starfish.getInstance(ctx).getToken());
            try {
                HttpResponse httpResponse = client.execute(httpGet);
                for (Header h : httpGet.getAllHeaders()) {
                    Log.d("gheader", h.toString());
                }
                BasicResponseHandler rh = new BasicResponseHandler();
                response = rh.handleResponse(httpResponse);
            } catch (HttpResponseException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.d("getresponse", response);
        return response;
    }
    public String executePostWithToken(Context ctx, String urls, Map<String, String> pairs) {
        return executePostWithToken(ctx, urls, pairs, null, null);
    }
    public String executePostWithToken(Context ctx, String urls, Map<String, String> pairs, String name, File file) {
        String response = "";
        DefaultHttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(urls);
        final List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        if(file!= null) {
            FileBody fileBody = new FileBody(file);
            reqEntity.addPart(name, fileBody);
        }
        try {
            for (Map.Entry<String, String> p : pairs.entrySet()) {
                reqEntity.addPart(p.getKey(), new StringBody(p.getValue()));
            }
            httpPost.setEntity(reqEntity);
            Log.d("set_user_info ent", httpPost.getEntity().toString());
        } catch (UnsupportedEncodingException e) {

            Log.d("set_user_info ent", "unsupported ex");
            e.printStackTrace();
        }
        httpPost.setHeader("Authorization", "Token " + Starfish.getInstance(ctx).getToken());
        try {
            HttpResponse httpResponse = client.execute(httpPost);
            for (Header h : httpPost.getAllHeaders()) {
                Log.d("gheader", h.toString());
            }
            BasicResponseHandler rh = new BasicResponseHandler();
            response = rh.handleResponse(httpResponse);
        } catch (HttpResponseException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d("getresponse", response);
        return response;
    }

    public String executeWOToken(String... urls) {
        String response = "";
        for (String url : urls) {
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);
            try {
                HttpResponse httpResponse = client.execute(httpGet);
                BasicResponseHandler rh = new BasicResponseHandler();
                response = rh.handleResponse(httpResponse);
                Log.d("getresponse to ", url + ":" + response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return response;
    }


    public String executePostWithToken(Context context, String url, File imageFile) {
        String response = "";
        DefaultHttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);
        FileBody fileBody = new FileBody(imageFile);
        MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        reqEntity.addPart("user_img", fileBody);
        httpPost.setEntity(reqEntity);
        Log.d("set_user_info ent", httpPost.getEntity().toString());
        httpPost.setHeader("Authorization", "Token " + Starfish.getInstance(context).getToken());
        try {
            HttpResponse httpResponse = client.execute(httpPost);
            BasicResponseHandler rh = new BasicResponseHandler();
            response = rh.handleResponse(httpResponse);
        } catch (HttpResponseException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d("getresponse", response);
        return response;

    }
}
