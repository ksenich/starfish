package com.example.starfishapp.utilities;

import android.app.*;
import android.content.*;
import com.example.starfishapp.pages.*;

public class WelcomePageTimer extends Thread {

    private int threeSeconds;
    private Activity welcomePage;

    public WelcomePageTimer(final Activity REQUESTER) {
        welcomePage = REQUESTER;
        threeSeconds = 3000;
    }

    @Override
    public void run() {
        showScreenForThreeSeconds();
        goToTheEntryPage();
        closeTheWelcomePage();
    }

    private void showScreenForThreeSeconds() {
        try {
            sleep(threeSeconds);
        } catch (final Exception E) {

        }
    }

    private void goToTheEntryPage() {
//		final Class<?> C = EntryPage.class;
//		final Intent I = new Intent(welcomePage, C);
//		welcomePage.startActivity(I);
    }

    private void closeTheWelcomePage() {
        welcomePage.finish();
    }
}