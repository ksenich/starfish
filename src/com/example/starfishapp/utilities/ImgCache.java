package com.example.starfishapp.utilities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import com.example.starfishapp.model.Category;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;

import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

public class ImgCache {
    final Map<String, Bitmap> cache = new HashMap<String, Bitmap>();

    public void bind(String url, ImageView view) {
        Log.d("loading picture", ""+url);
        ImgDownload download = new ImgDownload(url, view);
        download.execute();
    }

    public class ImgDownload extends AsyncTask<Object, Void, Bitmap> {
        final private String requestUrl;
        final private ImageView view;
//    private Bitmap pic;

        public ImgDownload(String requestUrl, ImageView view) {
            this.requestUrl = requestUrl;
            this.view = view;
        }

        @Override
        protected Bitmap doInBackground(Object... objects) {
            Bitmap bm = cache.get(requestUrl);
            if (bm == null) {
                try {
                    URL url = new URL(requestUrl);
                    URLConnection conn = url.openConnection();
                    return BitmapFactory.decodeStream(conn.getInputStream());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            return bm;
        }

        @Override
        protected void onPostExecute(Bitmap pic) {
            view.setImageBitmap(pic);
            cache.put(requestUrl, pic);
        }
    }
}
