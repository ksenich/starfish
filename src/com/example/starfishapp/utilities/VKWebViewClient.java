package com.example.starfishapp.utilities;

import static com.example.starfishapp.Starfish.*;
import static java.net.URLDecoder.*;

import java.net.*;
import java.util.*;

import android.app.*;
import android.content.*;
import android.graphics.*;
import android.webkit.*;
import com.example.starfishapp.*;

public class VKWebViewClient extends WebViewClient {

    private Activity requester;
    private String applicationHost;
    private Map<String, String> referencePairs;

    public VKWebViewClient(final Activity REQUESTER) {
        requester = REQUESTER;
        applicationHost = "starfish.pp.ua";
        referencePairs = new HashMap<String, String>();
    }

    @Override
    public void onPageStarted(final WebView VIEW, final String URL,
                              final Bitmap FAVICON) {
        try {
            performOnPageStartedWith(URL);
        } catch (final Exception E) {
        } finally {
            super.onPageStarted(VIEW, URL, FAVICON);
        }
    }

    private void performOnPageStartedWith(final String URL) throws Exception {
        final URL WEB_ADDRESS = new URL(URL);
        final String REFERENCE = WEB_ADDRESS.getRef();
        final String HOST = WEB_ADDRESS.getHost();
        if (applicationHost.equals(HOST)) {
            splitParameters(REFERENCE);
        }
    }

    private void splitParameters(final String REFERENCE)
            throws Exception {
//		final String[] PAIRS = REFERENCE.split("&");
//		final Class<?> C = MainActivity.class;
//		final Intent I = new Intent(requester, C);
//		final Starfish S = getInstance();
//		final UserInfo USER = S.getUserInfo();
//		for (String pair : PAIRS) {
//			splitParameter(pair);
//		}
//        final String ACCESS_TOKEN = referencePairs.get("access_token");
//        final String USER_ID = referencePairs.get("user_id");
//        final String EXPIRES_IN = referencePairs.get("expires_in");
////        S.loginVk(ACCESS_TOKEN, USER_ID, EXPIRES_IN);
//		I.putExtra("user", USER);
//		requester.startActivity(I);
//		requester.finish();
    }

    private void splitParameter(final String PAIR) throws Exception {
        final int INDEX = PAIR.indexOf("=");
        final String CHARSET_NAME = "UTF-8";
        final int START = 0;
        final int END = INDEX;
        final String STRING_FOR_KEY = PAIR.substring(START, END);
        final String KEY = decode(STRING_FOR_KEY, CHARSET_NAME);
        final int BEGINNING = INDEX + 1;
        final String STRING_FOR_VALUE = PAIR.substring(BEGINNING);
        final String VALUE = decode(STRING_FOR_VALUE, CHARSET_NAME);
        referencePairs.put(KEY, VALUE);
    }
}
