package com.example.starfishapp.utilities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.ImageView;

import java.io.File;

//It IS kinda weird class
//hard to use
//brittle
//dunno what to do
public class ImagePicker {
    private static final int PICK_IMAGE_REQUEST_CODE = 1;

    private File image;
    private ImageView view;
    private Activity context;

    public ImagePicker(Activity activity, ImageView imageView) {
        context = activity;
        view = imageView;
    }

    public boolean hasResult(){
        return image != null;
    }

    public File getImage(){
        return image;
    }

    /**
     * Starts new Activity that picks image
     */
    public void pick(){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        context.startActivityForResult(photoPickerIntent, PICK_IMAGE_REQUEST_CODE);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data){
        switch (requestCode) {
            case PICK_IMAGE_REQUEST_CODE: {
                if (resultCode == Activity.RESULT_OK) {
                    Uri selectedImage = data.getData();
                    String filePath = getPath(selectedImage);
                    String file_extn = filePath.substring(filePath.lastIndexOf(".") + 1);
                    if (file_extn.equals("img") || file_extn.equals("jpg") || file_extn.equals("jpeg") || file_extn.equals("gif") || file_extn.equals("png")) {
                        view.setImageURI(selectedImage);
                        image = new File(filePath);
                    } else {
                        //NOT IN REQUIRED FORMAT
                    }
                }
            }
        }
    }

    private String getPath(Uri uri) {
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = context.managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        String imagePath = cursor.getString(column_index);
        return imagePath;
    }
}
