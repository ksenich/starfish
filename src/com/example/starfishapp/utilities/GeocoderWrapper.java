package com.example.starfishapp.utilities;


import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

//It seems that geocoder isn't available on every device
//So this class uses plain old http request fallback
//if any problems arise
public class GeocoderWrapper {
    Context context;

    public GeocoderWrapper(Context context) {
        this.context = context;
    }

    public String getAddress(double lat, double lng){
        if(Geocoder.isPresent()){
            try {
                Geocoder gc = new Geocoder(context);
                List<Address> guesses = gc.getFromLocation(lat, lng, 1);
                if(guesses!=null && !guesses.isEmpty()){
                    return guesses.get(0).getAddressLine(0);
                }else{
                    return fallbackGetAddress(lat, lng);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            return fallbackGetAddress(lat, lng);
        }
        return null;
    }

    private String fallbackGetAddress(double lat, double lng) {
        return null;
    }

    public LatLng getLatLng(String address){
        if(Geocoder.isPresent()){
            try {
                Geocoder gc = new Geocoder(context);
                List<Address> addresses = gc.getFromLocationName(address, 1);
                if(addresses != null && !addresses.isEmpty()){
                    Address guess = addresses.get(0);
                    return new LatLng(guess.getLatitude(), guess.getLongitude());
                }else{
                    return fallbackGetLatLng(address);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            return fallbackGetLatLng(address);
        }
        return null;

    }

    private LatLng fallbackGetLatLng(String address) {
        String response = new DownloadWebPageTask().executeWOToken("google maps api get"+ URLEncoder.encode(address));//TODO: find that url
        //TODO: parse json and find latlng
        return null;
    }

}
