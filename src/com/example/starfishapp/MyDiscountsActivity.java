package com.example.starfishapp;

import android.content.*;
import android.os.*;
import android.support.v7.app.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import com.example.starfishapp.model.*;
import com.example.starfishapp.R.*;
import com.googlion.shield.adapters.*;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;
import com.loopj.android.image.*;

import java.util.*;

public class MyDiscountsActivity extends ActionBarActivity {
    public static String DISCOUNTED_OFFERS_KEY = "discounts_on";
    private List<Discount> discounts = new ArrayList<Discount>();
    private List<Discount> downloadedDiscountsList;
    private Context self;
    private List<Map<String, String>> discountsList;
    private ListView discountsLV;
    private PageScaler pageScaler;
    private int currentDiscountListItemPosition;
    private Discount currentDiscount;
    private BaseAdapter discountsAdapter = new BaseAdapter() {

        @Override
        public int getCount() {
            return downloadedDiscountsList.size();
        }

        @Override
        public Discount getItem(int position) {
            return downloadedDiscountsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Discount discount = downloadedDiscountsList.get(position);
            View view;
            if (convertView != null) {
                view = convertView;
            } else {
                view = getLayoutInflater().inflate(layout.user_recommendations_page_list_item, null);
            }
            TextView nameView = (TextView) view.findViewById(id.tvUserRecommendationOneTitle);
            nameView.setText(discount.name);
            TextView confirmationStatus = (TextView) view.findViewById(id.tvUserRecommendationOneDiscountStatus);
            ImageView confirmationImage = (ImageView) view.findViewById(id.ivUserRecommendationOneDiscountStatus);
            Discount.Status conS = discount.count_likes > 4 ? Discount.Status.OK : Discount.Status.FAIL;
            confirmationStatus.setText(getString(conS.confirmation_resource));
            confirmationStatus.setTextColor(conS.color);
            confirmationImage.setImageResource(conS.image_resource);
            TextView bonusStatus = (TextView) view.findViewById(id.tvUserRecommendationOneBonusesStatus);
            ImageView bonusImage = (ImageView) view.findViewById(id.ivUserRecommendationOneBonusesStatus);
            Discount.Status bonS = discount.count_bonuses > 0 ? Discount.Status.OK : Discount.Status.FAIL;
            bonusStatus.setText(getString(bonS.bonus_resource));
            bonusStatus.setTextColor(bonS.color);
            bonusImage.setImageResource(bonS.image_resource);
            SmartImageView image = (SmartImageView) view.findViewById(id.ivUserRecommendationOneImage);
            image.setImageUrl(discount.img_url);
            return view;

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.user_recommendations_page);
        initializeFields();
        setUpActionBar();
        setUpAddShareButton();
        scaleThisPage();
        translateThisPage();
        loadDiscounts();
        setOnItemClickListenerForDiscountsLV();
    }

    private void initializeFields() {
        downloadedDiscountsList = new ArrayList<Discount>();
        self = MyDiscountsActivity.this;
        discountsList = new LinkedList<Map<String, String>>();
        discountsLV = (ListView) findViewById(id.lvDiscounts);
        pageScaler = new PageScaler(MyDiscountsActivity.this);
        currentDiscountListItemPosition = 0;
        currentDiscount = new Discount();
    }

    private void setUpActionBar() {
        final ActionBar AB = getSupportActionBar();
        final boolean B = true;
        AB.setDisplayHomeAsUpEnabled(B);
    }

    private void setUpAddShareButton() {
        final View V = findViewById(id.bAddShare);
        final OnClickListener L = new OnClickListener() {
            @Override
            public void onClick(final View V) {
                final Context CT = self;
                final Class<?> C = AddOfferActivity.class;
                final Intent I = new Intent(CT, C);
                startActivity(I);
            }
        };
        V.setOnClickListener(L);
    }

    private void scaleThisPage() {
        pageScaler.scaleThePage();
    }

    private void translateThisPage() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForUserBillPageTranslation();
        final Translator T = new TranslatorOfThePageWithActionBar(SETTINGS);
        T.translate();
    }

    private Map<String, Map<String, Object>> getSettingsForUserBillPageTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getPageViewsIds());
        M.put("Page views default texts", getPageViewsDefaultTexts());
        M.put("Other settings", getOtherSettings());
        return M;
    }

    private Map<String, Object> getPageViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("bAddShare", id.bAddShare);
        return M;
    }

    private Map<String, Object> getPageViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("bAddShare", "+ Додати акцію");
        return M;
    }

    private Map<String, Object> getOtherSettings() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", MyDiscountsActivity.this);
        M.put("Page title translation key", "User shares page action bar title");
        M.put("Page title default text", "Мої акції");
        return M;
    }

    private void loadDiscounts() {
        new AsyncTask<Void, Void, List<Discount>>() {

            @Override
            protected List<Discount> doInBackground(final Void... PARAMETERS) {
                return Starfish.getInstance(self).loadDiscounts();
            }

            @Override
            protected void onPostExecute(final List<Discount> DISCOUNTS) {
                downloadedDiscountsList = DISCOUNTS;
                discountsLV.setAdapter(discountsAdapter);
//                addDiscountsToList();
//                setListAdapterForDiscountsLV();
//                scaleDiscountsLVDividerHeight();
                setListenerForDiscountsLV();
            }
        }.execute();
    }

    private void setListenerForDiscountsLV() {
        discountsLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(self, AddOfferActivity.class);
                intent.putExtra(AddOfferActivity.EDIT_KEY, downloadedDiscountsList.get(position).id);
                startActivity(intent);
            }
        });
    }

    private void addDiscountsToList() {
        // TODO Не зроблена server'на частина сторінки. Тому частина рядків закоментована і на їх місці стоять заглушки.
        final int SIZE = 10;//= downloadedDiscountsList.size();
        currentDiscountListItemPosition = 0;
        for (int i = 0; i < SIZE; ++i) {
            addDiscountToList();
        }
        pageScaler.scaleThePage();
    }

    private void addDiscountToList() {
        // TODO Не зроблена server'на частина сторінки. Тому частина рядків закоментована і на їх місці стоять заглушки.
        final Map<String, String> M = new HashMap<String, String>();
        //currentDiscount = downloadedDiscountsList.get(currentDiscountListItemPosition);
        M.put("User recommendation title", "Короткий и понятный заголовок...");
        //M.put("User recommendation title", getDiscountTitleFromCurrentDiscount());
        discountsList.add(M);
        ++currentDiscountListItemPosition;
    }

    private String getDiscountTitleFromCurrentDiscount() {
        return currentDiscount.name;
    }

    private void setListAdapterForDiscountsLV() {
        final ListAdapter LA = getListAdapterForDiscountsLV();
        discountsLV.setAdapter(LA);
    }

    private ListAdapter getListAdapterForDiscountsLV() {
        final Context C = self;
        final List<Map<String, String>> DATA = discountsList;
        final int RESOURCE = layout.user_recommendations_page_list_item;
        final String[] FROM = getDiscountHeadersArray();
        final int[] TO = getDiscountHeadersIdsArray();
        final PageScaler PS = pageScaler;
        return new ShieldSimpleAdapter(C, DATA, RESOURCE, FROM, TO, PS);
    }

    private String[] getDiscountHeadersArray() {
        final String[] S = new String[1];
        S[0] = "User recommendation title";
        return S;
    }

    private int[] getDiscountHeadersIdsArray() {
        final int[] I = new int[1];
        I[0] = id.tvUserRecommendationOneTitle;
        return I;
    }

    private void scaleDiscountsLVDividerHeight() {
        pageScaler.scaleDividerHeightOf(discountsLV);
    }

    private void setOnItemClickListenerForDiscountsLV() {
        // TODO Не зроблена server'на частина сторінки. Тому частина рядків закоментована і на їх місці стоять заглушки.
        final AdapterView.OnItemClickListener L = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> PARENT, final View V, final int POSITION, final long ID) {
                final Context CT = self;
                final Class<?> C = AddOfferActivity.class;
                final Intent I = new Intent(CT, C);
                //I.putExtra(AddOfferActivity.EDIT_KEY, d);
                startActivity(I);
            }
        };
        discountsLV.setOnItemClickListener(L);
    }
}