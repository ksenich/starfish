package com.example.starfishapp;

import static com.example.starfishapp.R.*;

import android.os.*;
import android.support.v7.app.*;
import android.view.*;
import android.widget.*;
import com.googlion.shield.listeners.*;
import com.googlion.shield.loaders.*;
import com.googlion.shield.specialists.*;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;

import java.util.*;

public class RecommendationsActivity extends ActionBarActivity {
    private List<Map<String, Object>> recommendationsList;
    private PageScaler pageScaler;

    @Override
    protected void onCreate(final Bundle SAVED_INSTANCE_STATE) {
        performOnCreateMethodFromSuperclass(SAVED_INSTANCE_STATE);
        setContentViewForThisPage();
        initializeFields();
        scale();
        translateThisPage();
        loadUserRecommendations();
    }

    private void performOnCreateMethodFromSuperclass(final Bundle SAVED_INSTANCE_STATE) {
        super.onCreate(SAVED_INSTANCE_STATE);
    }

    private void setContentViewForThisPage() {
        setContentView(layout.wish_list_page);
    }

    private void initializeFields() {
        recommendationsList = new LinkedList<Map<String, Object>>();
        pageScaler = new PageScaler(RecommendationsActivity.this);
    }

    private void scale() {
        scaleThisPage();
        scaleDividerHeightOfRecommendationsList();
    }

    private void scaleThisPage() {
        pageScaler.scaleThePage();
    }

    private void scaleDividerHeightOfRecommendationsList() {
        final ListView LV = getListViewOfThisPage();
        pageScaler.scaleDividerHeightOf(LV);
    }

    private ListView getListViewOfThisPage() {
        final View V = findViewById(id.lvWishes);
        return (ListView) V;
    }

    private void translateThisPage() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForPageTranslation();
        final Translator T = new TranslatorOfThePageWithActionBar(SETTINGS);
        T.translate();
    }

    private Map<String, Map<String, Object>> getSettingsForPageTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getPageViewsIds());
        M.put("Page views default texts", getPageViewsDefaultTexts());
        M.put("Other settings", getOtherSettingsForTranslation());
        return M;
    }

    private Map<String, Object> getPageViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvWishListPageMessage", id.tvWishListPageMessage);
        return M;
    }

    private Map<String, Object> getPageViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvWishListPageMessage", "У Вас ще нема порад.");
        return M;
    }

    private Map<String, Object> getOtherSettingsForTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", RecommendationsActivity.this);
        M.put("Page title translation key", "User recommendations page action bar title");
        M.put("Page title default text", "Мої поради");
        return M;
    }

    private void loadUserRecommendations() {
        showIndeterminateProgressBar();
        loadRecommendations();
    }

    private void showIndeterminateProgressBar() {
        final boolean VISIBLE = true;
        setProgressBarIndeterminateVisibility(VISIBLE);
    }

    private void loadRecommendations() {
        final RecommendationsActivity RA = RecommendationsActivity.this;
        final UserRecommendationsLoader URL = new UserRecommendationsLoader(RA);
        URL.loadRecommendations();
    }

    public void apply(final List<Map<String, Object>> L) {
        retrieveRecommendationsListFrom(L);
        hideIndeterminateProgressBar();
        displayUserRecommendations();
        setOnItemClickListenerForUserRecommendationsList();
    }

    private void retrieveRecommendationsListFrom(final List<Map<String, Object>> L) {
        recommendationsList = L;
    }

    private void hideIndeterminateProgressBar() {
        final boolean VISIBLE = false;
        setProgressBarIndeterminateVisibility(VISIBLE);
    }

    private void displayUserRecommendations() {
        if (thereAreNoRecommendations()) {
            displayMessage();
        } else {
            displayRecommendationsList();
        }
    }

    private boolean thereAreNoRecommendations() {
        return recommendationsList.isEmpty();
    }

    private void displayMessage() {
        final View V = findViewById(id.tvWishListPageMessage);
        V.setVisibility(View.VISIBLE);
    }

    private void displayRecommendationsList() {
        final Map<String, List<Map<String, Object>>> SETTINGS = getSettingsForListDisplaying();
        final ListViewSpecialist LVS = new ListViewSpecialist(SETTINGS);
        LVS.setUpListView();
    }

    private Map<String, List<Map<String, Object>>> getSettingsForListDisplaying() {
        final Map<String, List<Map<String, Object>>> M = new HashMap<String, List<Map<String, Object>>>();
        M.put("List view items data", getListViewItemsData());
        M.put("List view item fields to be filled", getListViewItemFieldsToBeFilled());
        M.put("Other settings", getOtherSettingsForListDisplaying());
        return M;
    }

    public List<Map<String, Object>> getListViewItemsData() {
        return recommendationsList;
    }

    private List<Map<String, Object>> getListViewItemFieldsToBeFilled() {
        final List<Map<String, Object>> L = new LinkedList<Map<String, Object>>();
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Recommendation title", id.tvWishTitle);
        M.put("Recommendation description", id.tvWishDescription);
        M.put("Recommendation image url", id.sivWish);
        M.put("Recommendation price", id.tvWishPrice);
        M.put("Recommendation time", id.tvWishTime);
        L.add(M);
        return L;
    }

    private List<Map<String, Object>> getOtherSettingsForListDisplaying() {
        final List<Map<String, Object>> L = new LinkedList<Map<String, Object>>();
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", RecommendationsActivity.this);
        M.put("List view id", id.lvWishes);
        M.put("Layout resource id", layout.wish_list_page_list_item);
        M.put("Page scaler", pageScaler);
        M.put("List view items translator", getTranslatorForListViewItems());
        M.put("Font specialist for list view", getFontSpecialistForProcessingListView());
        L.add(M);
        return L;
    }

    private Object getTranslatorForListViewItems() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForListItemTranslation();
        return new ListViewItemsTranslator(SETTINGS);
    }

    private Map<String, Map<String, Object>> getSettingsForListItemTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getListItemViewsIdsForTranslation());
        M.put("Page views default texts", getListItemsViewsDefaultTexts());
        M.put("Other settings", getOtherSettingsForTranslation());
        return M;
    }

    private Map<String, Object> getListItemViewsIdsForTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvWishTitle", id.tvWishTitle);
        M.put("tvWishDescription", id.tvWishDescription);
        return M;
    }

    private Map<String, Object> getListItemsViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvWishTitle", "Заголовок відсутній.");
        M.put("tvWishDescription", "Опис відсутній.");
        return M;
    }

    private FontSpecialistForListView getFontSpecialistForProcessingListView() {
        final Map<Object, Map> S = getSettingsForSettingFontsInListView();
        return new FontSpecialistForListView(S);
    }

    private Map<Object, Map> getSettingsForSettingFontsInListView() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingFontsInListView());
        M.put("Other settings", getOtherSettingsForSettingFontsInListView());
        return M;
    }

    private Map getMainSettingsForSettingFontsInListView() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvWishTitle, "kelsonsansregularru.otf");
        M.put(id.tvWishDescription, "robotoregular0.otf");
        M.put(id.tvWishPrice, "kelsonsansboldru.otf");
        M.put(id.tvWishTime, "kelsonsansboldru.otf");
        return M;
    }

    private Map getOtherSettingsForSettingFontsInListView() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", RecommendationsActivity.this);
        return M;
    }

    private void setOnItemClickListenerForUserRecommendationsList() {
        final RecommendationsActivity RA = RecommendationsActivity.this;
        final AdapterView.OnItemClickListener LISTENER = new OnUserRecommendationsListItemClickListener(RA);
        final ListView LV = getListViewOfThisPage();
        LV.setOnItemClickListener(LISTENER);
    }
}