package com.example.starfishapp.model;

import java.io.Serializable;

public class Language implements Serializable {
    public String name;
    public String code;
    public int id;
}
