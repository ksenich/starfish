package com.example.starfishapp.model;


import java.util.ArrayList;
import java.util.List;

public class Country {

    public String name;

    public int id;
    public List<Region> regions;
    public String url;

    public Country() {
        regions = new ArrayList<Region>();
    }

    public Country(String name, int id) {
        this();
        this.name = name;
        this.id = id;
    }


    public String toString() {
        return name;
    }
}
