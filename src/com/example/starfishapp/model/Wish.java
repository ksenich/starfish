package com.example.starfishapp.model;

/**
* Created by pc on 2014-08-30.
*/
public class Wish {
    public String name;
    public String description;
    public String image_url;
    public int days;
    public int hours;

}
