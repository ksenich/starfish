package com.example.starfishapp.model;

import java.io.Serializable;

public class UserInfo implements Serializable {
    public String name;
    public String img_url;
    public int settingsSet;
    public int lang;
    public int country;
    public int region;
    public String lang_name;
    public String country_name;
    public String region_name;
    public String telephone;
    public String email;
    public String first_name;
    public String last_name;
    public String cnt_likes;
    public String cnt_bonuses;

    @Override
    public String toString() {
        return String.format("{name:%s, set:%d, lang:%d, country:%d, region:%d}", name, settingsSet, lang, country, region);
    }
}
