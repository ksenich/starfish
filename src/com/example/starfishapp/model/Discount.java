package com.example.starfishapp.model;

import android.graphics.Color;
import com.example.starfishapp.MyDiscountsActivity;
import com.example.starfishapp.R;

import java.io.Serializable;

/**
* Created by pc on 2014-08-15.
*/
public class Discount implements Serializable {
    public int id;
    public String img_url;
    public int count_likes;
    public int count_bonuses;
    public String name;

    public enum Status {
        OK(R.drawable.ok_status_image, R.string.discount_confirmed, R.string.bonuses_credited, Color.GREEN),
        FAIL(R.drawable.fail_status_image, R.string.discount_not_confirmed, R.string.bonuses_not_credited, Color.RED),
        WAIT(R.drawable.waiting_status_image, R.string.discount_waiting, R.string.bonuses_waiting, Color.GRAY),;
        public int image_resource;
        public int confirmation_resource;
        public int bonus_resource;
        public int color;
        Status(int image_resource, int confirmation_resource, int bonus_resource, int color) {
            this.image_resource = image_resource;
            this.confirmation_resource = confirmation_resource;
            this.bonus_resource = bonus_resource;
            this.color = color;
        }
    }
}
