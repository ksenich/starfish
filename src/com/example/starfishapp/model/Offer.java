package com.example.starfishapp.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Offer implements Serializable {

    transient public Bitmap image;

    public String name;

    public String description = "";

    public String img_url;

    public String conditions = "";

    public int price;

    public double priceCoupon;

    public int id;
    //    ForeignCollection<LatLng> locations;
    public double lat;
    public double lng;
    //
//    public Category category;
//    
//    public Region region;
    public int category_id;
    public int region_id;
    public double discount;
    public String exchange;
    public String url;

    public Offer() {
    }

    public Offer(int id) {
        this();
        this.id = id;
    }
//    public static final Parcelable.Creator<Offer> CREATOR
//            = new Parcelable.Creator<Offer>() {
//
//        @Override
//        public Offer createFromParcel(Parcel parcel) {
//            Offer r = new Offer();
//            r.locations.add(parcel.readParcelable());
//            parcel.writeInt(category_id);
//            parcel.writeString(name);
//            parcel.writeString(description);
//            parcel.writeString(img_url);
//            parcel.writeInt(price);
//            parcel.writeDouble(priceCoupon);
//            parcel.writeInt(id);
//            parcel.writeString(exchange);
//            return r;
//        }
//
//        @Override
//        public Offer[] newArray(int i) {
//            return new Offer[0];
//        }
//    };

}
