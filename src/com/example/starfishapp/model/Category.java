package com.example.starfishapp.model;

import com.example.starfishapp.R;


import java.io.Serializable;


public class Category implements Serializable {

    public int id;

    public String name;
    public String img_url;
    public boolean selected;

    public Category() {
        selected = false;
    }

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }


    public String toString() {
        return name;
    }

    //TODO: OMG WHO WROTE THIS??????????
    public static CategoryIcon idToEnum(int category_id) {
        switch (category_id) {
            case 1:
                return CategoryIcon.AUTO;
            case 2:
                return CategoryIcon.FUN;
            case 3:
                return CategoryIcon.FOOD;
            case 4:
                return CategoryIcon.KIDS;
            case 5:
                return CategoryIcon.HEALTH;
            case 6:
                return CategoryIcon.EDUCATION;
            case 7:
                return CategoryIcon.CULTURE;
            case 8:
                return CategoryIcon.EDUCATION;
            case 9:
                return CategoryIcon.BEAUTY;
            default:
                return CategoryIcon.WHAT;
        }
    }
    public int getMapIconId(){
        return CategoryMapIcon[id];
    }
    public static int[] CategoryMapIcon = {
            R.drawable.red_pointer,
            R.drawable.c1,
            R.drawable.c2,
            R.drawable.c3,
            R.drawable.c4,
            R.drawable.c5,
            R.drawable.c6,
            R.drawable.c7,
            R.drawable.c8,
            R.drawable.c9,
            R.drawable.c10,
            R.drawable.c11,
            R.drawable.c12,
            R.drawable.c13,
            R.drawable.c14,
            R.drawable.c15,
            R.drawable.c16,
            R.drawable.c17,
            R.drawable.c18,
    };

    public enum CategoryIcon {
        AUTO(R.drawable.c1),
        BEAUTY(R.drawable.pt_beauty),
        CULTURE(R.drawable.pt_culture),
        EDUCATION(R.drawable.pt_education),
        FOOD(R.drawable.pt_food),
        FUN(R.drawable.pt_fun),
        HEALTH(R.drawable.pt_health),
        KIDS(R.drawable.pt_kids),
        WHAT(R.drawable.red_pointer);
        public int res_id;

        CategoryIcon(int res_id) {
            this.res_id = res_id;
        }
    }
}

