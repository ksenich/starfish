package com.example.starfishapp.model;

import org.json.*;

public class Supplier {
    private int id;
    private String name;
    private String logotypeURL;
    private String description;
    private String offersQuantity;
    private String commentsQuantity;

    public Supplier() {
        id = 0;
        name = "";
        logotypeURL = "";
        description = "";
        offersQuantity = "";
        commentsQuantity = "";
    }

    public Supplier(final JSONObject O) throws JSONException {
        id = O.getInt("id");
        name = O.getString("name");
        logotypeURL = O.getString("img_url");
        description = O.getString("description");
        offersQuantity = O.getString("cnt_proposals");
        commentsQuantity = O.getString("cnt_comments");
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLogotypeURL() {
        //TODO
        //return logotypeURL;
        return "http://static.comicvine.com/uploads/original/11112/111121597/3787196-9011617837-JL_Ba.jpg";
    }

    public String getDescription() {
        return description;
    }

    public String getOffersQuantity() {
        return offersQuantity;
    }

    public String getCommentsQuantity() {
        return commentsQuantity;
    }
}