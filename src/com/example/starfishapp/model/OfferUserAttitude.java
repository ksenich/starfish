package com.example.starfishapp.model;

import org.json.*;

import java.io.*;

public class OfferUserAttitude implements Serializable {
    private boolean like;
    private boolean dislike;
    private boolean favourite;
    private String imageURL;
    private String name;
    private String description;
    private int commentsQuantity;
    private int likesQuantity;
    private int dislikesQuantity;
    private int price;
    private double priceCoupon;
    private int userID;
    private int offerGooglionID;
    private String offerStarfishID;
    private String date;
    private String conditions;
    private String supplierName;
    private String offerLink;
    private double discount;

    public OfferUserAttitude() {
        name = "";
        description = "";
        like = false;
        dislike = false;
        favourite = false;
        likesQuantity = 0;
        dislikesQuantity = 0;
        commentsQuantity = 0;
        price = 0;
        userID = -1;
        offerGooglionID = -1;
        offerStarfishID = "";
        imageURL = "";
        date = "";
        conditions = "";
        supplierName = "";
        offerLink = "";
        priceCoupon = 0;
        discount = 0;
    }

    public OfferUserAttitude(final JSONObject JO) throws JSONException {
        name = JO.getString("name");
        description = JO.getString("description");
        like = JO.getInt("offer_liked") != 0;
        dislike = JO.getInt("offer_disliked") != 0;
        favourite = JO.getInt("offer_in_favourite") != 0;
        likesQuantity = JO.getInt("cnt_likes");
        dislikesQuantity = JO.getInt("cnt_dislikes");
        commentsQuantity = JO.getInt("cnt_comments");
        price = JO.getInt("price");
        userID = JO.getInt("user_id");
        offerGooglionID = JO.getInt("id");
        offerStarfishID = JO.getString("sf_id");
        imageURL = JO.getString("url_img");
        date = JO.getString("end_time");
        conditions = JO.getString("conditions");
        supplierName = JO.getString("supplier_name");
        offerLink = JO.getString("link");
        priceCoupon = JO.getDouble("pricecoupon");
        discount = JO.getDouble("discount");
    }

    public boolean isLiked() {
        return like;
    }

    public boolean isDisliked() {
        return dislike;
    }

    public boolean isFavourited() {
        return favourite;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getCommentsQuantity() {
        return commentsQuantity;
    }

    public int getLikesQuantity() {
        return likesQuantity;
    }

    public int getDislikesQuantity() {
        return dislikesQuantity;
    }

    public int getPrice() {
        return price;
    }

    public double getPriceCoupon() {
        return priceCoupon;
    }

    public int getUserID() {
        return userID;
    }

    public int getOfferGooglionID() {
        return offerGooglionID;
    }

    public String getOfferStarfishID() {
        return offerStarfishID;
    }

    public String getDate() {
        return date;
    }

    public String getConditions() {
        return conditions;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public String getOfferLink() {
        return offerLink;
    }

    public double getDiscount() {
        return discount;
    }
}
