package com.example.starfishapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class WebActivity extends Activity {

    static final private String starfishUrl = "http://starfishapp.pp.ua/app.php?action=get_token_soc&type=gp";

    class TokenListener {
        public void check(String html) {
            try {

                Log.d("login hunt", "checking");
                Object o = new JSONTokener(html).nextValue();
                if (o instanceof JSONObject) {
                    JSONObject jo = (JSONObject) o;
                    if (jo.has("token")) {
                        Log.d("login hunt", "token found");
                        Intent data = new Intent();
                        data.putExtra(WelcomeActivity.ACCESS_TOKEN_KEY, jo.getString("token"));
                        if (getParent() == null) {
                            setResult(Activity.RESULT_OK, data);
                        } else {
                            getParent().setResult(Activity.RESULT_OK, data);
                        }
                        finish();
                        finish();
                    } else {
                        Log.d("login hunt", "no token found");
                    }
                } else {

                    Log.d("login hunt", "not a json");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        final WebView wv = new WebView(this);
        wv.loadUrl(starfishUrl);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wv.addJavascriptInterface(new TokenListener(), "HTMLOUT");
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);

                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                Log.d("login hunt", "loaded:" + url);
                if (url.startsWith(starfishUrl)) {

                    Log.d("login hunt", "running js");
                    wv.loadUrl("javascript:window.HTMLOUT.check(document.getElementsByTagName(\"pre\")[0].innerHTML);");
                }
            }
        });
        wv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        setContentView(wv);
    }
}
