package com.example.starfishapp;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.util.Xml;
import android.view.View;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.example.starfishapp.model.Country;
import com.example.starfishapp.model.Language;
import com.example.starfishapp.model.Region;
import com.example.starfishapp.utilities.DownloadWebPageTask;
import com.googlion.shield.utilities.PageScaler;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class FirstLaunchActivity extends Activity {

    public static final String STRING = "RadioButton";
    private final FirstLaunchActivity self = FirstLaunchActivity.this;
    private List<Language> languages;
    private int selectedLang;
    private List<Country> countries;
    private int selectedCountry;
    private List<Region> regions;
    private int selectedRegion;
    private SparseArray<Language> langById = new SparseArray<Language>();
    private SparseArray<Country> countryById = new SparseArray<Country>();
    private SparseArray<Region> regionById = new SparseArray<Region>();
    private PageScaler pageScaler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.user_application_settings_menu);
        Log.d("reading user prefs", "");
        SharedPreferences sp = getSharedPreferences(Starfish.USER_PREFS_KEY, MODE_PRIVATE);
        pageScaler = new PageScaler(self);
        pageScaler.scaleThePage();
        selectedLang = sp.getInt(Starfish.ID_LANGUAGE_KEY, 0);
        selectedCountry = sp.getInt(Starfish.ID_COUNTRY_KEY, 0);
        selectedRegion = sp.getInt(Starfish.ID_REGION_KEY, 0);
        Log.d("finished reading user prefs", "" + selectedLang + ":" + selectedCountry + ":" + selectedRegion);
        findViewById(R.id.bApplyUserApplicationSettings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("writing user prefs", "");
                SharedPreferences.Editor e = getSharedPreferences(Starfish.USER_PREFS_KEY, MODE_PRIVATE).edit();
                e.putInt(Starfish.ID_LANGUAGE_KEY, selectedLang);
                e.putInt(Starfish.ID_COUNTRY_KEY, selectedCountry);
                e.putInt(Starfish.ID_REGION_KEY, selectedRegion);
                e.commit();
                Log.d("finished writing user prefs", "");
                finish();
                //set_user_info : id_language, id_country, id_region
                new AsyncTask<Void, Void, String>() {
                    String action = "set_user_info";
                    String url = "http://starfishapp.pp.ua/app.php?action=%s";

                    @Override
                    protected String doInBackground(Void... voids) {
                        HashMap<String, String> par = new HashMap<String, String>();
                        par.put(Starfish.ID_LANGUAGE_KEY, String.valueOf(selectedLang));
                        par.put(Starfish.ID_COUNTRY_KEY, String.valueOf(selectedCountry));
                        par.put(Starfish.ID_REGION_KEY, String.valueOf(selectedRegion));
                        String result = new DownloadWebPageTask().executePostWithToken(self,
                                String.format(url, action), par);
                        return result;
                    }

                    @Override
                    protected void onPostExecute(String s) {
                        Log.d(action, s);
                        if (s.contains("error")) {
                            Toast.makeText(self, "Couldn't save settings", Toast.LENGTH_SHORT).show();
                        }
                    }
                }.execute();
            }
        });
        final RadioGroup langRBs = (RadioGroup) findViewById(R.id.rgLangs);
        final RadioGroup countryRBs = (RadioGroup) findViewById(R.id.rgCountries);
        final RadioGroup regionRBs = (RadioGroup) findViewById(R.id.rgRegions);
        new AsyncTask<Void, Void, List<Language>>() {
            @Override
            protected List<Language> doInBackground(Void... voids) {
                return Starfish.getInstance(self).getLanguages();
            }

            @Override
            protected void onPostExecute(List<Language> languages) {
                self.languages = languages;
                boolean langIsSelected = false;
                langRBs.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        if (i != -1) {
                            selectedLang = langById.get(i).id;
                        }
                    }
                });

                for (int i = 0, languagesSize = languages.size(); i < languagesSize; i++) {
                    Language lan = languages.get(i);
                    RadioButton v = createRB();
                    v.setText(lan.name);
                    v.setId(i);
                    langRBs.addView(v);
                    langById.put(v.getId(), lan);
                    if (selectedLang == lan.id) {
                        langRBs.check(v.getId());
                        Log.d("selected lang is", String.valueOf(selectedLang));
                        langIsSelected = true;
                    }
                }

                if (!langIsSelected && langRBs.getChildCount() > 0) {
                    langRBs.check(langRBs.getChildAt(0).getId());
                }
                pageScaler.scaleChildrenOf(langRBs);

            }
        }.execute();


        new AsyncTask<Void, Void, List<Country>>() {
            @Override
            protected List<Country> doInBackground(Void... voids) {
                return Starfish.getInstance(self).getCountries();
            }

            @Override
            protected void onPostExecute(List<Country> countryList) {
                countries = countryList;
                boolean countryIsSelected = false;
                countryRBs.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        if (i != -1) {
                            selectedCountry = countryById.get(i).id;
                            new AsyncTask<Void, Void, List<Region>>() {
                                @Override
                                protected List<Region> doInBackground(Void... voids) {
                                    return Starfish.getInstance(self).getRegions(selectedCountry);
                                }

                                @Override
                                protected void onPostExecute(List<Region> regions) {
                                    regionRBs.removeAllViews();
                                    populateRegions(regions, regionRBs);
                                }
                            }.execute();
                        }
                    }
                });

                for (int i = 0, countryListSize = countryList.size(); i < countryListSize; i++) {
                    Country country = countryList.get(i);
                    RadioButton v = createRB();
                    v.setId(i);
                    v.setText(country.name);
                    countryRBs.addView(v);
                    countryById.put(v.getId(), country);
                    if (selectedCountry == country.id) {
                        countryRBs.check(v.getId());
                        Log.d("selected country is", String.valueOf(selectedCountry));
                        countryIsSelected = true;
                    }
                }

                if (!countryIsSelected && countryRBs.getChildCount() > 0) {
                    countryRBs.check(countryRBs.getChildAt(0).getId());
                }
                pageScaler.scaleChildrenOf(countryRBs);

            }
        }.execute();
        regionRBs.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i != -1) {
                    selectedRegion = regionById.get(i).id;
                }
            }
        });
    }

    private void populateRegions(List<Region> regionList, RadioGroup regionRBs) {
        regions = regionList;
        boolean langIsSelected = false;
        for (int i = 0, regionListSize = regionList.size(); i < regionListSize; i++) {
            Region region = regionList.get(i);
            RadioButton v = createRB();
            v.setId(i);
            v.setText(region.name);
            regionRBs.addView(v);
            regionById.put(v.getId(), region);
            if (i == 0) {
                regionRBs.check(v.getId());
            }
            if (selectedRegion == region.id) {
                regionRBs.check(v.getId());
                Log.d("selected region is", String.valueOf(selectedRegion) + ": " + Arrays.toString(Thread.currentThread().getStackTrace()));
                langIsSelected = true;
            }
        }
        if (!langIsSelected && regionRBs.getChildCount() > 0) {
            regionRBs.check(regionRBs.getChildAt(0).getId());
        }
        pageScaler.scaleChildrenOf(regionRBs);
    }

    private RadioButton createRB() {
        AttributeSet as = null;
        Resources r = getResources();
        XmlResourceParser parser = r.getLayout(R.layout.user_application_settings_menu_radio_button);

        int state = 0;
        do {
            try {
                state = parser.next();
            } catch (XmlPullParserException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            if (state == XmlPullParser.START_TAG) {
                if (parser.getName().equals("RadioButton")) {
                    as = Xml.asAttributeSet(parser);
                    Log.d("RadioButton created", "yes");
                    break;
                }
            }
        } while (state != XmlPullParser.END_DOCUMENT);
        if (as == null) {
            Log.d("RadioButton created", "no");
        }
        return new RadioButton(self, as);
    }

    @Override
    public void onWindowFocusChanged(final boolean HAS_FOCUS) {
        super.onWindowFocusChanged(HAS_FOCUS);
        pageScaler.scaleThePage();
    }


}
