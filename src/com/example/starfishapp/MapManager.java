package com.example.starfishapp;

import static android.graphics.BitmapFactory.*;
import static android.graphics.Bitmap.*;
import static com.google.android.gms.maps.model.BitmapDescriptorFactory.*;
import static com.example.starfishapp.model.Category.*;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.Toast;
import com.example.starfishapp.model.*;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;
import com.googlion.shield.utilities.PageScaler;

import java.util.*;


public class MapManager
        implements
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener {
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    Activity mActivity;
    SupportMapFragment mFragment;
    PopupWindow pv;
    Offer selectedOffer = null;
    private LocationClient mLocationClient;
    private LatLng location = new LatLng(0, 0);
    private GoogleMap map;
    private OfferInfoAdapter offerInfoAdapter;
    private Activity activity;
    private List<Marker> offerMarkers = new ArrayList<Marker>();
    private Marker selectedMarker;
    private PageScaler pageScaler;

    public MapManager(Activity activity, SupportMapFragment mapf, PageScaler PS) {
        this.activity = activity;
        mActivity = activity;
        mFragment = mapf;
        pageScaler = PS;
        offerInfoAdapter = new OfferInfoAdapter(activity);
        mLocationClient = new LocationClient(mActivity, this, this);
        if (mFragment != null) {
            mFragment.setMenuVisibility(true);

            map = mFragment.getMap();
            if (map != null) {
                map.setMyLocationEnabled(true);
                map.setOnMarkerClickListener(this);
                map.setOnInfoWindowClickListener(this);
                map.setInfoWindowAdapter(offerInfoAdapter);
//                circle = map.addCircle(new CircleOptions()
//                                .center(new LatLng(0, 0))
//                                .fillColor(Color.RED)
//                                .strokeWidth(0)
//                );
            }
        } else {
            Log.e("MapActivity", "Couldn't find mapFragment. Null returned");
        }
    }

    public static void goToOfferPage(Context ctx, int offer_id) {
        Intent intent = new Intent(ctx, ShareActivity.class);
        intent.putExtra(ShareActivity.OFFER_KEY, offer_id);
        ctx.startActivity(intent);
    }

    public void onStart() {
        if (isGooglePlayServicesAvailable()) {
            mLocationClient.connect();
        }
    }

    public void onStop() {
        mLocationClient.disconnect();
    }

    public void flyTo(LatLng point, int zoomLvl) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(point, zoomLvl);
        map.animateCamera(cameraUpdate);
        this.location = point;
    }

    @Override
    public void onConnected(Bundle dataBundle) {
        Toast.makeText(mActivity, "Connected", Toast.LENGTH_SHORT).show();
        Location location = mLocationClient.getLastLocation();
        if (location != null) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            setPosition(latLng);
            flyTo(latLng, 17);
        } else {
            Toast.makeText(mActivity, "Couldn't pinpoint your location.\nSorry.;(", Toast.LENGTH_SHORT).show();
            LatLng latLng = new LatLng(50.4458971, 30.5230393);
            this.location = latLng;
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17);
            map.animateCamera(cameraUpdate);
        }
    }

    @Override
    public void onDisconnected() {
        Toast.makeText(mActivity, "Disconnected. Please re-connect.",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(
                        mActivity,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                Log.e("ConnectionFailed", e.getMessage());
            }
        } else {
            Toast.makeText(mActivity, "Sorry. Location services not available to you", Toast.LENGTH_LONG).show();
        }
    }

    private boolean isGooglePlayServicesAvailable() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mActivity);
        if (ConnectionResult.SUCCESS == resultCode) {
            Log.d("Location Updates", "Google Play services is available.");
            return true;
        } else {
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(resultCode,
                    mActivity,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);

            if (errorDialog != null) {
                ErrorDialogFragment errorFragment = new ErrorDialogFragment();
                errorFragment.setDialog(errorDialog);
            }

            return false;
        }
    }

    public LatLng getLocation() {
        try {
            final Location L = mLocationClient.getLastLocation();
            location = new LatLng(L.getLatitude(), L.getLongitude());
        } catch (final Exception E) {
            location = new LatLng(50.4458971, 30.5230393);
        }
        return location;
    }

    public Activity getActivity() {
        return activity;
    }

    void setRadius(int r) {
//        circle.setRadius(r);
    }

    public void showCircle(LatLng pos, double radius) {
//        circle = map.addCircle(new CircleOptions()
//                .center(pos)
//                .fillColor(Color.RED)
//                .strokeWidth(0)
//                .radius(radius));
    }

    void setPosition(LatLng pos) {
//        circle.setCenter(pos);
    }

    public void showOffers(List<Offer> offers) {
        try {
            for (Offer o : offers) {
                showOffer(o);
            }
            showSelectedMarker();
        } catch (final Exception E) {
            E.printStackTrace();
        }
    }

    private void showSelectedMarker() {
//        selectedMarker.
//        if(selectedMarker!= null){
//        map.addMarker(new MarkerOptions()
//                .position(selectedMarker.getPosition())
//                .icon(BitmapDescriptorFactory.fromResource(Category.idToEnum(offer.category_id).res_id));
//        }
    }

    public void showOffer(Offer offer) {
//        for (LatLng loc : offer.locations) {
        final BitmapDescriptor ICON = getIconFrom(offer);
        Marker m = map.addMarker(new MarkerOptions()
                .position(new LatLng(offer.lat, offer.lng))
                .icon(ICON)//File(+".png"))
        );
        Category categoryById = Starfish.getInstance(activity).getCategoryById(offer.category_id);
        if (categoryById == null) {
            Log.d("category", "not found");
        }
        offerInfoAdapter.addOffer(offer, m);
        offerMarkers.add(m);
    }

    private BitmapDescriptor getIconFrom(final Offer O) {
        final Resources RES = mActivity.getResources();
        final Integer CATEGORY_ID = O.category_id;
        final Integer ID = CategoryMapIcon[CATEGORY_ID];
        final Bitmap SOURCE = decodeResource(RES, ID);
        final Float SOURCE_SIZE_SCALE = pageScaler.getScale();
        final Float SOURCE_SCALED_WIDTH = SOURCE.getWidth() * SOURCE_SIZE_SCALE;
        final Float SOURCE_SCALED_HEIGHT = SOURCE.getHeight() * SOURCE_SIZE_SCALE;
        final Float DST_SIZE_SCALE = 3f / 16f;
        final Float SCALED_WIDTH = SOURCE_SCALED_WIDTH * DST_SIZE_SCALE;
        final Float SCALED_HEIGHT = SOURCE_SCALED_HEIGHT * DST_SIZE_SCALE;
        final Integer DST_WIDTH = SCALED_WIDTH.intValue();
        final Integer DST_HEIGHT = SCALED_HEIGHT.intValue();
        final Bitmap IMAGE = createScaledBitmap(SOURCE, DST_WIDTH, DST_HEIGHT, false);
        return fromBitmap(IMAGE);
    }

    public void clear() {
        offerInfoAdapter.offers.clear();
        for (Marker m : offerMarkers)
            m.remove();
//        map.clear();
        offerMarkers.clear();
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Offer offer = offerInfoAdapter.offers.get(marker);
        goToOfferPage(getActivity(), offer.id);
    }

    private void startActivity(Intent intent) {
        getActivity().startActivity(intent);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        selectedMarker = marker;
        performClickOnSelectedMarker();
        return true;
    }

    public void performClickOnSelectedMarker() {
        Offer so = offerInfoAdapter.getOffer(selectedMarker);
        if (so != null) {
            selectedOffer = so;
            View infoContents = offerInfoAdapter.getInfoContents(selectedMarker);
            showPopup(infoContents);
        }
    }

    private void showPopup(final View V) {
        Log.d("showing popup", "popup");
        View closeButton = V.findViewById(R.id.ibClosePopUpWindow);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                V.setVisibility(View.GONE);
            }
        });
        V.findViewById(R.id.bGoToThePoppedUpOfferPage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToOfferPage(getActivity(), selectedOffer.id);
            }
        });
        V.findViewById(R.id.bCreateTheRouteToThePoppedUpOffer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View V) {
                showDirectionFromUserLocationToTheOffer();
                updateRouteMarkersPositions();
                addRouteMarkersToTheDirection();
            }

            private void updateRouteMarkersPositions() {
                updateStartMarkerPositionInPathOptionsMenu();
                updateFinishMarkerPositionInPathOptionsMenu();
            }

            private void updateStartMarkerPositionInPathOptionsMenu() {
                final View V = activity.findViewById(R.id.etStartAddress);
                final EditText ET = (EditText) V;
                final CharSequence TEXT = location.latitude + "," + location.longitude;
                ET.setText(TEXT);
            }

            private void updateFinishMarkerPositionInPathOptionsMenu() {
                final View V = activity.findViewById(R.id.etFinishAddress);
                final EditText ET = (EditText) V;
                final CharSequence TEXT = selectedOffer.lat + "," + selectedOffer.lng;
                ET.setText(TEXT);
            }

            private void showDirectionFromUserLocationToTheOffer() {
                final MainActivity MA = (MainActivity) activity;
                final double LATITUDE = selectedOffer.lat;
                final double LONGITUDE = selectedOffer.lng;
                final LatLng END = new LatLng(LATITUDE, LONGITUDE);
                MA.showDirections(END);
            }

            private void addRouteMarkersToTheDirection() {
                final MainActivity MA = (MainActivity) activity;
                MA.addRouteMarkersToTheMap();
            }
        });
        V.setVisibility(View.VISIBLE);
    }

    public LatLng getTopLeft() {
        return map.getProjection().getVisibleRegion().farLeft;
    }

    public LatLng getBotRight() {
        return map.getProjection().getVisibleRegion().nearRight;
    }

    public void setCameraChangeListener(GoogleMap.OnCameraChangeListener l) {
        map.setOnCameraChangeListener(l);
    }

    public static class ErrorDialogFragment extends DialogFragment {

        private Dialog mDialog;

        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }


}
