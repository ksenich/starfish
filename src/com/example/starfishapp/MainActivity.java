package com.example.starfishapp;

import static android.graphics.Color.*;
import static android.graphics.Typeface.createFromAsset;
import static android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE;
import static android.text.Spanned.SPAN_EXCLUSIVE_INCLUSIVE;
import static com.google.android.gms.maps.model.BitmapDescriptorFactory.*;
import static com.example.starfishapp.MainActivity.MODE.*;
import static com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.*;

import android.content.*;
import android.content.SharedPreferences.*;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.*;
import android.os.*;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.*;
import android.support.v7.app.*;
import android.support.v7.widget.SearchView;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.*;
import android.view.*;
import android.widget.*;
import com.example.starfishapp.helpers.*;
import com.example.starfishapp.model.*;
import com.example.starfishapp.pages.*;
import com.example.starfishapp.utilities.*;
import com.facebook.*;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;
import com.googlion.shield.listeners.*;
import com.googlion.shield.pages.*;
import com.googlion.shield.specialists.*;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;
import com.loopj.android.image.*;
import org.json.*;
import android.widget.SlidingDrawer.*;
import com.example.starfishapp.R.*;

import java.io.*;
import java.net.*;
import java.util.*;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

public class MainActivity extends ActionBarActivity implements GoogleMap.OnMapClickListener, Parcelable {
    private final Context self = MainActivity.this;
    long lastRequestTime;
    ACTION action = ACTION.NONE;
    private CategorySelector categorySelector;
    private ActionBarDrawerToggle drawerToggleButton;
    private EditText startET;
    private EditText finishET;
    private Marker startMarker;
    private Marker finishMarker;
    private Polyline pathLine;
    private GoogleMap map;
    private List<LatLng> pointsList;
    private ArrayList<Circle> mPathBorders = new ArrayList<Circle>();
    private LatLng location;
    private LatLng target;
    private MapManager mapManager;
    private double routeRadius;
    private View pathOptionsMenu;
    private PolylineOptions mapRouteOptions;
    private PageScaler pageScaler;
    private MODE chosenMode;
    //    private ListManager listManager;
    private OnGooglionDrawerOpenListener openListener;
    private SlidingDrawer slidingDrawer;
    private MainPageTranslator pageTranslator;
    private MainMenuTranslator menuTranslator;
    private OnMainPageSeekBarChangeListener routeRadiusSeekBarListener;
    private boolean clearButtonHasBeenPressed;
    private SlidingMenu slidingMenu;

    @Override
    protected void onStart() {
        super.onStart();
        mapManager.onStart();
        mapManager
                .setRadius(getSharedPreferences("map_settings", 0)
                        .getInt("radius", 0));
        Log.d("mappage", "onstart");
        loadUserInformation();
    }

    @Override
    protected void onStop() {
        // Disconnecting the client invalidates it.
        mapManager.onStop();
//        getSharedPreferences("map_settings", 0).edit().putInt("radius", (int) mapManager.getRadius());
        super.onStop();
    }

    private void prepareMap() {
        SupportMapFragment mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fMap));
        mapManager = new MapManager(this, mapFragment, pageScaler);

        if (mapFragment != null) {
            mapFragment.setMenuVisibility(true);
            map = mapFragment.getMap();
            if (map != null) {
                map.setOnMapClickListener(this);
                map.setMyLocationEnabled(true);
            } else {
                Log.e("OnCreate", "map is null");
            }
        } else {
            Log.e("MapActivity", "Couldn't find mapFragment. Null returned");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("mappage", "onCreate");
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.drawer_layout);
        setupConnectionMonitor();
//        SharedPreferences sp = getSharedPreferences(Starfish.USER_PREFS_KEY, Context.MODE_PRIVATE);
//        boolean settingsSet = sp.getBoolean(Starfish.SETTINGS_SET_KEY, false);
//        if (!settingsSet) {
//            final Intent I = new Intent(this, FirstLaunchActivity.class);
//            startActivity(I);
//        }


        initializeFields();
        initializePageTranslator();
        setUpSlidingMenu();
        initializeMenuTranslator();
        setTextForRouteRadiusTextView();
        setTheInformationForTheRouteByDefault();
        translateThisPage();
        scaleThisPage();
        setFontsForAllTextualViewsOfThisPage();
        setDrivingModeSelectedByDefault();
        prepareMap();
        prepareLeftDrawer();
        prepareDrawerToggleButton();
        prepareActionBar();
        prepareCategorySelector();
        prepareRadiusBar();
        prepareOptionsView();
        setUpSlidingDrawer();
    }

    private void initializeFields() {
        pointsList = new LinkedList<LatLng>();
        pathOptionsMenu = findViewById(R.id.rlPathOptionsMenu);
        pageScaler = new PageScaler(MainActivity.this);
        routeRadius = 0.1;
        routeRadiusSeekBarListener = new OnMainPageSeekBarChangeListener(MainActivity.this);
        clearButtonHasBeenPressed = true;
    }

    private void initializePageTranslator() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForMainPageTranslation();
        pageTranslator = new MainPageTranslator(SETTINGS);
    }

    private Map<String, Map<String, Object>> getSettingsForMainPageTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getPageViewsIdsForMainPageTranslation());
        M.put("Page views default texts", getPageViewsDefaultTextsForMainPageTranslation());
        M.put("Other settings", getOtherSettings());
        return M;
    }

    private Map<String, Object> getPageViewsIdsForMainPageTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.putAll(getFirstPartOfPageViewsIdsForMainPageTranslation());
        M.putAll(getSecondPartOfPageViewsIdsForMainPageTranslation());
        return M;
    }

    private Map<String, Object> getFirstPartOfPageViewsIdsForMainPageTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvApplicationSettingsMenuHeader", id.tvApplicationSettingsMenuHeader);
        M.put("tvApplicationSettingsMenuCountryChoice", id.tvApplicationSettingsMenuCountryChoice);
        M.put("tvApplicationSettingsMenuRegionChoice", id.tvApplicationSettingsMenuRegionChoice);
        M.put("tvApplicationSettingsMenuCategoryChoice", id.tvApplicationSettingsMenuCategoryChoice);
        M.put("tvWatchSharesList", id.tvWatchSharesList);
        M.put("tvGoToTheMap", id.tvGoToTheMap);
        M.put("tvSixKm", id.tvSixKm);
        M.put("tvFiveKm", id.tvFiveKm);
        M.put("tvFourKm", id.tvFourKm);
        M.put("tvThreeKm", id.tvThreeKm);
        M.put("tvTwoKm", id.tvTwoKm);
        M.put("tvOneKm", id.tvOneKm);
        M.put("tvHalfKm", id.tvHalfKm);
        M.put("tvZeroKm", id.tvZeroKm);
        M.put("bGoToThePoppedUpOfferPage", id.bGoToThePoppedUpOfferPage);
        M.put("bCreateTheRouteToThePoppedUpOffer", id.bCreateTheRouteToThePoppedUpOffer);
        return M;
    }

    private Map<String, Object> getSecondPartOfPageViewsIdsForMainPageTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvPoppedUpOfferTitle", id.tvPoppedUpOfferTitle);
        M.put("bCreateTheRoute", id.bCreateTheRoute);
        M.put("etFinishAddress", id.etFinishAddress);
        M.put("etStartAddress", id.etStartAddress);
        return M;
    }

    private Map<String, Object> getPageViewsDefaultTextsForMainPageTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.putAll(getFirstPartOfPageViewsDefaultTextsForMainPageTranslation());
        M.putAll(getSecondPartOfPageViewsDefaultTextsForMainPageTranslation());
        return M;
    }

    private Map<String, Object> getFirstPartOfPageViewsDefaultTextsForMainPageTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvApplicationSettingsMenuHeader", "Налаштування");
        M.put("tvApplicationSettingsMenuCountryChoice", "Вибір країни");
        M.put("tvApplicationSettingsMenuRegionChoice", "Вибір області");
        M.put("tvApplicationSettingsMenuCategoryChoice", "Вибір розряду");
        M.put("tvWatchSharesList", "Подивитись список акцій");
        M.put("tvGoToTheMap", "Подивитись на карті");
        M.put("tvSixKm", "6км");
        M.put("tvFiveKm", "5км");
        M.put("tvFourKm", "4км");
        M.put("tvThreeKm", "3км");
        M.put("tvTwoKm", "2км");
        M.put("tvOneKm", "1км");
        M.put("tvHalfKm", "0.5км");
        M.put("tvZeroKm", "0км");
        M.put("bGoToThePoppedUpOfferPage", "На сторінку акції");
        M.put("bCreateTheRouteToThePoppedUpOffer", "Прокласти маршрут");
        return M;
    }

    private Map<String, Object> getSecondPartOfPageViewsDefaultTextsForMainPageTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvPoppedUpOfferTitle", "Заголовок відсутній.");
        M.put("bCreateTheRoute", "Гайда!");
        M.put("etFinishAddress", "Кінець");
        M.put("etStartAddress", "Початок");
        return M;
    }

    private Map<String, Object> getOtherSettings() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", MainActivity.this);
        return M;
    }

    private void setUpSlidingMenu() {
        final MainActivity REQUESTER = MainActivity.this;
        final Integer OFFSET_FOR_MDPI = 120;
        final Float SCALE = pageScaler.getScreenScale();
        final Float SCALED_OFFSET = OFFSET_FOR_MDPI * SCALE;
        final Integer I = SCALED_OFFSET.intValue();
        final LayoutInflater LI = getLayoutInflater();
        final Integer RESOURCE = layout.main_menu;
        final View V = LI.inflate(RESOURCE, null, false);
        slidingMenu = new SlidingMenu(REQUESTER);
        slidingMenu.setBehindOffset(I);
        slidingMenu.setTouchModeAbove(TOUCHMODE_FULLSCREEN);
        slidingMenu.attachToActivity(REQUESTER, SLIDING_WINDOW);
        slidingMenu.setMenu(V);
    }

    private void initializeMenuTranslator() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForMainMenuTranslation();
        menuTranslator = new MainMenuTranslator(SETTINGS);
    }

    private Map<String, Map<String, Object>> getSettingsForMainMenuTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getPageViewsIdsForMainMenuTranslation());
        M.put("Page views default texts", getPageViewsDefaultTextsForMainMenuTranslation());
        M.put("Other settings", getOtherSettings());
        return M;
    }

    private Map<String, Object> getPageViewsIdsForMainMenuTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvMainMenuUserRecommendationsQuantityHeader", id.tvMainMenuUserRecommendationsQuantityHeader);
        M.put("tvMainMenuUserBonusesQuantityHeader", id.tvMainMenuUserBonusesQuantityHeader);
        M.put("tvMainMenuUserName", id.tvMainMenuUserName);
        M.put("tvMainMenuSuppliers", id.bMainMenuSuppliers);
        M.put("tvMainMenuUserRecommendations", id.bMainMenuUserRecommendations);
        M.put("tvMainMenuUserBill", id.bMainMenuUserBill);
        M.put("tvMainMenuUserDiscountCoupons", id.bMainMenuUserDiscountCoupons);
        M.put("tvMainMenuUserOffers", id.bMainMenuUserOffers);
        M.put("tvMainMenuWishList", id.bMainMenuWishList);
        M.put("tvMainMenuProfileSettings", id.bMainMenuProfileSettings);
        M.put("tvMainMenuQuitFromApplication", id.bMainMenuQuitFromApplication);
        return M;
    }

    private Map<String, Object> getPageViewsDefaultTextsForMainMenuTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvMainMenuUserRecommendationsQuantityHeader", "Поради:");
        M.put("tvMainMenuUserBonusesQuantityHeader", "На рахунку:");
        M.put("tvMainMenuUserName", "Невідомо");
        M.put("tvMainMenuSuppliers", "Заклади");
        M.put("tvMainMenuUserRecommendations", "Мої поради");
        M.put("tvMainMenuUserBill", "Мій рахунок");
        M.put("tvMainMenuUserDiscountCoupons", "Мої талони на знижку");
        M.put("tvMainMenuUserOffers", "Мої акції");
        M.put("tvMainMenuWishList", "Список бажань");
        M.put("tvMainMenuProfileSettings", "Налаштування облікового запису");
        M.put("tvMainMenuQuitFromApplication", "Вихід");
        return M;
    }



    private void setTextForRouteRadiusTextView() {
        routeRadiusSeekBarListener.showRouteRadius();
    }

    private void setTheInformationForTheRouteByDefault() {
        final SpannableStringBuilder TIME = getDefaultTextForTime();
        final SpannableStringBuilder TEXT = new SpannableStringBuilder();
        final String FAMILY = "";
        final AssetManager MGR = getAssets();
        final String PATH = "robotolight.otf";
        final Typeface TF = createFromAsset(MGR, PATH);
        final CustomTypefaceSpan WHAT = new CustomTypefaceSpan(FAMILY, TF);
        final Integer START = 0;
        final SpannableStringBuilder DISTANCE = getDefaultTextForDistance();
        final Integer END = DISTANCE.length();
        final View V = findViewById(id.tvRouteInformation);
        final TextView TV = (TextView) V;
        DISTANCE.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_INCLUSIVE);
        TEXT.append(TIME);
        TEXT.append(' ');
        TEXT.append(DISTANCE);
        TV.setText(TEXT);
    }

    private SpannableStringBuilder getDefaultTextForTime() {
        final SpannableStringBuilder TEXT = new SpannableStringBuilder();
        final CharSequence HEADER = pageTranslator.getHeaderForApproximateTime();
        TEXT.append("≈ 0 ");
        TEXT.append(HEADER);
        TEXT.append('.');
        return TEXT;
    }

    private SpannableStringBuilder getDefaultTextForDistance() {
        final SpannableStringBuilder TEXT = new SpannableStringBuilder();
        final CharSequence HEADER = pageTranslator.getHeaderForApproximateDistance();
        TEXT.append("(0 ");
        TEXT.append(HEADER);
        TEXT.append(')');
        return TEXT;
    }

    private void translateThisPage() {
        pageTranslator.translate();
        menuTranslator.translate();
    }

    private void scaleThisPage() {
        final View V = slidingMenu.getMenu();
        pageScaler.scaleThePage();
        pageScaler.scale(V);
    }

    private void setFontsForAllTextualViewsOfThisPage() {
        setFontsForAllTextualViewsOfMainMenu();
        setFontsForAllTextualViewsOfRestOfThePage();
    }

    private void setFontsForAllTextualViewsOfMainMenu() {
        final Map<Object, Map> S = getSettingsForSettingFontsInTheMainMenu();
        final MainMenuFontSpecialist MMFS = new MainMenuFontSpecialist(S);
        MMFS.setFonts();
    }

    private Map<Object, Map> getSettingsForSettingFontsInTheMainMenu() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingFontsInTheMainMenu());
        M.put("Other settings", getOtherSettingsForSettingFonts());
        return M;
    }

    private Map getMainSettingsForSettingFontsInTheMainMenu() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvMainMenuUserRecommendationsQuantity, "kelsonsansboldru.otf");
        M.put(id.tvMainMenuUserRecommendationsQuantityHeader, "helveticaneuecyrlight.otf");
        M.put(id.tvMainMenuUserBonusesQuantityHeader, "helveticaneuecyrlight.otf");
        M.put(id.tvMainMenuUserBonusesQuantity, "kelsonsansboldru.otf");
        M.put(id.tvMainMenuUserName, "kelsonsanslightru.otf");
        M.put(id.tvMainMenuUserLocation, "helveticaneuecyrlight.otf");
        M.put(id.bMainMenuSuppliers, "kelsonsansregularru.otf");
        M.put(id.bMainMenuUserRecommendations, "kelsonsansregularru.otf");
        M.put(id.bMainMenuUserBill, "kelsonsansregularru.otf");
        M.put(id.bMainMenuUserDiscountCoupons, "kelsonsansregularru.otf");
        M.put(id.bMainMenuUserOffers, "kelsonsansregularru.otf");
        M.put(id.bMainMenuWishList, "kelsonsansregularru.otf");
        M.put(id.bMainMenuProfileSettings, "kelsonsansregularru.otf");
        M.put(id.bMainMenuQuitFromApplication, "kelsonsansregularru.otf");
        return M;
    }

    private Map getOtherSettingsForSettingFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", MainActivity.this);
        return M;
    }

    private void setFontsForAllTextualViewsOfRestOfThePage() {
        final Map<Object, Map> S = getSettingsForSettingFontsOnTheRestOfThePage();
        final FontSpecialist FS = new FontSpecialist(S);
        FS.setFonts();
    }

    private Map<Object, Map> getSettingsForSettingFontsOnTheRestOfThePage() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingFontsOnTheRestOfThePage());
        M.put("Other settings", getOtherSettingsForSettingFonts());
        return M;
    }

    private Map getMainSettingsForSettingFontsOnTheRestOfThePage() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.putAll(getFirstPartOfMainSettingsForSettingFontsOnTheRestOfThePage());
        M.putAll(getSecondPartOfMainSettingsForSettingFontsOnTheRestOfThePage());
        return M;
    }

    private Map<Object, Object> getFirstPartOfMainSettingsForSettingFontsOnTheRestOfThePage() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvApplicationSettingsMenuHeader, "robotoregular0.otf");
        M.put(id.tvApplicationSettingsMenuCountryChoice, "kelsonsansregularru.otf");
        M.put(id.tvApplicationSettingsMenuRegionChoice, "kelsonsansregularru.otf");
        M.put(id.tvApplicationSettingsMenuCategoryChoice, "kelsonsansregularru.otf");
        M.put(id.tvWatchSharesList, "kelsonsanslightru.otf");
        M.put(id.tvGoToTheMap, "kelsonsanslightru.otf");
        M.put(id.tvSixKm, "robotoregular0.otf");
        M.put(id.tvFiveKm, "robotoregular0.otf");
        M.put(id.tvFourKm, "robotoregular0.otf");
        M.put(id.tvThreeKm, "robotoregular0.otf");
        M.put(id.tvTwoKm, "robotoregular0.otf");
        M.put(id.tvOneKm, "robotoregular0.otf");
        M.put(id.tvHalfKm, "robotoregular0.otf");
        M.put(id.tvZeroKm, "robotoregular0.otf");
        M.put(id.tvRouteRadius, "kelsonsansregularru.otf");
        M.put(id.tvPoppedUpOfferTitle, "kelsonsansregularru.otf");
        return M;
    }

    private Map<Object, Object> getSecondPartOfMainSettingsForSettingFontsOnTheRestOfThePage() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.bCreateTheRouteToThePoppedUpOffer, "robotobold.otf");
        M.put(id.bGoToThePoppedUpOfferPage, "robotobold.otf");
        M.put(id.bStart, "robotoblack.otf");
        M.put(id.bFinish, "robotoblack.otf");
        M.put(id.etStartAddress, "robotoregular0.otf");
        M.put(id.etFinishAddress, "robotoregular0.otf");
        M.put(id.tvRouteInformation, "robotobold.otf");
        M.put(id.bCreateTheRoute, "robotobold.otf");
        return M;
    }

    private void setDrivingModeSelectedByDefault() {
        final View V = findViewById(R.id.ibByTransport);
        final boolean SELECTED = true;
        V.setSelected(SELECTED);
        chosenMode = MODE.DRIVING;
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    private void setupConnectionMonitor() {
        BroadcastReceiver networkStateReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                Log.w("Network Listener", "Network Type Changed");
            }
        };

        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkStateReceiver, filter);
    }

    private void prepareActionBar() {
        final ActionBar AB = getSupportActionBar();
        AB.setDisplayHomeAsUpEnabled(true);
    }

    private void prepareRadiusBar() {
        final View V = findViewById(R.id.sbRouteRadius);
        final SeekBar SB = (SeekBar) V;
        final int SIX_KILOMETERS = 7000;
        SB.setOnSeekBarChangeListener(routeRadiusSeekBarListener);
        SB.setMax(SIX_KILOMETERS);
    }

    private void prepareDrawerToggleButton() {
        final View V = findViewById(id.dlMainPage);
        final DrawerLayout DL = (DrawerLayout) V;
        final MainActivity REQUESTER = MainActivity.this;
        drawerToggleButton = new GooglionDrawerListener(REQUESTER, DL);
        DL.setScrimColor(TRANSPARENT);
        DL.setDrawerListener(drawerToggleButton);
    }

    private void prepareLeftDrawer() {
        findViewById(R.id.bMainMenuProfileSettings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(self, AccountSettingsActivity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.bMainMenuQuitFromApplication).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Starfish.getInstance(MainActivity.this).logout();
                FbLogout();
                Intent intent = new Intent(MainActivity.this, WelcomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
        findViewById(R.id.bMainMenuSuppliers).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(self, VenuesActivity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.bMainMenuUserOffers).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(self, MyDiscountsActivity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.bMainMenuWishList).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(self, WishesListActivity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.bMainMenuUserRecommendations).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(self, RecommendationsActivity.class);
                startActivity(intent);
            }
        });
    }

    private void loadUserInformation() {
        new AsyncTask<Void, Void, UserInfo>() {

            @Override
            protected UserInfo doInBackground(Void... params) {
                return Starfish.getInstance(self).loadUserInfo();
            }

            @Override
            protected void onPostExecute(final UserInfo UI) {
                apply(UI);
            }
        }.execute();
    }

    private void apply(final UserInfo UI) {
        WebImage.setContext(self);
        WebImage.removeFromCache(UI.img_url);
        final Map<Object, Object> SETTINGS = getSettingsForMakingUserPhotoLookSpecialFrom(UI);
        final RoundedShadowedImagesSpecialist S = new RoundedShadowedImagesSpecialist(SETTINGS);
        S.makeImageLookSpecial();

        TextView userFullName = (TextView) findViewById(R.id.tvMainMenuUserName);
        userFullName.setText(UI.name);
        //show user country and region
        TextView userRecomendationsAmout = (TextView) findViewById(id.tvMainMenuUserRecommendationsQuantity);
        userRecomendationsAmout.setText(UI.cnt_likes);
        TextView userBonusesAmout = (TextView) findViewById(id.tvMainMenuUserBonusesQuantity);
        userBonusesAmout.setText(UI.cnt_bonuses);
        showUserLocationFrom(UI);
    }

    private Map<Object, Object> getSettingsForMakingUserPhotoLookSpecialFrom(final UserInfo UI) {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        final View V = findViewById(id.sivMainMenuUserPhoto);
        final ImageView IV = (ImageView) V;
        M.put("Image url", UI.img_url);
        M.put("Requester", MainActivity.this);
        M.put("Image view", IV);
        return M;
    }

    private void showUserLocationFrom(final UserInfo UI) {
        final CharSequence COUNTRY_HEADER = menuTranslator.getUserCountryHeader();
        final SpannableStringBuilder TEXT = new SpannableStringBuilder(COUNTRY_HEADER);
        final CharSequence COUNTRY_VALUE = getInformationAboutUserCountryFrom(UI);
        final CharSequence CITY_HEADER = menuTranslator.getUserCityHeader();
        final CharSequence CITY_VALUE = getInformationAboutUserCityFrom(UI);
        final View V = findViewById(id.tvMainMenuUserLocation);
        final TextView TV = (TextView) V;
        TEXT.append(' ');
        TEXT.append(COUNTRY_VALUE);
        TEXT.append("     ");
        TEXT.append(CITY_HEADER);
        TEXT.append(' ');
        TEXT.append(CITY_VALUE);
        TV.setText(TEXT);
    }

    private CharSequence getInformationAboutUserCountryFrom(final UserInfo UI) {
        final ForegroundColorSpan WHAT = getForegroundColorSpanForUserLocationValue();
        final Integer START = 0;
        final CharSequence COUNTRY = UI.country_name;
        final SpannableStringBuilder SSB = new SpannableStringBuilder(COUNTRY);
        final Integer END = SSB.length();
        SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
        return SSB;
    }

    private ForegroundColorSpan getForegroundColorSpanForUserLocationValue() {
        final Integer RED = 56;
        final Integer GREEN = 186;
        final Integer BLUE = 96;
        final int COLOR = rgb(RED, GREEN, BLUE);
        return new ForegroundColorSpan(COLOR);
    }

    private CharSequence getInformationAboutUserCityFrom(final UserInfo UI) {
        final ForegroundColorSpan WHAT = getForegroundColorSpanForUserLocationValue();
        final Integer START = 0;
        final CharSequence COUNTRY = UI.region_name;
        final SpannableStringBuilder SSB = new SpannableStringBuilder(COUNTRY);
        final Integer END = SSB.length();
        SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
        return SSB;
    }


    private void prepareCategorySelector() {
        View regions = findViewById(R.id.rlApplicationSettingsMenuRegionChoice);
        View countries = findViewById(R.id.rlApplicationSettingsMenuCountryChoice);
        View categories = findViewById(R.id.rlApplicationSettingsMenuCategoryChoice);

        categorySelector = new CategorySelector(this, pageScaler) {
            @Override
            public void onEverythingLoaded() {
                super.onEverythingLoaded();
                mapManager.setCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        clearTheMap();
                        loadOffers();
                        try {
                            addRouteMarkersToTheMap();
                        } catch (final Exception E) {
                            E.printStackTrace();
                        }
                        drawRouteWithItsBorderOnTheMap();
                    }
                });
            }
        };
        categorySelector.injectOnCategoriesListener(categories);
        categorySelector.injectOnRegionsListener(regions);
        categorySelector.injectOnCountriesListener(countries);
        categorySelector.setCategoriesListener(new CategorySelector.onCategoryChoiceListener() {
            @Override
            public void onCategoryChoice(Category category, boolean categorySelected) {
                if (categorySelected) {
                    addCategory(category.id);
                } else {
                    removeCategory(category.id);
                }
            }
        });
        CategorySelector.onRegionSelectedListener regionSelectedListener = new CategorySelector.onRegionSelectedListener() {
            @Override
            public void onRegionSelected(Region r) {
                Log.d("regionssellis", ".");
                if (userIsNotOnTheMap()) {
                    updateSharesListUnderTheMap();
                } else {
                    loadOffersOnTheMap();
                }
            }
        };
        categorySelector.setRegionSelectedListener(
                regionSelectedListener);
    }

    private void clearTheMap() {
        map.clear();
    }

    private void loadOffers() {
        final int[] IDS = categorySelector.getSelectedCategoriesIds();
        final boolean CLEAR = true;
        loadOffers(IDS, CLEAR);
    }

    public void addRouteMarkersToTheMap() {
        addStartMarkerToTheMap();
        addFinishMarkerToTheMap();
    }

    private void addStartMarkerToTheMap() {
        final MarkerOptions MO = getStartMarkerOptions();
        startMarker = map.addMarker(MO);
    }

    private MarkerOptions getStartMarkerOptions() {
        final MarkerOptions MO = new MarkerOptions();
        final BitmapDescriptor ICON = defaultMarker(HUE_BLUE);
        MO.position(location);
        MO.icon(ICON);
        return MO;
    }

    private void addFinishMarkerToTheMap() {
        final MarkerOptions MO = getFinishMarkerOptions();
        finishMarker = map.addMarker(MO);
    }

    private MarkerOptions getFinishMarkerOptions() {
        final MarkerOptions MO = new MarkerOptions();
        final BitmapDescriptor ICON = defaultMarker(HUE_ORANGE);
        MO.position(target);
        MO.icon(ICON);
        return MO;
    }

    private void drawRouteWithItsBorderOnTheMap() {
        try {
            drawRouteWithItsBorder();
        } catch (final Exception E) {
            E.printStackTrace();
        }
    }

    private void drawRouteWithItsBorder() {
        drawRouteOnTheMap();
        drawRouteBorderOnTheMap();
    }

    private void drawRouteOnTheMap() {
        mapRouteOptions.color(RED);
        map.addPolyline(mapRouteOptions);
    }

    private void drawRouteBorderOnTheMap() {
        final List<LatLng> PATH = mapRouteOptions.getPoints();
        final double RADIUS = routeRadius;
        addBorder(PATH, RADIUS);
    }

    private void addBorder(List<LatLng> path, double radius) {
        RouteCircler mRB = new RouteCircler();
        if (path == null || path.isEmpty()) return;
        ArrayList<RouteBoxer.LatLng> adaptedPath = new ArrayList<RouteBoxer.LatLng>(path.size());
        for (LatLng ll : path) {
            adaptedPath.add(new RouteBoxer.LatLng(ll.latitude, ll.longitude));
        }
        List<RouteBoxer.LatLng> res = mRB.circle(adaptedPath, radius);
        for (Circle p : mPathBorders) {
            p.remove();
        }
        mPathBorders.clear();
        final MapVisibleRegionBoundsCreator MVRBC = new MapVisibleRegionBoundsCreator(map);
        final List<RouteBoxer.LatLng> VISIBLE_POINTS_LIST = new LinkedList<RouteBoxer.LatLng>();
        final int MAX_ROUTE_POINTS = 200;
        for (RouteBoxer.LatLng v : res) {
            Log.d("Box", v.toString());
            if (MVRBC.pointIsVisible(v)) {
                VISIBLE_POINTS_LIST.add(v);
            }
        }
        if (VISIBLE_POINTS_LIST.size() <= MAX_ROUTE_POINTS) {
            addVisiblePointsToTheRoute(VISIBLE_POINTS_LIST);
        }
    }

    private void addVisiblePointsToTheRoute(final List<RouteBoxer.LatLng> VISIBLE_POINTS_LIST) {
        for (RouteBoxer.LatLng v : VISIBLE_POINTS_LIST) {
            addVisiblePointToTheRoute(v);
        }
    }

    private void addVisiblePointToTheRoute(final RouteBoxer.LatLng V) {
        final CircleOptions CO = getCircleOptionsFrom(V);
        final Circle C = map.addCircle(CO);
        mPathBorders.add(C);
    }

    private CircleOptions getCircleOptionsFrom(final RouteBoxer.LatLng POINT) {
        final CircleOptions CO = new CircleOptions();
        final double LATITUDE = POINT.lat;
        final double LONGITUDE = POINT.lng;
        final LatLng CENTER = new LatLng(LATITUDE, LONGITUDE);
        final int ALPHA = 128;
        final int RED = 0;
        final int GREEN = 0;
        final int BLUE = 255;
        final int COLOR = argb(ALPHA, RED, GREEN, BLUE);
        CO.center(CENTER);
        CO.radius(routeRadius);
        CO.strokeColor(TRANSPARENT);
        CO.fillColor(COLOR);
        return CO;
    }

    private boolean userIsNotOnTheMap() {
        return slidingDrawer.isOpened();
    }

    private void updateSharesListUnderTheMap() {
        openListener.updateSharesListUnderTheMap();
    }

    public void loadOffersOnTheMap() {
        mapManager.clear();
        final LatLng topLeft = mapManager.getTopLeft();
        final LatLng botRight = mapManager.getBotRight();

        new AsyncTask<Void, Void, List<Offer>>() {
            @Override
            protected List<Offer> doInBackground(Void... voids) {
                int[] ids = categorySelector.getSelectedCategoriesIds();
                Region selectedRegion = categorySelector.getSelectedRegion();
                if (selectedRegion != null) {
                    return Starfish.getInstance(MainActivity.this)
                            .getOffers(selectedRegion.id, ids,
                                    topLeft, botRight);
                } else {
                    return new ArrayList<Offer>();
                }
            }

            @Override
            protected void onPostExecute(List<Offer> offers) {
                mapManager.showOffers(offers);
//                        if(listManager.isOpened())
//                            listManager.showOffers(offers);
            }
        }.execute();
    }

    private void setUpSlidingDrawer() {
        final MainActivity REQUESTER = MainActivity.this;
        final View V = findViewById(R.id.sdToggleBetweenPages);
        final Bundle SETTINGS = getOnGooglionDrawerOpenListenerSettings();
        final OnDrawerCloseListener CLOSE_LISTENER = new OnGooglionDrawerCloseListener(REQUESTER);
        openListener = new OnGooglionDrawerOpenListener(SETTINGS);
        slidingDrawer = (SlidingDrawer) V;
        slidingDrawer.setOnDrawerCloseListener(CLOSE_LISTENER);
        slidingDrawer.setOnDrawerOpenListener(openListener);
    }

    private Bundle getOnGooglionDrawerOpenListenerSettings() {
        final Bundle S = new Bundle();
        S.putParcelable("Requester", MainActivity.this);
        S.putParcelable("Page scaler", pageScaler);
        return S;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (latLng != null) {
            switch (action) {
                case SELECT_START: {
                    final View V = findViewById(R.id.bStart);
                    location = latLng;
                    try {
                        startMarker.setPosition(latLng);
                    } catch (final Exception E) {
                        addStartMarkerToTheMap();
                    }
                    startET.setText(latLng.latitude + "," + latLng.longitude);
                    geoCode(latLng, startET);
                    deselect(V);
//                    if (circle != null) {
//                        circle.setCenter(location);
//                    }
                    break;
                }
                case SELECT_FINISH: {
                    final View V = findViewById(R.id.bFinish);
                    target = latLng;
                    try {
                        finishMarker.setPosition(latLng);
                    } catch (final Exception E) {
                        addFinishMarkerToTheMap();
                    }
                    finishET.setText(latLng.latitude + "," + latLng.longitude);
                    geoCode(latLng, finishET);
                    deselect(V);
                    break;
                }
            }
        }
    }

    public void geoCode(final LatLng point, final EditText receiver) {
        final String urlFormat = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... voids) {
                return new DownloadWebPageTask().executeWOToken(urlFormat + point.latitude + "," + point.longitude);
            }

            @Override
            protected void onPostExecute(String s) {
                try {
                    Object ob = new JSONTokener(s).nextValue();
                    if (!(ob instanceof JSONArray)) {
                        return;
                    }
                    JSONArray jo = (JSONArray) ob;
                    String address = jo.getJSONObject(0).getString("formatted_address");
                    receiver.setText(address);
                    Log.d("fd_address", address);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }

    public void showDirections(LatLng start, LatLng end, MODE mode) {
        showDirections(start.latitude + "," + start.longitude, end.latitude + "," + end.longitude, mode);
    }

    public void showDirections(LatLng start) {
        location = mapManager.getLocation();
        target = start;
        showDirections(location, start, NONE);
    }

    public void showDirections(String start, String end, MODE mode) {
        String url;
        try {
            url = getDirectionsUrl(start, end, mode.value);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        AsyncTask<String, String, String> task = new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String[] params) {
                return new DownloadWebPageTask().executeWOToken(params[0]);
            }

            @Override
            protected void onPostExecute(String result) {
                DirectionsJSONParser parser = new DirectionsJSONParser();
                try {
                    final JSONObject JSON_OBJECT = new JSONObject(result);
                    List<List<LatLng>> routes = parser.parse(JSON_OBJECT);
                    for (List<LatLng> route : routes) {
                        Log.d("asdf before", "showRoute");
                        showRoute(route);
                        Log.d("asdf after", "showRoute");
                        if (!route.isEmpty()) {
                            LatLng start = route.get(0);
                            LatLng finish = route.get(route.size() - 1);
                            mapManager.flyTo(start, 14);
                        }
                    }
                    if (!(routes.isEmpty())) {
                        if (!routes.get(0).isEmpty()) {
                            try {
                                startMarker.setPosition(routes.get(0).get(0));
                            } catch (final Exception E) {
                                E.getMessage();
                            }
                        }
                        final List<LatLng> lastRouteLeg = routes.get(routes.size() - 1);
                        if (!lastRouteLeg.isEmpty()) {
                            finishMarker.setPosition(lastRouteLeg.get(lastRouteLeg.size() - 1));
                        }
                    }
                    printRouteDistanceAndTimeInPathOptionsMenuFrom(JSON_OBJECT);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("BadDirectionsJSON", e.getMessage());
                }
            }

            private void printRouteDistanceAndTimeInPathOptionsMenuFrom(final JSONObject JSON_OBJECT) throws JSONException {
                final JSONArray ROUTES = JSON_OBJECT.getJSONArray("routes");
                final JSONObject ROUTE = ROUTES.getJSONObject(0);
                final JSONArray LEGS = ROUTE.getJSONArray("legs");
                final JSONObject LEG = LEGS.getJSONObject(0);
                printRouteInformationFrom(LEG);
            }

            private void printRouteInformationFrom(final JSONObject LEG) throws JSONException {
                final SpannableStringBuilder TIME = getTextForTimeFrom(LEG);
                final String FAMILY = "";
                final AssetManager MGR = getAssets();
                final String PATH = "robotolight.otf";
                final Typeface TF = createFromAsset(MGR, PATH);
                final CustomTypefaceSpan WHAT = new CustomTypefaceSpan(FAMILY, TF);
                final Integer START = 0;
                final SpannableStringBuilder DISTANCE = getTextForDistanceFrom(LEG);
                final Integer END = DISTANCE.length();
                final SpannableStringBuilder TEXT = new SpannableStringBuilder();
                final View V = findViewById(id.tvRouteInformation);
                final TextView TV = (TextView) V;
                DISTANCE.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_INCLUSIVE);
                TEXT.append(TIME);
                TEXT.append(' ');
                TEXT.append(DISTANCE);
                TV.setText(TEXT);
            }

            private SpannableStringBuilder getTextForTimeFrom(final JSONObject LEG) throws JSONException {
                final JSONObject TIME = LEG.getJSONObject("duration");
                final String TIME_TEXT = TIME.getString("text");
                final SpannableStringBuilder TEXT = new SpannableStringBuilder();
                TEXT.append("≈ ");
                TEXT.append(TIME_TEXT);
                return TEXT;
            }

            private SpannableStringBuilder getTextForDistanceFrom(final JSONObject LEG) throws JSONException {
                final SpannableStringBuilder TEXT = new SpannableStringBuilder();
                final JSONObject DISTANCE = LEG.getJSONObject("distance");
                final String DISTANCE_TEXT = DISTANCE.getString("text");
                TEXT.append('(');
                TEXT.append(DISTANCE_TEXT);
                TEXT.append(')');
                return TEXT;
            }
        };
        task.execute(url);
    }

    public String getDirectionsUrl(LatLng start, LatLng end, String mode) {
        try {
            return getDirectionsUrl(start.latitude + "," + start.longitude, end.latitude + "," + end.longitude, mode);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getDirectionsUrl(String start, String end, String mode) throws UnsupportedEncodingException {
        return "http://maps.googleapis.com/maps/api/directions/json?"
                + "origin=" + URLEncoder.encode(start, "utf8")
                + "&destination=" + URLEncoder.encode(end, "utf8")
                + "&sensor=false&mode=" + mode + "&language=" + pageTranslator.getLanguageAbbreviation();
    }

    private void showRoute(List<LatLng> route) {
        PolylineOptions rectLine = new PolylineOptions().width(3).color(
                Color.RED);
        Log.d("asdf before", "adding points");
        rectLine.addAll(route);
        pointsList = route;
        if (pathLine != null) {
            pathLine.remove();
        }

        Log.d("asdf before", "adding polyline");
        mapRouteOptions = rectLine;
        pathLine = map.addPolyline(rectLine);
        rectLine.color(Color.argb(128, 0, 0, 255));

        Log.d("asdf before", "add border");
        addBorder(route, routeRadius);
    }

    public void setRadius(double radius) {
        routeRadius = radius;
        addBorder(pointsList, routeRadius);
    }

    private void prepareOptionsView() {
        startET = (EditText) findViewById(R.id.etStartAddress);
        finishET = (EditText) findViewById(R.id.etFinishAddress);
    }

    @Override
    protected final void onPostCreate(final Bundle SAVED_INSTANCE_STATE) {
        super.onPostCreate(SAVED_INSTANCE_STATE);
        drawerToggleButton.syncState();
    }

    @Override
    public final boolean onCreateOptionsMenu(final Menu M) {
        inflateMenuItemsIn(M);
        setupSearchViewIn(M);
        return true;
    }

    private void inflateMenuItemsIn(final Menu M) {
        final MenuInflater MI = getMenuInflater();
        MI.inflate(R.menu.search, M);
        MI.inflate(R.menu.path_options, M);
        MI.inflate(R.menu.settings, M);
    }

    private void setupSearchViewIn(final Menu M) {
        final MenuItem SEARCH_ITEM = M.findItem(R.id.action_search);
        setupSearchView(SEARCH_ITEM);
    }

    private void FbLogout() {
        Session session = Session.getActiveSession();
        if (session == null) {
            session = new Session(this);
        }
        Session.setActiveSession(session);
        if (!session.isClosed()) {
            session.closeAndClearTokenInformation();
        }
    }

    @Override
    public final boolean onOptionsItemSelected(final MenuItem ITEM) {
        if (drawerToggleButton.onOptionsItemSelected(ITEM)) {
            return true;
        }
        switch (ITEM.getItemId()) {
            case R.id.action_new: {
                Intent intent = new Intent(this, AddOfferActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.action_messages: {
                final View V = findViewById(R.id.menuHolder);
                V.setVisibility(View.GONE);
                if (pathOptionsMenu.getVisibility() == View.VISIBLE)
                    pathOptionsMenu.setVisibility(View.GONE);
                else
                    pathOptionsMenu.setVisibility(View.VISIBLE);

                break;
            }
            case R.id.action_open_menu: {
                View v = findViewById(R.id.menuHolder);
                pathOptionsMenu.setVisibility(View.GONE);
                if (v.getVisibility() == View.GONE) {
                    v.setVisibility(View.VISIBLE);
                } else {
                    v.setVisibility(View.GONE);
                }
                break;
            }
        }
        return super.onOptionsItemSelected(ITEM);
    }

    public void addCategory(final int id) {
        if (userIsNotOnTheMap()) {
            openListener.updateSharesListUnderTheMap();
        } else {
            loadOffers(new int[]{id}, categorySelector.getSelectedCategoriesIds().length == 1);
        }
    }

    private void loadOffers(final int[] id, final boolean clear) {
        final LatLng topLeft = mapManager.getTopLeft();
        final LatLng botRight = mapManager.getBotRight();

        setProgressBarIndeterminateVisibility(true);
        new AsyncTask<Void, Void, List<Offer>>() {
            long startTime;

            @Override
            protected List<Offer> doInBackground(Void... voids) {
                startTime = lastRequestTime = System.currentTimeMillis();
                while (categorySelector.getSelectedRegion() == null) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (categorySelector.isRegionSelected()) {
                    return Starfish.getInstance(MainActivity.this).getOffers(categorySelector.getSelectedRegion().id, id,
                            topLeft, botRight);
                } else return new ArrayList<Offer>();
            }

            @Override
            protected void onPostExecute(List<Offer> offers) {
                setProgressBarIndeterminateVisibility(false);
                Log.d("loading complete", String.format("Start:%d, Last:%d", startTime, lastRequestTime));
                if (startTime == lastRequestTime) {
                    Log.d("load", "replaced");
                    if (clear) {
                        mapManager.clear();
                    }
                    mapManager.showOffers(offers);
                }
            }
        }.execute();
    }

    public void removeCategory(int id) {
        if (userIsNotOnTheMap()) {
            openListener.updateSharesListUnderTheMap();
        } else {
            loadOffers(categorySelector.getSelectedCategoriesIds(), true);
        }
    }

    public void showPathTo(Offer offer) {
        LatLng location = mapManager.getLocation();
    }

    private void setupSearchView(MenuItem searchItem) {

        SearchView mSearchView = (SearchView) searchItem.getActionView();
        mSearchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                goToSearchActivity(query);
                return false;
            }
        });
    }

    private void goToSearchActivity(String text) {
        Intent intent = new Intent(this, OffersSearchActivity.class);
        intent.putExtra("q", text);
        intent.putExtra("region", categorySelector.getSelectedRegion().id);
        intent.putExtra("categories", categorySelector.getSelectedCategoriesIds());
        startActivity(intent);
    }

    public CategorySelector getCategorySelector() {
        return categorySelector;
    }

    public OnGooglionDrawerOpenListener getOpenListener() {
        return openListener;
    }

    public MapManager getMapManager() {
        return mapManager;
    }

    public void goToTheUserBillPage(final View V) {
        final Context CT = MainActivity.this;
        final Class<?> C = UserBillPage.class;
        final Intent I = new Intent(CT, C);
        startActivity(I);
    }

    public void goToTheUserCouponsPage(final View V) {
        final Context CT = MainActivity.this;
        final Class<?> C = UserCouponsPage.class;
        final Intent I = new Intent(CT, C);
        startActivity(I);
    }

    public void clearTheRoute(final View V) {
        clearTheMap();
        loadOffers();
        markClearButtonAsPressed();
        deleteRouteMarkers();
        removeRouteFromTheMap();
        clearRoutePointsList();
        setTheInformationForTheRouteByDefault();
    }

    private void markClearButtonAsPressed() {
        clearButtonHasBeenPressed = true;
    }

    private void deleteRouteMarkers() {
        deleteStartMarker();
        deleteFinishMarker();
    }

    private void deleteStartMarker() {
        final View V = findViewById(R.id.etStartAddress);
        final EditText ET = (EditText) V;
        final CharSequence CS = "";
        location = null;
        startMarker = null;
        ET.setText(CS);
        //TODO підправити сторінку рахунку користувача (замінити paddingTop на Header View з відповідною висотою)
    }

    private void deleteFinishMarker() {
        final View V = findViewById(R.id.etFinishAddress);
        final EditText ET = (EditText) V;
        final CharSequence CS = "";
        target = null;
        finishMarker = null;
        ET.setText(CS);
    }

    private void removeRouteFromTheMap() {
        mapRouteOptions = new PolylineOptions();
    }

    private void clearRoutePointsList() {
        pointsList.clear();
    }

    public void setDrivingMode(final View V) {
        final View BY_TRANSPORT = findViewById(R.id.ibByTransport);
        final View ON_FOOT = findViewById(R.id.ibOnFoot);
        final boolean SELECTED = true;
        final boolean UNSELECTED = false;
        BY_TRANSPORT.setSelected(SELECTED);
        ON_FOOT.setSelected(UNSELECTED);
        chosenMode = MODE.DRIVING;
    }

    public void setWalkingMode(final View V) {
        final View ON_FOOT = findViewById(R.id.ibOnFoot);
        final View BY_TRANSPORT = findViewById(R.id.ibByTransport);
        final boolean SELECTED = true;
        final boolean UNSELECTED = false;
        ON_FOOT.setSelected(SELECTED);
        BY_TRANSPORT.setSelected(UNSELECTED);
        chosenMode = MODE.WALKING;
    }

    public void performClickOnStartButton(final View START_BUTTON) {
        final View FINISH_BUTTON = findViewById(R.id.bFinish);
        if (FINISH_BUTTON.isSelected()) {
            deselect(FINISH_BUTTON);
        }
        if (START_BUTTON.isSelected()) {
            deselect(START_BUTTON);
        } else {
            selectStartButton(START_BUTTON);
        }
    }

    private void deselect(final View V) {
        final boolean SELECTED = false;
        action = ACTION.NONE;
        V.setSelected(SELECTED);
    }

    private void selectStartButton(final View V) {
        final boolean SELECTED = true;
        action = ACTION.SELECT_START;
        V.setSelected(SELECTED);
    }

    public void performClickOnFinishButton(final View FINISH_BUTTON) {
        final View START_BUTTON = findViewById(R.id.bStart);
        if (START_BUTTON.isSelected()) {
            deselect(START_BUTTON);
        }
        if (FINISH_BUTTON.isSelected()) {
            deselect(FINISH_BUTTON);
        } else {
            selectFinishButton(FINISH_BUTTON);
        }
    }

    private void selectFinishButton(final View V) {
        final boolean SELECTED = true;
        action = ACTION.SELECT_FINISH;
        V.setSelected(SELECTED);
    }

    public void createTheRoute(final View V) {
        buildRoute();
        saveRouteStartAndFinishPointsCoordinates();
    }

    private void buildRoute() {
        //TODO Позначки кінців шляхоспису (коли той ще не побудований) зникають з карти.
        final String NAME = "Route";
        final int MODE = MODE_PRIVATE;
        final SharedPreferences SP = getSharedPreferences(NAME, MODE);
        final String START = SP.getString("Start point", getRouteStartPointCoordinates());
        final String FINISH = SP.getString("Finish point", getRouteStartPointCoordinates());
        clearButtonHasBeenPressed = false;
        showDirections(START, FINISH, chosenMode);
    }

    private String getRouteStartPointCoordinates() {
        final int ID = id.etStartAddress;
        return getTextFromTextViewWithId(ID);
    }

    private String getTextFromTextViewWithId(final int ID) {
        final View V = findViewById(ID);
        final EditText ET = (EditText) V;
        final CharSequence TEXT = ET.getText();
        return TEXT.toString();
    }

    private String getRouteFinishPointCoordinates() {
        final int ID = id.etFinishAddress;
        return getTextFromTextViewWithId(ID);
    }

    private void saveRouteStartAndFinishPointsCoordinates() {
        final String NAME = "Route";
        final int MODE = MODE_PRIVATE;
        final SharedPreferences SP = getSharedPreferences(NAME, MODE);
        final Editor E = SP.edit();
        E.putString("Start point", getRouteStartPointCoordinates());
        E.putString("Finish point", getRouteFinishPointCoordinates());
        E.commit();
    }

    @Override
    public final int describeContents() {
        return 0;
    }

    @Override
    public final void writeToParcel(final Parcel DESTINATION, final int FLAGS) {
    }

    @Override
    public final void onBackPressed() {
        if (slidingMenu.isMenuShowing()) {
            slidingMenu.showContent();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        translateThisPageAgainIfUserHasChangedApplicationLanguage();
        performOnResumeMethodFromSuperclass();
    }

    private void translateThisPageAgainIfUserHasChangedApplicationLanguage() {
        if (userHasChangedApplicationLanguage()) {
            translateThisPageAgain();
        }
    }

    private boolean userHasChangedApplicationLanguage() {
        final String NAME = "Chosen language";
        final int MODE = MODE_PRIVATE;
        final SharedPreferences SP = getSharedPreferences(NAME, MODE);
        final String KEY = "Application language is changed";
        final Editor E = SP.edit();
        final boolean DEFAULT_VALUE = false;
        final boolean B = SP.getBoolean(KEY, DEFAULT_VALUE);
        E.remove(KEY);
        E.commit();
        return B;
    }

    private void translateThisPageAgain() {
        resetTranslators();
        translateThisPage();
        setTextForRouteRadiusTextView();
        translateInformationAboutRoute();
        translateSharesListWithMoreButtonUnderTheMap();
        reloadPoppedUpOfferInformation();
    }

    private void resetTranslators() {
        initializePageTranslator();
        initializeMenuTranslator();
    }

    private void translateInformationAboutRoute() {
        if (userHasPressedClearButton()) {
            setTheInformationForTheRouteByDefault();
        } else {
            buildRoute();
        }
    }

    private boolean userHasPressedClearButton() {
        return clearButtonHasBeenPressed;
    }

    private void translateSharesListWithMoreButtonUnderTheMap() {
        openListener.clearSharesList();
        openListener.displayShares();
        openListener.translateFooterView();
    }

    private void reloadPoppedUpOfferInformation() {
        try {
            reloadOfferInformation();
        } catch (final Exception E) {
            E.printStackTrace();
        }
    }

    private void reloadOfferInformation() {
        final MapManager MM = getMapManager();
        MM.performClickOnSelectedMarker();
    }

    private void performOnResumeMethodFromSuperclass() {
        super.onResume();
    }

    public final MainPageTranslator getPageTranslator() {
        return pageTranslator;
    }

    public final PageScaler getPageScaler() {
        return pageScaler;
    }

    public final SlidingMenu getSlidingMenu() {
        return slidingMenu;
    }

    enum MODE {
        WALKING("walking"),
        TRANSIT("transit"),
        DRIVING("driving"),
        BICYCLING("bicycling "),
        NONE("");

        String value;

        MODE(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    private enum ACTION {
        SELECT_START,
        SELECT_FINISH,
        NONE
    }
}