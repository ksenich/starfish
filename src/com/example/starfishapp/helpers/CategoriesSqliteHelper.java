package com.example.starfishapp.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import com.example.starfishapp.model.Category;

import java.util.ArrayList;
import java.util.List;

public class CategoriesSqliteHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "categories.db";
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String INT_TYPE = "INT";
    private static final String BOOL_TYPE = "BOOLEAN";
    private static final String SQL_CREATE_OFFERS =
            "CREATE TABLE " + CategoryEntry.TABLE_NAME + " (" +
                    CategoryEntry._ID + " INT PRIMARY KEY NOT NULL," +
                    CategoryEntry.TITLE + TEXT_TYPE + COMMA_SEP +
                    CategoryEntry.SELECTED + BOOL_TYPE + COMMA_SEP +
                    " )";
    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + CategoryEntry.TABLE_NAME;

    public CategoriesSqliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_OFFERS);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public List<Category> getCategories() {
        String[] cols = {CategoryEntry.TITLE,
                CategoryEntry._ID,
                CategoryEntry.SELECTED};
        Cursor c = getReadableDatabase().query(false, CategoryEntry.TABLE_NAME, cols, null, null, null, null, null, null);
        List<Category> res = new ArrayList<Category>(c == null ? 0 : c.getCount());
        if (c != null) {
            for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
                res.add(new Category(c.getInt(1), c.getString(0)));
            }
        }
        return res;
    }

    public void saveCategories(List<Category> res) {
        for (Category cat : res) {
            ContentValues cv = new ContentValues();
            cv.put(CategoryEntry.TITLE, cat.name);
            cv.put(CategoryEntry._ID, cat.id);
            cv.put(CategoryEntry.SELECTED, cat.selected);
            getWritableDatabase().insert(CategoryEntry.TABLE_NAME, null, cv);
        }
    }

    public static abstract class CategoryEntry implements BaseColumns {
        public static final String TABLE_NAME = "offer";
        public static final String TITLE = "name";
        public static final String SELECTED = "selected";
    }

}
