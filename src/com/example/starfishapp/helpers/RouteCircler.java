package com.example.starfishapp.helpers;

import java.util.ArrayList;
import java.util.List;

import com.example.starfishapp.helpers.RouteBoxer.*;

public class RouteCircler {
    public List<LatLngBounds> box(List<LatLng> path, double radius) {
        ArrayList<LatLngBounds> res = new ArrayList<LatLngBounds>(path.size());
        for (LatLng p : path) {
            res.add(new LatLngBounds(new LatLng(p.lat - radius, p.lng - radius), new LatLng(p.lat + radius, p.lng + radius)));
        }
        return res;
    }

    public List<LatLng> circle(List<LatLng> path, double radius) {
        ArrayList<LatLng> res = new ArrayList<LatLng>(path.size());
        res.add(path.get(0));
        for (int i = 1, pathSize = path.size(); i < pathSize; i++) {
            LatLng p = path.get(i);
            LatLng q = path.get(i - 1);
            double d = p.distanceFrom(q);
            while (d > 0) {
                q = q.rhumbDestinationPoint(q.rhumbBearingTo(p), radius / 2);
                res.add(q);
                d -= radius / 2;
            }
            res.add(p);//new LatLngBounds(new LatLng(p.lat - radius, p.lng - radius), new LatLng(p.lat+radius, p.lng + radius)));
        }
        return res;
    }


}
