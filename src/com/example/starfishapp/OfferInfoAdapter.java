package com.example.starfishapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.starfishapp.model.Offer;
import com.example.starfishapp.utilities.ImgCache;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.util.HashMap;
import java.util.Map;

public class OfferInfoAdapter implements GoogleMap.InfoWindowAdapter {
    Context mContext;
    ImgCache cache = new ImgCache();
    Map<Marker, Offer> offers = new HashMap<Marker, Offer>();

    public OfferInfoAdapter(Context context) {
        mContext = context;
    }

    public void addOffer(Offer offer, Marker marker) {
        offers.put(marker, offer);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(final Marker marker) {
        final Activity A = (Activity) mContext;
        View resultView = A.findViewById(R.id.rlOfferPopupWindow);
        if (resultView != null) {
            final ImageView imageView = (ImageView) resultView.findViewById(R.id.ivPoppedUpOfferImage);
            TextView nameView = (TextView) resultView.findViewById(R.id.tvPoppedUpOfferTitle);
            final Offer offer = offers.get(marker);
            if (offer != null) {
                if (offer.image == null) {
                    ImgCache.ImgDownload d = cache.new ImgDownload(offer.img_url, imageView) {
                        @Override
                        protected void onPostExecute(Bitmap pic) {
                            super.onPostExecute(pic);
                            offer.image = pic;
                            if (marker.isInfoWindowShown()) {
                                marker.showInfoWindow();
                            }
                        }
                    };
                    d.execute();
                } else {
                    imageView.setImageBitmap(offer.image);
                }
                nameView.setText(Html.fromHtml(offer.name));
                return resultView;
            }
        }
        resultView = new TextView(mContext);
        ((TextView) resultView).setText("Nothing");
        return resultView;
    }

    public Context getContext() {
        return mContext;
    }

    public Offer getOffer(Marker marker) {
        return offers.get(marker);
    }
}
