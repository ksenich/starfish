package com.example.starfishapp;

import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.example.starfishapp.model.Offer;
import com.loopj.android.image.SmartImageView;

import java.util.ArrayList;
import java.util.List;

public class OfferAdapter extends ArrayAdapter<Offer> {
    MainActivity mContext;
    List<Offer> offers = new ArrayList<Offer>();

    public OfferAdapter(MainActivity context) {
        super(context, 0);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
            convertView = inflater.inflate(R.layout.proposition, parent, false);
        }
        SmartImageView imageView = (SmartImageView) convertView.findViewById(R.id.ivPoppedUpOfferImage);
        TextView nameView = (TextView) convertView.findViewById(R.id.offer_title);
        TextView descriptionView = (TextView) convertView.findViewById(R.id.offer_description);
        TextView priceView = (TextView) convertView.findViewById(R.id.price);
        final Offer offer = getItem(position);
        imageView.setImageUrl(offer.img_url);
        nameView.setText(offer.name);
        descriptionView.setText(Html.fromHtml(offer.description));
        priceView.setText(offer.price + "(" + offer.priceCoupon + ")");
        Button button = (Button) convertView.findViewById(R.id.show_path_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.showPathTo(offer);
            }
        });
        return convertView;
    }

    public void clear() {
        super.clear();
        offers.clear();
        notifyDataSetChanged();
    }
}
