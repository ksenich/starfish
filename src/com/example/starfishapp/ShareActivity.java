package com.example.starfishapp;

import android.content.*;
import android.os.*;
import android.support.v7.app.*;
import android.text.*;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.RelativeSizeSpan;
import android.util.*;
import android.view.*;
import android.widget.*;
import com.example.starfishapp.model.*;
import com.example.starfishapp.pages.*;
import com.example.starfishapp.R.*;
import com.googlion.shield.specialists.FontSpecialist;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;
import com.loopj.android.image.*;

import java.text.*;
import java.util.*;

import static android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE;

public class ShareActivity extends ActionBarActivity {
    public static final String OFFER_KEY = "com.example.starfishapp.ShareActivity.OFFER";
    private ActionBarActivity self = this;
    private SharePageTranslator pageTranslator;
    private View favButton;
    private View dislikeButton;
    private View likeButton;
    private TextView likeCounter;
    private TextView dislikeCounter;
    private boolean favButtonSelected;
    private boolean dislikeButtonSelected;
    private boolean likeButtonSelected;
    private int offer_id;
    private OfferUserAttitude offer;

    @Override
    protected void onCreate(final Bundle SAVED_INSTANCE_STATE) {
        super.onCreate(SAVED_INSTANCE_STATE);
        setContentView(layout.share_page);
        initializeOfferIdFrom(SAVED_INSTANCE_STATE);
        initializePageTranslator();
        setUpActionBar();
        setFontsForAllTextualViewsOfThisPage();
        scaleThisPage();
        translateThisPage();
        makeLinksInOfferConditionsAndDescriptionClickable();
        startLoadingOfferAttitude(offer_id);
    }

    private void initializeOfferIdFrom(final Bundle SAVED_INSTANCE_STATE) {
        try {
            offer_id = SAVED_INSTANCE_STATE.getInt(OFFER_KEY);
        } catch (final Exception E) {
            offer_id = getIntent().getIntExtra(OFFER_KEY, -1);
        }
    }

    private void initializePageTranslator() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForSharePageTranslation();
        pageTranslator = new SharePageTranslator(SETTINGS);
    }

    private Map<String, Map<String, Object>> getSettingsForSharePageTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getPageViewsIds());
        M.put("Page views default texts", getPageViewsDefaultTexts());
        M.put("Other settings", getOtherSettings());
        return M;
    }

    private Map<String, Object> getPageViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvShareName", id.tvShareName);
        M.put("tvShareLocation", id.tvShareLocation);
        M.put("tvDiscountCondition", id.tvDiscountCondition);
        M.put("bBuyCoupon", id.bBuyCoupon);
        M.put("tvAddToWishList", id.tvAddToWishList);
        M.put("tvShareConditionsHeader", id.tvShareConditionsHeader);
        M.put("tvShareConditions", id.tvShareConditions);
        M.put("tvShareDescriptionHeader", id.tvShareDescriptionHeader);
        M.put("tvShareDescription", id.tvShareDescription);
        return M;
    }

    private Map<String, Object> getPageViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvShareName", "Заголовок відсутній.");
        M.put("tvShareLocation", "Ніде.");
        M.put("tvDiscountCondition", "Знижка надається за наявності талону");
        M.put("bBuyCoupon", "Купити талон на знижку");
        M.put("tvAddToWishList", "+ додати до списку бажань");
        M.put("tvShareConditionsHeader", "Умови");
        M.put("tvShareConditions", "Умови відсутні.");
        M.put("tvShareDescriptionHeader", "Опис");
        M.put("tvShareDescription", "Опис відсутній.");
        return M;
    }

    private Map<String, Object> getOtherSettings() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", ShareActivity.this);
        return M;
    }

    private void setUpActionBar() {
        final ActionBar AB = getSupportActionBar();
        final CharSequence CS = "";
        AB.setTitle(CS);
    }

    private void setFontsForAllTextualViewsOfThisPage() {
        final Map<Object, Map> S = getSettingsForSettingFonts();
        final FontSpecialist FS = new FontSpecialist(S);
        FS.setFonts();
    }

    private Map<Object, Map> getSettingsForSettingFonts() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingFonts());
        M.put("Other settings", getOtherSettingsForSettingFonts());
        return M;
    }

    private Map getMainSettingsForSettingFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvShareName, "kelsonsansregularru.otf");
        M.put(id.tvShareLocation, "kelsonsansregularru.otf");
        M.put(id.tvSharePrice, "kelsonsansboldru.otf");
        M.put(id.tvShareDiscount, "kelsonsansboldru.otf");
        M.put(id.tvShareTime, "kelsonsansboldru.otf");
        M.put(id.tvLikesQuantity, "robotobold.otf");
        M.put(id.tvDislikesQuantity, "robotobold.otf");
        M.put(id.tvCommentsQuantity, "robotobold.otf");
        M.put(id.tvDiscountCondition, "robotoregular0.otf");
        M.put(id.bBuyCoupon, "robotobold.otf");
        M.put(id.tvAddToWishList, "robotobold.otf");
        M.put(id.tvShareConditionsHeader, "robotobold.otf");
        M.put(id.tvShareConditions, "robotoregular0.otf");
        M.put(id.tvShareDescriptionHeader, "robotobold.otf");
        M.put(id.tvShareDescription, "robotoregular0.otf");
        return M;
    }

    private Map getOtherSettingsForSettingFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", ShareActivity.this);
        return M;
    }

    private void scaleThisPage() {
        final ActionBarActivity REQUESTER = ShareActivity.this;
        final PageScaler PS = new PageScaler(REQUESTER);
        PS.scaleThePage();
    }

    private void translateThisPage() {
        pageTranslator.translate();
    }

    private void makeLinksInOfferConditionsAndDescriptionClickable() {
        makeLinksInOfferConditionsClickable();
        makeLinksInOfferDescriptionClickable();
    }

    private void makeLinksInOfferConditionsClickable() {
        final View V = findViewById(id.tvShareConditions);
        final TextView TV = (TextView) V;
        final MovementMethod MM = LinkMovementMethod.getInstance();
        TV.setMovementMethod(MM);
    }

    private void makeLinksInOfferDescriptionClickable() {
        final View V = findViewById(id.tvShareDescription);
        final TextView TV = (TextView) V;
        final MovementMethod MM = LinkMovementMethod.getInstance();
        TV.setMovementMethod(MM);
    }

    private void startLoadingOfferAttitude(final int id) {
        new AsyncTask<Void, Void, OfferUserAttitude>() {

            @Override
            protected OfferUserAttitude doInBackground(Void... params) {
                return Starfish.getInstance(self).loadOfferUserAttitude(id);
            }

            @Override
            protected void onPostExecute(final OfferUserAttitude OUA) {
                offer = OUA;
                fillInViews();
            }
        }.execute();
    }

    private void setUpListeners() {
        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isLiked())
                    sendLike();
                else
                    unsendLike();
            }
        });
        dislikeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isDisliked())
                    sendDislike();
                else
                    unsendDislike();
            }
        });
        favButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isFaved())
                    sendFav();
                else
                    unsendFav();
            }
        });
        Button buyButton = (Button) findViewById(id.bBuyCoupon);
        buyButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToBuyActivity();
            }
        });
        TextView addToWishes = (TextView) findViewById(id.tvAddToWishList);
        addToWishes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFav();
            }
        });
    }

    private void goToBuyActivity() {
        final ActionBar AB = getSupportActionBar();
        final Context PACKAGE_CONTEXT = ShareActivity.this;
        final Class<?> C = BuyActivity.class;
        final Intent I = new Intent(PACKAGE_CONTEXT, C);
        final CharSequence CS = pageTranslator.getBuyCouponMenuActionBarTitle();
        AB.setTitle(CS);
        I.putExtra("offer", offer);
        startActivity(I);
    }

    private void unsendLike() {
        int lc = Integer.valueOf(likeCounter.getText().toString());
        likeCounter.setText(String.valueOf(lc - 1));
        setLikeButtonSelected(false);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void[] params) {
                Starfish.getInstance(self).unLikeOffer(offer_id);
                return null;
            }
        }.execute();
    }

    private void unsendDislike() {
        int lc = Integer.valueOf(dislikeCounter.getText().toString());
        dislikeCounter.setText(String.valueOf(lc - 1));
        setDislikeButtonSelected(false);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void[] params) {
                Starfish.getInstance(self).unDislikeOffer(offer_id);
                return null;
            }
        }.execute();
    }

    private void unsendFav() {
        setFavButtonSelected(false);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void[] params) {
                Starfish.getInstance(self).unFavOffer(offer_id);
                return null;
            }
        }.execute();

    }

    private boolean isFaved() {
        return isFavButtonSelected();
    }

    private boolean isDisliked() {
        return isDislikeButtonSelected();
    }

    private boolean isLiked() {
        return isLikeButtonSelected();
    }

    private void sendFav() {
        setFavButtonSelected(true);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void[] params) {
                Starfish.getInstance(self).favOffer(offer_id);
                return null;
            }
        }.execute();
    }

    private void sendDislike() {
        int lc = Integer.valueOf(dislikeCounter.getText().toString());
        dislikeCounter.setText(String.valueOf(lc + 1));
        setDislikeButtonSelected(true);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void[] params) {
                Starfish.getInstance(self).dislikeOffer(offer_id);
                return null;
            }
        }.execute();
    }

    private void sendLike() {
        int lc = Integer.valueOf(likeCounter.getText().toString());
        likeCounter.setText(String.valueOf(lc + 1));
        setLikeButtonSelected(true);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void[] params) {
                Starfish.getInstance(self).likeOffer(offer_id);
                return null;
            }
        }.execute();
    }

    public void showComments(View v) {
        goToCommentsActivity();
    }

    private void fillInViews() {
        //TODO: maybe create some "Map<String, String> to views" mapper, akin to SimpleShieldAdapter
        //conditions
        TextView conditions = (TextView) findViewById(id.tvShareConditions);
        conditions.setText(Html.fromHtml("" + offer.getConditions()));
        //description
        TextView descriptionView = (TextView) findViewById(id.tvShareDescription);
        descriptionView.setText(Html.fromHtml("" + offer.getDescription()));

        fillInSharePriceTextView();
        fillInShareDiscountTextView();
        //image
        SmartImageView image = (SmartImageView) findViewById(id.sivShareImage);
        image.setImageUrl(offer.getImageURL());
        //name
        TextView name = (TextView) findViewById(id.tvShareName);
        name.setText(Html.fromHtml(offer.getName()));
        //supplier
        TextView supplierView = (TextView) findViewById(id.tvShareLocation);
        supplierView.setText(Html.fromHtml(offer.getSupplierName()));

        fillInShareTimeTextView();

        //add to wishlist
        TextView addToWishesView = (TextView) findViewById(id.tvAddToWishList);
        //offer is user's or not
        View offerSubmittedView = findViewById(id.vLikeStatus);
        if (offer.getLikesQuantity() > 3 || offer.getUserID() == 0) {
            offerSubmittedView.setBackgroundResource(drawable.share_page_button_one_background_green);
        } else {
            offerSubmittedView.setBackgroundResource(drawable.like_status);
        }
        //likes, dislikes, comments, favs
        likeButton = findViewById(id.rlLike);
        if (offer.isLiked()) {
            setLikeButtonSelected(true);
        }
        dislikeButton = findViewById(id.rlDislike);
        if (offer.isDisliked()) {
            setDislikeButtonSelected(true);
        }
        favButton = findViewById(id.bRecommend);
        if (offer.isFavourited()) {
            addToWishesView.setVisibility(View.GONE);
            setFavButtonSelected(true);
        }
        likeCounter = (TextView) findViewById(id.tvLikesQuantity);
        likeCounter.setText(String.valueOf(offer.getLikesQuantity()));
        dislikeCounter = (TextView) findViewById(id.tvDislikesQuantity);
        dislikeCounter.setText(String.valueOf(offer.getDislikesQuantity()));
        TextView commentCounter = (TextView) findViewById(id.tvCommentsQuantity);
        commentCounter.setText(String.valueOf(offer.getCommentsQuantity()));
        setUpListeners();
    }

    private void fillInSharePriceTextView() {
        final CharSequence VALUE = getSharePriceFromOffer();
        final SpannableStringBuilder TEXT = new SpannableStringBuilder(VALUE);
        final CharSequence HEADER = pageTranslator.getSharePriceHeader();
        final View V = findViewById(id.tvSharePrice);
        final TextView TV = (TextView) V;
        TEXT.append(' ');
        TEXT.append(HEADER);
        TV.setText(TEXT);
    }

    private CharSequence getSharePriceFromOffer() {
        final RelativeSizeSpan WHAT = getRelativeSizeSpanForShareInformationValue();
        final Integer START = 0;
        final Double D = offer.getPriceCoupon();
        final CharSequence PRICE = D + "";
        final SpannableStringBuilder SSB = new SpannableStringBuilder(PRICE);
        final Integer END = SSB.length();
        SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
        return SSB;
    }

    private RelativeSizeSpan getRelativeSizeSpanForShareInformationValue() {
        final Float VALUE_SIZE_FOR_MDPI = 57.78f;
        final Float HEADER_SIZE_FOR_MDPI = 26.67f;
        final Float PROPORTION = VALUE_SIZE_FOR_MDPI / HEADER_SIZE_FOR_MDPI;
        return new RelativeSizeSpan(PROPORTION);
    }

    private void fillInShareDiscountTextView() {
        final CharSequence VALUE = getShareDiscountFromOffer();
        final SpannableStringBuilder TEXT = new SpannableStringBuilder(VALUE);
        final CharSequence HEADER = "%";
        final View V = findViewById(id.tvShareDiscount);
        final TextView TV = (TextView) V;
        TEXT.append(' ');
        TEXT.append(HEADER);
        TV.setText(TEXT);
    }

    private CharSequence getShareDiscountFromOffer() {
        final RelativeSizeSpan WHAT = getRelativeSizeSpanForShareInformationValue();
        final Integer START = 0;
        final Double D = offer.getDiscount();
        final CharSequence DISCOUNT = D + "";
        final SpannableStringBuilder SSB = new SpannableStringBuilder(DISCOUNT);
        final Integer END = SSB.length();
        SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
        return SSB;
    }

    private void fillInShareTimeTextView() {
        final CharSequence DAYS_HEADER = pageTranslator.getShareTimeInDaysHeader();
        final SpannableStringBuilder TEXT = new SpannableStringBuilder(DAYS_HEADER);
        final CharSequence DAYS_VALUE = getShareTimeInDaysFromOffer();
        final CharSequence HOURS_HEADER = pageTranslator.getShareTimeInHoursHeader();
        final CharSequence HOURS_VALUE = getShareTimeInHoursFromOffer();
        final View V = findViewById(id.tvShareTime);
        final TextView TV = (TextView) V;
        TEXT.append(' ');
        TEXT.append(DAYS_VALUE);
        TEXT.append(' ');
        TEXT.append(HOURS_HEADER);
        TEXT.append(' ');
        TEXT.append(HOURS_VALUE);
        TV.setText(TEXT);
    }

    private CharSequence getShareTimeInDaysFromOffer() {
        final RelativeSizeSpan WHAT = getRelativeSizeSpanForShareTimeValue();
        final Integer START = 0;
        final String DATE = offer.getDate();
        final Date END_DATE = parseDate(DATE);
        final Long DAYS_LEFT = daysTo(END_DATE);
        final CharSequence DAYS = DAYS_LEFT + "";
        final SpannableStringBuilder SSB = new SpannableStringBuilder(DAYS);
        final Integer END = SSB.length();
        SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
        return SSB;
    }

    private RelativeSizeSpan getRelativeSizeSpanForShareTimeValue() {
        final Float VALUE_SIZE_FOR_MDPI = 44.44f;
        final Float HEADER_SIZE_FOR_MDPI = 26.67f;
        final Float PROPORTION = VALUE_SIZE_FOR_MDPI / HEADER_SIZE_FOR_MDPI;
        return new RelativeSizeSpan(PROPORTION);
    }

    private CharSequence getShareTimeInHoursFromOffer() {
        final RelativeSizeSpan WHAT = getRelativeSizeSpanForShareTimeValue();
        final Integer START = 0;
        final String DATE = offer.getDate();
        final Date END_DATE = parseDate(DATE);
        final Long HOURS_LEFT = hoursTo(END_DATE);
        final CharSequence HOURS = HOURS_LEFT + "";
        final SpannableStringBuilder SSB = new SpannableStringBuilder(HOURS);
        final Integer END = SSB.length();
        SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
        return SSB;
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(OFFER_KEY, offer_id);
        super.onSaveInstanceState(outState);
    }

    private void goToCommentsActivity() {
        Intent intent = new Intent(self, CommentsActivity.class);
        intent.putExtra("offer_id", offer_id);
        startActivity(intent);
    }

    Date parseDate(String date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.ENGLISH);
        try {
            return df.parse(date);
        } catch (ParseException e) {
            Log.e("shareActivity date parse error", "" + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    long daysTo(Date date) {
        Date now = new Date();
        long diff = date.getTime() - now.getTime();
        final long millisInDay = 1000 * 60 * 60 * 24;
        return diff / millisInDay;
    }

    private long hoursTo(Date date) {
        Date now = new Date();
        long diff = date.getTime() - now.getTime();
        final long millisInHour = 1000 * 60 * 60;
        long hoursInDay = 24;
        return (diff / millisInHour) % hoursInDay;
    }

    public boolean isFavButtonSelected() {
        return favButtonSelected;
    }

    public void setFavButtonSelected(boolean selected) {
        favButton.setSelected(selected);
        this.favButtonSelected = selected;
    }

    public boolean isDislikeButtonSelected() {
        return dislikeButtonSelected;
    }

    public void setDislikeButtonSelected(boolean selected) {
        dislikeButton.setSelected(selected);
        this.dislikeButtonSelected = selected;
    }

    public boolean isLikeButtonSelected() {
        return likeButtonSelected;
    }

    public void setLikeButtonSelected(boolean selected) {
        likeButton.setSelected(selected);
        this.likeButtonSelected = selected;
    }

    @Override
    protected void onResume() {
        final ActionBar AB = getSupportActionBar();
        final CharSequence CS = "";
        super.onResume();
        AB.setTitle(CS);
    }
}
