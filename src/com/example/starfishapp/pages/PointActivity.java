package com.example.starfishapp.pages;

import android.os.*;
import android.support.v7.app.*;
import android.view.*;
import android.widget.*;
import com.example.starfishapp.R.*;
import com.example.starfishapp.model.*;
import com.googlion.shield.loaders.*;
import com.googlion.shield.specialists.FontSpecialist;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;
import com.loopj.android.image.*;

import java.util.*;

public class PointActivity extends ActionBarActivity {

    public static final String SUPPLIER_ID_KEY = "supplier_id";
    private Supplier chosenVenue;

    public void onCreate(final Bundle SAVED_INSTANCE_STATE) {
        super.onCreate(SAVED_INSTANCE_STATE);
        setContentView(layout.chosen_venue_page);
        initializeChosenVenue();
        setUpActionBar();
        setFontsForAllTextualViewsOfThisPage();
        scaleThisPage();
        translateThisPage();
        loadVenue();
    }

    private void initializeChosenVenue() {
        chosenVenue = new Supplier();
    }

    private void setUpActionBar() {
        final ActionBar AB = getSupportActionBar();
        final String NAME = "Action bar title";
        final CharSequence CS = getIntent().getStringExtra(NAME);
        AB.setTitle(CS);
    }

    private void setFontsForAllTextualViewsOfThisPage() {
        final Map<Object,Map> S = getSettingsForSettingFonts();
        final FontSpecialist FS = new FontSpecialist(S);
        FS.setFonts();
    }

    private Map<Object, Map> getSettingsForSettingFonts() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingsFonts());
        M.put("Other settings", getOtherSettingsForSettingsFonts());
        return M;
    }

    private Map getMainSettingsForSettingsFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvChosenVenueName, "kelsonsansregularru.otf");
        M.put(id.tvChosenVenueDescription, "robotoregular0.otf");
        M.put(id.bAddToWishList, "robotoregular0.otf");
        M.put(id.tvDistanceToChosenVenue, "robotoregular0.otf");
        M.put(id.tvChosenVenueAddress, "robotoregular0.otf");
        M.put(id.tvCreateRouteToTheChosenVenue, "robotoregular0.otf");
        M.put(id.tvChosenVenueOffers, "kelsonsansregularru.otf");
        M.put(id.tvChosenVenueOffersQuantity, "kelsonsansboldru.otf");
        M.put(id.tvChosenVenueOffices, "kelsonsansregularru.otf");
        M.put(id.tvChosenVenueOfficesQuantity, "kelsonsansboldru.otf");
        M.put(id.tvChosenVenueComments, "kelsonsansregularru.otf");
        M.put(id.tvChosenVenueCommentsQuantity, "kelsonsansboldru.otf");
        M.put(id.tvChosenVenueContacts, "kelsonsansregularru.otf");
        return M;
    }

    private Map getOtherSettingsForSettingsFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", PointActivity.this);
        return M;
    }

    private void scaleThisPage() {
        final ActionBarActivity REQUESTER = PointActivity.this;
        final PageScaler PS = new PageScaler(REQUESTER);
        PS.scaleThePage();
    }

    private void translateThisPage() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForPointPageTranslation();
        final Translator T = new PageTranslator(SETTINGS);
        T.translate();
    }

    private Map<String, Map<String, Object>> getSettingsForPointPageTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getPageViewsIds());
        M.put("Page views default texts", getPageViewsDefaultTexts());
        M.put("Other settings", getOtherSettings());
        return M;
    }

    private Map<String, Object> getPageViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvChosenVenueName", id.tvChosenVenueName);
        M.put("tvChosenVenueDescription", id.tvChosenVenueDescription);
        M.put("bAddToWishList", id.bAddToWishList);
        M.put("tvDistanceToChosenVenue", id.tvDistanceToChosenVenue);
        M.put("tvChosenVenueAddress", id.tvChosenVenueAddress);
        M.put("tvCreateRouteToTheChosenVenue", id.tvCreateRouteToTheChosenVenue);
        M.put("tvChosenVenueOffers", id.tvChosenVenueOffers);
        M.put("tvChosenVenueOffices", id.tvChosenVenueOffices);
        M.put("tvChosenVenueComments", id.tvChosenVenueComments);
        M.put("tvChosenVenueContacts", id.tvChosenVenueContacts);
        return M;
    }

    private Map<String, Object> getPageViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvChosenVenueName", "Заголовок відсутній.");
        M.put("tvChosenVenueDescription", "Опис відсутній.");
        M.put("bAddToWishList", "До списку бажань");
        M.put("tvDistanceToChosenVenue", "0 м");
        M.put("tvChosenVenueAddress", "Ніде.");
        M.put("tvCreateRouteToTheChosenVenue", "прокласти маршрут");
        M.put("tvChosenVenueOffers", "Пропозиції");
        M.put("tvChosenVenueOffices", "Відділи");
        M.put("tvChosenVenueComments", "Відгуки");
        M.put("tvChosenVenueContacts", "Зв'язок");
        return M;
    }

    private Map<String, Object> getOtherSettings() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", PointActivity.this);
        return M;
    }

    private void loadVenue() {
        final PointActivity REQUESTER = PointActivity.this;
        final PointLoader PL = new PointLoader(REQUESTER);
        PL.execute();
    }

    public void setUpVenueDataFrom(final Supplier S) {
        retrieveChosenVenueFrom(S);
        setUpVenueName();
        setUpVenueDescription();
        setUpVenueLogotype();
    }

    private void retrieveChosenVenueFrom(final Supplier S) {
        chosenVenue = S;
    }

    private void setUpVenueName() {
        final View V = findViewById(id.tvChosenVenueName);
        final TextView TV = (TextView) V;
        final CharSequence CS = chosenVenue.getName();
        TV.setText(CS);
    }

    private void setUpVenueDescription() {
        final View V = findViewById(id.tvChosenVenueDescription);
        final TextView TV = (TextView) V;
        final CharSequence CS = chosenVenue.getDescription();
        TV.setText(CS);
    }

    private void setUpVenueLogotype() {
        final View V = findViewById(id.sivChosenVenueLogotype);
        final SmartImageView SIV = (SmartImageView) V;
        final String URL = chosenVenue.getLogotypeURL();
        SIV.setImageUrl(URL);
    }
}