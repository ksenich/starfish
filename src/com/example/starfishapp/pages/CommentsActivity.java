package com.example.starfishapp.pages;

import android.app.*;
import android.os.*;
import android.text.*;
import android.view.*;
import android.widget.*;
import com.example.starfishapp.model.*;
import com.googlion.shield.loaders.*;
import com.googlion.shield.specialists.*;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;

import java.util.*;

import com.example.starfishapp.R.*;

public class CommentsActivity extends Activity implements Parcelable {
    private int offerId;
    private PageScaler pageScaler;
    private ListView commentsLV;
    private List<Map<String, Object>> commentsList;
    private List<Comment> downloadedCommentsList;

    protected void onCreate(final Bundle SAVED_INSTANCE_STATE) {
        super.onCreate(SAVED_INSTANCE_STATE);
        setContentView(layout.share_comments_page);
        initializeFields();
        translateThisMenu();
        scale();
        setFontsForAllTextualViewsOfThisPage();
        loadComments();
    }

    private void initializeFields() {
        offerId = getOfferId();
        pageScaler = new PageScaler(CommentsActivity.this);
        commentsList = new LinkedList<Map<String, Object>>();
        downloadedCommentsList = new LinkedList<Comment>();
        commentsLV = (ListView) findViewById(id.lvComments);
    }

    private int getOfferId() {
        final String NAME = "offer_id";
        final int DEFAULT_VALUE = -1;
        return getIntent().getIntExtra(NAME, DEFAULT_VALUE);
    }

    private void translateThisMenu() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForMenuTranslation();
        final Translator T = new PageTranslator(SETTINGS);
        T.translate();
    }

    private Map<String, Map<String, Object>> getSettingsForMenuTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getPageViewsIds());
        M.put("Page views default texts", getPageViewsDefaultTexts());
        M.put("Other settings", getOtherSettings());
        return M;
    }

    private Map<String, Object> getPageViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvShareCommentsPageHeader", id.tvShareCommentsPageHeader);
        M.put("bSendComment", id.bSendComment);
        return M;
    }

    private Map<String, Object> getPageViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvShareCommentsPageHeader", "Відгуки");
        M.put("bSendComment", "Надіслати");
        return M;
    }

    private Map<String, Object> getOtherSettings() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", CommentsActivity.this);
        return M;
    }

    private void scale() {
        pageScaler.scaleThePage();
        pageScaler.scaleDividerHeightOf(commentsLV);
    }

    private void setFontsForAllTextualViewsOfThisPage() {
        final Map<Object, Map> S = getSettingsForSettingFonts();
        final FontSpecialist FS = new FontSpecialist(S);
        FS.setFonts();
    }

    private Map<Object, Map> getSettingsForSettingFonts() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingFonts());
        M.put("Other settings", getOtherSettingsForSettingFonts());
        return M;
    }

    private Map getMainSettingsForSettingFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvShareCommentsPageHeader, "kelsonsansboldru.otf");
        M.put(id.etComment, "kelsonsanslightru.otf");
        M.put(id.bSendComment, "robotobold.otf");
        return M;
    }

    private Map getOtherSettingsForSettingFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", CommentsActivity.this);
        return M;
    }

    private void loadComments() {
        if (offerHasId()) {
            startLoadingComments();
        }
    }

    private boolean offerHasId() {
        return offerId != -1;
    }

    public void startLoadingComments() {
        final Bundle SETTINGS = getOfferCommentsLoadingSettings();
        final OfferCommentsLoader OCL = new OfferCommentsLoader(SETTINGS);
        OCL.loadComments();
    }

    private Bundle getOfferCommentsLoadingSettings() {
        final Bundle B = new Bundle();
        B.putInt("Offer id", offerId);
        B.putParcelable("Requester", CommentsActivity.this);
        return B;
    }

    public void apply(final List<Comment> L) {
        retrieveDownloadedCommentsListFrom(L);
        clearCommentsList();
        displayComments();
    }

    private void retrieveDownloadedCommentsListFrom(final List<Comment> L) {
        downloadedCommentsList = L;
    }

    private void clearCommentsList() {
        commentsList.clear();
    }

    private void displayComments() {
        final Map<String, List<Map<String, Object>>> SETTINGS = getSettingsForListDisplaying();
        final ListViewSpecialist LVS = new ShareCommentsPageListViewSpecialist(SETTINGS);
        LVS.setUpListView();
    }

    private Map<String, List<Map<String, Object>>> getSettingsForListDisplaying() {
        final Map<String, List<Map<String, Object>>> M = new HashMap<String, List<Map<String, Object>>>();
        M.put("List view items data", getListViewItemsData());
        M.put("List view item fields to be filled", getListViewItemFieldsToBeFilled());
        M.put("Other settings", getOtherSettingsForListDisplaying());
        return M;
    }

    private List<Map<String, Object>> getListViewItemsData() {
        addCommentsToList();
        return commentsList;
    }

    private void addCommentsToList() {
        for (final Comment C : downloadedCommentsList) {
            addCommentToList(C);
        }
    }

    private void addCommentToList(final Comment C) {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Comment author photo", C.img_url);
        M.put("Comment author name", C.user_name);
        M.put("Comment short content", C.text);
        M.put("Comment time", C.time);
        commentsList.add(M);
    }

    private List<Map<String, Object>> getListViewItemFieldsToBeFilled() {
        final List<Map<String, Object>> L = new LinkedList<Map<String, Object>>();
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Comment author photo", id.sivCommentAuthorPhoto);
        M.put("Comment author name", id.tvCommentAuthorName);
        M.put("Comment short content", id.tvCommentShortContent);
        M.put("Comment time", id.tvCommentTime);
        L.add(M);
        return L;
    }

    private List<Map<String, Object>> getOtherSettingsForListDisplaying() {
        final List<Map<String, Object>> L = new LinkedList<Map<String, Object>>();
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", CommentsActivity.this);
        M.put("List view id", id.lvComments);
        M.put("Layout resource id", layout.share_comments_page_list_item);
        M.put("Page scaler", pageScaler);
        M.put("List view items translator", getTranslatorForListViewItems());
        M.put("Font specialist for list view", getFontSpecialistForProcessingListView());
        L.add(M);
        return L;
    }

    private Object getTranslatorForListViewItems() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForListItemTranslation();
        return new ListViewItemsTranslator(SETTINGS);
    }

    private Map<String, Map<String, Object>> getSettingsForListItemTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getListItemViewsIds());
        M.put("Page views default texts", getListItemsViewsDefaultTexts());
        M.put("Other settings", getOtherSettingsForTranslation());
        return M;
    }

    private Map<String, Object> getListItemViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvCommentAuthorName", id.tvCommentAuthorName);
        M.put("tvCommentShortContent", id.tvCommentShortContent);
        return M;
    }

    private Map<String, Object> getListItemsViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvCommentAuthorName", "Невідомо");
        M.put("tvCommentShortContent", "Відгук відсутній.");
        return M;
    }

    private Map<String, Object> getOtherSettingsForTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", CommentsActivity.this);
        return M;
    }

    private FontSpecialistForListView getFontSpecialistForProcessingListView() {
        final Map<Object, Map> S = getSettingsForSettingFontsInListView();
        return new FontSpecialistForListView(S);
    }

    private Map<Object, Map> getSettingsForSettingFontsInListView() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingsFontsInListView());
        M.put("Other settings", getOtherSettingsForSettingsFontsInListView());
        return M;
    }

    private Map getMainSettingsForSettingsFontsInListView() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvCommentAuthorName, "kelsonsanslightru.otf");
        M.put(id.tvCommentShortContent, "robotoregular0.otf");
        M.put(id.tvCommentTime, "robotoregular0.otf");
        return M;
    }

    private Map getOtherSettingsForSettingsFontsInListView() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", CommentsActivity.this);
        return M;
    }

    public void sendComment(final View V) {
        final Bundle SETTINGS = getOfferCommentsSendingSettings();
        final OfferCommentsSender OCS = new OfferCommentsSender(SETTINGS);
        OCS.execute();
    }

    private Bundle getOfferCommentsSendingSettings() {
        final View V = findViewById(id.etComment);
        final EditText ET = (EditText) V;
        final Editable E = ET.getText();
        final String COMMENT = E.toString();
        final CommentsActivity REQUESTER = CommentsActivity.this;
        final Bundle B = new Bundle();
        B.putParcelable("Requester", REQUESTER);
        B.putString("Comment", COMMENT);
        B.putInt("Offer id", offerId);
        return B;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(final Parcel DESTINATION, final int FLAGS) {
    }
}