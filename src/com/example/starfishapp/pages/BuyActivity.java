package com.example.starfishapp.pages;

import static android.graphics.Typeface.*;
import static android.text.Spanned.*;
import static org.apache.http.util.EncodingUtils.*;

import android.app.*;
import android.content.*;
import android.content.res.*;
import android.graphics.*;
import android.os.*;
import android.text.*;
import android.text.style.*;
import android.view.*;
import android.webkit.*;
import android.widget.*;
import com.example.starfishapp.R.*;
import com.example.starfishapp.model.*;
import com.googlion.shield.loaders.*;
import com.googlion.shield.specialists.*;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;

import java.io.*;
import java.util.*;

public class BuyActivity extends Activity {
    private int couponsQuantity;
    private OfferUserAttitude offer;
    private UserInfo userInformation;
    private PageScaler pageScaler;
    private BuyCouponMenuTranslator menuTranslator;

    @Override
    protected void onCreate(final Bundle SAVED_INSTANCE_STATE) {
        performOnCreateMethodFromSuperclass(SAVED_INSTANCE_STATE);
        setContentViewForThisPage();
        initializeFields();
        scaleBuyCouponMenu();
        loadUserInformation();
        translateThisMenu();
        setFontsForAllTextualViewsOfThisPage();
        fillInViews();
    }

    private void performOnCreateMethodFromSuperclass(final Bundle SAVED_INSTANCE_STATE) {
        super.onCreate(SAVED_INSTANCE_STATE);
    }

    private void setContentViewForThisPage() {
        setContentView(layout.buy_coupon_menu);
    }

    private void initializeFields() {
        couponsQuantity = 0;
        offer = getInitializedOffer();
        userInformation = new UserInfo();
        pageScaler = new PageScaler(BuyActivity.this);
        menuTranslator = getInitializedMenuTranslator();
    }

    private OfferUserAttitude getInitializedOffer() {
        final String NAME = "offer";
        final Serializable S = getIntent().getSerializableExtra(NAME);
        return (OfferUserAttitude) S;
    }

    private BuyCouponMenuTranslator getInitializedMenuTranslator() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForMenuTranslation();
        return new BuyCouponMenuTranslator(SETTINGS);
    }

    private Map<String, Map<String, Object>> getSettingsForMenuTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getPageViewsIds());
        M.put("Page views default texts", getPageViewsDefaultTexts());
        M.put("Other settings", getOtherSettingsForMenuTranslation());
        return M;
    }

    private Map<String, Object> getPageViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvCouponPriceHeaderOne", id.tvCouponPriceHeader);
        M.put("tvCouponPriceHeaderTwo", id.tvCouponPrice);
        M.put("tvCouponQuantityHeader", id.tvCouponsQuantityHeader);
        M.put("tvCouponTotalHeaderOne", id.tvCouponsTotalHeader);
        M.put("tvUserBonuses", id.tvUserBonuses);
        M.put("bBuyFromBalance", id.bGoToTheCouponPayment);
        return M;
    }

    private Map<String, Object> getPageViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvCouponPriceHeaderOne", "вартість талону:");
        M.put("tvCouponPriceHeaderTwo", "грн");
        M.put("tvCouponQuantityHeader", "кількість талонів:");
        M.put("tvCouponTotalHeaderOne", "Всього:");
        M.put("tvUserBonuses", "Бонусів на Вашому рахунку:");
        M.put("bBuyFromBalance", "Перейти до оплати");
        return M;
    }

    private Map<String, Object> getOtherSettingsForMenuTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", BuyActivity.this);
        return M;
    }

    private void scaleBuyCouponMenu() {
        pageScaler.scaleThePage();
    }

    private void loadUserInformation() {
        final Activity REQUESTER = BuyActivity.this;
        final UserInformationLoader UBL = new BuyCouponMenuUserInformationLoader(REQUESTER);
        UBL.loadUserInformation();
    }

    public void retrieveUserInformationFrom(final UserInfo UI) {
        getUserInformationFrom(UI);
        fillUpUserBonusesTextView();
    }

    private void getUserInformationFrom(final UserInfo UI) {
        userInformation = UI;
    }

    private void fillUpUserBonusesTextView() {
        final View V = findViewById(id.tvUserBonuses);
        final TextView TV = (TextView) V;
        final CharSequence OLD_TEXT = TV.getText();
        final SpannableStringBuilder NEW_TEXT = new SpannableStringBuilder(OLD_TEXT);
        final CharSequence USER_BONUSES = getUserBonusesFromUserInformation();
        NEW_TEXT.append(' ');
        NEW_TEXT.append(USER_BONUSES);
        TV.setText(NEW_TEXT);
    }

    private CharSequence getUserBonusesFromUserInformation() {
        final CharSequence BONUSES = userInformation.cnt_bonuses;
        final SpannableStringBuilder SSB = new SpannableStringBuilder(BONUSES);
        final String FAMILY = "";
        final AssetManager MGR = getAssets();
        final String PATH = "kelsonsansboldru.otf";
        final Typeface TF = createFromAsset(MGR, PATH);
        final CustomTypefaceSpan WHAT = new CustomTypefaceSpan(FAMILY, TF);
        final Integer START = 0;
        final Integer END = SSB.length();
        SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
        return SSB;
    }

    private void translateThisMenu() {
        menuTranslator.translate();
    }

    private void setFontsForAllTextualViewsOfThisPage() {
        final Map<Object, Map> S = getSettingsForSettingFonts();
        final FontSpecialist FS = new FontSpecialist(S);
        FS.setFonts();
    }

    private Map<Object, Map> getSettingsForSettingFonts() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingFonts());
        M.put("Other settings", getOtherSettingsForSettingFonts());
        return M;
    }

    private Map getMainSettingsForSettingFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvCouponPriceHeader, "robotoregular0.otf");
        M.put(id.tvCouponPrice, "kelsonsansboldru.otf");
        M.put(id.tvCouponsQuantityHeader, "robotoregular0.otf");
        M.put(id.tvCouponsQuantity, "kelsonsansboldru.otf");
        M.put(id.tvAddCoupon, "kelsonsansregularru.otf");
        M.put(id.tvSubtractCoupon, "kelsonsansregularru.otf");
        M.put(id.tvCouponsTotalHeader, "robotoregular0.otf");
        M.put(id.tvCouponsTotal, "kelsonsansboldru.otf");
        M.put(id.tvUserBonuses, "kelsonsanslightru.otf");
        M.put(id.bGoToTheCouponPayment, "robotobold.otf");
        return M;
    }

    private Map getOtherSettingsForSettingFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", BuyActivity.this);
        return M;
    }

    private void fillInViews() {
        fillInCouponPriceTextView();
        setCouponQuantityByDefault();
    }

    private void fillInCouponPriceTextView() {
        final CharSequence COUPON_PRICE = getCouponPriceFromOffer();
        final SpannableStringBuilder NEW_TEXT = new SpannableStringBuilder(COUPON_PRICE);
        final View V = findViewById(id.tvCouponPrice);
        final TextView TV = (TextView) V;
        final CharSequence OLD_TEXT = TV.getText();
        NEW_TEXT.append(OLD_TEXT);
        TV.setText(NEW_TEXT);
    }

    private CharSequence getCouponPriceFromOffer() {
        final RelativeSizeSpan WHAT = getRelativeSizeSpanForCouponPrice();
        final Integer START = 0;
        final Integer I = (int) offer.getPriceCoupon();
        final CharSequence PRICE = I + "";
        final SpannableStringBuilder SSB = new SpannableStringBuilder(PRICE);
        final Integer END = SSB.length();
        SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
        return SSB;
    }

    private RelativeSizeSpan getRelativeSizeSpanForCouponPrice() {
        final Float VALUE_SIZE_FOR_MDPI = 88.89f;
        final Float HEADER_SIZE_FOR_MDPI = 44.44f;
        final Float PROPORTION = VALUE_SIZE_FOR_MDPI / HEADER_SIZE_FOR_MDPI;
        return new RelativeSizeSpan(PROPORTION);
    }

    private void setCouponQuantityByDefault() {
        setQuantityByDefault();
        setCouponQuantity();
    }

    private void setQuantityByDefault() {
        couponsQuantity = 1;
    }

    private void setCouponQuantity() {
        setCouponQuantityByDefaultIfItHasReachedItsMinimum();
        fillInCouponQuantityTextView();
        fillInCouponsTotalTextView();
    }

    private void setCouponQuantityByDefaultIfItHasReachedItsMinimum() {
        if (couponQuantityHasReachedItsMinimum()) {
            setQuantityByDefault();
        }
    }

    private boolean couponQuantityHasReachedItsMinimum() {
        return couponsQuantity < 1;
    }

    private void fillInCouponQuantityTextView() {
        final View V = findViewById(id.tvCouponsQuantity);
        final TextView TV = (TextView) V;
        final String TEXT = couponsQuantity + "";
        TV.setText(TEXT);
    }

    private void fillInCouponsTotalTextView() {
        final CharSequence VALUE = getCouponsTotalFromCouponPriceAndCouponsQuantity();
        final SpannableStringBuilder TEXT = new SpannableStringBuilder(VALUE);
        final CharSequence HEADER = menuTranslator.getCouponsTotalHeaderTwo();
        final View V = findViewById(id.tvCouponsTotal);
        final TextView TV = (TextView) V;
        TEXT.append(' ');
        TEXT.append(HEADER);
        TV.setText(TEXT);
    }

    private RelativeSizeSpan getRelativeSizeSpanForCouponsTotal() {
        final Float VALUE_SIZE_FOR_MDPI = 133.33f;
        final Float HEADER_SIZE_FOR_MDPI = 88.89f;
        final Float PROPORTION = VALUE_SIZE_FOR_MDPI / HEADER_SIZE_FOR_MDPI;
        return new RelativeSizeSpan(PROPORTION);
    }

    private CharSequence getCouponsTotalFromCouponPriceAndCouponsQuantity() {
        final RelativeSizeSpan WHAT = getRelativeSizeSpanForCouponsTotal();
        final Integer START = 0;
        final Integer A = couponsQuantity;
        final Integer B = (int) offer.getPriceCoupon();
        final Integer C = A * B;
        final CharSequence COUPONS_TOTAL = C + "";
        final SpannableStringBuilder SSB = new SpannableStringBuilder(COUPONS_TOTAL);
        final Integer END = SSB.length();
        SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
        return SSB;
    }

    public void addCoupon(final View V) {
        increaseCouponQuantity();
        setCouponQuantity();
    }

    private void increaseCouponQuantity() {
        ++couponsQuantity;
    }

    public void subtractCoupon(final View V) {
        decreaseCouponQuantity();
        setCouponQuantity();
    }

    private void decreaseCouponQuantity() {
        --couponsQuantity;
    }

    public void goToTheCouponPayment(final View V) {
        final Context C = BuyActivity.this;
        final WebView WV = new WebView(C);
        final String URL = "http://starfishkupon.com/apis/googlion/";
        final CharSequence POST_REQUEST_DATA = getDataForThePostRequest();
        final String DATA = POST_REQUEST_DATA.toString();
        final String CHARSET = "BASE64";
        final byte[] POST_DATA = getBytes(DATA, CHARSET);
        WV.postUrl(URL, POST_DATA);
        setContentView(WV);
    }

    private CharSequence getDataForThePostRequest() {
        final StringBuilder SB = new StringBuilder("sfid=");
        SB.append(offer.getOfferStarfishID());
        SB.append("&glid=");
        SB.append(offer.getOfferGooglionID());
        SB.append("&glur=");
        SB.append("http://googlion.com/action/");
        SB.append(offer.getOfferLink());
        SB.append('-');
        SB.append(offer.getOfferGooglionID());
        SB.append("&glcn=");
        SB.append(getOfferCouponsQuantity());
        SB.append("&glem=");
        SB.append(userInformation.email);
        return SB;
    }

    private CharSequence getOfferCouponsQuantity() {
        final View V = findViewById(id.tvCouponsQuantity);
        final TextView TV = (TextView) V;
        return TV.getText();
    }
}