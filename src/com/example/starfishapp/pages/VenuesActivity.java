package com.example.starfishapp.pages;

import static com.example.starfishapp.R.*;

import android.os.*;
import android.support.v7.app.*;
import android.view.*;
import android.widget.*;
import android.widget.AdapterView.*;
import com.example.starfishapp.model.*;
import com.googlion.shield.listeners.*;
import com.googlion.shield.loaders.*;
import com.googlion.shield.specialists.*;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;

import java.util.*;

public class VenuesActivity extends ActionBarActivity {
    private PageScaler pageScaler;
    private List<Map<String, Object>> categoriesList;
    private List<Category> downloadedCategoriesList;

    @Override
    protected void onCreate(final Bundle SAVED_INSTANCE_STATE) {
        performOnCreateMethodFromSuperclass(SAVED_INSTANCE_STATE);
        setContentViewForThisPage();
        initializeFields();
        translateThisPage();
        scaleThisPage();
        loadCategories();
    }

    private void performOnCreateMethodFromSuperclass(final Bundle SAVED_INSTANCE_STATE) {
        super.onCreate(SAVED_INSTANCE_STATE);
    }

    private void setContentViewForThisPage() {
        setContentView(layout.venues_categories_page);
    }

    private void initializeFields() {
        pageScaler = new PageScaler(VenuesActivity.this);
        categoriesList = new LinkedList<Map<String, Object>>();
        downloadedCategoriesList = new LinkedList<Category>();
    }

    private void translateThisPage() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForPageTranslation();
        final Translator T = new TranslatorOfThePageWithActionBar(SETTINGS);
        T.translate();
    }

    private Map<String, Map<String, Object>> getSettingsForPageTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Other settings", getOtherSettingsForPageTranslation());
        return M;
    }

    private Map<String, Object> getOtherSettingsForPageTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", VenuesActivity.this);
        M.put("Page title translation key", "Venues page action bar title");
        M.put("Page title default text", "Заклади");
        return M;
    }

    private void scaleThisPage() {
        pageScaler.scaleThePage();
    }

    private void loadCategories() {
        final VenuesActivity REQUESTER = VenuesActivity.this;
        final CategoriesLoader CL = new CategoriesLoader(REQUESTER);
        CL.loadCategories();
    }

    public void apply(final List<Category> L) {
        retrieveDownloadedCategoriesListFrom(L);
        displayCategories();
        setOnItemClickListenerForCategoriesList();
    }

    private void retrieveDownloadedCategoriesListFrom(final List<Category> L) {
        downloadedCategoriesList = L;
    }

    private void displayCategories() {
        final Map<String, List<Map<String, Object>>> SETTINGS = getSettingsForListDisplaying();
        final ListViewSpecialist LVS = new ListViewSpecialist(SETTINGS);
        LVS.setUpListView();
    }

    private Map<String, List<Map<String, Object>>> getSettingsForListDisplaying() {
        final Map<String, List<Map<String, Object>>> M = new HashMap<String, List<Map<String, Object>>>();
        M.put("List view items data", getListViewItemsData());
        M.put("List view item fields to be filled", getListViewItemFieldsToBeFilled());
        M.put("Other settings", getOtherSettingsForListDisplaying());
        return M;
    }

    private List<Map<String, Object>> getListViewItemsData() {
        addCategoriesToList();
        return categoriesList;
    }

    private void addCategoriesToList() {
        for (final Category C : downloadedCategoriesList) {
            addCategoryToList(C);
        }
    }

    private void addCategoryToList(final Category C) {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Venue name", C.name);
        M.put("Venue image", C.getMapIconId());
        categoriesList.add(M);
    }

    private List<Map<String, Object>> getListViewItemFieldsToBeFilled() {
        final List<Map<String, Object>> L = new LinkedList<Map<String, Object>>();
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Venue name", id.tvVenues);
        M.put("Venue image", id.ivVenues);
        L.add(M);
        return L;
    }

    private List<Map<String, Object>> getOtherSettingsForListDisplaying() {
        final List<Map<String, Object>> L = new LinkedList<Map<String, Object>>();
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", VenuesActivity.this);
        M.put("List view id", id.lvVenuesCategories);
        M.put("Layout resource id", layout.venues_categories_page_list_item);
        M.put("Page scaler", pageScaler);
        M.put("Font specialist for list view", getFontSpecialistForProcessingListView());
        L.add(M);
        return L;
    }

    private FontSpecialistForListView getFontSpecialistForProcessingListView() {
        final Map<Object, Map> S = getSettingsForSettingFontsInListView();
        return new FontSpecialistForListView(S);
    }

    private Map<Object, Map> getSettingsForSettingFontsInListView() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingsFontsInListView());
        M.put("Other settings", getOtherSettingsForSettingsFontsInListView());
        return M;
    }

    private Map getMainSettingsForSettingsFontsInListView() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvVenues, "kelsonsansregularru.otf");
        return M;
    }

    private Map getOtherSettingsForSettingsFontsInListView() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", VenuesActivity.this);
        return M;
    }

    private void setOnItemClickListenerForCategoriesList() {
        final VenuesActivity VA = VenuesActivity.this;
        final OnItemClickListener LISTENER = new OnCategoriesListItemClickListener(VA);
        final View V = findViewById(id.lvVenuesCategories);
        final ListView LV = (ListView) V;
        LV.setOnItemClickListener(LISTENER);
    }

    public List<Category> getDownloadedCategoriesList() {
        return downloadedCategoriesList;
    }
}