package com.example.starfishapp.pages;

import android.content.*;
import android.os.*;
import android.support.v7.app.*;
import android.text.SpannableStringBuilder;
import android.text.style.RelativeSizeSpan;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import android.widget.AdapterView.*;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.*;
import com.example.starfishapp.R.*;
import com.example.starfishapp.model.*;
import com.googlion.shield.listeners.*;
import com.googlion.shield.loaders.*;
import com.googlion.shield.specialists.*;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;

import java.util.*;

import static android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE;

public class OffersSearchActivity extends ActionBarActivity implements OnClickListener {
    private List<Map<String, Object>> offersList;
    private List<Offer> downloadedOffersList;
    private PageScaler pageScaler;
    private String textToSearch;
    private int pagesQuantity;
    private SearchView searchField;

    @Override
    protected void onCreate(Bundle SAVED_INSTANCE_STATE) {
        performOnCreateMethodFromSuperclass(SAVED_INSTANCE_STATE);
        setContentViewForThisPage();
        initializeFields();
        setUpActionBar();
        scale();
        loadOffers();
    }

    private void performOnCreateMethodFromSuperclass(final Bundle SAVED_INSTANCE_STATE) {
        super.onCreate(SAVED_INSTANCE_STATE);
    }

    private void setContentViewForThisPage() {
        setContentView(layout.shares_found_page);
    }

    private void initializeFields() {
        offersList = new LinkedList<Map<String, Object>>();
        downloadedOffersList = new LinkedList<Offer>();
        pageScaler = new PageScaler(OffersSearchActivity.this);
        textToSearch = getIntent().getStringExtra("q");
        pagesQuantity = 1;
        searchField = new SearchView(OffersSearchActivity.this);
    }

    private void setUpActionBar() {
        final ActionBar AB = getSupportActionBar();
        final CharSequence CS = "";
        final boolean B = true;
        AB.setTitle(CS);
        AB.setDisplayHomeAsUpEnabled(B);
    }

    private void scale() {
        scaleThisPage();
        scaleDividerHeightOfOffersList();
    }

    private void scaleThisPage() {
        pageScaler.scaleThePage();
    }

    private void scaleDividerHeightOfOffersList() {
        final ListView LV = getListViewOfThisPage();
        pageScaler.scaleDividerHeightOf(LV);
    }

    private ListView getListViewOfThisPage() {
        final View V = findViewById(id.lvOffers);
        return (ListView) V;
    }

    private void loadOffers() {
        final ActionBarActivity REQUESTER = OffersSearchActivity.this;
        final OffersLoader OL = new OffersSearchPageOffersLoader(REQUESTER);
        OL.loadOffers();
    }

    public void apply(final List<Offer> L) {
        retrieveDownloadedOffersListFrom(L);
        displayOffers();
        setOnItemClickListenerForOffersList();
        leaveUserOnTheLastItemBeforeMoreButtonIsPressed();
        restoreSearchFieldQuery();
    }

    private void retrieveDownloadedOffersListFrom(final List<Offer> L) {
        downloadedOffersList.addAll(L);
    }

    private void displayOffers() {
        final Map<String, List<Map<String, Object>>> SETTINGS = getSettingsForListDisplaying();
        final ListViewSpecialist LVS = new ListViewSpecialist(SETTINGS);
        LVS.setUpListView();
    }

    private Map<String, List<Map<String, Object>>> getSettingsForListDisplaying() {
        final Map<String, List<Map<String, Object>>> M = new HashMap<String, List<Map<String, Object>>>();
        M.put("List view items data", getListViewItemsData());
        M.put("List view item fields to be filled", getListViewItemFieldsToBeFilled());
        M.put("Other settings", getOtherSettingsForListDisplaying());
        return M;
    }

    private List<Map<String, Object>> getListViewItemsData() {
        addOffersToList();
        return offersList;
    }

    private void addOffersToList() {
        for (final Offer O : downloadedOffersList) {
            addOfferToList(O);
        }
    }

    private void addOfferToList(final Offer CURRENT_OFFER) {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Share title", CURRENT_OFFER.name);
        M.put("Share description", CURRENT_OFFER.description);
        M.put("Share price", getSharePriceFrom(CURRENT_OFFER));
        M.put("Share image url", CURRENT_OFFER.img_url);
        M.put("Share discount", getShareDiscountFrom(CURRENT_OFFER));
        M.put("Share discount price", getShareDiscountPriceFrom(CURRENT_OFFER));
        offersList.add(M);
    }

    private Object getSharePriceFrom(final Offer CURRENT_OFFER) {
        final CharSequence VALUE = getValueOfSharePriceFrom(CURRENT_OFFER);
        final SpannableStringBuilder TEXT = new SpannableStringBuilder(VALUE);
        final Map<String, Map<String, Object>> S = getSettingsForPageTranslation();
        final SharesFoundPageTranslator T = new SharesFoundPageTranslator(S);
        final CharSequence HEADER = T.getSharePriceHeader();
        TEXT.append(' ');
        TEXT.append(HEADER);
        return TEXT;
    }

    private CharSequence getValueOfSharePriceFrom(final Offer CURRENT_OFFER) {
        final RelativeSizeSpan WHAT = getRelativeSizeSpanForValue();
        final Integer START = 0;
        final CharSequence PRICE = CURRENT_OFFER.price + "";
        final SpannableStringBuilder SSB = new SpannableStringBuilder(PRICE);
        final Integer END = SSB.length();
        SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
        return SSB;
    }

    private RelativeSizeSpan getRelativeSizeSpanForValue() {
        final Float VALUE_SIZE_FOR_MDPI = 40f;
        final Float HEADER_SIZE_FOR_MDPI = 26.67f;
        final Float PROPORTION = VALUE_SIZE_FOR_MDPI / HEADER_SIZE_FOR_MDPI;
        return new RelativeSizeSpan(PROPORTION);
    }

    private Object getShareDiscountFrom(final Offer CURRENT_OFFER) {
        final CharSequence VALUE = getValueOfShareDiscountFrom(CURRENT_OFFER);
        final SpannableStringBuilder TEXT = new SpannableStringBuilder(VALUE);
        final CharSequence HEADER = "%";
        TEXT.append(' ');
        TEXT.append(HEADER);
        return TEXT;
    }

    private CharSequence getValueOfShareDiscountFrom(final Offer CURRENT_OFFER) {
        final RelativeSizeSpan WHAT = getRelativeSizeSpanForValue();
        final Integer START = 0;
        final CharSequence PRICE = CURRENT_OFFER.discount + "";
        final SpannableStringBuilder SSB = new SpannableStringBuilder(PRICE);
        final Integer END = SSB.length();
        SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
        return SSB;
    }

    private Object getShareDiscountPriceFrom(final Offer CURRENT_OFFER) {
        final CharSequence VALUE = getValueOfShareDiscountPriceFrom(CURRENT_OFFER);
        final SpannableStringBuilder TEXT = new SpannableStringBuilder(VALUE);
        final Map<String, Map<String, Object>> S = getSettingsForPageTranslation();
        final SharesFoundPageTranslator T = new SharesFoundPageTranslator(S);
        final CharSequence HEADER = T.getShareDiscountPriceHeader();
        TEXT.append(' ');
        TEXT.append(HEADER);
        return TEXT;
    }

    private CharSequence getValueOfShareDiscountPriceFrom(final Offer CURRENT_OFFER) {
        final RelativeSizeSpan WHAT = getRelativeSizeSpanForValue();
        final Integer START = 0;
        final CharSequence PRICE = CURRENT_OFFER.priceCoupon + "";
        final SpannableStringBuilder SSB = new SpannableStringBuilder(PRICE);
        final Integer END = SSB.length();
        SSB.setSpan(WHAT, START, END, SPAN_EXCLUSIVE_EXCLUSIVE);
        return SSB;
    }

    private Map<String, Map<String, Object>> getSettingsForPageTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getIdsOfPageViews());
        M.put("Page views default texts", getDefaultTextsOfPageViews());
        M.put("Other settings", getOtherSettingsForTranslation());
        return M;
    }

    private Map<String, Object> getIdsOfPageViews() {
        return new HashMap<String, Object>();
    }

    private Map<String, Object> getDefaultTextsOfPageViews() {
        return new HashMap<String, Object>();
    }

    private List<Map<String, Object>> getListViewItemFieldsToBeFilled() {
        final List<Map<String, Object>> L = new LinkedList<Map<String, Object>>();
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Share title", id.tvOfferTitle);
        M.put("Share description", id.tvOfferDescription);
        M.put("Share price", id.tvOfferPrice);
        M.put("Share image url", id.sivOfferImage);
        M.put("Share discount", id.tvOfferDiscount);
        M.put("Share discount price", id.tvOfferDiscountPrice);
        L.add(M);
        return L;
    }

    private List<Map<String, Object>> getOtherSettingsForListDisplaying() {
        final List<Map<String, Object>> L = new LinkedList<Map<String, Object>>();
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", OffersSearchActivity.this);
        M.put("List view id", id.lvOffers);
        M.put("Layout resource id", layout.see_shares_list_page_list_item);
        M.put("Page scaler", pageScaler);
        M.put("List view items translator", getTranslatorForListViewItems());
        M.put("Font specialist for list view", getFontSpecialistForProcessingListView());
        L.add(M);
        return L;
    }

    private Object getTranslatorForListViewItems() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForListItemTranslation();
        return new ListViewItemsTranslator(SETTINGS);
    }

    private Map<String, Map<String, Object>> getSettingsForListItemTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getListItemViewsIds());
        M.put("Page views default texts", getListItemViewsDefaultTexts());
        M.put("Other settings", getOtherSettingsForTranslation());
        return M;
    }

    private Map<String, Object> getListItemViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvOfferTitle", id.tvOfferTitle);
        return M;
    }

    private Map<String, Object> getListItemViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvOfferTitle", "Заголовок відсутній.");
        return M;
    }

    private Map<String, Object> getOtherSettingsForTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", OffersSearchActivity.this);
        return M;
    }

    private FontSpecialistForListView getFontSpecialistForProcessingListView() {
        final Map<Object, Map> S = getSettingsForSettingFontsInListView();
        return new FontSpecialistForListView(S);
    }

    private Map<Object, Map> getSettingsForSettingFontsInListView() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingFontsInListView());
        M.put("Other settings", getOtherSettingsForSettingFontsInListView());
        return M;
    }

    private Map getMainSettingsForSettingFontsInListView() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvOfferTitle, "kelsonsansregularru.otf");
        M.put(id.tvOfferPrice, "kelsonsansboldru.otf");
        M.put(id.tvOfferDiscount, "kelsonsansboldru.otf");
        M.put(id.tvOfferDiscountPrice, "kelsonsansboldru.otf");
        return M;
    }

    private Map getOtherSettingsForSettingFontsInListView() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", OffersSearchActivity.this);
        return M;
    }

    private void setOnItemClickListenerForOffersList() {
        final ActionBarActivity A = OffersSearchActivity.this;
        final OnItemClickListener L = new OnOffersSearchPageOffersListItemClickListener(A);
        final ListView LV = getListViewOfThisPage();
        LV.setOnItemClickListener(L);
    }

    private void leaveUserOnTheLastItemBeforeMoreButtonIsPressed() {
        final int ITEMS_PER_PAGE = 10;
        final int CURRENT_PAGE = pagesQuantity - 1;
        final int LAST_ITEM_POSITION = ITEMS_PER_PAGE * CURRENT_PAGE - 1;
        final ListView LV = getListViewOfThisPage();
        LV.setSelection(LAST_ITEM_POSITION);
    }

    private void restoreSearchFieldQuery() {
        final CharSequence QUERY = textToSearch;
        final boolean SUBMIT = false;
        searchField.setQuery(QUERY, SUBMIT);
    }

    public void translateFooterView() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForFooterViewTranslation();
        final Translator T = new PageTranslator(SETTINGS);
        T.translate();
    }

    private Map<String, Map<String, Object>> getSettingsForFooterViewTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getPageViewsIds());
        M.put("Page views default texts", getPageViewsDefaultTexts());
        M.put("Other settings", getOtherSettingsForTranslation());
        return M;
    }

    private Map<String, Object> getPageViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("bGetMoreShares", id.bGetMoreShares);
        return M;
    }

    private Map<String, Object> getPageViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("bGetMoreShares", "Ще");
        return M;
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu M) {
        inflateSearchViewIn(M);
        setupSearchViewIn(M);
        return menuCanBeShown(M);
    }

    private void inflateSearchViewIn(final Menu M) {
        final MenuInflater MI = getMenuInflater();
        final int MENU_RESOURCE = menu.search;
        MI.inflate(MENU_RESOURCE, M);
    }

    private void setupSearchViewIn(final Menu M) {
        final OffersSearchActivity REQUESTER = OffersSearchActivity.this;
        final int ID = id.action_search;
        final MenuItem MI = M.findItem(ID);
        final View V = MI.getActionView();
        final CharSequence QUERY = textToSearch;
        final boolean SUBMIT = false;
        final OnQueryTextListener L = new OnOffersSearchPageQueryTextListener(REQUESTER);
        searchField = (SearchView) V;
        searchField.setQuery(QUERY, SUBMIT);
        searchField.setOnQueryTextListener(L);
    }

    private boolean menuCanBeShown(final Menu M) {
        return super.onCreateOptionsMenu(M);
    }

    public void updateOffers(final String TEXT_TO_SEARCH) {
        retrieveTextToSearchFrom(TEXT_TO_SEARCH);
        resetPagesQuantity();
        clearOffersList();
        clearDownloadedOffersList();
        loadOffers();
        clearSearchFieldToAvoidTheSecondCallOfOnQueryTextSubmit();
    }

    private void retrieveTextToSearchFrom(final String TEXT_TO_SEARCH) {
        textToSearch = TEXT_TO_SEARCH;
    }

    private void resetPagesQuantity() {
        pagesQuantity = 1;
    }

    private void clearOffersList() {
        offersList.clear();
    }

    private void clearDownloadedOffersList() {
        downloadedOffersList.clear();
    }

    private void clearSearchFieldToAvoidTheSecondCallOfOnQueryTextSubmit() {
        final CharSequence QUERY = "";
        final boolean SUBMIT = false;
        searchField.setQuery(QUERY, SUBMIT);
    }

    @Override
    public void onClick(final View V) {
        increasePagesQuantity();
        clearOffersList();
        loadOffers();
        clearSearchFieldToAvoidTheSecondCallOfOnQueryTextSubmit();
    }

    private void increasePagesQuantity() {
        ++pagesQuantity;
    }

    public List<Offer> getDownloadedOffersList() {
        return downloadedOffersList;
    }

    public PageScaler getPageScaler() {
        return pageScaler;
    }

    public String getTextToSearch() {
        return textToSearch;
    }

    public int getPagesQuantity() {
        return pagesQuantity;
    }

    public int getSelectedRegionId() {
        final String NAME = "region";
        final int DEFAULT_VALUE = -1;
        final Intent I = getIntent();
        return I.getIntExtra(NAME, DEFAULT_VALUE);
    }

    public int[] getSelectedCategoriesIds() {
        final String NAME = "categories";
        final Intent I = getIntent();
        return I.getIntArrayExtra(NAME);
    }
}