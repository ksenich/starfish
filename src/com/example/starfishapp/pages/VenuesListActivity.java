package com.example.starfishapp.pages;

import android.os.*;
import android.support.v7.app.*;
import android.view.View;
import android.widget.*;
import com.example.starfishapp.R.*;
import com.example.starfishapp.model.*;
import com.googlion.shield.listeners.*;
import com.googlion.shield.loaders.*;
import com.googlion.shield.specialists.*;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;
import android.widget.AdapterView.*;

import java.util.*;

public class VenuesListActivity extends ActionBarActivity {
    private List<Map<String, Object>> venuesList;
    private List<Supplier> downloadedVenuesList;
    private PageScaler pageScaler;

    @Override
    protected void onCreate(final Bundle SAVED_INSTANCE_STATE) {
        performOnCreateMethodFromSuperclass(SAVED_INSTANCE_STATE);
        setContentViewForThisPage();
        initializeFields();
        scale();
        setUpActionBar();
        loadVenues();
    }

    private void performOnCreateMethodFromSuperclass(final Bundle SAVED_INSTANCE_STATE) {
        super.onCreate(SAVED_INSTANCE_STATE);
    }

    private void setContentViewForThisPage() {
        setContentView(layout.chosen_category_venues_page);
    }

    private void initializeFields() {
        venuesList = new LinkedList<Map<String, Object>>();
        downloadedVenuesList = new LinkedList<Supplier>();
        pageScaler = new PageScaler(VenuesListActivity.this);
    }

    private void scale() {
        scaleThisPage();
        scaleDividerHeightOfVenuesList();
    }

    private void scaleThisPage() {
        pageScaler.scaleThePage();
    }

    private void scaleDividerHeightOfVenuesList() {
        final View V = findViewById(id.lvChosenCategoryVenues);
        final ListView LV = (ListView) V;
        pageScaler.scaleDividerHeightOf(LV);
    }

    private void setUpActionBar() {
        final ActionBar AB = getSupportActionBar();
        final String NAME = "Action bar title";
        final CharSequence CS = getIntent().getStringExtra(NAME);
        AB.setTitle(CS);
    }

    private void loadVenues() {
        final VenuesListActivity REQUESTER = VenuesListActivity.this;
        final VenuesLoader VL = new VenuesLoader(REQUESTER);
        VL.loadVenues();
    }

    public void apply(final List<Supplier> L) {
        retrieveDownloadedVenuesListFrom(L);
        displayVenues();
        setOnItemClickListenerForVenuesList();
    }

    private void retrieveDownloadedVenuesListFrom(final List<Supplier> L) {
        downloadedVenuesList = L;
    }

    private void displayVenues() {
        final Map<String, List<Map<String, Object>>> SETTINGS = getSettingsForListDisplaying();
        final ListViewSpecialist LVS = new ListViewSpecialist(SETTINGS);
        LVS.setUpListView();
    }

    private Map<String, List<Map<String, Object>>> getSettingsForListDisplaying() {
        final Map<String, List<Map<String, Object>>> M = new HashMap<String, List<Map<String, Object>>>();
        M.put("List view items data", getListViewItemsData());
        M.put("List view item fields to be filled", getListViewItemFieldsToBeFilled());
        M.put("Other settings", getOtherSettingsForListDisplaying());
        return M;
    }

    private List<Map<String, Object>> getListViewItemsData() {
        addVenuesToList();
        return getFilledVenuesList();
    }

    private void addVenuesToList() {
        for (final Supplier S : downloadedVenuesList) {
            addVenueToList(S);
        }
    }

    private void addVenueToList(final Supplier VENUE) {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Venue name", VENUE.getName());
        M.put("Venue description", VENUE.getDescription());
        M.put("Venue comments quantity", VENUE.getCommentsQuantity());
        M.put("Venue offers quantity", VENUE.getOffersQuantity());
        M.put("Venue logotype url", VENUE.getLogotypeURL());
        venuesList.add(M);
    }

    private List<Map<String, Object>> getFilledVenuesList() {
        return venuesList;
    }

    private List<Map<String, Object>> getListViewItemFieldsToBeFilled() {
        final List<Map<String, Object>> L = new LinkedList<Map<String, Object>>();
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Venue name", id.tvVenueName);
        M.put("Venue description", id.tvVenueDescription);
        M.put("Venue comments quantity", id.tvVenueCommentsQuantity);
        M.put("Venue offers quantity", id.tvVenueOffersQuantity);
        M.put("Venue logotype url", id.sivVenueLogotype);
        L.add(M);
        return L;
    }

    private List<Map<String, Object>> getOtherSettingsForListDisplaying() {
        final List<Map<String, Object>> L = new LinkedList<Map<String, Object>>();
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", VenuesListActivity.this);
        M.put("List view id", id.lvChosenCategoryVenues);
        M.put("Layout resource id", layout.chosen_category_venues_page_list_item);
        M.put("Page scaler", pageScaler);
        M.put("List view items translator", getTranslatorForListViewItems());
        M.put("Font specialist for list view", getFontSpecialistForProcessingListView());
        L.add(M);
        return L;
    }

    private Object getTranslatorForListViewItems() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForListItemTranslation();
        return new ListViewItemsTranslator(SETTINGS);
    }

    private Map<String, Map<String, Object>> getSettingsForListItemTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getIdsOfPageViews());
        M.put("Page views default texts", getDefaultTextsOfPageViews());
        M.put("Other settings", getOtherSettingsForListItemTranslation());
        return M;
    }

    private Map<String, Object> getIdsOfPageViews() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvVenueName", id.tvVenueName);
        M.put("tvVenueDescription", id.tvVenueDescription);
        M.put("tvVenueOffersQuantityHeader", id.tvVenueOffersQuantityHeader);
        M.put("tvVenueCommentsQuantityHeader", id.tvVenueCommentsQuantityHeader);
        return M;
    }

    private Map<String, Object> getDefaultTextsOfPageViews() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvVenueName", "Заголовок відсутній.");
        M.put("tvVenueDescription", "Опис відсутній.");
        M.put("tvVenueOffersQuantityHeader", "Пропозицій:");
        M.put("tvVenueCommentsQuantityHeader", "Відгуків: ");
        return M;
    }

    private Map<String, Object> getOtherSettingsForListItemTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", VenuesListActivity.this);
        return M;
    }

    private FontSpecialistForListView getFontSpecialistForProcessingListView() {
        final Map<Object, Map> S = getSettingsForSettingFontsInListView();
        return new FontSpecialistForListView(S);
    }

    private Map<Object, Map> getSettingsForSettingFontsInListView() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingFontsInListView());
        M.put("Other settings", getOtherSettingsForSettingFontsInListView());
        return M;
    }

    private Map getMainSettingsForSettingFontsInListView() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvVenueName, "kelsonsansregularru.otf");
        M.put(id.tvVenueDescription, "robotoregular0.otf");
        M.put(id.tvVenueOffersQuantityHeader, "robotoregular0.otf");
        M.put(id.tvVenueOffersQuantity, "kelsonsansboldru.otf");
        M.put(id.tvVenueCommentsQuantityHeader, "robotoregular0.otf");
        M.put(id.tvVenueCommentsQuantity, "kelsonsansboldru.otf");
        return M;
    }

    private Map getOtherSettingsForSettingFontsInListView() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", VenuesListActivity.this);
        return M;
    }

    private void setOnItemClickListenerForVenuesList() {
        final VenuesListActivity REQUESTER = VenuesListActivity.this;
        final OnItemClickListener L = new OnGooglionVenuesListItemClickListener(REQUESTER);
        final View V = findViewById(id.lvChosenCategoryVenues);
        final ListView LV = (ListView) V;
        LV.setOnItemClickListener(L);
    }

    public List<Supplier> getDownloadedVenuesList() {
        return downloadedVenuesList;
    }
}