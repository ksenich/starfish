package com.example.starfishapp.pages;

import static com.example.starfishapp.Starfish.*;

import android.app.*;
import android.os.*;
import android.text.*;
import android.view.*;
import android.widget.*;
import com.example.starfishapp.*;
import com.example.starfishapp.Starfish.*;
import com.example.starfishapp.R.*;
import com.googlion.shield.callbacks.*;
import com.googlion.shield.specialists.*;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;

import java.util.*;

public class AuthorizationPage extends Activity {
    @Override
    protected void onCreate(final Bundle SAVED_INSTANCE_STATE) {
        performOnCreateMethodFromSuperclass(SAVED_INSTANCE_STATE);
        setContentViewForThisPage();
        scaleThisPage();
        setFontsForAllTextualViewsOfThisPage();
        translateThisPage();
    }

    private void performOnCreateMethodFromSuperclass(final Bundle SAVED_INSTANCE_STATE) {
        super.onCreate(SAVED_INSTANCE_STATE);
    }

    private void setContentViewForThisPage() {
        setContentView(layout.authorization_page);
    }

    private void scaleThisPage() {
        final Activity REQUESTER = AuthorizationPage.this;
        final PageScaler PS = new PageScaler(REQUESTER);
        PS.scaleThePage();
    }

    private void setFontsForAllTextualViewsOfThisPage() {
        final Map<Object, Map> S = getSettingsForSettingFonts();
        final FontSpecialist FS = new FontSpecialist(S);
        FS.setFonts();
    }

    private Map<Object, Map> getSettingsForSettingFonts() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingsFonts());
        M.put("Other settings", getOtherSettingsForSettingsFonts());
        return M;
    }

    private Map getMainSettingsForSettingsFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvAuthorizationPageDiscounts, "kelsonsanslightru.otf");
        M.put(id.tvAuthorizationPageShares, "kelsonsanslightru.otf");
        M.put(id.tvAuthorizationPageSales, "kelsonsansregularru.otf");
        M.put(id.tvAuthorizationPageDiscountsMobileMap, "kelsonsansboldru.otf");
        M.put(id.etAuthorizationLogin, "kelsonsanslightru.otf");
        M.put(id.etAuthorizationPassword, "kelsonsanslightru.otf");
        M.put(id.bEnterTheApplication, "robotobold.otf");
        return M;
    }

    private Map getOtherSettingsForSettingsFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", AuthorizationPage.this);
        return M;
    }

    private void translateThisPage() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForPageTranslation();
        final Translator T = new PageTranslator(SETTINGS);
        T.translate();
    }

    private Map<String, Map<String, Object>> getSettingsForPageTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getPageViewsIdsForTranslation());
        M.put("Page views default texts", getPageViewsDefaultTextsForTranslation());
        M.put("Other settings", getOtherSettingsForTranslation());
        return M;
    }

    private Map<String, Object> getPageViewsIdsForTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvAuthorizationPageDiscounts", id.tvAuthorizationPageDiscounts);
        M.put("tvAuthorizationPageShares", id.tvAuthorizationPageShares);
        M.put("tvAuthorizationPageSales", id.tvAuthorizationPageSales);
        M.put("tvAuthorizationPageDiscountsMobileMap", id.tvAuthorizationPageDiscountsMobileMap);
        M.put("etAuthorizationLogin", id.etAuthorizationLogin);
        M.put("etAuthorizationPassword", id.etAuthorizationPassword);
        M.put("bEnterTheApplication", id.bEnterTheApplication);
        return M;
    }

    private Map<String, Object> getPageViewsDefaultTextsForTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("tvAuthorizationPageDiscounts", "ЗНИЖКИ");
        M.put("tvAuthorizationPageShares", "АКЦІЇ");
        M.put("tvAuthorizationPageSales", "РОЗПРОДАЖІ");
        M.put("tvAuthorizationPageDiscountsMobileMap", "Рухома карта знижок");
        M.put("etAuthorizationLogin", "Логін");
        M.put("etAuthorizationPassword", "Пароль");
        M.put("bEnterTheApplication", "Вхід");
        return M;
    }

    private Map<String, Object> getOtherSettingsForTranslation() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", AuthorizationPage.this);
        return M;
    }

    public void authorizeUser(final View V) {
        final Map<String, Object> SETTINGS = getSettingsForAuthorization();
        final AuthorizationPage AP = AuthorizationPage.this;
        final Starfish S = getInstance(AP);
        S.login(SETTINGS);
    }

    private Map<String, Object> getSettingsForAuthorization() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Login", getLoginForAuthorization());
        M.put("Password", getPasswordForAuthorization());
        M.put("Callback", getCallbackOfAuthorization());
        return M;
    }

    private String getLoginForAuthorization() {
        final int ID = id.etAuthorizationLogin;
        return getTextFromTextViewWithId(ID);
    }

    private String getTextFromTextViewWithId(final int ID) {
        final View V = findViewById(ID);
        final EditText ET = (EditText) V;
        final Editable E = ET.getText();
        return E.toString();
    }

    private String getPasswordForAuthorization() {
        final int ID = id.etAuthorizationPassword;
        return getTextFromTextViewWithId(ID);
    }

    private LoginCallback getCallbackOfAuthorization() {
        final AuthorizationPage AP = AuthorizationPage.this;
        return new AuthorizationPageLoginCallback(AP);
    }
}