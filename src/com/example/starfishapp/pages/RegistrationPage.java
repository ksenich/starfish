package com.example.starfishapp.pages;

import android.app.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import com.example.starfishapp.R.*;
import com.example.starfishapp.*;
import com.googlion.shield.specialists.FontSpecialist;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;

import java.util.*;

public class RegistrationPage extends Activity {
    private final int minPassLen = 3;
    private EditText first_name;
    private EditText password;
    private EditText last_name;
    private EditText email;
    private int minFNameLen = 3;

    @Override
    protected void onCreate(final Bundle SAVED_INSTANCE_STATE) {
        super.onCreate(SAVED_INSTANCE_STATE);
        setContentView(layout.registration_page);
        initializeEditTexts();
        setFontsForAllTextualViewsOfThisPage();
        scaleThisPage();
        translateThisPage();
    }

    private void initializeEditTexts() {
        first_name = (EditText) findViewById(id.etRegistrationFirstName);
        last_name = (EditText) findViewById(id.etRegistrationLastName);
        email = (EditText) findViewById(id.etRegistrationEmail);
        password = (EditText) findViewById(id.etRegistrationPassword);
    }

    private void setFontsForAllTextualViewsOfThisPage() {
        final Map<Object, Map> S = getSettingsForSettingFonts();
        final FontSpecialist FS = new FontSpecialist(S);
        FS.setFonts();
    }

    private Map<Object, Map> getSettingsForSettingFonts() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingsFonts());
        M.put("Other settings", getOtherSettingsForSettingsFonts());
        return M;
    }

    private Map getMainSettingsForSettingsFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.etRegistrationFirstName, "kelsonsanslightru.otf");
        M.put(id.etRegistrationLastName, "kelsonsanslightru.otf");
        M.put(id.etRegistrationEmail, "kelsonsanslightru.otf");
        M.put(id.etRegistrationPassword, "kelsonsanslightru.otf");
        M.put(id.bRegisterInTheApplication, "robotobold.otf");
        return M;
    }

    private Map getOtherSettingsForSettingsFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", RegistrationPage.this);
        return M;
    }

    private void scaleThisPage() {
        final Activity REQUESTER = RegistrationPage.this;
        final PageScaler PS = new PageScaler(REQUESTER);
        PS.scaleThePage();
    }

    private void translateThisPage() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForRegistrationPageTranslation();
        final Translator T = new PageTranslator(SETTINGS);
        T.translate();
    }

    private Map<String, Map<String, Object>> getSettingsForRegistrationPageTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getPageViewsIds());
        M.put("Page views default texts", getPageViewsDefaultTexts());
        M.put("Other settings", getOtherSettings());
        return M;
    }

    private Map<String, Object> getPageViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("etRegistrationFirstName", id.etRegistrationFirstName);
        M.put("etRegistrationLastName", id.etRegistrationLastName);
        M.put("etRegistrationEmail", id.etRegistrationEmail);
        M.put("etRegistrationPassword", id.etRegistrationPassword);
        M.put("bRegisterInTheApplication", id.bRegisterInTheApplication);
        return M;
    }

    private Map<String, Object> getPageViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("etRegistrationFirstName", "Ім'я");
        M.put("etRegistrationLastName", "Прізвище");
        M.put("etRegistrationEmail", "Е-скринька");
        M.put("etRegistrationPassword", "Пароль");
        M.put("bRegisterInTheApplication", "Приписатися");
        return M;
    }

    private Map<String, Object> getOtherSettings() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", RegistrationPage.this);
        return M;
    }

    public void registerUser(final View V) {
        String pass = password.getText().toString();
        String name = first_name.getText().toString();
        String mail = email.getText().toString();
        String lname = last_name.getText().toString();

        password.setError(null);
        first_name.setError(null);
        email.setError(null);

        boolean error = false;
        if (!validateMail()) {
            email.setError("Введите корректный email.");
            error = true;
        }

        if (!validatePass()) {
            password.setError("Длина пароля не меньше " + minPassLen + " символа.");
            error = true;
        }

        if (!validateName()) {
            first_name.setError("Длина имени - минимум " + minFNameLen + " символа.");
            error = true;
        }

        if (error) {
            return;
        }

        Starfish.getInstance(this).register(mail, pass, name, lname);
        finish();
    }

    private boolean validateMail() {
        return email.getText().toString().matches("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");
    }

    private boolean validateName() {
        return first_name.getText().length() >= minFNameLen;
    }

    private boolean validatePass() {
        return password.getText().length() >= minPassLen;
    }
}