package com.example.starfishapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.*;
import android.content.SharedPreferences.*;
import android.os.*;
import android.support.v7.app.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import com.example.starfishapp.model.*;
import com.example.starfishapp.utilities.*;
import com.googlion.shield.adapters.*;
import com.googlion.shield.listeners.*;
import com.googlion.shield.specialists.*;
import com.googlion.shield.translators.*;
import com.googlion.shield.utilities.*;
import com.loopj.android.image.*;
import org.json.*;
import com.example.starfishapp.R.*;

import java.io.*;
import java.net.*;
import java.util.*;

public class AccountSettingsActivity extends ActionBarActivity {

    private final Activity self = this;
    private TextView language;
    private TextView country;
    private TextView region;
    private int lid;
    private int cid;
    private int rid;
    private EditText telephone;
    //private EditText email;
    private ImagePicker picker;
    private PageScaler pageScaler;
    private ProfileSettingsPageTranslator pageTranslator;
    private String chosenLanguageName;
    private int chosenLanguagePosition;

    @Override
    protected void onCreate(final Bundle SAVED_INSTANCE_STATE) {
        super.onCreate(SAVED_INSTANCE_STATE);
        setContentView(layout.profile_settings_page);
        initializeChosenLanguageName();
        initializeChosenLanguagePosition();
        initializePageTranslator();
        initializePageScaler();
        translateThisPage();
        setFontsForAllTextualViewsOfThisPage();
        scaleThisPage();
        setUpActionBar();
        setUpUserInfo();
    }

    private void initializeChosenLanguageName() {
        final String NAME = "Chosen language";
        final int MODE = MODE_PRIVATE;
        final SharedPreferences SP = getSharedPreferences(NAME, MODE);
        final String KEY = "Chosen language name";
        final String DEFAULT_VALUE = "Українська";
        chosenLanguageName = SP.getString(KEY, DEFAULT_VALUE);
    }

    private void initializeChosenLanguagePosition() {
        chosenLanguagePosition = 0;
    }

    private void initializePageScaler() {
        final Activity REQUESTER = self;
        pageScaler = new PageScaler(REQUESTER);
    }

    private void initializePageTranslator() {
        final Map<String, Map<String, Object>> SETTINGS = getSettingsForProfileSettingsPageTranslation();
        pageTranslator = new ProfileSettingsPageTranslator(SETTINGS);
    }

    private Map<String, Map<String, Object>> getSettingsForProfileSettingsPageTranslation() {
        final Map<String, Map<String, Object>> M = new HashMap<String, Map<String, Object>>();
        M.put("Page views ids", getPageViewsIds());
        M.put("Page views default texts", getPageViewsDefaultTexts());
        M.put("Other settings", getOtherSettings());
        return M;
    }

    private Map<String, Object> getPageViewsIds() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("bChangeUserPhoto", id.bChangeUserPhoto);
        M.put("tvLanguageHeader", id.tvUserLanguageHeader);
        M.put("tvCountryHeader", id.tvUserCountryHeader);
        M.put("tvCityHeader", id.tvUserCityHeader);
        M.put("tvFirstNameHeader", id.tvUserFirstNameHeader);
        M.put("tvLastNameHeader", id.tvUserLastNameHeader);
        M.put("tvPhoneNumberHeader", id.tvUserPhoneNumberHeader);
        M.put("tvGoToTheProfileBindingPageHeader", id.tvSaveChanges);
        return M;
    }

    private Map<String, Object> getPageViewsDefaultTexts() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("bChangeUserPhoto", "Змінити світлину");
        M.put("tvLanguageHeader", "Мова");
        M.put("tvCountryHeader", "Країна");
        M.put("tvCityHeader", "Місто");
        M.put("tvFirstNameHeader", "Ім'я");
        M.put("tvLastNameHeader", "Прізвище");
        M.put("tvPhoneNumberHeader", "Телефон");
        M.put("tvGoToTheProfileBindingPageHeader", "Зберегти зміни");
        return M;
    }

    private Map<String, Object> getOtherSettings() {
        final Map<String, Object> M = new HashMap<String, Object>();
        M.put("Requester", AccountSettingsActivity.this);
        M.put("Page title translation key", "Profile settings page action bar title");
        M.put("Page title default text", "Налаштування облікового запису");
        return M;
    }

    private void translateThisPage() {
        pageTranslator.translate();
    }

    private void setFontsForAllTextualViewsOfThisPage() {
        final Map<Object,Map> S = getSettingsForSettingFonts();
        final FontSpecialist FS = new FontSpecialist(S);
        FS.setFonts();
    }

    private Map<Object, Map> getSettingsForSettingFonts() {
        final Map<Object, Map> M = new HashMap<Object, Map>();
        M.put("Main settings", getMainSettingsForSettingsFonts());
        M.put("Other settings", getOtherSettingsForSettingsFonts());
        return M;
    }

    private Map getMainSettingsForSettingsFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put(id.tvUserName, "kelsonsanslightru.otf");
        M.put(id.bChangeUserPhoto, "robotobold.otf");
        M.put(id.tvUserLanguageHeader, "kelsonsanslightru.otf");
        M.put(id.tvUserChosenLanguage, "kelsonsansregularru.otf");
        M.put(id.tvUserCountryHeader, "kelsonsanslightru.otf");
        M.put(id.tvUserChosenCountry, "kelsonsansregularru.otf");
        M.put(id.tvUserCityHeader, "kelsonsanslightru.otf");
        M.put(id.tvUserChosenCity, "kelsonsansregularru.otf");
        M.put(id.tvUserFirstNameHeader, "kelsonsanslightru.otf");
        M.put(id.etUserFirstName, "kelsonsansregularru.otf");
        M.put(id.tvUserLastNameHeader, "kelsonsanslightru.otf");
        M.put(id.etUserLastName, "kelsonsansregularru.otf");
        M.put(id.tvUserPhoneNumberHeader, "kelsonsanslightru.otf");
        M.put(id.etUserPhoneNumber, "kelsonsansregularru.otf");
        M.put(id.tvSaveChanges, "kelsonsansregularru.otf");
        return M;
    }

    private Map getOtherSettingsForSettingsFonts() {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        M.put("Requester", AccountSettingsActivity.this);
        return M;
    }

    private void scaleThisPage() {
        pageScaler.scaleThePage();
    }

    private void setUpActionBar() {
        final ActionBar AB = getSupportActionBar();
        final boolean B = true;
        AB.setDisplayHomeAsUpEnabled(B);
    }

    private void setUpUserInfo() {
        new AsyncTask<Void, Void, UserInfo>() {
            @Override
            protected UserInfo doInBackground(Void... params) {
                return Starfish.getInstance(self).loadUserInfo();
            }

            @Override
            protected void onPostExecute(UserInfo userInfo) {
                setupViews(userInfo);
            }
        }.execute();
    }

    private void setupViews(final UserInfo UI) {
        TextView name = (TextView) findViewById(id.tvUserName);
        SmartImageView image = (SmartImageView) findViewById(id.sivUserPhoto);
        final Map<Object, Object> SETTINGS = getSettingsForImageRoundingUserPhoto(UI);
        final RoundedImagesSpecialist S = new RoundedImagesSpecialist(SETTINGS);
        S.makeImageLookSpecial();
        telephone = (EditText) findViewById(id.etUserPhoneNumber);
        //email = (EditText) findViewById(id.tvChosenEmail);
        telephone.setText(UI.telephone);
        //email.setText(user.email);
        final EditText fname = (EditText) findViewById(id.etUserFirstName);
        final EditText lname = (EditText) findViewById(id.etUserLastName);
        fname.setText(UI.first_name);
        lname.setText(UI.last_name);
        name.setText(UI.name);
        WebImage.setContext(self);
        WebImage.removeFromCache(UI.img_url);
        image.setImageUrl(UI.img_url);
        language = (TextView) findViewById(id.tvUserChosenLanguage);

        //TODO
        language.setText(chosenLanguageName);
        //language.setText(user.lang_name);

        lid = UI.lang;
        country = (TextView) findViewById(id.tvUserChosenCountry);
        country.setText(UI.country_name);
        cid = UI.country;
        region = (TextView) findViewById(id.tvUserChosenCity);
        region.setText(UI.region_name);
        rid = UI.region;
        loadOptions();
        View saveButton = findViewById(id.rlSaveChanges);
        saveButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        UI.lang = lid;
                        UI.country = cid;
                        UI.region = rid;
                        UI.telephone = telephone.getText().toString();
                        //user.email = email.getText().toString();
                        try {
                            UI.first_name = URLEncoder.encode(fname.getText().toString(), "UTF-8");
                            UI.last_name = URLEncoder.encode(lname.getText().toString(), "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        new AsyncTask<Void, Void, String>() {

                            @Override
                            protected String doInBackground(Void... params) {
                                Starfish.getInstance(self).saveUserInfo(UI, picker.getImage());
                                return null;
                            }

                            @Override
                            protected void onPostExecute(String s) {
                                finish();
                            }
                        }.execute();

                        saveInformationAboutChosenLanguage();
                    }

                    private void saveInformationAboutChosenLanguage() {
                        final View V = findViewById(id.tvUserChosenLanguage);
                        final TextView TV = (TextView) V;
                        final CharSequence TEXT = TV.getText();
                        final String NAME = "Chosen language";
                        final int MODE = MODE_PRIVATE;
                        final SharedPreferences SP = getSharedPreferences(NAME, MODE);
                        final Editor E = SP.edit();
                        final JSONObject O = pageTranslator.getChosenLanguage();
                        E.putString("Language content", O.toString());
                        E.putString("Chosen language name", TEXT.toString());
                        E.putInt("Chosen language position", chosenLanguagePosition);
                        E.putBoolean("Application language is changed", true);
                        E.commit();
                    }
                }


        );
        picker = new ImagePicker(this, image);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.pick();
            }
        });
        Button pickButton = (Button) findViewById(id.bChangeUserPhoto);
        pickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.pick();
            }
        });
    }

    private Map<Object, Object> getSettingsForImageRoundingUserPhoto(final UserInfo UI) {
        final Map<Object, Object> M = new HashMap<Object, Object>();
        final View V = findViewById(id.sivUserPhoto);
        final ImageView IV = (ImageView) V;
        M.put("Image url", UI.img_url);
        M.put("Requester", AccountSettingsActivity.this);
        M.put("Image view", IV);
        return M;
    }

    private void loadOptions() {
        loadLangOptions();
        loadCountryOptions();
    }

    private void loadLangOptions() {
        // TODO
        new AsyncTask<Void, Void, List<Language>>() {

            @Override
            protected List<Language> doInBackground(Void... params) {
                return Starfish.getInstance(self).loadLanguages();
            }

            @Override
            protected void onPostExecute(final List<Language> languages) {
                final String[] ll = new String[languages.size()];
                int count = 0;
                for (int i = 0; i < languages.size(); i++) {
                    Language l = languages.get(i);
                    if (l.id == lid) {
                        count = i;
                    }
                    ll[i] = l.name;
                }
                language.setText(languages.get(count).name);
                final int c = count;
                language.setOnClickListener(new View.OnClickListener() {
                    int n = c;

                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder b = new AlertDialog.Builder(self);
//                        final ListAdapter LA = new ShieldArrayAdapter<String>(self, R.layout.googlion_select_dialog_item, ll, pageScaler);
                        //TODO
//                        b.setSingleChoiceItems(LA, n, new DialogInterface.OnClickListener() {
                        b.setSingleChoiceItems(ll, n, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                language.setText(ll[i]);
                                n = i;
                                lid = languages.get(i).id;
                                dialogInterface.dismiss();
                            }
                        });
                        b.create().show();
                    }
                });
            }
        };//.execute();
        // TODO
        final Activity A = AccountSettingsActivity.this;
        final OnClickListener L = new OnLanguageFieldClickListener(A);
        language.setOnClickListener(L);
    }

    private void loadCountryOptions() {
        new AsyncTask<Void, Void, List<Country>>() {

            @Override
            protected List<Country> doInBackground(Void... params) {
                return Starfish.getInstance(self).getCountries();
            }

            @Override
            protected void onPostExecute(final List<Country> countries) {
                final String[] ll = new String[countries.size()];
                int count = 0;
                for (int i = 0; i < countries.size(); i++) {
                    Country l = countries.get(i);
                    if (l.id == cid) {
                        count = i;
                    }
                    ll[i] = l.name;
                }
                cid = countries.get(count).id;
                loadRegionOptions(cid);
                country.setText(countries.get(count).name);
                cid = countries.get(count).id;
                loadRegionOptions(cid);
                final int c = count;
                country.setOnClickListener(new View.OnClickListener() {
                    int n = c;

                    @Override
                    public void onClick(View v) {
                        final ListAdapter LA = getListAdapterForAlertDialog();
                        AlertDialog.Builder b = new AlertDialog.Builder(self);

                        b.setSingleChoiceItems(LA, n, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                n = i;
                                country.setText(ll[i]);
                                cid = countries.get(i).id;
                                loadRegionOptions(cid);
                                dialogInterface.dismiss();
                            }
                        });
                        b.create().show();
                    }

                    private ListAdapter getListAdapterForAlertDialog() {
                        final Context C = self;
                        final AccountSettingsActivity ASA = (AccountSettingsActivity) self;
                        final int LAYOUT_RESOURCE_ID = layout.googlion_select_dialog_item;
                        final PageScaler PS = ASA.getPageScaler();
                        return new ShieldArrayAdapter<CharSequence>(C, LAYOUT_RESOURCE_ID, ll, PS);
                    }
                });
            }
        }.execute();
    }

    private void loadRegionOptions(final int country_id) {
        new AsyncTask<Void, Void, List<Region>>() {

            @Override
            protected List<Region> doInBackground(Void... params) {
                return Starfish.getInstance(self).getRegions(country_id);
            }

            @Override
            protected void onPostExecute(final List<Region> regions) {
                final String[] ll = new String[regions.size()];
                int count = 0;
                for (int i = 0; i < regions.size(); i++) {
                    Region l = regions.get(i);
                    if (l.id == rid) {
                        count = i;
                    }
                    ll[i] = l.name;
                }
                region.setText(regions.get(count).name);
                final int c = count;
                region.setOnClickListener(new View.OnClickListener() {
                    int n = c;

                    @Override
                    public void onClick(View v) {
                        final ListAdapter LA = getListAdapterForAlertDialog();
                        AlertDialog.Builder b = new AlertDialog.Builder(self);

                        b.setSingleChoiceItems(LA, n, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                n = i;
                                region.setText(ll[i]);
                                rid = regions.get(i).id;
                                dialogInterface.dismiss();
                            }
                        });
                        b.create().show();
                    }

                    private ListAdapter getListAdapterForAlertDialog() {
                        final Context C = self;
                        final AccountSettingsActivity ASA = (AccountSettingsActivity) self;
                        final int LAYOUT_RESOURCE_ID = layout.googlion_select_dialog_item;
                        final PageScaler PS = ASA.getPageScaler();
                        return new ShieldArrayAdapter<CharSequence>(C, LAYOUT_RESOURCE_ID, ll, PS);
                    }
                });
            }
        }.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        picker.onActivityResult(requestCode, resultCode, data);
    }

    public ProfileSettingsPageTranslator getPageTranslator() {
        return pageTranslator;
    }

    public void setChosenLanguagePosition(final int I) {
        chosenLanguagePosition = I;
    }

    public PageScaler getPageScaler() {
        return pageScaler;
    }
}